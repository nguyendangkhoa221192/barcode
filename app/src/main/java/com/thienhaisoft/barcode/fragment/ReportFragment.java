package com.thienhaisoft.barcode.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayReportAdapter;
import com.thienhaisoft.barcode.entities.ListReportEntities;
import com.thienhaisoft.barcode.entities.json.object.ReportEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.DateUtils;
import com.thienhaisoft.barcode.utils.JsonUtils;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

public class ReportFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView lvReportSellout;
    private ListView lvReportSellin;
    private ListView lvReportMHTT;
    private View rootView;
    private TextView txt_kpi_percent;
    private TextView txt_kpi_recipe;
    private TextView txt_choose_time;
    private TextView txt_salein_1_title, txt_salein_2_title, txt_mhtt_title;
    private Button btn_submit;

    private final String FORMAT_DATE = Constants.FORMAT_DATE;

    private Activity activity = null;

    private GlobalInfo info;

    private String kpi_recipe = "";
    private double kpi_percent = 0.0;

    private String user_id = "";


    ListReportEntities listReportEntities;

    private ProgressDialog progressDialog = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ReportFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReportFragment newInstance(String param1, String param2) {
        ReportFragment fragment = new ReportFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        activity = getActivity();
        info = (GlobalInfo) getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_report, container, false);
        lvReportSellout = (ListView) rootView.findViewById(R.id.lvReportSellout);
        lvReportSellin = (ListView) rootView.findViewById(R.id.lvReportSellin);
        lvReportMHTT = (ListView) rootView.findViewById(R.id.lvReportMHTT);
        txt_kpi_percent = (TextView) rootView.findViewById(R.id.kpi_percent);
        txt_kpi_recipe = (TextView) rootView.findViewById(R.id.kpi_recipe);
        txt_choose_time = (TextView) rootView.findViewById(R.id.report_choose_time);
        btn_submit = (Button) rootView.findViewById(R.id.report_btn_submit);
        txt_salein_1_title = (TextView) rootView.findViewById(R.id.txt_salein_1_title);
        txt_salein_2_title = (TextView) rootView.findViewById(R.id.txt_salein_2_title);
        txt_mhtt_title = (TextView) rootView.findViewById(R.id.txt_mhtt_title);

        final Calendar cal = Calendar.getInstance();
        txt_choose_time.setText(Common.getTextFromTime(cal, FORMAT_DATE));

        txt_choose_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.showDatePickerDialog(activity, txt_choose_time, FORMAT_DATE);
            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.getTimeFromTextview(txt_choose_time, FORMAT_DATE) > Calendar.getInstance().getTimeInMillis()) {
                    Common.showAlertDialog(activity, "Vui lòng chọn ngày trước ngày hiện tại.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            txt_choose_time.setText(Common.getTextFromTime(cal, FORMAT_DATE));
                        }
                    });
                } else {
                    renderListReportSellOut(Common.getTimeDivision1000(Common.getTimeFromTextview(txt_choose_time, FORMAT_DATE)));
                }
            }
        });

        progressDialog = new ProgressDialog(activity);
        user_id = StringUtil.toString(info.getStaffId());
        renderListReportSellOut(Common.getTimeDivision1000(cal.getTimeInMillis()));

        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This inteface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void renderListReportSellOut() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Freportsaleoutstore");
        data.put("id", user_id);

        final String data_report = data.toString();
        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(activity, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "Đang tải dữ liệu, vui lòng chờ...", "Thông báo");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Report: Render list report sell out; Result: " + failString + "; Data: " + data_report, getActivity());
                }

                @Override
                public void uiEnd() {
                    if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                        lvReportSellout.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    }
                    renderListReportSellIn();
                }
            });
        } catch (Exception ex) {
           ex.printStackTrace();
        }
    }

    private void renderListReportSellIn() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Freportsaleinstore");
        data.put("id", user_id);
        final String data_report = data.toString();

        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(activity, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {

                }

                @Override
                public void success(String successString, int type) {
                    try {
                        listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Report: Render list report sell in; Result: " + failString + "; Data: " + data_report, getActivity());
                }

                @Override
                public void uiEnd() {
                    if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                        lvReportSellin.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    }
                    renderListReportMHTT();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void renderListReportMHTT() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Freportmhttstore");
        data.put("id", user_id);

        final String data_report = data.toString();
        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(activity, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {

                }

                @Override
                public void success(String successString, int type) {
                    try {
                        listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Report: Render list report MHTT; Result: " + failString + "; Data: " + data_report, getActivity());
                }

                @Override
                public void uiEnd() {
                    if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                        lvReportMHTT.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    }
                    loadKPIData();
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void loadKPIData() {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", user_id);
        final String data_report = data.toString();
        String url = Constants.API_KPI_DATA;
        APIUtils.LoadJSON(getActivity(), data, url, new APICallBack() {
            @Override
            public void uiStart() {

            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    double salein1 = 0;
                    double salein2 = 0;
                    double mhtt = 0;
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has("ratingpercent")) {
                            JSONArray array = result.getJSONArray("ratingpercent");
                            JSONObject object = array.getJSONObject(0);
                            if (object.has("name")) {
                                kpi_recipe = object.getString("name").split("=")[1];
                            }
                            if (object.has("salein1")) {
                                salein1 = object.getDouble("salein1");
                            }
                            if (object.has("salein2")) {
                                salein2 = object.getDouble("salein2");
                            }
                            if (object.has("mhtt")) {
                                mhtt = object.getDouble("mhtt");
                            }
                        }
                        HashMap<String, String> result_kpi = new HashMap<String, String>();
                        if (result.has("kpiresult")) {
                            JSONArray array = result.getJSONArray("kpiresult");
                            for (int i = 0; i < array.length(); i ++) {
                                JSONObject object = array.getJSONObject(i);
                                result_kpi.put(object.getString("name"), object.getString("percent"));
                            }
                        }
                        Double result_salein1 = (Double.parseDouble(result_kpi.get("salein1"))*salein1)/100;
                        Double result_salein2 = (Double.parseDouble(result_kpi.get("salein2"))*salein2)/100;
                        Double result_mhtt = (Double.parseDouble(result_kpi.get("mhtt"))*mhtt)/100;
                        kpi_percent = result_salein1 + result_salein2 + result_mhtt;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Report: Render KPI; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                txt_kpi_recipe.setText(kpi_recipe);
                txt_kpi_percent.setText(String.format("%.2f", kpi_percent) + "%");
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    // Load sell out with filter time
    private void renderListReportSellOut(final long time) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_CHANGE_REPORT_SELLOUT;

        data.put("p1", user_id);
        data.put("p2", StringUtil.toString(time));
        final String data_report = data.toString();

        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "Đang tải dữ liệu, vui lòng chờ...", "Thông báo");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Report: Render list report sell out - time; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                    lvReportSellout.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    txt_salein_1_title.setText(listReportEntities.getData().get(0).getStore_code());
                }
                renderListReportSellIn(time);
            }
        });
    }

    // Load sell in with filter time
    private void renderListReportSellIn(final long time) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_CHANGE_REPORT_SELLIN;

        data.put("p1", user_id);
        data.put("p2", StringUtil.toString(time));
        final String data_report = data.toString();

        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {

            }

            @Override
            public void success(String successString, int type) {
                try {
                    listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Report: Render list report sell in - time; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                    lvReportSellin.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    txt_salein_2_title.setText(listReportEntities.getData().get(0).getStore_code());
                }
                renderListReportMHTT(time);
            }
        });
    }

    // Load mhtt with filter time
    private void renderListReportMHTT(final long time) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_CHANGE_REPORT_MHTT;

        data.put("p1", user_id);
        data.put("p2", StringUtil.toString(time));
        final String data_report = data.toString();

        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {

            }

            @Override
            public void success(String successString, int type) {
                try {
                    listReportEntities = (ListReportEntities) JsonUtils.toObject(successString, ListReportEntities.class);
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Report: Render list report MHTT - time; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (listReportEntities != null && listReportEntities.getStatus() == 1) {
                    lvReportMHTT.setAdapter(new ArrayReportAdapter(activity, listReportEntities.getData()));
                    txt_mhtt_title.setText(listReportEntities.getData().get(0).getStore_code());
                }
                loadKPIData(time);
            }
        });
    }

    // Load kpi with filter time
    private void loadKPIData(long time) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_CHANGE_REPORT_KPI;

        data.put("p1", user_id);
        data.put("p2", StringUtil.toString(time));
        final String data_report = data.toString();

        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {

            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    double salein1 = 0;
                    double salein2 = 0;
                    double mhtt = 0;
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has("ratingpercent")) {
                            JSONArray array = result.getJSONArray("ratingpercent");
                            JSONObject object = array.getJSONObject(0);
                            if (object.has("name")) {
                                kpi_recipe = object.getString("name").split("=")[1];
                            }
                            if (object.has("salein1")) {
                                salein1 = object.getDouble("salein1");
                            }
                            if (object.has("salein2")) {
                                salein2 = object.getDouble("salein2");
                            }
                            if (object.has("mhtt")) {
                                mhtt = object.getDouble("mhtt");
                            }
                        }
                        HashMap<String, String> result_kpi = new HashMap<String, String>();
                        if (result.has("kpiresult")) {
                            JSONArray array = result.getJSONArray("kpiresult");
                            for (int i = 0; i < array.length(); i ++) {
                                JSONObject object = array.getJSONObject(i);
                                result_kpi.put(object.getString("name"), object.getString("percent"));
                            }
                        }
                        Double result_salein1 = (Double.parseDouble(result_kpi.get("salein1"))*salein1)/100;
                        Double result_salein2 = (Double.parseDouble(result_kpi.get("salein2"))*salein2)/100;
                        Double result_mhtt = (Double.parseDouble(result_kpi.get("mhtt"))*mhtt)/100;
                        kpi_percent = result_salein1 + result_salein2 + result_mhtt;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Report: Render KPI - time; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                txt_kpi_recipe.setText(kpi_recipe);
                txt_kpi_percent.setText(String.format("%.2f", kpi_percent) + "%");
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

}
