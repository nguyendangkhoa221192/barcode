package com.thienhaisoft.barcode.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.activity.ListCustomer;
import com.thienhaisoft.barcode.activity.LoginActivity;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileFrament.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileFrament#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileFrament extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public ProfileFrament() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ProfileFrament.
     */
    // TODO: Rename and change types and number of parameters
    public static ProfileFrament newInstance(String param1, String param2) {
        ProfileFrament fragment = new ProfileFrament();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    private EditText edt_fullname, edt_password, edt_confirm_password;
    private TextView txt_role;
    private Button btn_changePW, btn_submit, btn_cancel;
    private LinearLayout lnl_content_password;
    private LinearLayout lnl_content_confirm_password;
    private LinearLayout lnl_content_submit;
    private LinearLayout lnl_content_change;
    private GlobalInfo info;
    private String userid = "";
    private String full_name = "";
    private String role = "";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_profile_frament, container, false);
        edt_fullname = (EditText) rootView.findViewById(R.id.profile_txt_fullname);
        txt_role = (TextView) rootView.findViewById(R.id.profile_txt_role);

        edt_password = (EditText) rootView.findViewById(R.id.profile_txt_password);
        edt_confirm_password = (EditText) rootView.findViewById(R.id.profile_txt_confirm_password);

        lnl_content_password = (LinearLayout) rootView.findViewById(R.id.profile_content_password);
        lnl_content_confirm_password = (LinearLayout) rootView.findViewById(R.id.profile_content_confirm_password);
        lnl_content_submit = (LinearLayout) rootView.findViewById(R.id.profile_content_submit);
        lnl_content_change = (LinearLayout) rootView.findViewById(R.id.profile_content_change);

        btn_changePW = (Button) rootView.findViewById(R.id.profile_btn_changepassword);
        btn_cancel = (Button) rootView.findViewById(R.id.profile_btn_cancel);
        btn_submit = (Button) rootView.findViewById(R.id.profile_btn_submit);

        info = (GlobalInfo) getActivity().getApplicationContext();

        userid = StringUtil.toString(info.getStaffId());
        full_name = info.getStaff().getRole().getFull_name();
        role = info.getStaff().getRole().getRole();

        edt_fullname.setText(full_name);
        txt_role.setText(role);

        btn_changePW.setOnClickListener(this);
        btn_cancel.setOnClickListener(this);
        btn_submit.setOnClickListener(this);

        return rootView;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.profile_btn_changepassword:
                changeStView();
                break;
            case R.id.profile_btn_cancel:
                resetStView();
                break;
            case R.id.profile_btn_submit:
                submitChangeInfo();
                break;
            default:
                break;
        }
    }

    // Function reset status view
    private void resetStView() {
        edt_fullname.setEnabled(false);
        lnl_content_confirm_password.setVisibility(View.GONE);
        lnl_content_password.setVisibility(View.GONE);
        lnl_content_submit.setVisibility(View.GONE);
        lnl_content_change.setVisibility(View.VISIBLE);
        edt_fullname.setText(full_name);
        edt_confirm_password.setText("");
        edt_password.setText("");
    }

    // Function change status when change info
    private void changeStView() {
        edt_fullname.setEnabled(true);
        lnl_content_confirm_password.setVisibility(View.VISIBLE);
        lnl_content_password.setVisibility(View.VISIBLE);
        lnl_content_submit.setVisibility(View.VISIBLE);
        lnl_content_change.setVisibility(View.GONE);
    }

    // Function handle change info of user
    private void submitChangeInfo() {
        String pass = edt_password.getText().toString();
        String pass_conf = edt_confirm_password.getText().toString();
        String fn = edt_fullname.getText().toString();
        if (pass.equals(pass_conf)) {
            if (!pass.equals("")) {
                handleChangeInfo(fn, pass, userid, getActivity());
            } else {
                Toast.makeText(getActivity(), "Nhập password để thay đổi thông tin.", Toast.LENGTH_LONG).show();
            }
        } else {
            Toast.makeText(getActivity(), "Password không khớp nhau.", Toast.LENGTH_LONG).show();
        }
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    boolean success = false;

    private void handleChangeInfo(String fn, String pw, String user_id, Activity activity) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_CHANGE_INFO_PROFILE;
        data.put("userid", user_id);
        if (!fn.equals("")) {
            data.put("full_name", fn);
        }
        data.put("password", pw);
        data.put("confirmPassword", pw);
        final String report_data = data.toString();
        final ProgressDialog dialog = new ProgressDialog(activity);
        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(dialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        success = true;
                    } else {
                        success = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Profile: Đỗi thông tin; Result: " + failString + "; Data: " + report_data, getActivity());
            }

            @Override
            public void uiEnd() {
                if (success) {
                    Common.showAlertDialog(getActivity(), "Đỗi thông tin thành công, vui lòng đăng nhập lại.", "Success", "", "", "Ok", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            Intent i = new Intent(getActivity(), LoginActivity.class);
                            getActivity().finish();
                            getActivity().startActivity(i);
                        }
                    });
                } else {
                    Common.showAlertDialog(getActivity(), "Đỗi thông tin thất bại, vui lòng thử lại.", "Fail", "", "", "Ok", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {

                        }
                    });
                }
                Common.FinishProgressDialog(dialog);
            }
        });
    }
}
