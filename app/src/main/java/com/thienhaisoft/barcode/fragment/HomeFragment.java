package com.thienhaisoft.barcode.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.SingleListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} inteface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private View rootView;

    private Activity activity = null;

    private GlobalInfo info;

    private Button btnCheckin;
    private Button btnCreateSaleOrder;
    private Button btnGetGift;
    private Button btnWarehouse;
    private Button btnReport;
    private Button btnCheckout;
    private Button btnNotify;
    private Button btnCloseStore;
    private Button btnStoreReport;
    private Button btnListCustomer;
    private Button btnCheckLocation;
    private Button btnReportOrder;
    private Button btnStoreGift;
    private LinearLayout linearLayout_store;
    private GlobalInfo globalInfo = null;
    private String storeName = "";
    private String storeAddress = "";
    private String time  = "";
    private String phone = "";
    private String latLng = "";
    private TextView txt_title_store;


    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        activity = getActivity();
        info = (GlobalInfo)getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_home, container, false);
//        lvStore = (ListView)rootView.findViewById(R.id.lvListStore);
        btnCheckin = (Button) rootView.findViewById(R.id.btnCheckin);
        btnCreateSaleOrder = (Button) rootView.findViewById(R.id.btnSaleorder);
        btnGetGift = (Button) rootView.findViewById(R.id.btnGetGift);
        btnWarehouse = (Button) rootView.findViewById(R.id.btnWareHouse);
        btnReport = (Button) rootView.findViewById(R.id.btnReport);
        btnCheckout = (Button) rootView.findViewById(R.id.btnCheckout);
        btnNotify = (Button) rootView.findViewById(R.id.btnNotification);
        btnCloseStore = (Button) rootView.findViewById(R.id.btnCloseStore);
        btnCheckLocation = (Button)rootView.findViewById(R.id.btnCheckLocation);
        btnListCustomer = (Button) rootView.findViewById(R.id.btnListCustomer);
        btnStoreGift = (Button) rootView.findViewById(R.id.btnStoreGift);
        linearLayout_store = (LinearLayout) rootView.findViewById(R.id.fragmentHome_currentStore);
        txt_title_store = (TextView) rootView.findViewById(R.id.fragmentHome_title_store);
        btnStoreReport = (Button) rootView.findViewById(R.id.btnStoreReport);
        btnReportOrder = (Button) rootView.findViewById(R.id.btnReportOrder);


        globalInfo = (GlobalInfo) getActivity().getApplicationContext();
        if (globalInfo.getCurStore() != null) {
            StoreEntities store = globalInfo.getCurStore();
            if (store.getLat() == 0 && store.getLng() == 0) {
                btnCheckLocation.setTextColor(Color.BLACK);
            } else {
                btnCheckLocation.setTextColor(Color.parseColor("#929090"));
            }

            if (store.getCheckin() == null) {
                btnCheckin.setEnabled(true);
                btnCheckin.setTextColor(Color.BLACK);
            } else {
                btnCheckin.setTextColor(Color.parseColor("#929090"));
                btnCheckin.setEnabled(false);
            }

            if (store.getCheckout() == null) {
                btnCheckout.setEnabled(true);
                btnCheckout.setTextColor(Color.BLACK);
            } else {
                btnCheckout.setEnabled(false);
                btnCheckout.setTextColor(Color.parseColor("#929090"));
            }

            linearLayout_store.setVisibility(View.VISIBLE);
            linearLayout_store.setBackgroundResource(R.color.gray_line_list_item);
            txt_title_store.setText("Thông tin store hiện tại:");

            TextView tvStoreName = (TextView) linearLayout_store.findViewById(R.id.tvStoreName);
            TextView tvStoreAddress = (TextView) linearLayout_store.findViewById(R.id.tvStoreAdress);
            TextView tvStoreTime = (TextView) linearLayout_store.findViewById(R.id.tvStoreTime);
            TextView tvStorePhone = (TextView) linearLayout_store.findViewById(R.id.tvStorePhone);

            try {
                time = "";
                if (store != null) {
                    storeName = store.getStore_name();
                    storeAddress = store.getAddress();
                    phone = store.getPhone();
                    latLng = store.getLat() + "," + store.getLng();

                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date dBegin = sdf.parse(store.getCreated_begin());
                    if (dBegin != null) {

                        time += StringUtil.dateToString(dBegin, "dd/MM/yyyy")  + " " ;

                        time += StringUtil.dateToString(dBegin, "hh:mm") + " - ";
                    }
                    Date dEnd = sdf.parse(store.getCreated_end());
                    if (dEnd != null) {
                        time += StringUtil.dateToString(dEnd, "hh:mm");
                    }
                }

                tvStoreName.setText(storeName);
                tvStoreAddress.setText(storeAddress);
                tvStoreTime.setText(time);
                tvStorePhone.setText(latLng);
            } catch(Exception ex) {
                txt_title_store.setText("Vui lòng chọn store để tiếp tục công việc.");
                linearLayout_store.setVisibility(View.GONE);
                ex.printStackTrace();
            }



        } else {
            txt_title_store.setText("Vui lòng chọn store để tiếp tục công việc.");
            linearLayout_store.setVisibility(View.GONE);
        }

        btnCheckin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalInfo.getCurStore() != null && globalInfo.getCurStore().getStatus() == 0 && globalInfo.getCurStore().isActive()) {
                    return;
                }
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_CHECKIN);
                getActivity().sendBroadcast(i);
            }
        });

        btnCreateSaleOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalInfo.getCurStore() != null && (globalInfo.getCurStore().getCheckin() == null || globalInfo.getCurStore().getCheckin().isEmpty())) {
                    if (globalInfo.getCurStore().getStatus() != 0 || globalInfo.getCurStore().getLock_order() != 1) {
                        Common.showAlertDialog(activity, "Thông báo", "Checkin trước khi thực hiện bán hàng.", "Ok", new SingleListener() {
                            @Override
                            public void myFunction() {

                            }
                        });
                        return;
                    }
                }
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_CREATE_SALEORDER);
                getActivity().sendBroadcast(i);
            }
        });

        btnGetGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_GET_GIFT);
                getActivity().sendBroadcast(i);
            }
        });

        btnWarehouse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_WAREHOUSE);
                getActivity().sendBroadcast(i);
            }
        });

        btnReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_REPORT);
                getActivity().sendBroadcast(i);
            }
        });

        btnCheckout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalInfo.getCurStore() != null && globalInfo.getCurStore().getStatus() == 0 && globalInfo.getCurStore().isActive()) {
                    return;
                }
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_CHECKOUT);
                getActivity().sendBroadcast(i);
            }
        });

        btnNotify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_NOTIFY);
                getActivity().sendBroadcast(i);
            }
        });

        btnCloseStore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalInfo.getCurStore() != null && globalInfo.getCurStore().getStatus() == 0 && globalInfo.getCurStore().isActive()) {
                    return;
                }
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_CLOSE_STORE);
                getActivity().sendBroadcast(i);
            }
        });

        btnCheckLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (globalInfo.getCurStore() != null && globalInfo.getCurStore().getStatus() == 0 && globalInfo.getCurStore().isActive()) {
                    return;
                }
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_CHECK_LOCATION);
                getActivity().sendBroadcast(i);
            }
        });

        btnListCustomer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_LIST_CUSTOMER);
                getActivity().sendBroadcast(i);
            }
        });
        btnStoreReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_REPORT_STORE);
                getActivity().sendBroadcast(i);
            }
        });
        btnReportOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_REPORT_ORDER);
                getActivity().sendBroadcast(i);
            }
        });
        btnStoreGift.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_STORE_GIFT);
                getActivity().sendBroadcast(i);
            }
        });


        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This inteface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }



}
