package com.thienhaisoft.barcode.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.activity.ChangeGiftActivity;
import com.thienhaisoft.barcode.activity.EarnPointActivity;
import com.thienhaisoft.barcode.activity.SamplingActivity;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;


public class SaleOrderFragment extends Fragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    // TODO: Create View
    private EditText edt_phone_number = null;
    private EditText edt_customer_name = null;
    private EditText edt_email = null;
    private EditText edt_address = null;
    private TextView txt_total_point = null;

    private RadioGroup radg_gender = null;
    private Button btn_earn_point = null;
    private Button btn_change_gift = null;
    private Button btn_samling = null;
    private ImageButton btn_back = null;
    private Activity activity = null;
    private String str_result_phone_number = "";
    private Boolean bl_search_success = false;
    private GlobalInfo globalInfo = null;
    private String store_id = "";
    private String user_id = "";
    private int i_Pid = 0;
    private String dtdd = "";
    private InformationCustomer informationCustomer;

    ArrayList<InformationCustomer> informationCustomerArrayList = new ArrayList<>();

    private ImageButton imgbtn_save = null;

    private boolean bl_check_create_success = false;

    private ProgressDialog progressDialog = null;

    private OnFragmentInteractionListener mListener;

    public SaleOrderFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PhotosFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SaleOrderFragment newInstance(String param1, String param2) {
        SaleOrderFragment fragment = new SaleOrderFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!edt_phone_number.getText().toString().equals("")) {
            searchCustomerForResume(edt_phone_number.getText().toString());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_saleorder, container, false);
        init(view);
        return view;
    }

    private void init(View view) {
        activity = getActivity();
        progressDialog = new ProgressDialog(activity);

        globalInfo = (GlobalInfo) activity.getApplicationContext();
        edt_phone_number = (EditText) view.findViewById(R.id.sellout_edt_phone_number);
        edt_customer_name = (EditText) view.findViewById(R.id.sellout_edt_customer_name);
        edt_email = (EditText) view.findViewById(R.id.sellout_edt_email);
        edt_address = (EditText) view.findViewById(R.id.sellout_edt_address);
        txt_total_point = (TextView) view.findViewById(R.id.saleorder_txt_total_point);

        btn_change_gift = (Button) view.findViewById(R.id.sellout_btn_change_gift);
        btn_earn_point = (Button) view.findViewById(R.id.sellout_btn_earn_point);
        btn_back = (ImageButton) view.findViewById(R.id.sellout_btn_back);
        imgbtn_save = (ImageButton) view.findViewById(R.id.sellout_imgbtn_save);
        radg_gender = (RadioGroup) view.findViewById(R.id.sellout_radg_gender);
        btn_samling = (Button) view.findViewById(R.id.sellout_btn_samling);

        user_id = StringUtil.toString(globalInfo.getStaffId());
        store_id = StringUtil.toString(globalInfo.getCurStore().getStore_id());
        txt_total_point.setText("0.0");
        imgbtn_save.setOnClickListener(this);
        btn_back.setOnClickListener(this);
        btn_change_gift.setOnClickListener(this);
        btn_earn_point.setOnClickListener(this);
        btn_samling.setOnClickListener(this);

        if (globalInfo.getCurStore().getStatus() != 0) {
            btn_samling.setEnabled(true);
            btn_change_gift.setEnabled(true);
        } else {
            btn_samling.setEnabled(false);
            btn_change_gift.setEnabled(false);
        }

        edt_phone_number.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    if (!edt_phone_number.getText().toString().equals("")) {
                        searchCustimer(edt_phone_number.getText().toString());
                    }
                }
            }
        });
    }

    private void searchCustimer(String str_phone_number) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", user_id);
        data.put("string", str_phone_number);
        final String data_report = data.toString();
        String url = Constants.API_SEARCH_CUSTOMER;
        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        bl_search_success = true;
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            informationCustomerArrayList.clear();
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                InformationCustomer informationCustomer = new InformationCustomer();
                                JSONObject item = data.getJSONObject(i);
                                informationCustomer.partJSON(item);
                                informationCustomerArrayList.add(informationCustomer);
                            }
                        }
                    } else {
                        bl_search_success = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("SaleOrder: Tìm kiếm khách hàng; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (bl_search_success && informationCustomerArrayList.size() > 0) {
                    informationCustomer = informationCustomerArrayList.get(0);
                    String phone_number = informationCustomer.getStr_DTDD();
                    String customer_name = informationCustomer.getStr_HoLot() + informationCustomer.getStr_Ten();
                    String str_info_custome_search = "DTDD: " + phone_number + "\r\n" + "Khách hàng: " + customer_name;
                    Common.showAlertDialog(activity, str_info_custome_search, "", "", "Tìm lại", "Chấp nhận", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {
                            edt_phone_number.setText("");
                        }

                        @Override
                        public void functionPositive() {
                            edt_phone_number.setText(informationCustomer.getStr_DTDD());
                            String ho_ten = "";
                            if (informationCustomer.getStr_HoLot() != null) {
                                ho_ten = informationCustomer.getStr_HoLot() + " ";
                            }
                            if (informationCustomer.getStr_Ten() != null) {
                                ho_ten = ho_ten + informationCustomer.getStr_Ten();
                            }
                            edt_customer_name.setText(ho_ten);
                            edt_address.setText(informationCustomer.getStr_DiaChi());
                            i_Pid = informationCustomer.getI_Pid();
                            str_result_phone_number = informationCustomer.getStr_DTDD();
                            txt_total_point.setText(informationCustomer.getDou_pointsproduct() + "");
                        }
                    });
                } else {
                    Common.showAlertDialog(activity, "SDT này chưa đăng ký", "", "", "", "Tạo mới", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {
                            edt_phone_number.setText("");
                        }

                        @Override
                        public void functionPositive() {
                            edt_customer_name.setText("");
                            edt_address.setText("");
                            edt_email.setText("");
                            txt_total_point.setText("0.0");
                            i_Pid = 0;
                            informationCustomer = new InformationCustomer();
                        }
                    });
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void searchCustomerForResume(String str_phone_number) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", user_id);
        data.put("string", str_phone_number);
        final String data_report = data.toString();
        String url = Constants.API_SEARCH_CUSTOMER;
        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        bl_search_success = true;
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            informationCustomerArrayList.clear();
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                InformationCustomer informationCustomer = new InformationCustomer();
                                JSONObject item = data.getJSONObject(i);
                                informationCustomer.partJSON(item);
                                informationCustomerArrayList.add(informationCustomer);
                            }
                        }
                    } else {
                        bl_search_success = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("SaleOrder: Tìm kiếm khi reload; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (bl_search_success && informationCustomerArrayList.size() > 0) {
                    informationCustomer = informationCustomerArrayList.get(0);
                    edt_phone_number.setText(informationCustomer.getStr_DTDD());
                    String ho_ten = "";
                    if (informationCustomer.getStr_HoLot() != null) {
                        ho_ten = informationCustomer.getStr_HoLot() + " ";
                    }
                    if (informationCustomer.getStr_Ten() != null) {
                        ho_ten = ho_ten + informationCustomer.getStr_Ten();
                    }
                    edt_customer_name.setText(ho_ten);
                    edt_address.setText(informationCustomer.getStr_DiaChi());
                    i_Pid = informationCustomer.getI_Pid();
                    str_result_phone_number = informationCustomer.getStr_DTDD();
                    txt_total_point.setText(informationCustomer.getDou_pointsproduct() + "");
                } else {
                    edt_customer_name.setText("");
                    edt_address.setText("");
                    edt_email.setText("");
                    txt_total_point.setText("0.0");
                    i_Pid = 0;
                    informationCustomer = new InformationCustomer();
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void searchCustomerForCreate(String str_phone_number) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", user_id);
        data.put("string", str_phone_number);
        final String data_report = data.toString();
        String url = Constants.API_SEARCH_CUSTOMER;
        APIUtils.LoadJSON(activity, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        bl_search_success = true;
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            informationCustomerArrayList.clear();
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                InformationCustomer informationCustomer = new InformationCustomer();
                                JSONObject item = data.getJSONObject(i);
                                informationCustomer.partJSON(item);
                                informationCustomerArrayList.add(informationCustomer);
                            }
                        }
                    } else {
                        bl_search_success = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("SaleOrder: Tìm kiếm khi tạo khách hàng mới; Result: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (bl_search_success && informationCustomerArrayList.size() > 0) {
                    informationCustomer = informationCustomerArrayList.get(0);
                    i_Pid = informationCustomer.getI_Pid();
                    earnPoint();
                } else {
                    i_Pid = 0;
                    informationCustomer = new InformationCustomer();
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sellout_btn_change_gift:
                changeGift();
                break;
            case R.id.sellout_btn_earn_point:
                earnPoint();
                break;
            case R.id.sellout_imgbtn_save:
                checkisEmptyInputInfo();
                break;
            case R.id.sellout_btn_back:
                getActivity().onBackPressed();
                break;
            case R.id.sellout_btn_samling:
                handleActionSamling();
                break;
            default:
                break;
        }
    }

    private void handleActionSamling() {
        if (i_Pid == 0) {
            informationCustomer = new InformationCustomer();
        }
        Intent i = new Intent(activity, SamplingActivity.class);
        i.putExtra(Constants.RESULT_INFO_CUSTOMER_INFO, informationCustomer);
        startActivity(i);
    }

    private void checkisEmptyInputInfo() {
        if (i_Pid == 0) {
            if (edt_address.getText().toString().isEmpty() ||
                    edt_customer_name.getText().toString().isEmpty() ||
                    edt_phone_number.getText().toString().isEmpty() ||
                    (edt_phone_number.getText().toString().length() < 10) ||
                    (edt_phone_number.getText().toString().length() > 11)) {
                String textError = "";
                if (edt_address.getText().toString().isEmpty()) {
                    textError = "địa chỉ";
                }
                if (edt_customer_name.getText().toString().isEmpty()) {
                    textError = "tên khách hàng";
                }
                if (edt_phone_number.getText().toString().isEmpty() ||
                        (edt_phone_number.getText().toString().length() < 10) ||
                        (edt_phone_number.getText().toString().length() > 11)) {
                    textError = "đúng số điện thoại";
                }
                Common.showAlertDialog(activity, "Xin nhập " + textError + ".", "Thiếu thông tin", "", "Ok", "", new MyListener() {
                    @Override
                    public void functionNegative() {

                    }

                    @Override
                    public void functionNeutral() {

                    }

                    @Override
                    public void functionPositive() {

                    }
                });
            } else {
                saveCustomer();
            }
        } else {
            Toast.makeText(activity, "Khách hàng đã tồn tại", Toast.LENGTH_LONG).show();
        }
    }

    private void changeGift() {
        if (i_Pid != 0) {
            Intent i = new Intent(activity, ChangeGiftActivity.class);
//            i.putExtra(Constants.RESULT_INFO_CUSTOMER_PID, i_Pid);
//            i.putExtra(Constants.RESULT_INFO_CUSTOMER_PHONE, str_result_phone_number);
            i.putExtra(Constants.RESULT_INFO_CUSTOMER_INFO, informationCustomer);
            startActivity(i);
        } else {
            Toast.makeText(activity, "Nhap Thong Tin Khach Hang", Toast.LENGTH_LONG).show();
        }
    }

    private void earnPoint() {
        if (i_Pid != 0) {
            Intent i = new Intent(activity, EarnPointActivity.class);
//            i.putExtra(Constants.RESULT_INFO_CUSTOMER_PID, i_Pid);
//            i.putExtra(Constants.RESULT_INFO_CUSTOMER_PHONE, str_result_phone_number);
            i.putExtra(Constants.RESULT_INFO_CUSTOMER_INFO, informationCustomer);
            startActivity(i);
        } else {
            Toast.makeText(activity, "Nhap Thong Tin Khach Hang", Toast.LENGTH_LONG).show();
        }
    }

    private void saveCustomer() {
        HashMap<String, String> data = new HashMap<>();
        data = getDataForRequestAPI();
        final String data_report = data.toString();
        APIUtils.LoadJSON(activity, data, Constants.API_CREATE_CUSTOMER, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "Please wait...", "Creating user");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject jsonObject = new JSONObject(successString);
                    if (jsonObject.has(Constants.RESULTAPI_STATUS)) {
                        if (jsonObject.getInt(Constants.RESULTAPI_STATUS) == 1) {
                            JSONObject info = jsonObject.getJSONObject(Constants.RESULTAPI_DATA);
                            i_Pid = info.getInt(Constants.RESULT_INFO_CUSTOMER_PID);
                            dtdd = info.getString(Constants.RESULTAPI_INFO_CUSTOMER_BY_USER_DTDD);
                            bl_check_create_success = true;
                        } else {
                            bl_check_create_success = false;
                        }
                    }

                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("SaleOrder: Tạo khách hàng mới; Res ult: " + failString + "; Data: " + data_report, getActivity());
            }

            @Override
            public void uiEnd() {
                if (bl_check_create_success) {
                    Common.showAlertDialog(activity, "Tạo khách hàng thành công. Mã khách hàng là: " + i_Pid, "Thành công", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            searchCustomerForCreate(dtdd);
                        }
                    });
                } else {
                    Common.showAlertDialog(activity, "Không tạo được khách hàng. Vui lòng thử lại.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {

                        }
                    });
                }
                bl_check_create_success = false;
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private HashMap<String, String> getDataForRequestAPI() {
        HashMap<String, String> result = new HashMap<>();

        String str_customer_name = "";
        String str_phone_number = "";
        int i_gender = 0;
        String str_address = "";
        String str_email = "";

        str_customer_name = edt_customer_name.getText().toString();
        str_phone_number = edt_phone_number.getText().toString();
        str_address = edt_address.getText().toString();
        str_email = edt_email.getText().toString();

        if (radg_gender.getCheckedRadioButtonId() == R.id.sellout_rad_female) {
            i_gender = 0;
        } else {
            i_gender = 1;
        }

        result.put(Constants.PAR_CREATE_CUSTOMER_PHONE_NUMBER, str_phone_number);
        result.put(Constants.PAR_CREATE_CUSTOMER_NAME, str_customer_name);
        result.put(Constants.PAR_CREATE_CUSTOMER_GENDER, i_gender + "");
        result.put(Constants.PAR_CREATE_CUSTOMER_ADDRESS, str_address);
        result.put(Constants.RESULTAPI_INFO_CUSTOMER_BY_USER_STORE_ID, store_id);
        result.put(Constants.PAR_CREATE_CUSTOMER_EMAIL, str_email);
        result.put(Constants.PAR_CREATE_CUSTOMER_CREATEBYUSER, user_id);

        return result;
    }

    private boolean validEmail(String email) {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This inteface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
