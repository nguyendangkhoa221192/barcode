package com.thienhaisoft.barcode.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayNotifyAdapter;
import com.thienhaisoft.barcode.entities.ListNotifyEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.DateUtils;
import com.thienhaisoft.barcode.utils.JsonUtils;
import com.thienhaisoft.barcode.utils.StringUtil;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.concurrent.ExecutionException;

public class CheckLocationFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private View rootView;
    private GoogleMap map;
    private MapView mapView;
    private Button btnCancelLocation;
    private Button btnUpdateLocation;


    private LatLng newLocation = null;


    private Activity activity = null;

    private GlobalInfo info;
    private ProgressDialog progressDialog = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public CheckLocationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CheckLocationFragment newInstance(String param1, String param2) {
        CheckLocationFragment fragment = new CheckLocationFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        activity = getActivity();
        info = (GlobalInfo) getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_check_location, container, false);
        progressDialog = new ProgressDialog(activity);
        btnCancelLocation = (Button) rootView.findViewById(R.id.btnMapCancel);
        btnUpdateLocation = (Button) rootView.findViewById(R.id.btnMapUpdate);

        mapView = (MapView) rootView.findViewById(R.id.mvCheckLocation);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();

        try {
            MapsInitializer.initialize(activity.getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap mMap) {
                map = mMap;

                // For showing a move to my location button
                try {
                    if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(activity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        Toast.makeText(activity.getApplicationContext(), R.string.ERROR_PERMISSION_GRANTED_LOCATION, Toast.LENGTH_SHORT).show();
                        return;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                map.setMyLocationEnabled(true);

                // For dropping a marker at a point on the Map
                double lat = 10.823129;
                double lng = 106.626184;
                if (info.getCurrentBestLocation() != null) {
                    lat = info.getCurrentBestLocation().getLatitude();
                    lng = info.getCurrentBestLocation().getLongitude();

                }
                String storeTile = "";
                String storeDescription = "";
                if (info.getCurStore() != null && info.getCurStore().getLat() != 0 && info.getCurStore().getLng() != 0) {
                    lat = info.getCurStore().getLat();
                    lng = info.getCurStore().getLng();
                    storeTile = info.getCurStore().getTitle();
                    storeDescription = info.getCurStore().getDescription();
                } else {
                    newLocation = new LatLng(lat, lng);
                }
                LatLng storeLocation = new LatLng(lat, lng);
                MarkerOptions markerOptions = new MarkerOptions().position(storeLocation).title(storeTile).snippet(storeDescription);
//                markerOptions.draggable(true);
                map.addMarker(markerOptions);

                // For zooming automatically to the location of the marker
                CameraPosition cameraPosition = new CameraPosition.Builder().target(storeLocation).zoom(12).build();
                map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

                final String finalStoreTile = storeTile;
                final String finalStoreDescription = storeDescription;
                map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
                    @Override
                    public void onMapClick(LatLng latLng) {
                        map.clear();
                        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title(finalStoreTile).snippet(finalStoreDescription);
                        map.addMarker(markerOptions);
                        newLocation = latLng;
                    }
                });

//                map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
//                    @Override
//                    public void onMarkerDragStart(Marker marker) {
//
//                    }
//
//                    @Override
//                    public void onMarkerDrag(Marker marker) {
//
//                    }
//
//                    @Override
//                    public void onMarkerDragEnd(Marker marker) {
//                        marker.setSnippet(marker.getPosition().latitude + "");
//                        map.animateCamera(CameraUpdateFactory.newLatLng(marker.getPosition()));
//                        newLocation = marker.getPosition();
//
//                    }
//                });



            }
        });

        btnCancelLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent();
                i.setAction(Constants.BROADCAST_LIST_FUNCTION);
                i.putExtra(Constants.KEY_FRAGMENT_VALUE, Constants.ACTION_GO_HOME);
                getActivity().sendBroadcast(i);
            }
        });

        btnUpdateLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (newLocation == null) {
                    Toast.makeText(activity.getApplicationContext(), R.string.check_location_renew_fail, Toast.LENGTH_SHORT).show();
                    return;
                }
                pushCheckLocation();
            }
        });

        if (info != null && info.getCurStore() != null && info.getCurStore().getLat() == 0 && info.getCurStore().getLng() == 0) {
            btnUpdateLocation.setEnabled(true);
        }
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This inteface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void pushCheckLocation() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fupdatelocationstore");
        data.put("storeid", StringUtil.toString(info.getCurStore().getId_store()));
        data.put("lat", StringUtil.toString(newLocation.latitude));
        data.put("lng", StringUtil.toString(newLocation.longitude));

        final String data_report = data.toString();

        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(activity, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "Đang tải dữ liệu, vui lòng chờ...", "Thông báo");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        boolean isSuccess = false;
                        try {
                            JSONObject json = new JSONObject(successString);
                            if (json != null) {
                                int status = (int) json.get("status");
                                if (status == 1) {
                                    isSuccess = true;
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent i = new Intent();
                        i.setAction(Constants.BROADCAST_ALERT);
                        if (isSuccess) {
                            i.putExtra(Constants.KEY_ALERT_VALUE, R.string.check_location_success);
                            info.getCurStore().setLat(newLocation.latitude);
                            info.getCurStore().setLng(newLocation.longitude);
                        } else {
                            i.putExtra(Constants.KEY_ALERT_VALUE, R.string.check_location_fail);
                        }
                        getActivity().sendBroadcast(i);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("CheckLocation: pushCheckLocation; Result: " + failString + "; Data: " + data_report, getActivity());
                }

                @Override
                public void uiEnd() {
                    Common.FinishProgressDialog(progressDialog);
                }
            });
        } catch (Exception ex) {
           ex.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
