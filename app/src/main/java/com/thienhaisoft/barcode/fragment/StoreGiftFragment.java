package com.thienhaisoft.barcode.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayReportAdapter;
import com.thienhaisoft.barcode.adapter.ArrayStoreGiftAdapter;
import com.thienhaisoft.barcode.entities.ListReportEntities;
import com.thienhaisoft.barcode.entities.ListStoreGiftEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.DateUtils;
import com.thienhaisoft.barcode.utils.JsonUtils;
import com.thienhaisoft.barcode.utils.StringUtil;

import java.util.HashMap;

public class StoreGiftFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private ListView lvStoreGift;
    private ListView lvReportSellin;
    private ListView lvReportMHTT;
    private View rootView;

    private Activity activity = null;

    private GlobalInfo info;


    ListStoreGiftEntities listStoreGiftEntities;

    private ProgressDialog progressDialog = null;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public StoreGiftFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StoreGiftFragment newInstance(String param1, String param2) {
        StoreGiftFragment fragment = new StoreGiftFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
        activity = getActivity();
        info = (GlobalInfo) getActivity().getApplicationContext();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        rootView = inflater.inflate(R.layout.fragment_store_gift, container, false);
        lvStoreGift = (ListView) rootView.findViewById(R.id.lvListStoreGift);
        progressDialog = new ProgressDialog(activity);
        renderListStoreGift();


        lvStoreGift.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {

//                if (listStoreGiftEntities.getData() != null && !listStoreGiftEntities.getData().isEmpty()) {
//                    final NotifyEntities notify = listStoreGiftEntities.getData().get(index);
//                    new AlertDialog.Builder(activity)
//                            .setIconAttribute(android.R.attr.alertDialogIcon)
//                            .setTitle(R.string.warning)
//                            .setMessage("Bạn có muốn chọn store " + notify.getStore_code() + " - " + notify.getStore_name() + "?")
//                            .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
//                                @Override
//                                public void onClick(DialogInterface dialog, int which) {
//                                    info.setCurStore(notify);
//                                    Intent i = new Intent();
//                                    i.setAction(Constants.BROADCAST_CHOOSE_STORE);
//                                    getActivity().sendBroadcast(i);
//                                    return;
//
//                                }
//                            })
//                            .setNegativeButton(R.string.no, null)
//                            .show();
//                }

            }
        });


        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnFragmentInteractionListener) {
//            mListener = (OnFragmentInteractionListener) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnFragmentInteractionListener");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This inteface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private void renderListStoreGift() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fviewstorestockbyusermore");
        data.put("userid", StringUtil.toString(info.getStaffId()));
        final String data_report = data.toString();

        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(activity, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "Đang tải dữ liệu, vui lòng chờ...", "Thông báo");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        listStoreGiftEntities = (ListStoreGiftEntities) JsonUtils.toObject(successString, ListStoreGiftEntities.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("StoreGift: Load list gift; Result: " + failString + "; Data: " + data_report, getActivity());
                }

                @Override
                public void uiEnd() {
                    if (listStoreGiftEntities != null && listStoreGiftEntities.getStatus() == 1) {
                        lvStoreGift.setAdapter(new ArrayStoreGiftAdapter(activity, listStoreGiftEntities.getData()));
                    }
                    Common.FinishProgressDialog(progressDialog);
                }
            });
        } catch (Exception ex) {
           ex.printStackTrace();
        }
    }


}
