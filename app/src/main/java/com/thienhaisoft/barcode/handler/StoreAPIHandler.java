package com.thienhaisoft.barcode.handler;

import com.thienhaisoft.barcode.entities.ListStoreEntities;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.utils.JsonUtils;

/**
 * Created by user on 25/05/2017.
 */

public class StoreAPIHandler implements APICallBack {

    private ListStoreEntities listStore;

    boolean isSuccess = false;

    public StoreAPIHandler(ListStoreEntities listStore) {
        this.listStore = listStore;
    }

    @Override
    public void uiStart() {
    }

    @Override
    public void success(String successString, int type) {
        listStore = (ListStoreEntities)JsonUtils.toObject(successString, ListStoreEntities.class);
    }

    @Override
    public void fail(String failString) {
        listStore = null;
    }

    @Override
    public void uiEnd() {
        // TODO: 25/05/2017
    }
}
