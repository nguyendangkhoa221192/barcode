/**
 * Desciption: create Connection to server and return result string
 * Author: Tran Quang Long
 * Create date: Feb 25, 2014
 *
 */

package com.thienhaisoft.barcode.httpUtils;

import java.io.InputStream;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.json.JSONObject;

import android.app.Activity;

import com.thienhaisoft.barcode.utils.Constants;

public class APIUtils {

	public static int TIMEOUT_TIME = 30000;

	public static void LoadJSON(final Activity activity, final HashMap<String, String> data,
			final String url, final APICallBack apiCallBack) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				apiCallBack.uiStart();
			}
		});
		new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {

					ArrayList<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
					if (data != null) {
						Set<String> set = data.keySet();
						for (String key : set) {
							BasicNameValuePair value = new BasicNameValuePair(key, data.get(key));
							list.add(value);
						}
					}

					HttpParams params = new BasicHttpParams();
					HttpConnectionParams.setSoTimeout(params, TIMEOUT_TIME);
					HttpConnectionParams.setConnectionTimeout(params, TIMEOUT_TIME);

					DefaultHttpClient client = new DefaultHttpClient(params);
					HttpPost post = new HttpPost(url);
					post.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
					HttpResponse response = null;
					response = client.execute(post);
					final StringBuilder builder = new StringBuilder();
					if (response != null
							&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

						InputStream inputStream = response.getEntity().getContent();
						Scanner scanner = new Scanner(inputStream);
						while (scanner.hasNext()) {
							builder.append(scanner.nextLine());
						}
						inputStream.close();
						scanner.close();
						apiCallBack.success(builder.toString(), 0);
					} else {
						apiCallBack.fail(response != null && response.getStatusLine() != null ? "response null"
								: "" + response.getStatusLine().getStatusCode());
					}
				} catch (final Exception e) {
					apiCallBack.fail(e.getMessage());
				} finally {
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							apiCallBack.uiEnd();
						}
					});
				}
			}
		}).start();
	}

	public static void LoadJSON(final Activity activity, final HashMap<String, String> data, final HashMap<String, String> header,
			final String url, final APICallBack apiCallBack) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				apiCallBack.uiStart();
			}
		});
		new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {

					// final String url = "http://ip.jsontest.com/";

					ArrayList<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
					if (data != null) {
						Set<String> set = data.keySet();
						for (String key : set) {
							BasicNameValuePair value = new BasicNameValuePair(key, data.get(key));
							list.add(value);
						}
					}

					HttpParams params = new BasicHttpParams();
					HttpConnectionParams.setSoTimeout(params, TIMEOUT_TIME);
					HttpConnectionParams.setConnectionTimeout(params, TIMEOUT_TIME);

					DefaultHttpClient client = new DefaultHttpClient(params);
					HttpPost post = new HttpPost(url);
					if(header != null && header.size() != 0){
						post.setHeader(Constants.API_PARAM_COOKIE, header.get(Constants.PAR_SESSIONID));
					}
					post.setEntity(new UrlEncodedFormEntity(list, "UTF-8"));
					HttpResponse response = null;
					response = client.execute(post);
					final StringBuilder builder = new StringBuilder();
					if (response != null
							&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

						InputStream inputStream = response.getEntity().getContent();
						Scanner scanner = new Scanner(inputStream);
						while (scanner.hasNext()) {
							builder.append(scanner.nextLine());
						}
						inputStream.close();
						scanner.close();
						apiCallBack.success(builder.toString(), 0);
					} else {
						apiCallBack.fail(response != null && response.getStatusLine() != null ? "response null"
								: "" + response.getStatusLine().getStatusCode());
					}
				} catch (final Exception e) {
					apiCallBack.fail(e.getMessage());
				} finally {
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							apiCallBack.uiEnd();
						}
					});
				}
			}
		}).start();
	}

	public static void LoadJSONGet(final Activity activity, final HashMap<String, String> data, final HashMap<String, String> header,
			final String url, final APICallBack apiCallBack) {
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				apiCallBack.uiStart();
			}
		});
		new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {
					String urlGet = "";
					if (data != null && data.size() > 0) {
						urlGet = url + "?";
						boolean first = true;
						// final String url = "http://ip.jsontest.com/";
						if (data != null) {
							Set<String> set = data.keySet();
							for (String key : set) {
								if (first) {
									urlGet = urlGet + key + "=" + data.get(key);
									first = false;
								} else {
									urlGet = urlGet + "&" + key + "=" + data.get(key);
								}
							}
						}
					} else {
						urlGet = url;
					}

					HttpParams params = new BasicHttpParams();
					HttpConnectionParams.setSoTimeout(params, TIMEOUT_TIME);
					HttpConnectionParams.setConnectionTimeout(params, TIMEOUT_TIME);

					DefaultHttpClient client = new DefaultHttpClient(params);
					HttpGet get = new HttpGet(urlGet);
					if(header != null && header.size() > 0){
						Set<String> set = header.keySet();
						for (String key : set) {
							get.addHeader(key, header.get(key));
						}
					}
					HttpResponse response = null;
					response = client.execute(get);
					final StringBuilder builder = new StringBuilder();
					if (response != null
							&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

						InputStream inputStream = response.getEntity().getContent();
						Scanner scanner = new Scanner(inputStream);
						while (scanner.hasNext()) {
							builder.append(scanner.nextLine());
						}
						inputStream.close();
						scanner.close();
						apiCallBack.success(builder.toString(), 0);
					} else {
						apiCallBack.fail(response != null && response.getStatusLine() != null ? "response null"
								: "" + response.getStatusLine().getStatusCode());
					}
				} catch (final Exception e) {
					apiCallBack.fail(e.getMessage());
				} finally {
					activity.runOnUiThread(new Runnable() {

						@Override
						public void run() {
							apiCallBack.uiEnd();
						}
					});
				}
			}
		}).start();
	}

    public static void LoadJSONGet(final Activity activity, final JSONObject jsonObject, final HashMap<String, String> header,
                                                final String url, final APICallBack apiCallBack) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                apiCallBack.uiStart();
            }
        });
        new Thread(new Runnable() {
            @Override
            public synchronized void run() {
                try {
                    String urlGet = "";
                    urlGet = url + URLEncoder.encode(jsonObject.toString(), "UTF-8");
                    HttpParams params = new BasicHttpParams();
                    HttpConnectionParams.setSoTimeout(params, TIMEOUT_TIME);
                    HttpConnectionParams.setConnectionTimeout(params, TIMEOUT_TIME);

                    DefaultHttpClient client = new DefaultHttpClient(params);
                    HttpGet get = new HttpGet(urlGet);
                    if(header != null && header.size() > 0){
                        get.setHeader(Constants.API_PARAM_COOKIE, header.get(Constants.PAR_SESSIONID));
                    }
                    HttpResponse response = null;
                    response = client.execute(get);
                    final StringBuilder builder = new StringBuilder();
                    if (response != null
                            && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                        InputStream inputStream = response.getEntity().getContent();
                        Scanner scanner = new Scanner(inputStream);
                        while (scanner.hasNext()) {
                            builder.append(scanner.nextLine());
                        }
                        inputStream.close();
                        scanner.close();
                        apiCallBack.success(builder.toString(), 0);
                    } else {
                        apiCallBack.fail(response != null && response.getStatusLine() != null ? "response null"
                                : "" + response.getStatusLine().getStatusCode());
                    }
                } catch (final Exception e) {
                    apiCallBack.fail(e.getMessage());
                } finally {
                    activity.runOnUiThread(new Runnable() {

                        @Override
                        public void run() {
                            apiCallBack.uiEnd();
                        }
                    });
                }
            }
        }).start();
    }

	public static void doPostData(final Activity activity, final HashMap<String, String> data, final HashMap<String, String> header,
								  final String url, final APICallBack apiCallBack) {

		if (activity != null) {
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					apiCallBack.uiStart();
				}
			});
		}

		new Thread(new Runnable() {
			@Override
			public synchronized void run() {
				try {

					String urlPost = url + "?";
					boolean first = true;
					// final String url = "http://ip.jsontest.com/";
					if (data != null) {
						Set<String> set = data.keySet();
						for (String key : set) {
							if(first){
								urlPost = urlPost + key + "=" + data.get(key);
								first = false;
							}else{
								urlPost = urlPost + "&" + key + "=" + data.get(key);
							}
						}
					}
					urlPost = urlPost.replaceAll(" ","+");

					HttpParams params = new BasicHttpParams();
					HttpConnectionParams.setSoTimeout(params, TIMEOUT_TIME);
					HttpConnectionParams.setConnectionTimeout(params, TIMEOUT_TIME);

					DefaultHttpClient client = new DefaultHttpClient(params);
					HttpPost post = new HttpPost(urlPost);
					if(header != null && header.size() != 0){
						post.setHeader(Constants.API_PARAM_COOKIE, header.get(Constants.PAR_SESSIONID));
					}
					HttpResponse response = null;
					response = client.execute(post);
					final StringBuilder builder = new StringBuilder();
					if (response != null
							&& response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

						InputStream inputStream = response.getEntity().getContent();
						Scanner scanner = new Scanner(inputStream);
						while (scanner.hasNext()) {
							builder.append(scanner.nextLine());
						}
						inputStream.close();
						scanner.close();
						apiCallBack.success(builder.toString(), 0);

					} else {
						apiCallBack.fail(response != null && response.getStatusLine() != null ? "response null"
								: "" + response.getStatusLine().getStatusCode());
					}
				} catch (final Exception e) {
					apiCallBack.fail(e.getMessage());
				} finally {

					if (activity != null) {
						activity.runOnUiThread(new Runnable() {

							@Override
							public void run() {
								apiCallBack.uiEnd();
							}
						});
					}
				}
			}
		}).start();
	}
}
