package com.thienhaisoft.barcode.activity;

import android.Manifest;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.FileProvider;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.fragment.CheckLocationFragment;
import com.thienhaisoft.barcode.fragment.ChooseStoreFragment;
import com.thienhaisoft.barcode.fragment.HomeFragment;
import com.thienhaisoft.barcode.fragment.NotifyFragment;
import com.thienhaisoft.barcode.fragment.ProfileFrament;
import com.thienhaisoft.barcode.fragment.ReportFragment;
import com.thienhaisoft.barcode.fragment.SaleOrderFragment;
import com.thienhaisoft.barcode.fragment.StoreGiftFragment;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.AndroidMultiPartEntity;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.FileUtil;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private NavigationView navigationView;
    DrawerLayout drawer;
    private View navHeader;

    private ProgressDialog progressDialog = null;

    private Handler mHandler;

    private Toolbar toolbar;

    private TextView txtStoreName;
    private TextView tvStaffCode;
    private TextView tvStaffName;

    FloatingActionButton btnCreateOrder;

    private String[] activityTitles;

    public static int navItemIndex = 0;
    private boolean isCameraResult = false;

    public LocationManager mLocationManager;

    public static String CURRENT_TAG = Constants.TAG_HOME;

    protected static final int CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE = 1;

    private boolean isTakingPhoto = false;
    private int ACTION_CAMERA = Constants.ACTION_CAMENRA_DEFAULT;

    private String mCurrentPhotoPath;

    private Uri imageUri;

    private GlobalInfo info;
    private BroadcastReceiver thsReceiver;
    private int distanceFromApi = 0;


    //init taking photo
    public ArrayList<String> GalleryList = new ArrayList<String>();
    public static String sFilePath = "";
    public static File CurrentFile = null;
    public static Uri CurrentUri = null;

    private static int ACTION_CHECKIN = 1;
    private static int ACTION_CHECKOUT = 2;
    private static int ACTION_CLOSE_STORE = 3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HidenKeyboard.setupUI(findViewById(android.R.id.content), this);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        GlobalInfo.setContext(this.getApplicationContext());
        mHandler = new Handler();

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        navigationView = (NavigationView) findViewById(R.id.nav_view);
//        btnCreateOrder = (FloatingActionButton) findViewById(R.id.btnCreateOrder);
//        btnCreateOrder.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Toast.makeText(getApplicationContext(), "Qua màn hình tạo đơn hàng", Toast.LENGTH_SHORT).show();
//                navItemIndex = 1;
//                CURRENT_TAG = Constants.TAG_SALE_ORDER;
//                loadHomeFragment();
//
//            }
//        });
        progressDialog = new ProgressDialog(this);

        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        navHeader = navigationView.getHeaderView(0);
        txtStoreName = (TextView) navHeader.findViewById(R.id.tvStoreName);
        tvStaffCode = (TextView) navHeader.findViewById(R.id.tvStaffCode);


        // load toolbar titles from string resources
//        activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

        // load nav menu cell_shape data
        loadNavHeader();

        // initializing navigation menu
        setUpNavigationView();

        if (savedInstanceState == null) {
            navItemIndex = 3;
            CURRENT_TAG = Constants.TAG_NOTIFICATIONS;
            loadHomeFragment();
        }

        info = (GlobalInfo) getApplicationContext();

        if (info != null && info.getStaff() != null) {
            tvStaffCode.setText(info.getStaffName() + " - " + info.getStaffFullName());
//            tvStaffName.setText(info.getStaffFullName());
        }

        if (info != null && info.getStaff() != null && info.getStaff().getRole() != null) {
            distanceFromApi = Integer.parseInt(StringUtil.toString(info.getStaff().getRole().getDistance()));
        }

        registerReceiver();


//        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
//        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
//                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
//        drawer.setDrawerListener(toggle);
//        toggle.syncState();
//
//        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
//        navigationView.setNavigationItemSelectedListener(this);

        updateLocation(true);
    }

    private void loadNavHeader() {
        /**
         * set hinh, thong tin tren cell_shape navigation
         */


//        // loading cell_shape background image
//        Glide.with(this).load(urlNavHeaderBg)
//                .crossFade()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imgNavHeaderBg);
//
//        // Loading profile image
//        Glide.with(this).load(urlProfileImg)
//                .crossFade()
//                .thumbnail(0.5f)
//                .bitmapTransform(new CircleTransform(this))
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .into(imgProfile);
//        // showing dot next to notifications label
//        navigationView.getMenu().getItem(3).setActionView(R.layout.menu_dot);
    }


    private void setUpNavigationView() {
        //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            // This method will trigger on item Click of navigation menu
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {

                //Check to see which item was being clicked and perform appropriate action
                switch (menuItem.getItemId()) {
                    //Replacing the main content with ContentFragment Which is our Inbox View;
                    case R.id.nav_home:
                        navItemIndex = 0;
                        CURRENT_TAG = Constants.TAG_HOME;
                        break;
                    case R.id.nav_saleOrder:
                        if (info.getCurStore() == null) {
                            navItemIndex = 2;
                            CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                        } else {
                            navItemIndex = 1;
                            CURRENT_TAG = Constants.TAG_SALE_ORDER;
                        }
                        break;
                    case R.id.nav_chooseStore:
                        navItemIndex = 2;
                        CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                        break;
                    case R.id.nav_notify:
                        navItemIndex = 3;
                        CURRENT_TAG = Constants.TAG_NOTIFICATIONS;
                        break;
                    case R.id.nav_check_location:
                        if (info.getCurStore() == null) {
                            navItemIndex = 2;
                            CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                        } else {
                            navItemIndex = 4;
                            CURRENT_TAG = Constants.TAG_CHECK_LOCATION;
                        }
                        break;
                    case R.id.nav_report:
                        navItemIndex = 5;
                        CURRENT_TAG = Constants.TAG_REPORT;
                        break;
                    case R.id.nav_store_gift:
                        navItemIndex = 6;
                        CURRENT_TAG = Constants.TAG_STORE_GIFT;
                        break;
                    case R.id.nav_logout:
                        Intent i = new Intent(MainActivity.this, LoginActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(i);
                        drawer.closeDrawers();
                        return true;
                    case R.id.nav_instructions:
                        Intent intent = new Intent(MainActivity.this, InstroActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_profile:
                        navItemIndex = 7;
                        CURRENT_TAG = Constants.TAG_PROFILE;
                        break;
                    default:
                        navItemIndex = 0;
                }

                //Checking if the item is in checked state or not, if not make it in checked state
                if (menuItem.isChecked()) {
                    menuItem.setChecked(false);
                } else {
                    menuItem.setChecked(true);
                }
                menuItem.setChecked(true);

                loadHomeFragment();

                return true;
            }
        });


        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.closeDrawer) {

            @Override
            public void onDrawerClosed(View drawerView) {
                // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                super.onDrawerOpened(drawerView);
            }
        };

        //Setting the actionbarToggle to drawer layout
        drawer.setDrawerListener(actionBarDrawerToggle);

        //calling sync state is necessary or else your hamburger icon wont show up
        actionBarDrawerToggle.syncState();
    }

    private void loadHomeFragment() {
        // selecting appropriate nav menu item
        selectNavMenu();

        // set toolbar title
        setToolbarTitle();

        // if user select the current navigation menu again, don't do anything
        // just close the navigation drawer
        if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
            drawer.closeDrawers();

            // show or hide the fab button (nut create sale order position right-bottom)
            toggleFab();
            return;
        }
        // Sometimes, when fragment has huge data, screen seems hanging
        // when switching between navigation menus
        // So using runnable, the fragment is loaded with cross fade effect
        // This effect can be seen in GMail app
        Runnable mPendingRunnable = new Runnable() {
            @Override
            public void run() {
                // update the main content by replacing fragments
                Fragment fragment = getHomeFragment();
                FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                        android.R.anim.fade_out);
                fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                fragmentTransaction.commitAllowingStateLoss();
            }
        };

        // If mPendingRunnable is not null, then add to the message queue
        if (mPendingRunnable != null) {
            mHandler.post(mPendingRunnable);
        }

        // show or hide the fab button
        toggleFab();

        //Closing drawer on item click
        drawer.closeDrawers();

        // refresh toolbar menu
        invalidateOptionsMenu();
    }

    private void setToolbarTitle() {
        getSupportActionBar().setTitle(activityTitles[navItemIndex]);
    }

    private void selectNavMenu() {
        navigationView.getMenu().getItem(navItemIndex).setChecked(true);
    }

    private void toggleFab() {
//        if (navItemIndex == 0) {
//            btnCreateOrder.show();
//        } else {
//            btnCreateOrder.hide();
//        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // home
                HomeFragment homeFragment = new HomeFragment();
                return homeFragment;
            case 1:
                SaleOrderFragment saleOrderFragment = new SaleOrderFragment();
                return saleOrderFragment;
            case 2:
                ChooseStoreFragment chooseStoreFragment = new ChooseStoreFragment();
                return chooseStoreFragment;
            case 3:
                NotifyFragment notifyFragment = new NotifyFragment();
                return notifyFragment;
            case 4:
                CheckLocationFragment checkLocation = new CheckLocationFragment();
                return checkLocation;
            case 5:
                ReportFragment report = new ReportFragment();
                return report;
            case 6:
                StoreGiftFragment storeGift = new StoreGiftFragment();
                return storeGift;
            case 7:
                ProfileFrament profileFrament = new ProfileFrament();
                return profileFrament;
            default:
                return new HomeFragment();
        }
    }

    @Override
    public void onBackPressed() {
        if (navItemIndex == 0 && CURRENT_TAG == Constants.TAG_HOME) {
            new AlertDialog.Builder(this)
                    .setIconAttribute(android.R.attr.alertDialogIcon)
                    .setTitle(R.string.warning)
                    .setMessage(R.string.confirm_quit)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (drawer.isDrawerOpen(GravityCompat.START)) {
                                drawer.closeDrawers();
                            }
                            finish();
                            return;
                        }

                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }

        navItemIndex = 0;
        CURRENT_TAG = Constants.TAG_HOME;
        loadHomeFragment();


//        super.onBackPressed();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_checkin) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private Location currentBestLocation = null;

    public void onClickCheckin(MenuItem item) {
//        Snackbar.make(navHeader, R.string.ui_checkin_msg, Snackbar.LENGTH_INDEFINITE)
//                .setAction("Action", null).show();
//        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
//
//        currentBestLocation = getLastBestLocation();
//        if (currentBestLocation == null) {
//            Snackbar.make(navHeader, R.string.UPDATE_LOCATION_FAIL, Snackbar.LENGTH_SHORT)
//                    .setAction("Action", null).show();
//            return;
//        }
//
//        pushLocationCheckin();
//        isCheckIn = true;
//
//        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getApplicationContext(), R.string.ERROR_PERMISSION_GRANTED_LOCATION, Toast.LENGTH_SHORT).show();
//            return;
//        }
//        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.LOCATION_REFRESH_TIME,
//                Constants.LOCATION_REFRESH_DISTANCE, mLocationListener);

    }

    public void onClickCheckout(MenuItem item) {
        Snackbar.make(navHeader, R.string.ui_checkout_msg, Snackbar.LENGTH_INDEFINITE)
                .setAction("Action", null).show();
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        currentBestLocation = getLastBestLocation();
        if (currentBestLocation == null) {
            Snackbar.make(navHeader, R.string.UPDATE_LOCATION_FAIL, Snackbar.LENGTH_SHORT)
                    .setAction("Action", null).show();
            return;
        }

        pushLocationCheckout();
        ACTION_CAMERA = Constants.ACTION_CAMENRA_CHECKOUT;
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                Toast.makeText(getApplicationContext(), R.string.ERROR_PERMISSION_GRANTED_LOCATION, Toast.LENGTH_SHORT).show();
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.LOCATION_REFRESH_TIME,
                Constants.LOCATION_REFRESH_DISTANCE, mLocationListener);

    }


    /**
     * This method modify the last know good location according to the arguments.
     *
     * @param location The possible new location.
     */
    void makeUseOfNewLocation(Location location) {
        if (isBetterLocation(location, currentBestLocation)) {
            currentBestLocation = location;
        }
    }


    /**
     * Determines whether one location reading is better than the current location fix
     *
     * @param location            The new location that you want to evaluate
     * @param currentBestLocation The current location fix, to which you want to compare the new one.
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > Constants.LOCATION_REFRESH_DISTANCE;
        boolean isSignificantlyOlder = timeDelta < -Constants.LOCATION_REFRESH_DISTANCE;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location,
        // because the user has likely moved.
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse.
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

    private Location getLastBestLocation() {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                showDialogWarning(R.string.ERROR_PERMISSION_GRANTED_LOCATION);
                return null;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Location locationGPS = mLocationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        Location locationNet = mLocationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

        long GPSLocationTime = 0;
        if (null != locationGPS) {
            GPSLocationTime = locationGPS.getTime();
        }

        long NetLocationTime = 0;

        if (null != locationNet) {
            NetLocationTime = locationNet.getTime();
        }

        if (0 < GPSLocationTime - NetLocationTime) {
            info.setCurrentBestLocation(locationGPS);
            return locationGPS;
        } else {
            info.setCurrentBestLocation(locationNet);
            return locationNet;
        }
    }


    private final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            makeUseOfNewLocation(location);
            if (currentBestLocation == null) {
                currentBestLocation = location;
            }
//            if (isCheckIn) {
//                pushLocationCheckin();
//            } else {
//                pushLocationCheckout();
//            }
        }

        @Override
        public void onStatusChanged(String s, int i, Bundle bundle) {

        }

        @Override
        public void onProviderEnabled(String s) {

        }

        @Override
        public void onProviderDisabled(String s) {

        }
    };

    private final int TYPE_CHECK_IN = 1;
    private final int TYPE_CHECK_OUT = 2;
    private final int TYPE_CLOSE_STORE = 3;

    private void pushLocationCheckin() {

        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > (distanceFromApi * Constants.LOCATION_MULTI_DISTANCE)) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi * 10) + " m!");
            return;
        }
        long date = System.currentTimeMillis()/1000; // khong lay milisecond

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fcheckinandupload");
        data.put("event_id", "1");
        data.put("userid", StringUtil.toString(info.getStaffId()));
//        try {
//            data.put("description", URLEncoder.encode("Checkin tại store: " + info.getCurStore().getStore_name(), "UTF-8"));
            data.put("description", ("Checkin tại store: " + info.getCurStore().getStore_name()).replaceAll(" ", "_").replaceAll(":", "_"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        data.put("created_date", StringUtil.toString(date));
        data.put("start_time", StringUtil.toString(date));
        data.put("end_time", StringUtil.toString(date));
        data.put("checkin_time", StringUtil.toString(date));
//        data.put("checkout_time", StringUtil.toString(date));
        data.put("lat", StringUtil.toString(currentBestLocation.getLatitude()));
        data.put("lng", StringUtil.toString(currentBestLocation.getLongitude()));
        data.put("work_hours", "1");
        data.put("work_result", StringUtil.toString(date));
        data.put("reason", "checkin");
        data.put("absent_approve", "0");
        data.put("work_content", "checkin");
        data.put("absent_approve_by", "1");
        data.put("storeid", StringUtil.toString(info.getCurStore().getStore_id()));
        data.put("shift", info.getCurStore().getCa());


//        HashMap<String, String> cell_shape = new HashMap<>();
//        cell_shape.put("accept", "application/json");
//        cell_shape.put("accept-encoding", "gzip, deflate");
//        cell_shape.put("accept-language", "en-US,en;q=0.8");
//        cell_shape.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader, progressDialog));
            new UploadFileToServer(data, TYPE_CHECK_IN).execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ACTION_CAMERA = Constants.ACTION_CAMENRA_DEFAULT;
    }

    private void pushLocationCheckout() {
        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > (distanceFromApi * Constants.LOCATION_MULTI_DISTANCE)) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi * 10) + " m!");
            return;
        }

        long date = System.currentTimeMillis()/1000; // khong lay milisecond

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fcheckinandupload");
        data.put("event_id", "1");
        data.put("userid", StringUtil.toString(info.getStaffId()));
//        try {
//            data.put("description", URLEncoder.encode("Checkout tại store: " + info.getCurStore().getStore_name(), "UTF-8"));
//        data.put("description", ("Checkout tại store: " + info.getCurStore().getStore_name()).replaceAll(" ", "_").replaceAll(":", "_"));
        data.put("description", ("Checkout").replaceAll(" ", "_"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        data.put("created_date", StringUtil.toString(date));
        data.put("start_time", StringUtil.toString(date));
        data.put("end_time", StringUtil.toString(date));
        data.put("checkout_time", StringUtil.toString(date));
        data.put("lat", StringUtil.toString(currentBestLocation.getLatitude()));
        data.put("lng", StringUtil.toString(currentBestLocation.getLongitude()));
        data.put("work_hours", "1");
        data.put("work_result", StringUtil.toString(date));
        data.put("reason", "checkout");
        data.put("absent_approve", "0");
        data.put("work_content", "checkout");
        data.put("absent_approve_by", "1");
        data.put("storeid", StringUtil.toString(info.getCurStore().getStore_id()));
        data.put("shift", info.getCurStore().getCa());

//        HashMap<String, String> cell_shape = new HashMap<>();
//        cell_shape.put("accept", "application/json");
//        cell_shape.put("accept-encoding", "gzip, deflate");
//        cell_shape.put("accept-language", "en-US,en;q=0.8");
//        cell_shape.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader, progressDialog));
            new UploadFileToServer(data, TYPE_CHECK_OUT).execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ACTION_CAMERA = Constants.ACTION_CAMENRA_DEFAULT;
    }

    private void pushLocationCloseStore() {

        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > distanceFromApi * Constants.LOCATION_MULTI_DISTANCE) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi) + " m!");
            return;
        }
        long date = System.currentTimeMillis()/1000; // khong lay milisecond

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fcheckinandupload");
        data.put("event_id", "1");
        data.put("userid", StringUtil.toString(info.getStaffId()));
//        try {
//            data.put("description", URLEncoder.encode("Close tại store: " + info.getCurStore().getStore_name(), "UTF-8"));
        data.put("description", ("Close tại store: " + info.getCurStore().getStore_name()).replaceAll(" ", "_").replaceAll(":", "_"));
//        } catch (UnsupportedEncodingException e) {
//            e.printStackTrace();
//        }
        data.put("created_date", StringUtil.toString(date));
        data.put("start_time", StringUtil.toString(date));
        data.put("end_time", StringUtil.toString(date));
        data.put("checkin_time", StringUtil.toString(date));
//        data.put("checkout_time", StringUtil.toString(date));
        data.put("lat", StringUtil.toString(currentBestLocation.getLatitude()));
        data.put("lng", StringUtil.toString(currentBestLocation.getLongitude()));
        data.put("work_hours", "1");
        data.put("work_result", StringUtil.toString(date));
        data.put("reason", "close");
        data.put("absent_approve", "0");
        data.put("work_content", "close");
        data.put("absent_approve_by", "1");
        data.put("storeid", StringUtil.toString(info.getCurStore().getStore_id()));
        data.put("shift", info.getCurStore().getCa());

//        HashMap<String, String> cell_shape = new HashMap<>();
//        cell_shape.put("accept", "application/json");
//        cell_shape.put("accept-encoding", "gzip, deflate");
//        cell_shape.put("accept-language", "en-US,en;q=0.8");
//        cell_shape.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader, progressDialog));
            new UploadFileToServer(data, TYPE_CLOSE_STORE).execute();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        ACTION_CAMERA = Constants.ACTION_CAMENRA_DEFAULT;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putParcelable("file_uri", imageUri);
        super.onSaveInstanceState(outState);

        // save file url in bundle as it will be null on scren orientation
        // changes
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        imageUri = savedInstanceState.getParcelable("file_uri");
        super.onRestoreInstanceState(savedInstanceState);

        // get the file url
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            switch (requestCode) {
                case CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE:
                    streamPhotoTaking = null;
                    if (data != null) {
                        if (bmpPhotoTaking != null) {
                            bmpPhotoTaking.recycle();
                        }
                        streamPhotoTaking = getContentResolver().openInputStream(data.getData());
                    }
                    switch (ACTION_CAMERA) {
                        case Constants.ACTION_CAMENRA_CHECKIN:
                            pushLocationCheckin();
                            break;
                        case Constants.ACTION_CAMENRA_CHECKOUT:
                            pushLocationCheckout();
                            break;
                        case Constants.ACTION_CAMENRA_CLOSE_STORE:
                            pushLocationCloseStore();
                            break;
                    }
                    break;
                default:
            }


        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (isTakingPhoto) {
                isTakingPhoto = false;
            }
        }


    }

    private void registerReceiver() {
        if (thsReceiver == null) {
            thsReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    switch (action) {
                        case Constants.BROADCAST_CHOOSE_STORE:
                            updateUIChooseStore();
                            break;
                        case Constants.BROADCAST_LIST_FUNCTION:
                            String fragmentTag = intent.getExtras() == null ? "" : intent.getExtras().getString(Constants.KEY_FRAGMENT_VALUE);
                            doActionClickHomeFunction(fragmentTag);
                            break;
                        case Constants.BROADCAST_ALERT:
                            if (intent.getExtras() != null) {
                                int alert = intent.getExtras().getInt(Constants.KEY_ALERT_VALUE);
                                showDialogWarning(alert);
                            }
                            break;
                        default:
                    }
                }
            };
        }


        IntentFilter filter = new IntentFilter();
        filter.addAction(Constants.BROADCAST_CHOOSE_STORE);
        filter.addAction(Constants.BROADCAST_LIST_FUNCTION);
        filter.addAction(Constants.BROADCAST_ALERT);
        this.registerReceiver(thsReceiver, filter);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            this.unregisterReceiver(this.thsReceiver);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        registerReceiver();
//        if (isCameraResult) {
//            cameraResult();
//        }
    }

    private void updateLocation (boolean isCheckEnableGPS) {
        try {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getApplicationContext(), R.string.ERROR_PERMISSION_GRANTED_LOCATION, Toast.LENGTH_SHORT).show();
                showDialogWarning(R.string.ERROR_PERMISSION_GRANTED_LOCATION);
                return;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if (mLocationManager == null) {
            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        }

        if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) && isCheckEnableGPS) {
            startActivity(new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
        }

        mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, Constants.LOCATION_REFRESH_TIME,
                Constants.LOCATION_REFRESH_DISTANCE, mLocationListener);
    }


    @Override
    protected void onPause() {
        super.onPause();
        this.unregisterReceiver(this.thsReceiver);
    }

    private void updateUIChooseStore() {
        if (info == null || info.getCurStore() == null) {
            txtStoreName.setText("Vui lòng chọn store làm việc!");
            return;
        }

        txtStoreName.setText(info.getCurStore().getStore_name());

        navItemIndex = 0;
        CURRENT_TAG = Constants.TAG_HOME;
        loadHomeFragment();


    }


    private void doActionClickHomeFunction(String fragmentTag) {
        if (StringUtil.isNullOrEmpty(fragmentTag)) {
            return;
        }

        switch (fragmentTag) {
            case Constants.ACTION_CHECKIN:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    doCheckin();
                }

                break;
            case Constants.ACTION_CREATE_SALEORDER:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    navItemIndex = 1;
                    CURRENT_TAG = Constants.TAG_SALE_ORDER;
                    loadHomeFragment();
                }

                break;
            case Constants.ACTION_GET_GIFT:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    navItemIndex = 1;
                    CURRENT_TAG = Constants.TAG_SALE_ORDER;
                    loadHomeFragment();
                }
                break;
            case Constants.ACTION_WAREHOUSE:
//                Common.showProgressDialog(progressDialog, "Đang xử lý dữ liệu, vui lòng chờ...", "Thông báo");
//                Toast.makeText(getApplicationContext(), "ACTION_WAREHOUSE", Toast.LENGTH_SHORT).show();
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    Intent i = new Intent(this, SettingStore.class);
                    startActivity(i);
                }
                break;
            case Constants.ACTION_REPORT:
                navItemIndex = 5;
                CURRENT_TAG = Constants.TAG_REPORT;
                loadHomeFragment();

                break;
            case Constants.ACTION_CHECKOUT:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    doCheckout();
                }


                break;
            case Constants.ACTION_NOTIFY:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    navItemIndex = 3;
                    CURRENT_TAG = Constants.TAG_NOTIFICATIONS;
                    loadHomeFragment();
                }
                break;
            case Constants.ACTION_CLOSE_STORE:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    doCloseStore();
                }
                break;
            case Constants.ACTION_CHECK_LOCATION:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    navItemIndex = 4;
                    CURRENT_TAG = Constants.TAG_CHECK_LOCATION;
                    mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
                    getLastBestLocation();
                    loadHomeFragment();
                }
                break;
            case Constants.ACTION_LIST_CUSTOMER:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    Intent i = new Intent(this, ListCustomer.class);
                    startActivity(i);
                }
                break;
            case Constants.ACTION_GO_HOME:
                navItemIndex = 0;
                CURRENT_TAG = Constants.TAG_HOME;
                loadHomeFragment();
                break;
            case Constants.ACTION_REPORT_STORE:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    Intent i = new Intent(this, ReportStoreActivity.class);
                    startActivity(i);
                }
                break;
            case Constants.ACTION_REPORT_ORDER:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    Intent i = new Intent(this, ReportOrderActivity.class);
                    startActivity(i);
                }
                break;
            case Constants.ACTION_STORE_GIFT:
                if (info.getCurStore() == null) {
                    navItemIndex = 2;
                    CURRENT_TAG = Constants.TAG_CHOOSE_STORE;
                    loadHomeFragment();
                } else {
                    Intent i = new Intent(this, ReportStoreGiftActivity.class);
                    startActivity(i);
                }
                break;
            default:
                break;
        }
    }

    private void doCheckin() {
        if (info.getCurStore() == null) {
            return;
        }

        if (info.getCurStore().getLat() == 0 && info.getCurStore().getLng() == 0) {
            pushCheckLocation(ACTION_CHECKIN);
            return;
        }

        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        updateLocation(true);

        currentBestLocation = getLastBestLocation();
        if (currentBestLocation == null) {
            showDialogWarning(R.string.UPDATE_LOCATION_FAIL);
            return;
        }

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > (distanceFromApi * Constants.LOCATION_MULTI_DISTANCE)) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi) + " m!");
            return;
        } else {
            ACTION_CAMERA = Constants.ACTION_CAMENRA_CHECKIN;
            if(info.getCurStore().getCheckin() == null || info.getCurStore().getCheckin().isEmpty()) {
                startCamera();
            } else {
                Toast.makeText(this, "Đã checkin thành công.", Toast.LENGTH_SHORT);
            }

        }
    }

    private void doCheckout() {
        if (info.getCurStore() == null) {
            return;
        }
        if (info.getCurStore().getLat() == 0 && info.getCurStore().getLng() == 0) {
            pushCheckLocation(ACTION_CHECKOUT);
            return;
        }

        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        updateLocation(true);

        currentBestLocation = getLastBestLocation();
        if (currentBestLocation == null) {
            showDialogWarning(R.string.UPDATE_LOCATION_FAIL);
            return;
        }

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > (distanceFromApi * Constants.LOCATION_MULTI_DISTANCE)) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi) + " m!");
            return;
        } else {
            ACTION_CAMERA = Constants.ACTION_CAMENRA_CHECKOUT;
            if (info.getCurStore().getCheckin() != null && !info.getCurStore().getCheckin().isEmpty()) {
                if (info.getCurStore().getCheckout() == null || info.getCurStore().getCheckout().isEmpty()) {
                    startCamera();
                } else {
                    Toast.makeText(this, "Đã checkout thành công.", Toast.LENGTH_SHORT);
                }
            } else {
                Toast.makeText(this, "Checkin trước khi checkout.", Toast.LENGTH_SHORT);
            }
        }
    }

    private void doCloseStore() {
        if (info.getCurStore() == null) {
            return;
        }
        if (info.getCurStore().getLat() == 0 && info.getCurStore().getLng() == 0) {
            pushCheckLocation(ACTION_CLOSE_STORE);
            return;
        }

        Location storeLocation = new Location("");
        storeLocation.setLatitude(info.getCurStore().getLat());
        storeLocation.setLongitude(info.getCurStore().getLng());

        updateLocation(true);

        currentBestLocation = getLastBestLocation();
        if (currentBestLocation == null) {
            showDialogWarning(R.string.UPDATE_LOCATION_FAIL);
            return;
        }

        float distance = currentBestLocation.distanceTo(storeLocation);
        if (distance > (distanceFromApi * Constants.LOCATION_MULTI_DISTANCE)) {
            showDialogWarning(getString(R.string.LOCATION_DISTANCE_FAIL) + StringUtil.toString(distanceFromApi) + " m!");
            return;
        } else {
            ACTION_CAMERA = Constants.ACTION_CAMENRA_CLOSE_STORE;
            startCamera();
        }
    }

    private void FillPhotoList() {
        // initialize the list!
        this.GalleryList.clear();
        String[] projection = {MediaStore.Images.ImageColumns.DISPLAY_NAME};
        // intialize the Uri and the Cursor, and the current expected size.
        Cursor c = null;
        Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
        //
        // Query the Uri to get the data path.  Only if the Uri is valid.
        if (u != null) {
            c = managedQuery(u, projection, null, null, null);
        }

        // If we found the cursor and found a record in it (we also have the id).
        if ((c != null) && (c.moveToFirst())) {
            do {
                // Loop each and add to the list.
                this.GalleryList.add(c.getString(0));
            }
            while (c.moveToNext());
        }
    }

    private String getTempFileString() {

        // Only one time will we grab this location.
//        final File path = new File(Environment.getExternalStorageDirectory(),
//                getString(getApplicationInfo().labelRes));
        // 28/12/2017 fix for android 7
        final File path = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        //
        // If this does not exist, we can create it here.
        if (!path.exists()) {
            path.mkdir();
        }
        //
        return new File(path, String.valueOf(System.currentTimeMillis()) + ".jpg").getPath();
    }

    public void setsFilePath(String value) {
        // We just updated this value. Set the property first.
        sFilePath = value;
        //
        // initialize these two
        CurrentFile = null;
        CurrentUri = null;
        //
        // If we have something real, setup the file and the Uri.
        if (!sFilePath.equalsIgnoreCase("")) {
            CurrentFile = new File(sFilePath);

            // 28/12/2017 fix for android 7
            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.KITKAT) {
                CurrentUri = FileProvider.getUriForFile(this,"com.thienhaisoft.ams",CurrentFile);
            } else {
                CurrentUri = Uri.fromFile(CurrentFile);
            }

            //CurrentUri = Uri.fromFile(CurrentFile);
        }
    }

    public void startCamera() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            // Specify the output. This will be unique.
            setsFilePath(getTempFileString());
            //
            intent.putExtra(MediaStore.EXTRA_OUTPUT, CurrentUri);
            //
            // Keep a list for afterwards
//        FillPhotoList();
            //
            // finally start the intent and wait for a result.
            startActivityForResult(intent, CAPTURE_IMAGE_ACTIVITY_REQUEST_CODE);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void cameraResult() {
        Common.showProgressDialog(progressDialog, "Đang xử lý dữ liệu, vui lòng chờ...", "Thông báo");
        new Thread(new Runnable() {
            public void run() {

                // This is ##### ridiculous.  Some versions of Android save
                // to the MediaStore as well.  Not sure why!  We don't know what
                // name Android will give either, so we get to search for this
                // manually and remove it.
                String[] projection = {MediaStore.Images.ImageColumns.SIZE,
                        MediaStore.Images.ImageColumns.DISPLAY_NAME,
                        MediaStore.Images.ImageColumns.DATA,
                        BaseColumns._ID,};
                //
                // intialize the Uri and the Cursor, and the current expected size.
                Cursor c = null;
                Uri u = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                //
                if (CurrentFile != null) {
                    // Query the Uri to get the data path.  Only if the Uri is valid,
                    // and we had a valid size to be searching for.
                    if ((u != null) && (CurrentFile.length() > 0)) {
                        c = managedQuery(u, projection, null, null, null);
                    }
                    //
                    // If we found the cursor and found a record in it (we also have the size).
                    if ((c != null) && (c.moveToFirst())) {
                        do {
                            // Check each area in the gallary we built before.
                            boolean bFound = false;
                            for (String sGallery : GalleryList) {
                                if (sGallery.equalsIgnoreCase(c.getString(1))) {
                                    bFound = true;
                                    break;
                                }
                            }
                            //
                            // To here we looped the full gallery.
                            if (!bFound) {
                                // This is the NEW image.  If the size is bigger, copy it.
                                // Then delete it!
                                File f = new File(c.getString(2));

                                // Ensure it's there, check size, and delete!
                                if ((f.exists()) && (CurrentFile.length() < c.getLong(0)) && (CurrentFile.delete())) {
                                    // Finally we can stop the copy.
                                    try {
                                        CurrentFile.createNewFile();
                                        FileChannel source = null;
                                        FileChannel destination = null;
                                        try {
                                            source = new FileInputStream(f).getChannel();
                                            destination = new FileOutputStream(CurrentFile).getChannel();
                                            destination.transferFrom(source, 0, source.size());
                                        } finally {
                                            if (source != null) {
                                                source.close();
                                            }
                                            if (destination != null) {
                                                destination.close();
                                            }
                                        }
                                    } catch (IOException e) {
                                        // Could not copy the file over.
//                                this.CallToast(MainActivity.this, getString(R.string.ErrorOccured), 0);
                                    }
                                }
                                //
                                ContentResolver cr = getContentResolver();
                                cr.delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                                        BaseColumns._ID + "=" + c.getString(3), null);
                                break;
                            }
                        }
                        while (c.moveToNext());
                    }
                }

                if (CurrentFile != null) {
                    //check in/out
                    Common.FinishProgressDialog(progressDialog);

                    switch (ACTION_CAMERA) {
                        case Constants.ACTION_CAMENRA_CHECKIN:
                            pushLocationCheckin();
                            break;
                        case Constants.ACTION_CAMENRA_CHECKOUT:
                            pushLocationCheckout();
                            break;
                        case Constants.ACTION_CAMENRA_CLOSE_STORE:
                            pushLocationCloseStore();
                            break;
                    }
                }
                isCameraResult = false;
            }
        }).start();
    }

    private void showDialogWarning(int title) {
        new AlertDialog.Builder(this)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setTitle(R.string.warning)
                .setMessage(title)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawers();
                        }
//                        finish();
                        return;
                    }

                })
                .show();
    }

    private void showDialogWarning(String title) {
        new AlertDialog.Builder(this)
                .setIconAttribute(android.R.attr.alertDialogIcon)
                .setTitle(R.string.warning)
                .setMessage(title)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (drawer.isDrawerOpen(GravityCompat.START)) {
                            drawer.closeDrawers();
                        }
//                        finish();
                        return;
                    }

                })
                .show();
    }


    long totalSize = 0;
    Bitmap bmpPhotoTaking = null;
    InputStream streamPhotoTaking = null;

    private class UploadFileToServer extends AsyncTask<Void, Integer, String> {

        HashMap<String, String> data = new HashMap<>();
        int typeUpload;

        public UploadFileToServer(HashMap<String, String> data, int type) {
            this.data = data;
            this.typeUpload = type;
        }

        @Override
        protected void onPreExecute() {
            // setting progress bar to zero
//            progressBar.setProgress(0);
            super.onPreExecute();
        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
//            Message msg = new Message();
//            Bundle bn = new Bundle();
//            bn.putInt("percentProcess", progress[0]);
//            msg.setData(bn);
//            handle.sendMessage(msg);
        }

        @Override
        protected String doInBackground(Void... params) {
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    String msg = "Đang upload hình ảnh check in....";
                    if (TYPE_CHECK_OUT == typeUpload) {
                        msg = "Đang upload hình ảnh check out....";
                    } else if (TYPE_CLOSE_STORE == typeUpload) {
                        msg = "Đang upload hình ảnh đóng cửa....";
                    }
                    progressDialog.setProgress(0);
                    progressDialog.setMax(100);
                    progressDialog.setMessage(msg);
                    progressDialog.setTitle("Thông báo");
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                    progressDialog.show();
                }
            });
            return uploadFile();
        }

        @SuppressWarnings("deprecation")
        private String uploadFile() {
            String responseString = null;

            HttpClient httpclient = new DefaultHttpClient();

            String url = ServerPath.API_URL + "?";
            InputStream in = null;
            ByteArrayOutputStream bos = null;
            try {
                AndroidMultiPartEntity entity = new AndroidMultiPartEntity(
                        new AndroidMultiPartEntity.ProgressListener() {
                            @Override
                            public void transferred(long num) {
                                publishProgress((int) ((num / (float) totalSize) * Constants.LOCATION_MULTI_DISTANCE));
                            }
                        });

                // Adding file data to http body
                if (streamPhotoTaking != null) {
                    bmpPhotoTaking = BitmapFactory.decodeStream(streamPhotoTaking);
                } else {
                    bmpPhotoTaking = BitmapFactory.decodeFile(sFilePath);
                }

                double lat = 0;
                double lng = 0;

                if(currentBestLocation != null) {
                    lat = currentBestLocation.getLatitude();
                    lng = currentBestLocation.getLongitude();
                }

                String time = Common.getTextFromTime(Calendar.getInstance(), Constants.FORMAT_DATE);
                String model_device = Common.getModelDevice();
                String text_info = "";
                if (!time.isEmpty() && !model_device.isEmpty()) {
                    text_info = time + ";" + model_device;
                }

                bmpPhotoTaking = FileUtil.buildImageWithText(MainActivity.this, bmpPhotoTaking, text_info, lat, lng);

                bos = new ByteArrayOutputStream();
                bmpPhotoTaking.compress(Bitmap.CompressFormat.JPEG, 70, bos);
                in = new ByteArrayInputStream(bos.toByteArray());
                entity.addPart("upfile", new InputStreamBody(in, "image/jpeg", "filename"));

                // Extra parameters if you want to pass to server
                if (data != null) {
                    boolean first = true;
                    Set<String> set = data.keySet();
                    for (String key : set) {
                        if (first) {
                            url = url + key + "=" + data.get(key);
                            first = false;
                        } else {
                            url = url + "&" + key + "=" + data.get(key);
                        }
                    }
                }


                totalSize = entity.getContentLength();
                HttpPost httppost = new HttpPost(url);
                httppost.setEntity(entity);

                // Making server call
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity r_entity = response.getEntity();

                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode == 200) {
                    // Server response
                    responseString = EntityUtils.toString(r_entity);
                } else {
                    responseString = "Error occurred! Http Status Code: "
                            + statusCode;
                }

            } catch (Exception e) {
                responseString = e.getMessage();
            } finally {
                try {
                    if (streamPhotoTaking != null) {
                        streamPhotoTaking.close();
                    }
                    if (in != null) {
                        in.close();
                    }
                    if (bos != null) {
                        bos.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return responseString;

        }

        @Override
        protected void onPostExecute(String result) {
            boolean isSuccess = false;
            try {
                if (result != null) {
                    JSONObject json = new JSONObject(result);
                    if (json != null) {
                        int status = (int) json.get("status");
                        if (status == 1) {
                            isSuccess = true;
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            // showing the server response in an alert dialog
            if (isSuccess) {
                StoreEntities store = info.getCurStore();
                if (TYPE_CHECK_IN == typeUpload) {
                    showDialogWarning(R.string.check_in_success);
                    store.setCheckin("successCheckin");
                } else if (TYPE_CHECK_OUT == typeUpload) {
                    showDialogWarning(R.string.check_out_success);
                    store.setCheckout("successCheckout");
                } else if (TYPE_CLOSE_STORE == typeUpload) {
                    showDialogWarning(R.string.close_store_success);
                }
                info.setCurStore(store);
            } else {
                if (TYPE_CHECK_IN == typeUpload) {
                    showDialogWarning(R.string.check_in_fail);
                } else if (TYPE_CHECK_OUT == typeUpload) {
                    showDialogWarning(R.string.check_out_fail);
                } else if (TYPE_CLOSE_STORE == typeUpload) {
                    showDialogWarning(R.string.close_store_fail);
                }
            }
            bmpPhotoTaking = null;
            progressDialog.dismiss();
            super.onPostExecute(result);
        }

    }

    Handler handle = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bn = msg.getData();
            if (bn != null) {
                int currentPercent = bn.getInt("percentProcess");
                progressDialog.setProgress(currentPercent);
                if (progressDialog.getProgress() == progressDialog
                        .getMax()) {
                    progressDialog.dismiss();
                }
            }
        }
    };

    private void pushCheckLocation(int type) {
        updateLocation(true);
        currentBestLocation = getLastBestLocation();
        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fupdatelocationstore");
        data.put("storeid", StringUtil.toString(info.getCurStore().getId_store()));
        data.put("lat", StringUtil.toString(currentBestLocation.getLatitude()));
        data.put("lng", StringUtil.toString(currentBestLocation.getLongitude()));

        final String data_report = data.toString();

        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
//            APIUtils.doPostData(MainActivity.this, data, cell_shape, ServerPath.API_URL, new HandlerLocation(navHeader));
            APIUtils.doPostData(MainActivity.this, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "Đang chấm vị trí store, vui lòng chờ...", "Thông báo");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        boolean isSuccess = false;
                        try {
                            if (successString != null) {
                                JSONObject json = new JSONObject(successString);
                                if (json != null) {
                                    int status = (int) json.get("status");
                                    if (status == 1) {
                                        isSuccess = true;
                                    }
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Intent i = new Intent();
                        i.setAction(Constants.BROADCAST_ALERT);
                        if (isSuccess) {
                            i.putExtra(Constants.KEY_ALERT_VALUE, R.string.check_location_success);
                            info.getCurStore().setLat(currentBestLocation.getLatitude());
                            info.getCurStore().setLng(currentBestLocation.getLongitude());
//                            if (ACTION_CHECKIN == type) {
//                                doCheckin();
//                            } else if (ACTION_CHECKOUT == type) {
//                                doCheckout();
//                            } else if (ACTION_CLOSE_STORE== type) {
//                                doCloseStore();
//                            }

                        } else {
                            i.putExtra(Constants.KEY_ALERT_VALUE, R.string.check_location_fail);
                        }
                        sendBroadcast(i);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("MainActivity: pushCheckLocation; Result: " + failString + "; Data: " + data_report, MainActivity.this);
                }

                @Override
                public void uiEnd() {
                    Common.FinishProgressDialog(progressDialog);
                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


}
