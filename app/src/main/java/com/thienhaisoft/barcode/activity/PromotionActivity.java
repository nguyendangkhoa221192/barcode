package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListPromotion;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.items.PromotionItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

public class PromotionActivity extends Activity implements View.OnClickListener{

    private String store_id = "";
    private String user_id = "";
    private GlobalInfo globalInfo = null;
    private boolean bl_config_success = false;
    private ArrayAdapterListPromotion arrayAdapterListPromotion;
    private ListView lv_promotion;
    private Button btn_accept;
    private Button btn_reset;
    private ArrayList<PromotionItem> list_promotion;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promotion);
        globalInfo = (GlobalInfo) getApplicationContext();
        user_id = StringUtil.toString(globalInfo.getStaffId());
        StoreEntities storeInfo = globalInfo.getCurStore();
        store_id = StringUtil.toString(storeInfo.getStore_id());
        lv_promotion = (ListView) findViewById(R.id.changegift_lv_history_change_gift);
        btn_accept = (Button) findViewById(R.id.changegift_btn_save);
        btn_reset = (Button) findViewById(R.id.changegift_btn_image);
        btn_accept.setOnClickListener(this);
        btn_reset.setOnClickListener(this);

        TextView txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.promotion_title);
        ImageButton btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        list_promotion = (ArrayList<PromotionItem>) getIntent().getSerializableExtra("promotion_info");
        Collections.sort(list_promotion, new Comparator<PromotionItem>() {
            @Override
            public int compare(PromotionItem t1, PromotionItem t2) {
                if (t1.getProduct_id().compareToIgnoreCase(t2.getProduct_id()) != 0) {
                    return 1;
                } else {
                    if (t1.getPromotion_canget() < t2.getPromotion_canget()) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            }
        });
        if (list_promotion.size() > 0) {
            setAdapterPromotion(list_promotion);
        } else {
            PromotionActivity.this.finish();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_back:
                PromotionActivity.this.finish();
                break;
            case R.id.changegift_btn_image:
                resetAction();
                break;
            case R.id.changegift_btn_save:
                handlePromotion(list_promotion);
            default:
                break;
        }
    }

    private void resetAction() {
        for (int i = 0; i < list_promotion.size(); i++) {
            list_promotion.get(i).setNumber_choose(0);
        }
        arrayAdapterListPromotion.swapItem(list_promotion);
}

    private void setAdapterPromotion(ArrayList<PromotionItem> listPromotion) {
        ArrayList<String> list_temp_product_id = new ArrayList<>();

        int total_number_buy = 0;
        for (int i = 0; i < listPromotion.size(); i++) {
            PromotionItem product_item = listPromotion.get(i);
            if (!list_temp_product_id.contains(product_item.getProduct_id())) {
                list_temp_product_id.add(product_item.getProduct_id());
                total_number_buy = total_number_buy + product_item.getSoluongdamua();
            }
        }
        if (total_number_buy > 0) {
            arrayAdapterListPromotion = new ArrayAdapterListPromotion(this, listPromotion, total_number_buy);
            lv_promotion.setAdapter(arrayAdapterListPromotion);
        } else {
            Common.showAlertDialog(this, "Không lấy được thông tin khuyến mãi.", "Thất bại", "", "", "Ok", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {

                }

                @Override
                public void functionPositive() {
                    PromotionActivity.this.finish();
                }
            });
        }
    }

    private void handlePromotion(final ArrayList<PromotionItem> data_promotion) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_UPDATE_STORE;
        String order_id = data_promotion.get(0).getId_order();
        String str_data = "";
        if (createDataForActionConfig(data_promotion, order_id) != null) {
            str_data = createDataForActionConfig(data_promotion, order_id);
            data.put("data", str_data);
            final String data_report = data.toString();
            APIUtils.LoadJSON(this, data, url, new APICallBack() {
                @Override
                public void uiStart() {

                }

                @Override
                public void success(String successString, int type) {
                    try {
                        JSONObject result = new JSONObject(successString);
                        if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                            bl_config_success = true;
                        } else {
                            bl_config_success = false;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Khuyến Mãi: Xữ lý khuyến mãi; Result: " + failString + "; Data: " + data_report, PromotionActivity.this);
                    bl_config_success = false;
                }

                @Override
                public void uiEnd() {
                    if (bl_config_success) {
                        Common.showAlertDialog(PromotionActivity.this, "Đỗi khuyến mãi thành công.", "Thành công", "", "", "OK", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                PromotionActivity.this.finish();
                            }
                        });
                    } else {
                        Common.showAlertDialog(PromotionActivity.this, "Đỗi khuyến mãi thất bại.", "Lỗi", "", "Thử lại", "Kết thúc", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {
                                handlePromotion(data_promotion);
                            }

                            @Override
                            public void functionPositive() {
                                PromotionActivity.this.finish();
                            }
                        });
                    }
                }
            });
        } else {
            Common.showAlertDialog(PromotionActivity.this, "Đỗi khuyến mãi thất bại.", "Lỗi", "", "Thử lại", "Kết thúc", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {
                    handlePromotion(data_promotion);
                }

                @Override
                public void functionPositive() {
                    PromotionActivity.this.finish();
                }
            });
        }
    }

    private String createDataForActionConfig(ArrayList<PromotionItem> data_promotion, String idOrder) {
        JSONObject objectData = new JSONObject();
        int checkNumber = 0;
        try {
            Calendar cal_current = Calendar.getInstance();
            Date date = cal_current.getTime();

            objectData.put("create_date", (date.getTime() / 1000) + "");
            objectData.put("update_date", (cal_current.getTimeInMillis() / 1000) + "");

            JSONArray jsonArrayCartList = new JSONArray();
            JSONArray array_promotion = new JSONArray();
            for (int i = 0; i < data_promotion.size(); i++) {
                PromotionItem productItem = data_promotion.get(i);
                if (productItem.getNumber_choose() != 0) {
                    checkNumber++;
                    JSONObject objectItemCartList = new JSONObject();
                    JSONObject objectItemPromotion = new JSONObject();
                    objectItemCartList.put("store_id", store_id + "");
                    objectItemCartList.put("product_id", productItem.getFree_product_id());
                    objectItemCartList.put("quantity", "-" + productItem.getNumber_choose());
                    objectItemCartList.put("update_user", user_id);
                    objectItemCartList.put("create_date", (date.getTime() / 1000) + "");
                    objectItemCartList.put("update_date", (cal_current.getTimeInMillis() / 1000) + "");
                    objectItemCartList.put("tran_type", "5");
                    objectItemCartList.put("create_user", "1");

                    objectItemPromotion.put("promotion_store_id", store_id + "");
                    objectItemPromotion.put("promotion_free_product_id", productItem.getFree_product_id());
                    objectItemPromotion.put("promotion_free_product_name", productItem.getFree_product_name());
                    objectItemPromotion.put("promotion_quantity", "-" + productItem.getNumber_choose());
                    objectItemPromotion.put("promotion_update_user", user_id);

                    array_promotion.put(objectItemPromotion);
                    jsonArrayCartList.put(objectItemCartList);
                }
            }
            objectData.put("products_list", jsonArrayCartList);
            objectData.put("id_order", idOrder);
            objectData.put("array", array_promotion);
            StoreEntities current_store = globalInfo.getCurStore();
            objectData.put("lock_order_date", current_store.getLock_order_date());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (checkNumber > 0)
            return objectData.toString();
        else
            return null;
    }
}
