package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterReportStore;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.InforCustomerByUser;
import com.thienhaisoft.barcode.items.ReportStoreItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportStoreActivity extends Activity {

    private ImageButton btn_back;
    private TextView txt_header;
    private GlobalInfo globalInfo;
    private String store_id = "";
    private ListView lv_report;
    private ProgressDialog progressDialog;
    private ArrayList<ReportStoreItem> storeItemArrayList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_store);
        init();
    }

    private void init() {
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReportStoreActivity.this.finish();
            }
        });
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText("Báo cáo tồn kho");
        globalInfo = (GlobalInfo) getApplicationContext();
        store_id = StringUtil.toString(globalInfo.getCurStore().getId_store());
        progressDialog = new ProgressDialog(ReportStoreActivity.this);
        storeItemArrayList = new ArrayList<>();
        lv_report = (ListView) findViewById(R.id.lv_report_store);
        loadReportStore(store_id);
    }

    private void loadReportStore(String store_id) {
        HashMap<String, String> data = new HashMap<>();
        data.put("store_id", store_id);
        String url = Constants.API_REPORT_STORE;
        final String data_report = data.toString();
        APIUtils.LoadJSON(ReportStoreActivity.this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
                storeItemArrayList.clear();
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject item = data.getJSONObject(i);
                                ReportStoreItem report_item = new ReportStoreItem();
                                report_item.partJSON(item);
                                storeItemArrayList.add(report_item);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Báo Cáo Tồn Kho: Load report; Result: " + failString + "; Data: " + data_report, ReportStoreActivity.this);
            }

            @Override
            public void uiEnd() {
                if (storeItemArrayList.size() > 0) {
                    setDataForListView(storeItemArrayList);
                    Common.FinishProgressDialog(progressDialog);
                } else {
                    Common.showAlertDialog(ReportStoreActivity.this, "Không có dữ liệu.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            ReportStoreActivity.this.finish();
                        }
                    });
                    Common.FinishProgressDialog(progressDialog);
                }
            }
        });
    }

    private void setDataForListView(ArrayList<ReportStoreItem> list) {
        ArrayAdapterReportStore arrayAdapterReportStore = new ArrayAdapterReportStore(this, list);
        lv_report.setAdapter(arrayAdapterReportStore);
    }
}
