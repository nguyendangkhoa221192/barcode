package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterReportStoreGift;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ReportStoreItem;
import com.thienhaisoft.barcode.items.StoreGiftItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ReportStoreGiftActivity extends Activity {

    private ImageButton btn_back;
    private TextView txt_header;
    private GlobalInfo globalInfo;
    private String user_id = "";
    private ListView lv_report;
    private ProgressDialog progressDialog;
    private ArrayList<StoreGiftItem> storeGiftList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_store_gift);
        init();
    }

    private void init() {
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReportStoreGiftActivity.this.finish();
            }
        });
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText("Báo cáo tồn kho quà");
        globalInfo = (GlobalInfo) getApplicationContext();
        user_id = StringUtil.toString(globalInfo.getStaffId());
        progressDialog = new ProgressDialog(ReportStoreGiftActivity.this);
        storeGiftList = new ArrayList<>();
        lv_report = (ListView) findViewById(R.id.lv_report_store_gift);
        loadReportStoreGift();
    }

    private void loadReportStoreGift() {
        HashMap<String, String> data = new HashMap<>();
        String url = "";
        url = Constants.API_REPORT_STORE_GIFT;
        data.put("userid", user_id);
        final String data_report = data.toString();
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject item = data.getJSONObject(i);
                                StoreGiftItem report_item = new StoreGiftItem();
                                report_item.partJSON(item);
                                storeGiftList.add(report_item);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Báo cáo tồn kho quà: Load báo cáo; Result: " + failString + "; Data: " + data_report, ReportStoreGiftActivity.this);
            }

            @Override
            public void uiEnd() {
                if (storeGiftList.size() > 0) {
                    setDataForListView();
                    Common.FinishProgressDialog(progressDialog);
                } else {
                    Common.showAlertDialog(ReportStoreGiftActivity.this, "Không có dữ liệu.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            ReportStoreGiftActivity.this.finish();
                        }
                    });
                    Common.FinishProgressDialog(progressDialog);
                }
            }
        });
    }

    private void setDataForListView() {
        ArrayAdapterReportStoreGift arrayAdapterReportStoreGift = new ArrayAdapterReportStoreGift(this, storeGiftList);
        lv_report.setAdapter(arrayAdapterReportStoreGift);
    }
}
