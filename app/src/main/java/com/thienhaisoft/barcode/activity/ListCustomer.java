package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListCustomer;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.InforCustomerByUser;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListCustomer extends Activity {

    private ProgressDialog progressDialog;
    private ArrayList<InforCustomerByUser> customerByUserArrayList = new ArrayList<>();
    private ImageButton btn_back;
    private GlobalInfo globalInfo;
    private ListView lv_list_customer;
    private EditText edt_search;
    private TextView txt_header;
    private String user_id;
    private String store_id = "";
    private ArrayAdapterListCustomer adapterListCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_customer);
        HidenKeyboard.setupUI(findViewById(android.R.id.content), ListCustomer.this);
        init();
    }

    private void init() {
        progressDialog = new ProgressDialog(this);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.list_customer);
        globalInfo = (GlobalInfo) getApplicationContext();
        user_id = StringUtil.toString(globalInfo.getStaffId());
        store_id = StringUtil.toString(globalInfo.getCurStore().getId_store());
        lv_list_customer = (ListView) findViewById(R.id.listcustomer_lv_show_customer);
        edt_search = (EditText) findViewById(R.id.listcustomer_edt_search);
        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                adapterListCustomer.getFilter().filter(s);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ListCustomer.this.finish();
            }
        });

        loadCustomerByUser(user_id);
    }

    private void loadCustomerByUser (String user_id) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_INFO_CUSTOMER_BY_USER + user_id + "&store_id=" + store_id;
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        customerByUserArrayList.clear();
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                String item = data.getString(i);
                                InforCustomerByUser user = Common.getGson().fromJson(item, InforCustomerByUser.class);
                                customerByUserArrayList.add(user);
                            }
                        }
                    } else {
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Result: " + failString + "; Data: " + "store_id=" + store_id, ListCustomer.this);
            }

            @Override
            public void uiEnd() {
                if (customerByUserArrayList.size() > 0) {
                    setValuesIntoListView();
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void setValuesIntoListView() {
        adapterListCustomer = new ArrayAdapterListCustomer(this, customerByUserArrayList);
        lv_list_customer.setAdapter(adapterListCustomer);
    }
}
