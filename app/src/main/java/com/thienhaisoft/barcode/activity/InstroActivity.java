package com.thienhaisoft.barcode.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;

public class InstroActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener {

    public static final String YOUTUBE_API_KEY = "AIzaSyBoJjgHUevhA5ksQcVKAoLFnHBh_-VPh7Q";
    private TextView txt_header;
    private ImageButton btn_back;
    private static final int RECOVERY_REQUEST = 1;
    private YouTubePlayerView youTubeView;
    private ProgressDialog dialog;
    private String str_id_video = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instro);
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.instro_title);

        btn_back = (ImageButton) findViewById(R.id.btn_back);

        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                InstroActivity.this.finish();
            }
        });
        dialog = new ProgressDialog(this);

        youTubeView = (YouTubePlayerView) findViewById(R.id.youtube_view);
        loadVersion();
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b) {
            youTubePlayer.cueVideo(str_id_video);
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        if (youTubeInitializationResult.isUserRecoverableError()) {
            youTubeInitializationResult.getErrorDialog(this, RECOVERY_REQUEST).show();
        } else {
            String error = String.format(getString(R.string.player_error), youTubeInitializationResult.toString());
            Toast.makeText(this, error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RECOVERY_REQUEST) {
            // Retry initialization if user performed a recovery action
            getYouTubePlayerProvider().initialize(YOUTUBE_API_KEY, this);
        }
    }

    protected YouTubePlayer.Provider getYouTubePlayerProvider() {
        return youTubeView;
    }

    private void loadVersion() {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_VIEW_VERSION;
        APIUtils.LoadJSONGet(this, data, null, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(dialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            JSONObject item = data.getJSONObject(0);
                            if (item.has("url_guide")) {
                                String url_item = item.getString("url_guide");
                                String[] array_split = url_item.split("/");

                                str_id_video = array_split[array_split.length - 1];
//                                str_id_video = "jnhrfO4RGiQ";
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {

            }

            @Override
            public void uiEnd() {
                Common.FinishProgressDialog(dialog);
                youTubeView.initialize(YOUTUBE_API_KEY, InstroActivity.this);
            }
        });
    }
}
