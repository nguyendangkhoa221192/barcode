package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListProduct;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.items.OrderClass;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.items.PromotionItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;

public class EarnPointActivity extends Activity implements View.OnClickListener {

    //    public static ArrayList<OrderClass> list_sell_out = new ArrayList<>();
    private String[] str_purpose = {"Mua dùng", "Làm quà", "Bán lại"};
    private Spinner sp_purpose = null;
    private Button btn_save = null;
    private Button btn_reset = null;
    private Button btn_image = null;
    private ListView lv_sell_out = null;
    private ImageButton btn_back = null;
    private ProgressDialog progressDialog = null;
    private ArrayList<ProductItem> listItemProduct = new ArrayList<>();
    private InformationCustomer informationCustomer = new InformationCustomer();
    private int id_order = 0;
    private String user_id = "";
    private String store_id = "";
    private TextView txt_customer_name = null;
    private TextView txt_customer_phone = null;
    private TextView txt_customer_total_point = null;
    private TextView txt_customer_point_order = null;
    private TextView txt_header = null;
    private EditText edt_search_product = null;
    private GlobalInfo globalInfo = null;
    private JSONObject resultDataOrder = null;
    private JSONArray resultDataPromotion = null;
    private ArrayAdapterListProduct adt_spinner_product = null;
    private boolean bl_config_success = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sell_out);
        // Set adapter for product list

        HidenKeyboard.setupUI(findViewById(android.R.id.content), EarnPointActivity.this);
        ArrayAdapter<String> adt_spinner_purpose = new ArrayAdapter<String>(this, R.layout.layout_sp_item, str_purpose);
        adt_spinner_purpose.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

//        sp_product = (Spinner) findViewById(R.id.sellout_sp_choose_product);
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.changegift_earnpoint);
        sp_purpose = (Spinner) findViewById(R.id.sellout_sp_choose_purpose);
        sp_purpose.setAdapter(adt_spinner_purpose);

        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_save = (Button) findViewById(R.id.sellout_btn_save);
        btn_reset = (Button) findViewById(R.id.sellout_btn_reset);
        txt_customer_name = (TextView) findViewById(R.id.sellout_txt_name_customer);
        txt_customer_phone = (TextView) findViewById(R.id.sellout_txt_phone_number);
        txt_customer_total_point = (TextView) findViewById(R.id.sellout_txt_total_point);
        txt_customer_point_order = (TextView) findViewById(R.id.sellout_txt_point_in_order);
        txt_customer_point_order.setText("0.0");
        edt_search_product = (EditText) findViewById(R.id.earnpoit_edt_search);

        globalInfo = (GlobalInfo) getApplicationContext();
        user_id = StringUtil.toString(globalInfo.getStaffId());
        StoreEntities storeInfo = globalInfo.getCurStore();
        store_id = StringUtil.toString(storeInfo.getStore_id());
        progressDialog = new ProgressDialog(this);
        btn_back.setOnClickListener(this);
        btn_save.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        listItemProduct.clear();

        // Load adapter list view
        lv_sell_out = (ListView) findViewById(R.id.sellout_lv_show_sell_out);
        informationCustomer = (InformationCustomer) getIntent().getSerializableExtra(Constants.RESULT_INFO_CUSTOMER_INFO);
        if (informationCustomer != null && informationCustomer.getI_Pid() > 0) {
            loadProductList();
            txt_customer_name.setText(informationCustomer.getStr_HoLot() + " " + informationCustomer.getStr_Ten());
            txt_customer_phone.setText(informationCustomer.getStr_DTDD());
            txt_customer_total_point.setText(informationCustomer.getDou_pointsproduct() + "");
            edt_search_product.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adt_spinner_product.getFilter().filter(s);
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            // <1>
//        loadListOrderFromServer(user_id, 0, 0);
        } else {
            Common.showAlertDialog(this, "Không lấy được thông tin khách hàng. Vui lòng thử lại!", "Lỗi", "", "", "OK", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {

                }

                @Override
                public void functionPositive() {
                    EarnPointActivity.this.finish();
                }
            });
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.sellout_btn_save:
                __save_info_sell_out();
                break;
            case R.id.sellout_btn_reset:
                resetAction();
                break;
            case R.id.btn_back:
                this.finish();
                break;
            default:
                break;
        }
    }

    private void resetAction() {
        for (int i = 0; i < listItemProduct.size(); i++) {
            listItemProduct.get(i).setNumber_choose(0);
        }
        adt_spinner_product.swapItem(listItemProduct);
        txt_customer_point_order.setText("0.0");
        txt_customer_total_point.setText(informationCustomer.getDou_pointsproduct() + "");
    }

    private void loadProductList() {
        String url = Constants.API_LIST_PRODUCT;
        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fviewproductslist");
        data.put(Constants.PAR_LIST_PRODUCT, store_id);
        final String data_report = data.toString();
        APIUtils.LoadJSONGet(this, data, null, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            listItemProduct.clear();
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject itemData = data.getJSONObject(i);
                                ProductItem productItem = new ProductItem();
                                productItem.partJSON(itemData);
                                listItemProduct.add(productItem);
                            }
                        }
                    } else {
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Bán hàng: Load list SP; Result: " + failString + "; Data: " + data_report, EarnPointActivity.this);
            }

            @Override
            public void uiEnd() {
                if (listItemProduct.size() <= 0) {
                    Common.showAlertDialog(EarnPointActivity.this, "Không lấy được thông tin sản phẩm. Vui lòng thử lại.", "Lỗi", "", "", "Ok", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            EarnPointActivity.this.finish();
                        }
                    });
                } else {
                    setValuesListProduct(listItemProduct);
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void setValuesListProduct(ArrayList<ProductItem> listItemProduct) {
        adt_spinner_product = new ArrayAdapterListProduct(this, listItemProduct, informationCustomer.getDou_pointsproduct());
        lv_sell_out.setAdapter(adt_spinner_product);
    }

    private void __save_info_sell_out() {
        callAPICreateOrder();
    }

    private void callAPICreateOrder() {
        HashMap<String, String> header = new HashMap<>();
        String url = Constants.API_CREATE_ORDER;
        int i_Pid = informationCustomer.getI_Pid();
        id_order = 0;
        if (i_Pid > 0) {
            final JSONObject jsonObjectData = getDataForCreateOrder(i_Pid, listItemProduct);
            final String data_report = jsonObjectData.toString();
            APIUtils.LoadJSONGet(this, jsonObjectData, header, url, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "", "");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        JSONObject jsonObject = new JSONObject(successString);
                        if (jsonObject.has(Constants.RESULTAPI_STATUS) && jsonObject.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                            resultDataOrder = jsonObject.getJSONObject(Constants.RESULTAPI_DATA);
                            if (resultDataOrder.has(Constants.RESULT_CREATE_ORDER_ID_ORDER)) {
                                id_order = resultDataOrder.getInt(Constants.RESULT_CREATE_ORDER_ID_ORDER);
                            }
                            if (jsonObject.has(Constants.RESULT_CREATE_ORDER_LIST_PROMOTION)) {
                                resultDataPromotion = jsonObject.getJSONArray(Constants.RESULT_CREATE_ORDER_LIST_PROMOTION);
                            }
                        } else {
                            id_order = 0;
                        }
                    } catch (Exception ex) {

                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Bán hàng: Tạo đơn hàng; Result: " + failString + "; Data: " + data_report, EarnPointActivity.this);
                }

                @Override
                public void uiEnd() {
                    Common.FinishProgressDialog(progressDialog);
                    if (id_order > 0) {
                        // <1>
//                        loadListOrderFromServer(user_id, 0, 0);
                        // handle promotion product after
                        Common.showAlertDialog(EarnPointActivity.this, "Tạo đơn hàng thành công. Mã đơn hàng là: " + id_order, "Thành công", "", "", "Kết thúc", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                handlePromotionAfterSuccessOrder(resultDataPromotion, id_order);
                            }
                        });
                    } else {
                        Common.showAlertDialog(EarnPointActivity.this, "Tạo đơn hàng không thành công.", "Lỗi", "", "Thử lại", "Kết thúc", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                EarnPointActivity.this.finish();
                            }
                        });
                    }
                }
            });
        } else {
            EarnPointActivity.this.finish();
        }
    }

    private void handlePromotionAfterSuccessOrder(JSONArray resultPromotion, final int id) {
        try {
            if (resultPromotion != null && resultPromotion.length() > 0) {
                ArrayList<PromotionItem> result_promotion = new ArrayList<>();
                for (int j = 0; j < resultPromotion.length(); j++) {
                    JSONObject promotionInfo = resultPromotion.getJSONObject(j);
                    PromotionItem item = new PromotionItem();
                    item.partJSON(promotionInfo);
                    DateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    long from_date = ((Date) formatDate.parse(item.getFrom_date())).getTime();
                    long to_date = ((Date) formatDate.parse(item.getTo_date())).getTime();
                    long current_time = Calendar.getInstance().getTime().getTime();
                    if(from_date < current_time && to_date > current_time) {
                        result_promotion.add(item);
                    }
                }
                if (result_promotion.size() > 0) {
                    Collections.sort(result_promotion, new Comparator<PromotionItem>() {
                        @Override
                        public int compare(PromotionItem t1, PromotionItem t2) {
                            if (Integer.parseInt(t1.getFree_product_id()) >= Integer.parseInt(t2.getFree_product_id())) {
                                if (Integer.parseInt(t1.getFree_product_id()) >= Integer.parseInt(t1.getFree_product_id())) {
                                    return -1;
                                } else {
                                    return 1;
                                }
                            } else {
                                return 1;
                            }
                        }
                    });
                    ArrayList<PromotionItem> temp = new ArrayList<>();
                    String str_temp = result_promotion.get(0).getFree_product_id();
                    temp.add(result_promotion.get(0));

                    for (int k = 1; k < result_promotion.size(); k++) {
                        if (!str_temp.equals(result_promotion.get(k).getFree_product_id())) {
                            temp.add(result_promotion.get(k));
                            str_temp = result_promotion.get(k).getFree_product_id();
                        }
                    }
                    final ArrayList<PromotionItem> result_fro_config_store = result_promotion;
                    Common.showAlertDialog(this, "Có thông tin khuyến mãi.", "Khuyến mãi", "", "Không nhận", "Nhận", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {
                            EarnPointActivity.this.finish();
                        }

                        @Override
                        public void functionPositive() {
                            handlePromotion(result_fro_config_store);
                        }
                    });
                } else {
                    EarnPointActivity.this.finish();
                }
            } else {
                EarnPointActivity.this.finish();
            }
        } catch (Exception ex) {
            EarnPointActivity.this.finish();
        }
    }

    private void handlePromotion(final ArrayList<PromotionItem> data_promotion) {
        Intent intent = new Intent(this, PromotionActivity.class);
        intent.putExtra("promotion_info", data_promotion);
        startActivity(intent);
    }

    private JSONObject getDataForCreateOrder(int customerID, ArrayList<ProductItem> listproductItem) {
        JSONObject objectData = new JSONObject();
        try {
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_ID_ORDER_STATUS, 1 + "");
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_UPDATE_USER, user_id);
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_STORE_ID, store_id);
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_ID_STORE, store_id);
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_ID_CUSTOMER, customerID + "");
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_ID_ORDER_TYPE, 1 + "");
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID_USER, user_id);
            objectData.put("shift", globalInfo.getCurStore().getShift());


            JSONArray jsonArrayCartList = new JSONArray();
            Double total_price = 0.0;
            for (int i = 0; i < listproductItem.size(); i++) {
                ProductItem productItem = listproductItem.get(i);
                if (productItem.getNumber_choose() > 0) {
                    JSONObject objectItemCartList = new JSONObject();
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID_PRODUCT, productItem.getId_product());
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID_PRODUCT_FEATURE, "");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID, "p1");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID_CUSTOMIZATION_GROUP, "");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_ID_CUSTOMIZATION, "");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_CUSTOMIZATION_NAME, "");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_NAME, productItem.getName() + "");
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_QUANTITY, productItem.getNumber_choose());
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_PRICE_TAX, productItem.getBase_price());
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_PRICE, productItem.getBase_price());
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_TAX, 0);
                    objectItemCartList.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST_PRODUCT_NOTE, "");
                    total_price = total_price + productItem.getNumber_choose() * productItem.getBase_price();
                    jsonArrayCartList.put(objectItemCartList);
                }
            }
            String str_total_price = total_price.toString();
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_TOTAL_PRICE, str_total_price);
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_TOTAL_PRICE_TAX, "123");
            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_GRAND_TOTAL, str_total_price);

            objectData.put(Constants.PAR_EARNPOINT_CREATEODER_CARTLIST + customerID, jsonArrayCartList);
            objectData.put("lock_order_date", globalInfo.getCurStore().getLock_order_date());

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return objectData;
    }

    @Override
    protected void onResume() {
//        loadProductList();
        super.onResume();
    }
}
