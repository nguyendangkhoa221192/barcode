package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListSamling;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ChangedInMonth;
import com.thienhaisoft.barcode.items.InforCustomerByUser;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.items.SamplingItem;
import com.thienhaisoft.barcode.items.TargetMonthly;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.SingleListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class SamplingActivity extends Activity implements View.OnClickListener {

    private TextView txt_header = null;
    private GlobalInfo globalInfo = null;
    private ImageButton btn_back = null;
    private String user_id, store_id;
    private ProgressDialog dialog;
    private ArrayList<SamplingItem> listSampling;
    private ListView lv_sampling;
    private TextView txt_name_customer = null;
    private TextView txt_phone = null;
    private int i_Pid = 0;
    private boolean isSuccess = false;
    private Button btn_save;
    private int limitsampling = 0;
    private int soluong_da_doi_sampling = 0;
    private ChangedInMonth changedInMonth;
    private TargetMonthly targetMonthly;


    private ArrayAdapterListSamling samlingLV;

    private InformationCustomer informationCustomer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_samling);
        HidenKeyboard.setupUI(findViewById(android.R.id.content), SamplingActivity.this);
        init();
    }

    private void init() {
        // Header title
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.title_samling);

        // Header button back
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(this);

        // Global info
        globalInfo = (GlobalInfo) getApplicationContext();
        user_id = StringUtil.toString(globalInfo.getStaffId());
        store_id = StringUtil.toString(globalInfo.getCurStore().getStore_id());

        // Customer name
        txt_name_customer = (TextView) findViewById(R.id.samling_txt_name);

        // Customer phone
        txt_phone = (TextView) findViewById(R.id.samling_txt_phone_number);

        // Project dialog
        dialog = new ProgressDialog(this);

        // List item samling
        listSampling = new ArrayList<>();

        // List view samling
        lv_sampling = (ListView) findViewById(R.id.samling_lv);

        // Button submit
        btn_save = (Button) findViewById(R.id.samling_btn_save);
        btn_save.setOnClickListener(this);

        // Get info customer from intent
        informationCustomer = (InformationCustomer) getIntent().getSerializableExtra(Constants.RESULT_INFO_CUSTOMER_INFO);
        if (informationCustomer != null && informationCustomer.getI_Pid() > 0) {
            i_Pid = informationCustomer.getI_Pid();
            txt_name_customer.setText(informationCustomer.getStr_HoLot() + informationCustomer.getStr_Ten());
            txt_phone.setText(informationCustomer.getStr_DTDD());
        } else {
            txt_name_customer.setText("Khách hàng không xác định.");
            txt_phone.setVisibility(View.INVISIBLE);
        }
        handleApiGetList();
    }

    private void handleApiGetList() {
        HashMap<String, String> data = new HashMap<>();
        data.put("p1", user_id);
        data.put("p2", String.valueOf(globalInfo.getCurStore().getStore_id()));
        data.put("p3", globalInfo.getCurStore().getShift());
        data.put("p4", globalInfo.getCurStore().getCreated_begin_by_int());
        String url = Constants.API_SAMLING;
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(dialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (Common.isSuccessAPI(result)) {
                        if (result.has("limitsampling")) {
                            limitsampling = result.getInt("limitsampling");
                        }
                        if (result.has("soluong_da_doi_sampling")) {
                            soluong_da_doi_sampling = result.getInt("soluong_da_doi_sampling");
                        }
                        if (result.has("chitieutrongthang")) {
                            Gson gson = new Gson();
                            targetMonthly = gson.fromJson(result.getString("chitieutrongthang"), TargetMonthly.class);
                        }
                        if (result.has("dadoitrongthang")) {
                            Gson gson = new Gson();
                            changedInMonth = gson.fromJson(result.getString("dadoitrongthang"), ChangedInMonth.class);
                        }
                        JSONArray arrayItem = result.getJSONArray(Constants.RESULTAPI_DATA);
                        Gson gson = new Gson();
                        for (int i = 0; i < arrayItem.length(); i++) {
                            String object = arrayItem.getString(i);
                            SamplingItem item = gson.fromJson(object, SamplingItem.class);
                            listSampling.add(item);
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {

            }

            @Override
            public void uiEnd() {
                if (listSampling.size() > 0) {
                    ((TextView) findViewById(R.id.samling_ct_ytv)).setText(targetMonthly.getYtv());
                    ((TextView) findViewById(R.id.samling_ct_knp)).setText(targetMonthly.getKnp());
                    ((TextView) findViewById(R.id.samling_ct_kns)).setText(targetMonthly.getKns());
                    ((TextView) findViewById(R.id.samling_ct_dln)).setText(targetMonthly.getDln());
                    ((TextView) findViewById(R.id.samling_ct_jn)).setText(targetMonthly.getJn());

                    ((TextView) findViewById(R.id.samling_dd_ytv)).setText(changedInMonth.getYtvLotang());
                    ((TextView) findViewById(R.id.samling_dd_knp)).setText(changedInMonth.getKnpLotang());
                    ((TextView) findViewById(R.id.samling_dd_kns)).setText(changedInMonth.getKnsLotang());
                    ((TextView) findViewById(R.id.samling_dd_dln)).setText(changedInMonth.getDlnLotang());
                    ((TextView) findViewById(R.id.samling_dd_jn)).setText(changedInMonth.getJnLotang());
                    samlingLV = new ArrayAdapterListSamling(SamplingActivity.this, listSampling);
                    lv_sampling.setAdapter(samlingLV);
                }
                Common.FinishProgressDialog(dialog);
            }
        });
    }

    private void loadCustomerByUser(String user_id) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_INFO_CUSTOMER_BY_USER + user_id + "&store_id=" + store_id;
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {

                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                String item = data.getString(i);
                                InforCustomerByUser user = Common.getGson().fromJson(item, InforCustomerByUser.class);
                                if (user.getCustomerId().equals(String.valueOf(i_Pid))) {
//                                    customerByUser = user;
                                    break;
                                }
                            }
                        }
                    } else {
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
            }

            @Override
            public void uiEnd() {
//                if (customerByUser.getCustomerId() != null && !customerByUser.getCustomerId().isEmpty()) {
//                    samlingLV = new AdapterSamlingLV(SamplingActivity.this, listSampling);
//                    lv_sampling.setAdapter(samlingLV);
//                    Common.FinishProgressDialog(dialog);
//                } else {
//                    Common.showAlertDialog(SamplingActivity.this, "", "Khách hàng không thuộc nhân viên này. Vui lòng thử lại.", "OK", new SingleListener() {
//                        @Override
//                        public void myFunction() {
//                            SamplingActivity.this.finish();
//                        }
//                    });
//                    Common.FinishProgressDialog(dialog);
//                }
            }
        });
    }

    private void handleActionSampling() {
        String date_create = createDataForApi();
        if (date_create == null || date_create.equals("")) {
            Common.showAlertDialog(SamplingActivity.this, "Lỗi", "Quá số lượng đỗi samling trong ca.\nGiới hạn còn lại là: " + limitsampling + "\nSố lượng đã đỗi là: " + soluong_da_doi_sampling, "OK", new SingleListener() {
                @Override
                public void myFunction() {
                }
            });
        } else {
            HashMap<String, String> data = new HashMap<>();
            data.put(Constants.RESULTAPI_DATA, date_create);
            String url = Constants.API_SAMLING_ACTION;
            APIUtils.LoadJSON(this, data, url, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(dialog, "Đang xữ lý...", "");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        if (Common.isSuccessAPI(new JSONObject(successString))) {
                            isSuccess = true;
                        }
                    } catch (Exception ex) {
                        isSuccess = false;
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    isSuccess = false;
                }

                @Override
                public void uiEnd() {
                    if (isSuccess) {
                        Common.showAlertDialog(SamplingActivity.this, "Thành công", "Sampling thành công", "OK", new SingleListener() {
                            @Override
                            public void myFunction() {
                                SamplingActivity.this.finish();
                            }
                        });
                        Common.FinishProgressDialog(dialog);
                    } else {
                        Common.showAlertDialog(SamplingActivity.this, "Lỗi", "Có lỗi xẩy ra, vui lòng thử lại.", "OK", new SingleListener() {
                            @Override
                            public void myFunction() {

                            }
                        });
                        Common.FinishProgressDialog(dialog);
                    }
                }
            });
        }
    }

    private String createDataForApi() {
        JSONObject jsonObject = new JSONObject();
        int total_number = 0;
        StoreEntities customerByUser = globalInfo.getCurStore();
        try {
            jsonObject.put("p", StringUtil.toString(globalInfo.getCurStore().getCa()));
            jsonObject.put("p1", customerByUser.getMien_id()); // Mien ID
            jsonObject.put("p2", customerByUser.getMien_code()); // Mien code
            jsonObject.put("p3", customerByUser.getVung_id()); // Vung id
            jsonObject.put("p4", customerByUser.getVung_code()); // Vung code
            jsonObject.put("p5", customerByUser.getShop_id()); // Npp id
            jsonObject.put("p6", customerByUser.getShop_code()); // Npp code
            jsonObject.put("p7", customerByUser.getStore_id()); // Store id
            jsonObject.put("p8", customerByUser.getStore_code()); // Store code
            jsonObject.put("p9", customerByUser.getStore_name()); // Store name
            jsonObject.put("p10", customerByUser.getUser_id()); // User id
            jsonObject.put("p11", globalInfo.getStaff().getRole().getUsername()); // User code
            jsonObject.put("p12", globalInfo.getStaffFullName()); // User name
            jsonObject.put("p13", informationCustomer.getI_Pid() == 0 ? "" : informationCustomer.getI_Pid()); // Customer id
            jsonObject.put("p14", Common.returnStringIfNull(informationCustomer.getStr_HoLot()) + Common.returnStringIfNull(informationCustomer.getStr_Ten())); // Customer name
            jsonObject.put("p15", informationCustomer.getStr_DTDD()); // Phone number
            jsonObject.put("p22", customerByUser.getId()); // Id shift
            jsonObject.put("shift", customerByUser.getShift()); // Id shift

            JSONArray cart_list = new JSONArray();
            for (int i = 0; i < listSampling.size(); i++) {
                SamplingItem giftItem = listSampling.get(i);
                if (giftItem.getNumber_choose() > 0) {
                    JSONObject itemCartList = new JSONObject();
                    itemCartList.put("p16", giftItem.getProductId()); // Product id
                    itemCartList.put("p17", giftItem.getProductCode()); // Product code
                    itemCartList.put("p18", replaceQuoteToSpace(giftItem.getProductName())); // Product name
                    itemCartList.put("p19", giftItem.getTondauthang()); // Exist start month
                    itemCartList.put("p20", giftItem.getQuantity()); // Number before sampling
                    itemCartList.put("p21", giftItem.getNumber_choose()); // Number choose
                    cart_list.put(itemCartList);
                    total_number = total_number + giftItem.getNumber_choose();
                }
            }
            jsonObject.put("cart_list", cart_list);
            jsonObject.put("lock_order_date", globalInfo.getCurStore().getLock_order_date());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (total_number + soluong_da_doi_sampling > limitsampling) {
            return "";
        } else {
            return jsonObject.toString();
        }
    }

    private String replaceQuoteToSpace(String str) {
        return str.replace("'", " ");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                SamplingActivity.this.finish();
                break;
            case R.id.samling_btn_save:
                handleActionSampling();
                break;
            default:
                break;
        }
    }
}
