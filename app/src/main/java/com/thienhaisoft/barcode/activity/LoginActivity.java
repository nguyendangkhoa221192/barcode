package com.thienhaisoft.barcode.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayStoreAdapter;
import com.thienhaisoft.barcode.entities.ListStoreEntities;
import com.thienhaisoft.barcode.entities.ListVersionEntities;
import com.thienhaisoft.barcode.entities.StaffEntities;
import com.thienhaisoft.barcode.entities.json.object.VersionEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.utils.CheckNewVersionApp;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.DateUtils;
import com.thienhaisoft.barcode.utils.HandlerLocation;
import com.thienhaisoft.barcode.utils.JsonUtils;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {

    /**
     * Id to identity READ_CONTACTS permission request.
     */
    private static final int REQUEST_READ_CONTACTS = 0;

    /**
     * A dummy authentication store containing known user names and passwords.
     * TODO: remove after connecting to a real authentication system.
     */
    private static final String[] DUMMY_CREDENTIALS = new String[]{
            "nvbh001", "123456"
    };
    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    // UI references.
    private AutoCompleteTextView edUserName;
    private EditText edPassword;
    private View mProgressView;
    private View mLoginFormView;
    private CheckBox cbAutoLogin;

    private GlobalInfo info;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // Set up the login form.
        edUserName = (AutoCompleteTextView) findViewById(R.id.tbUserName);

        edPassword = (EditText) findViewById(R.id.tbPassword);
        edPassword.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        cbAutoLogin = (CheckBox) findViewById(R.id.cbAutoLogin);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        progressDialog = new ProgressDialog(this);
        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);

        info = (GlobalInfo) getApplicationContext();
        initAutoLogin();
        checkversion();

    }

    @Override
    protected void onResume() {
//        checkNewVersion(Constants.VERSION_APP);
        super.onResume();
    }

    private void checkNewVersion(String currentVersion) {
        CheckNewVersionApp versionChecker = new CheckNewVersionApp(LoginActivity.this, currentVersion);
        try {
            String newVersion = versionChecker.execute().get();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void initAutoLogin() {
        SharedPreferences prefs = getApplicationContext().getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (prefs != null) {
            String userName = prefs.getString("userName", "");
            String pass = prefs.getString("pass", "");
            boolean isAutoLogin = prefs.getBoolean("autoLogin", false);
            if (isAutoLogin) {
                edUserName.setText(userName);
                edPassword.setText(pass);
                cbAutoLogin.setChecked(true);
            }
        }

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        if (mAuthTask != null) {
            return;
        }

        // Reset errors.
        edUserName.setError(null);
        edPassword.setError(null);

        // Store values at the time of the login attempt.
        String username = edUserName.getText().toString();
        String password = edPassword.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
            edPassword.setError(getString(R.string.error_invalid_password));
            focusView = edPassword;
            cancel = true;
        }

        // Check for a valid username address.
        if (TextUtils.isEmpty(username)) {
            edUserName.setError(getString(R.string.error_field_required));
            focusView = edUserName;
            cancel = true;
        } else if (!isUserNameValid(username)) {
            edUserName.setError(getString(R.string.error_invalid_password));
            focusView = edUserName;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            showProgress(true);
            mAuthTask = new UserLoginTask(username, password);
            mAuthTask.execute((Void) null);
        }
    }

    private boolean isUserNameValid(String username) {
        return username.length() >= 5 && username.length() < 20;
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 5 && password.length() < 20;
    }

    /**
     * Shows the progress UI and hides the login form.
     */
    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void addUserNamesToAutoComplete(List<String> listUsername) {
        //Create adapter to tell the AutoCompleteTextView what to show in its dropdown list.
        ArrayAdapter<String> adapter =
                new ArrayAdapter<>(LoginActivity.this,
                        android.R.layout.simple_dropdown_item_1line, listUsername);

        edUserName.setAdapter(adapter);
    }

    private void setPreferences(String userName, String pass) {
        if (!cbAutoLogin.isChecked()) {
            return;
        }

        SharedPreferences prefs = getApplicationContext().getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE);
        if (prefs == null) {
            return;
        }

        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("userName", userName);
        editor.putString("pass", pass);
        editor.putBoolean("autoLogin", true);
        editor.commit();
    }


    private void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
//                intent.putExtra(Constants.LOGIN_SUCCESS);
        startActivity(intent);
    }


    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, Boolean> {

        private final String username;
        private final String password;

        UserLoginTask(String username, String password) {
            this.username = username;
            this.password = password;
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            // TODO: attempt authentication against a network service.

//            try {
            // Simulate network access.
//                Thread.sleep(2000);
//            } catch (InterruptedException e) {
//                return false;
//            }

//                if (DUMMY_CREDENTIALS[0].equals(username)) {
//                    // Account exists, return true if the password matches.
//                    return DUMMY_CREDENTIALS[1].equals(password);
//                }


            try {

                String urlPost = ServerPath.API_URL + "?r=api%2Flogin&username=" + URLEncoder.encode(username) + "&password=" + URLEncoder.encode(password);
//                urlPost = URLEncoder.encode(urlPost);

                HttpParams httpParams = new BasicHttpParams();
                HttpConnectionParams.setSoTimeout(httpParams, APIUtils.TIMEOUT_TIME);
                HttpConnectionParams.setConnectionTimeout(httpParams, APIUtils.TIMEOUT_TIME);

                DefaultHttpClient client = new DefaultHttpClient(httpParams);
                HttpPost post = new HttpPost(urlPost);

                HttpResponse response = null;
                response = client.execute(post);
                final StringBuilder builder = new StringBuilder();
                if (response != null
                        && response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {

                    InputStream inputStream = response.getEntity().getContent();
                    Scanner scanner = new Scanner(inputStream);
                    while (scanner.hasNext()) {
                        builder.append(scanner.nextLine());
                    }
                    inputStream.close();
                    scanner.close();
                    StaffEntities staff = parseUserLogin(builder.toString());
                    if (staff == null || staff.getRole() == null || staff.getRole().getId() <= 0) {
                        return false;
                    }

                    info.setStaff(staff);
                    return true;
                }
            } catch (final Exception e) {
                e.printStackTrace();
            }
            return false;
        }

        @Override
        protected void onPostExecute(final Boolean success) {
            mAuthTask = null;
            showProgress(false);

            if (success) {
                finish();
                setPreferences(username, password);
                goToMainActivity();
            } else {
                edPassword.setError(getString(R.string.error_incorrect_password));
                edPassword.requestFocus();
            }
        }

        @Override
        protected void onCancelled() {
            mAuthTask = null;
            showProgress(false);
        }
    }

    private StaffEntities parseUserLogin(String json) {
        if (StringUtil.isNullOrEmpty(json)) {
            return null;
        }
        return (StaffEntities) JsonUtils.toObject(json, StaffEntities.class);
    }

    private boolean isForceUpdate = false;
    private ProgressDialog progressDialog = null;
    private ListVersionEntities listVersionEntities;

    private void checkversion() {
        String date = DateUtils.now();

        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fviewversion");


        HashMap<String, String> header = new HashMap<>();
        header.put("accept", "application/json");
        header.put("accept-encoding", "gzip, deflate");
        header.put("accept-language", "en-US,en;q=0.8");
        header.put("content-type", "application/json");
        try {
            APIUtils.LoadJSONGet(this, data, header, ServerPath.API_URL, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "Đang kiểm tra phiên bản! Vui lòng chờ!", "Thông báo");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        listVersionEntities = (ListVersionEntities) JsonUtils.toObject(successString, ListVersionEntities.class);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Login: check version; Result: " + failString, LoginActivity.this);
                }

                @Override
                public void uiEnd() {
                    Common.FinishProgressDialog(progressDialog);
                    if (listVersionEntities != null) {
                        List<VersionEntities> version = listVersionEntities.getData();
                        String currentVersion = version.get(0).getCurrent_version();
                        if (version.get(0).getForce_download().equals("1")) {
                            String versionname = "";
                            try {
                                PackageInfo pInfo = LoginActivity.this.getPackageManager().getPackageInfo(getPackageName(), 0);
                                versionname = pInfo.versionName;
                            } catch (PackageManager.NameNotFoundException e) {
                                e.printStackTrace();
                            }
                            if (versionCompare(currentVersion, versionname) > 0) {
                                isForceUpdate = true;
                            }
                        }
                    }

                    if (isForceUpdate) {
                        new AlertDialog.Builder(LoginActivity.this)
                                .setIconAttribute(android.R.attr.alertDialogIcon)
                                .setTitle(R.string.warning)
                                .setMessage("Bạn đang dùng phiên bản cũ, vui lòng cập nhật phiên bản mới!")
                                .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        final String appPackageName = getPackageName(); // getPackageName() from Context or Activity object
                                        try {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                                        } catch (android.content.ActivityNotFoundException anfe) {
                                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                                        }
                                        exitsApp();

                                    }
                                })
                                .setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        exitsApp();
                                    }
                                })
                                .show();
                    }


                }
            });
        } catch (Exception ex) {
            ex.printStackTrace();
        }


    }

    private void exitsApp() {
        finish();
        System.exit(0);
    }

    /**
     * @param str1
     * @param str2
     * @return The result is a negative integer if str1 is _numerically_ less than str2.
     * The result is a positive integer if str1 is _numerically_ greater than str2.
     * The result is zero if the strings are _numerically_ equal.
     */
    private static int versionCompare(String str1, String str2) {
        String[] vals1 = str1.split("\\.");
        String[] vals2 = str2.split("\\.");
        int i = 0;
        // set index to first non-equal ordinal or length of shortest version string
        while (i < vals1.length && i < vals2.length && vals1[i].equals(vals2[i])) {
            i++;
        }
        // compare first non-equal ordinal number
        if (i < vals1.length && i < vals2.length) {
            int diff = Integer.valueOf(vals1[i]).compareTo(Integer.valueOf(vals2[i]));
            return Integer.signum(diff);
        }
        // the strings are equal or one string is a substring of the other
        // e.g. "1.2.3" = "1.2.3" or "1.2.3" < "1.2.3.4"
        return Integer.signum(vals1.length - vals2.length);
    }

}

