package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterReportOrder;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ReportOrderItem;
import com.thienhaisoft.barcode.items.ReportStoreItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ReportOrderActivity extends Activity {

    private ImageButton btn_back;
    private TextView txt_header;
    private GlobalInfo globalInfo;
    private String user_id;
    private ArrayList<ReportOrderItem> list;
    private ProgressDialog progressDialog;
    private ListView lv_report_order;
    private TextView txt_time;
    private Button btn_time;
    private final String FORMAT_DATE = Constants.FORMAT_DATE;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report_order);
        init();
    }

    private void init() {
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ReportOrderActivity.this.finish();
            }
        });
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        lv_report_order = (ListView) findViewById(R.id.lv_report_order);
        txt_header.setText("Báo cáo chi tiết đơn hàng");
        globalInfo = (GlobalInfo) getApplicationContext();
        txt_time = (TextView) findViewById(R.id.report_order_txt_time);
        btn_time = (Button) findViewById(R.id.report_order_btn_time);


        user_id = StringUtil.toString(globalInfo.getStaffId());
        list = new ArrayList<>();

        // Get current time
        final Calendar cal = Calendar.getInstance();
        txt_time.setText(Common.getTextFromTime(cal, FORMAT_DATE));

        getReportOrderDateFromServer(Common.getTimeDivision1000(cal.getTimeInMillis()));


        txt_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Common.showDatePickerDialog(ReportOrderActivity.this, txt_time, FORMAT_DATE);
            }
        });

        btn_time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Common.getTimeFromTextview(txt_time, FORMAT_DATE) > Calendar.getInstance().getTimeInMillis()) {
                    Common.showAlertDialog(ReportOrderActivity.this, "Vui lòng chọn ngày trước ngày hiện tại.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            txt_time.setText(Common.getTextFromTime(cal, FORMAT_DATE));
                        }
                    });
                } else {
                    getReportOrderDateFromServer(Common.getTimeDivision1000(Common.getTimeFromTextview(txt_time, FORMAT_DATE)));
                }
            }
        });
    }

    private void getReportOrderDateFromServer(long time) {
        HashMap<String, String> data = new HashMap<>();
        data.put("id", user_id);
        data.put("p2", StringUtil.toString(time));
        String url = Constants.API_REPORT_ORDER_FILTER;
        final String data_report = data.toString();
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                progressDialog = new ProgressDialog(ReportOrderActivity.this);
                Common.showProgressDialog(progressDialog, "", "");
                list.clear();
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject item = data.getJSONObject(i);
                                ReportOrderItem report_item = new ReportOrderItem();
                                report_item.partJSON(item);
                                list.add(report_item);
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Result: " + failString + "; Data: " + data_report, ReportOrderActivity.this);
                ReportOrderActivity.this.finish();
            }

            @Override
            public void uiEnd() {
                if (list.size() <= 0) {
                    Common.FinishProgressDialog(progressDialog);
                    Common.showAlertDialog(ReportOrderActivity.this, "Không có dữ liệu.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {

                        }
                    });
                }
                setDataToListView(list);
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void setDataToListView(ArrayList<ReportOrderItem> list) {
        ArrayAdapterReportOrder arrayAdapterReportOrder = new ArrayAdapterReportOrder(ReportOrderActivity.this, list);
        lv_report_order.setAdapter(arrayAdapterReportOrder);
    }
}
