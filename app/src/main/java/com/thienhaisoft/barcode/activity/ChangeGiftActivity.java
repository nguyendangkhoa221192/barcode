package com.thienhaisoft.barcode.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListGiftCanGet;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListViewChangeGift;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ChangeGiftItem;
import com.thienhaisoft.barcode.items.GiftItem;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.items.OrderClass;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class ChangeGiftActivity extends Activity implements View.OnClickListener {

    private TextView txt_header = null;
    private ImageButton btn_back = null;
    private TextView txt_name_customer = null;
    private TextView txt_phone = null;
    private TextView txt_point_order = null;
    private TextView txt_point_total = null;
    private Button btn_save = null;
    private Button btn_reset = null;
    private int i_Pid = 0;
    private String user_id = "";
    private String store_id = "";
    private ProgressDialog progressDialog = null;
    private GiftItem giftItem = new GiftItem();
    private GiftItem giftItem_forchange = null;
    private ListView lv_changeGift = null;
    private boolean bl_change_gift_success = false;
    private boolean bl_search_success = false;
    private ArrayList<GiftItem> giftItemArrayList = new ArrayList<>();
    private GlobalInfo globalInfo = null;
    ArrayAdapterListGiftCanGet arrayAdapterListGiftCanGet = null;
    private InformationCustomer informationCustomer = new InformationCustomer();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_gift);
        HidenKeyboard.setupUI(findViewById(android.R.id.content), ChangeGiftActivity.this);
        init();
    }

    private void init() {
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText(R.string.changegift_changegift);
        btn_save = (Button) findViewById(R.id.changegift_btn_save);
        btn_reset = (Button) findViewById(R.id.changegift_btn_image);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        txt_name_customer = (TextView) findViewById(R.id.changegift_txt_name);
        txt_phone = (TextView) findViewById(R.id.changegift_txt_phone_number);
        txt_point_order = (TextView) findViewById(R.id.changegift_txt_point_in_order);
        txt_point_order.setText("0.0");
        txt_point_total = (TextView) findViewById(R.id.changegift_txt_point_accumulation);
        globalInfo = (GlobalInfo) getApplicationContext();
        lv_changeGift = (ListView) findViewById(R.id.changegift_lv_history_change_gift);

        user_id = StringUtil.toString(globalInfo.getStaffId());
        store_id = StringUtil.toString(globalInfo.getCurStore().getStore_id());

        progressDialog = new ProgressDialog(this);
        Intent i = getIntent();
        informationCustomer = (InformationCustomer) i.getSerializableExtra(Constants.RESULT_INFO_CUSTOMER_INFO);
        if (informationCustomer.getI_Pid() > 0) {
            i_Pid = informationCustomer.getI_Pid();
            txt_name_customer.setText(informationCustomer.getStr_HoLot() + informationCustomer.getStr_Ten());
            txt_phone.setText(informationCustomer.getStr_DTDD());
            txt_point_total.setText(informationCustomer.getDou_pointsproduct() + "");
            loadInfoChangeGift();
        } else {
            Common.showAlertDialog(this, "Không lấy được thông tin khách hàng. Vui lòng thử lại!", "Lỗi", "", "", "OK", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {

                }

                @Override
                public void functionPositive() {
                    ChangeGiftActivity.this.finish();
                }
            });
        }

        btn_save.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        btn_back.setOnClickListener(this);

    }

    private void loadInfoChangeGift() {
        HashMap<String, String> data = new HashMap<>();
        data.put(Constants.PAR_ID, i_Pid + "");
        data.put("userid", user_id + "");
        data.put("p3", globalInfo.getCurStore().getShift());
        data.put("p4", globalInfo.getCurStore().getCreated_begin_by_int());
        final String data_report = data.toString();
        String url = Constants.API_LIST_GIFT_CAN_GET;
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                giftItemArrayList.clear();
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONArray listGift = result.getJSONArray(Constants.RESULTAPI_DATA);
                            bl_search_success = true;
                            for (int i = 0; i < listGift.length(); i++) {
                                GiftItem giftItem = new GiftItem();
                                giftItem.partJSON(listGift.getJSONObject(i));
                                giftItemArrayList.add(giftItem);
                            }
                        }
                    } else {
                        bl_search_success = false;
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Đỗi quà: Load list; Result: " + failString + "; Data: " + data_report, ChangeGiftActivity.this);
            }

            @Override
            public void uiEnd() {
                if (bl_search_success) {
                    loadInfoForDisplayListGift(giftItemArrayList);
                } else {
                    Common.showAlertDialog(ChangeGiftActivity.this, "Khách hàng không đủ điều kiện đổi quà.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            ChangeGiftActivity.this.finish();
                        }
                    });
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void loadInfoForDisplayListGift(ArrayList<GiftItem> listGift) {
        arrayAdapterListGiftCanGet = new ArrayAdapterListGiftCanGet(this, listGift, informationCustomer.getDou_pointsproduct());
        lv_changeGift.setAdapter(arrayAdapterListGiftCanGet);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                this.finish();
                break;
            case R.id.changegift_btn_image:
                resetAction();
                break;
            case R.id.changegift_btn_save:
                handleChangeGiftAction();
                break;
            default:
                break;
        }
    }

    private void resetAction() {
        for (int i = 0; i < giftItemArrayList.size(); i++) {
            giftItemArrayList.get(i).setI_number_choose(0);
        }
        arrayAdapterListGiftCanGet.swapItem(giftItemArrayList);
        txt_point_order.setText("0.0");
        txt_point_total.setText(informationCustomer.getDou_pointsproduct() + "");
    }

    private void handleChangeGiftAction() {
        String values_data = getInformationForChangeGift(giftItemArrayList);
        if (values_data != null && !values_data.equals("")) {
            // Change gift action
            HashMap<String, String> data = new HashMap<>();
            data.put(Constants.RESULTAPI_DATA, values_data);
            final String data_report = data.toString();
            APIUtils.LoadJSON(this, data, Constants.API_CHANGE_GIFT, new APICallBack() {
                @Override
                public void uiStart() {
                    Common.showProgressDialog(progressDialog, "", "");
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        JSONObject result = new JSONObject(successString);
                        if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                            bl_change_gift_success = true;
                            if (result.has(Constants.RESULTAPI_DATA)) {
                                JSONArray listGift = result.getJSONArray(Constants.RESULTAPI_DATA);
                                if (listGift.length() > 0) {
                                    JSONObject gift = listGift.getJSONObject(0);
                                }
                            }
                        } else {
                            bl_change_gift_success = false;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Đỗi quà: Submit đỗi quà; Result: " + failString + "; Data: " + data_report, ChangeGiftActivity.this);
                }

                @Override
                public void uiEnd() {
                    Common.FinishProgressDialog(progressDialog);
                    if (bl_change_gift_success) {
                        Common.showAlertDialog(ChangeGiftActivity.this, "Quà đã được đổi.", "Thành công", "", "", "Kết thúc", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                ChangeGiftActivity.this.finish();
                            }
                        });
                    } else {
                        Common.showAlertDialog(ChangeGiftActivity.this, "Đổi quà không thành công.", "Lỗi", "", "Thử lại", "Kết thúc", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                ChangeGiftActivity.this.finish();
                            }
                        });
                    }
                }
            });
        } else {
            Common.showAlertDialog(ChangeGiftActivity.this, "Số lượng quà không hợp lệ. Vui lòng kiểm tra lại.", "Lỗi", "", "Thử lại", "Kết thúc", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {

                }

                @Override
                public void functionPositive() {
                    ChangeGiftActivity.this.finish();
                }
            });
        }
    }

    private String getInformationForChangeGift(ArrayList<GiftItem> listGiftItem) {
        JSONObject jsonObject = new JSONObject();
        Double total_point = 0.0;
        try {
            jsonObject.put("id_order_status", "1");
            jsonObject.put("id_store", store_id);
            jsonObject.put("idcustomer", i_Pid + "");
            jsonObject.put("userid", user_id);
            jsonObject.put("id_order_type", "1");
            jsonObject.put("total_price", "12345678");
            jsonObject.put("total_price_tax", "12345678");
            jsonObject.put("grand_total", "12345678");
            jsonObject.put("shift", globalInfo.getCurStore().getShift());

            JSONArray cart_list = new JSONArray();
            for (int i = 0; i < listGiftItem.size(); i++) {
                GiftItem giftItem = listGiftItem.get(i);
                if (giftItem.getI_number_choose() > 0) {
                    JSONObject itemCartList = new JSONObject();
                    itemCartList.put("id_product", giftItem.getI_product_id());
                    itemCartList.put("point", (giftItem.getDou_point_to_product() * giftItem.getI_number_choose()) + "");
                    itemCartList.put("id_product_feature", "");
                    itemCartList.put("id", "p1");
                    itemCartList.put("id_customization_group", "");
                    itemCartList.put("id_customization", "");
                    itemCartList.put("customization_name", "");
                    itemCartList.put("name", giftItem.getStr_gift_name());
                    itemCartList.put("quantity", giftItem.getI_number_choose() + "");
                    itemCartList.put("price_tax", 38000);
                    itemCartList.put("price", 38000);
                    itemCartList.put("tax", 0);
                    itemCartList.put("product_note", "");
                    cart_list.put(itemCartList);
                }
            }
            jsonObject.put("cart_list", cart_list);
            jsonObject.put("point", informationCustomer.getDou_pointsproduct() + "");
            jsonObject.put("lock_order_date", globalInfo.getCurStore().getLock_order_date());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (total_point > informationCustomer.getDou_pointsproduct()) {
            return "";
        } else {
            return jsonObject.toString();
        }
    }
}
