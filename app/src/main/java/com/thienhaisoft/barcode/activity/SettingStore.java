package com.thienhaisoft.barcode.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.location.LocationProvider;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListProduct;
import com.thienhaisoft.barcode.adapter.ArrayAdapterListProductStore;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

public class SettingStore extends Activity implements View.OnClickListener {

    private TextView txt_store_name;
    private TextView txt_date;
    private TextView txt_header;
    private Button btn_config;
    private Button btn_reset;
    private ImageButton btn_back;
    private EditText edt_search;
    private ListView lv_product;
    private ArrayList<ProductItem> listItemProduct = new ArrayList<>();
    private GlobalInfo globalInfo;
    private String store_id;
    private String user_id;
    private Calendar cal_current;
    private boolean bl_checkdate = false;
    private boolean bl_config_success = false;
    private ProgressDialog progressDialog;
    private ArrayAdapterListProductStore adt_spinner_product;
    private String date_validation = null;
    private String stock_validation = null;
    private String time_from_server = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_store);
        init();
    }

    private void init() {
        HidenKeyboard.setupUI(findViewById(android.R.id.content), SettingStore.this);
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_header.setText("Điều chỉnh kho store");
        txt_store_name = (TextView) findViewById(R.id.settingstore_store_name);
        txt_date = (TextView) findViewById(R.id.settingstore_store_date);
        btn_config = (Button) findViewById(R.id.settingstore_store_config);
        btn_reset = (Button) findViewById(R.id.settingstore_store_reset);
        edt_search = (EditText) findViewById(R.id.settingstore_edt_search);
        lv_product = (ListView) findViewById(R.id.settingstore_lv_show_product);
        globalInfo = (GlobalInfo) getApplicationContext();
        btn_back = (ImageButton) findViewById(R.id.btn_back);

        cal_current = Calendar.getInstance();
        cal_current.add(Calendar.MONTH, -1);
        txt_date.setText(getDateFormat(cal_current.getActualMaximum(Calendar.DAY_OF_MONTH), cal_current.get(Calendar.MONTH), cal_current.get(Calendar.YEAR)));

        btn_back.setOnClickListener(this);
        btn_config.setOnClickListener(this);
        btn_reset.setOnClickListener(this);
        StoreEntities storeInfo = globalInfo.getCurStore();
        txt_store_name.setText(StringUtil.toString(globalInfo.getCurStore().getStore_name()));
        store_id = StringUtil.toString(storeInfo.getStore_id());
        user_id = StringUtil.toString(globalInfo.getStaffId());
        progressDialog = new ProgressDialog(this);

//        txt_date.setOnClickListener(this);

        edt_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adt_spinner_product.getFilter().filter(charSequence);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        callAPIProductStore();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.settingstore_store_config:
                actionCheckDateForSetting();
                break;
            case R.id.settingstore_store_reset:
                actionReset();
                break;
            case R.id.btn_back:
                this.finish();
                break;
//            case R.id.settingstore_store_date:
//                Calendar cal_choice = Calendar.getInstance();
//                int date_choice = cal_choice.get(Calendar.DAY_OF_MONTH);
//                if (date_choice != 2 && date_choice != 1) {
//                    showDateForChoose(cal_current);
//                }
//                break;
            default:
                break;
        }
    }

    private void showDateForChoose(final Calendar cal) {
        final Dialog d = new Dialog(SettingStore.this);
        d.setTitle("Chọn ngày");
        d.setContentView(R.layout.layout_dialog_pick_number_date);
        Button b1 = (Button) d.findViewById(R.id.button1);
        Button b2 = (Button) d.findViewById(R.id.button2);
        final NumberPicker np = (NumberPicker) d.findViewById(R.id.numberPicker1);
        int date_last_month = cal.getActualMaximum(Calendar.DAY_OF_MONTH);
        np.setMaxValue(date_last_month);
        np.setMinValue(1);
        np.setWrapSelectorWheel(false);
        b1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txt_date.setText(getDateFormat(np.getValue(), cal.get(Calendar.MONTH), cal.get(Calendar.YEAR)));
                d.dismiss();
            }
        });
        b2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        d.getWindow().setLayout((6 * width) / 7, (4 * height) / 5);
        d.show();

    }

    private void actionCheckDateForSetting() {
        final HashMap<String, String> data = new HashMap<>();
//        String url = Constants.API_GET_LOCK_DATE + store_id;
        String url = Constants.API_LOCK_STOCK;
        data.put("id", store_id);
        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                progressDialog.show();
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            JSONObject item = result.getJSONArray(Constants.RESULTAPI_DATA).getJSONObject(0);
                            if (item.has("lockdate")) {
                                date_validation = item.getString("lockdate");
                            }
                            if (item.has("lockstock")) {
                                stock_validation = item.getString("lockstock");
                            }
                            if (item.has("timesystem")) {
                                time_from_server = item.getString("timesystem");
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Điều Chỉnh Kho: Check thông tin ngày tháng; Result: " + failString, SettingStore.this);
            }

            @Override
            public void uiEnd() {
                Date date_valib = Calendar.getInstance().getTime();
                Date date_server = Calendar.getInstance().getTime();
                if (date_validation != null && stock_validation != null && time_from_server != null) {
                    try {
                        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
                        date_valib = simpleDateFormat.parse(date_validation);
                        date_server = simpleDateFormat.parse(time_from_server);
                        bl_checkdate = true;
                    } catch (Exception ex) {
                        bl_checkdate = false;
                        ex.printStackTrace();
                    }
                } else {
                    bl_checkdate = false;
                }
                if (bl_checkdate) {
                    Calendar cal_lockdate = Calendar.getInstance();
                    cal_lockdate.setTime(date_valib);
                    cal_lockdate = Common.setTimeToZero(cal_lockdate);
                    int lockdate_date = cal_lockdate.get(Calendar.DAY_OF_MONTH);

                    Calendar cal_server = Calendar.getInstance();
                    cal_server.setTime(date_server);
                    cal_server = Common.setTimeToZero(cal_current);
                    int current_date = cal_server.get(Calendar.DAY_OF_MONTH);

                    int stock = 0;
                    try {
                        stock = Integer.parseInt(stock_validation);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }

                    if (current_date != 1 && current_date != 2) {
                        // Message error can't config store
                        if (lockdate_date != 3) {
                            actionConfig(true, date_validation, stock);
                        } else {
                            Common.showAlertDialog(SettingStore.this, "Hôm nay không được điều chỉnh kho.", "Lỗi", "", "", "OK", new MyListener() {
                                @Override
                                public void functionNegative() {

                                }

                                @Override
                                public void functionNeutral() {

                                }

                                @Override
                                public void functionPositive() {

                                }
                            });
                            progressDialog.dismiss();
                        }
                    } else {
                        actionConfig(false, date_validation, stock);
                    }
                } else {
                    Common.showAlertDialog(SettingStore.this, "Điều chỉnh store thất bại.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {

                        }
                    });
                    progressDialog.dismiss();
                }
            }
        });
    }

    private void actionConfig(boolean lockdate, String str_lockdate, int lockstock) {
        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_UPDATE_STORE;
        String str_data = createDataForActionConfig(listItemProduct, lockdate, str_lockdate, lockstock);
        if (str_data != null) {
            data.put("data", str_data);
            final String data_report = data.toString();
            APIUtils.LoadJSON(this, data, url, new APICallBack() {
                @Override
                public void uiStart() {

                }

                @Override
                public void success(String successString, int type) {
                    try {
                        JSONObject result = new JSONObject(successString);
                        if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                            bl_config_success = true;
                        } else {
                            bl_config_success = false;
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.sendDebugMessage("Điều Chỉnh Kho: Điều chỉnh; Result: " + failString + "; Data: " + data_report, SettingStore.this);
                    bl_config_success = false;
                }

                @Override
                public void uiEnd() {
                    if (bl_config_success) {
                        Common.showAlertDialog(SettingStore.this, "Điều chỉnh store thành công.", "Thành công", "", "", "OK", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {
                                actionReset();
                                callAPIProductStore();
                            }
                        });
                    } else {
                        Common.showAlertDialog(SettingStore.this, "Điều chỉnh store thất bại.", "Lỗi", "", "", "OK", new MyListener() {
                            @Override
                            public void functionNegative() {

                            }

                            @Override
                            public void functionNeutral() {

                            }

                            @Override
                            public void functionPositive() {

                            }
                        });
                    }
                    Common.FinishProgressDialog(progressDialog);
                }
            });
        } else {
            String str_error = "Số lượng điều chỉnh không hợp lệ.\nVui lòng kiểm tra lại hoặc yêu cầu admin mở khóa.";
//            String str_error = "Lỗi số lượng: \n";
//            for (int i = 0; i < listItemProduct.size(); i ++) {
//               ProductItem item = listItemProduct.get(i);
//               if (item.getNumber_choose() > item.getQuantity()) {
//                   str_error = str_error + item.getCode() + ": SL-" + item.getQuantity() + " | DC-" + item.getNumber_choose() + "\n";
//               }
//           }
            Common.FinishProgressDialog(progressDialog);
            Common.showAlertDialog(this, str_error, "Lỗi", "", "", "OK", new MyListener() {
                @Override
                public void functionNegative() {

                }

                @Override
                public void functionNeutral() {

                }

                @Override
                public void functionPositive() {

                }
            });
        }
    }

    private String createDataForActionConfig(ArrayList<ProductItem> listItemProduct, boolean lockdate, String str_lockdate, int stock_validation) {
        JSONObject objectData = new JSONObject();
        boolean isCreate = true;
        try {
            String str_date = txt_date.getText().toString();
            Date date = cal_current.getTime();
            SimpleDateFormat format_date = new SimpleDateFormat("dd/MM/yyyy");
            date = format_date.parse(str_date);

            if (lockdate) {
                SimpleDateFormat date_format_lockdate = new SimpleDateFormat("yyyy-MM-dd");
                date = date_format_lockdate.parse(str_lockdate);
            }

            objectData.put("create_date", (date.getTime() / 1000) + "");
            objectData.put("update_date", (cal_current.getTimeInMillis() / 1000) + "");
            JSONArray jsonArrayCartList = new JSONArray();
            switch (stock_validation) {
                case 0:
                    isCreate = false;
                    break;
                case 1:
                    for (int i = 0; i < listItemProduct.size(); i++) {
                        ProductItem productItem = listItemProduct.get(i);
                        if (productItem.getNumber_choose() != 0) {
                            if (productItem.getNumber_choose() <= 0 && checkValibNumChoose(productItem)) {
                                JSONObject objectItemCartList = createDataItemOrder(productItem, date);
                                jsonArrayCartList.put(objectItemCartList);
                            } else {
                                isCreate = false;
                                break;
                            }
                        }
                    }
                    break;
                case 2:
                    for (int i = 0; i < listItemProduct.size(); i++) {
                        ProductItem productItem = listItemProduct.get(i);
                        if (productItem.getNumber_choose() != 0) {
                            if (productItem.getNumber_choose() >= 0 && checkValibNumChoose(productItem)) {
                                JSONObject objectItemCartList = createDataItemOrder(productItem, date);
                                jsonArrayCartList.put(objectItemCartList);
                            } else {
                                isCreate = false;
                                break;
                            }
                        }
                    }
                    break;
                default:
                    for (int i = 0; i < listItemProduct.size(); i++) {
                        ProductItem productItem = listItemProduct.get(i);
                        if (productItem.getNumber_choose() != 0) {
                            if (checkValibNumChoose(productItem)) {
                                JSONObject objectItemCartList = createDataItemOrder(productItem, date);
                                jsonArrayCartList.put(objectItemCartList);
                            } else {
                                isCreate = false;
                                break;
                            }
                        }
                    }
                    break;
            }
            objectData.put("products_list", jsonArrayCartList);

        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        if (isCreate) {
            return objectData.toString();
        } else {
            return null;
        }
    }

    private boolean checkValibNumChoose(ProductItem productItem) {
        return productItem.getNumber_choose() >= -(productItem.getQuantity());
    }

    private JSONObject createDataItemOrder(ProductItem productItem, Date date) {
        JSONObject objectItemCartList = new JSONObject();
        try {
            objectItemCartList.put("store_id", store_id + "");
            objectItemCartList.put("product_id", productItem.getId_product() + "");
            objectItemCartList.put("quantity", productItem.getNumber_choose() + "");
            objectItemCartList.put("update_user", user_id);
            objectItemCartList.put("create_date", (date.getTime() / 1000) + "");
            objectItemCartList.put("update_date", (cal_current.getTimeInMillis() / 1000) + "");
            if (productItem.getNumber_choose() > 0) {
                objectItemCartList.put("tran_type", "2");
            } else {
                objectItemCartList.put("tran_type", "3");
            }
            objectItemCartList.put("create_user", "1");
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
        return objectItemCartList;
    }

    private void actionReset() {
        for (int i = 0; i < listItemProduct.size(); i++) {
            listItemProduct.get(i).setNumber_choose(0);
        }
        adt_spinner_product.swapItem(listItemProduct);
    }

    private void callAPIProductStore() {
        String url = Constants.API_LIST_PRODUCT;
        HashMap<String, String> data = new HashMap<>();
        data.put("r", "api%2Fviewproductslist");
        data.put(Constants.PAR_LIST_PRODUCT, store_id);
        final String data_report = data.toString();
        APIUtils.LoadJSONGet(this, data, null, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(progressDialog, "", "");
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has(Constants.RESULTAPI_DATA)) {
                            listItemProduct.clear();
                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
                            for (int i = 0; i < data.length(); i++) {
                                JSONObject itemData = data.getJSONObject(i);
                                ProductItem productItem = new ProductItem();
                                productItem.partJSON(itemData);
                                listItemProduct.add(productItem);
                            }
                        }
                    } else {
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                Common.sendDebugMessage("Điều chỉnh kho: Load list sản phẩm: Result: " + failString + "; Data: " + data_report, SettingStore.this);
            }

            @Override
            public void uiEnd() {
                if (listItemProduct.size() <= 0) {
                    Common.showAlertDialog(SettingStore.this, "Không lấy được thông tin sản phẩm. Vui lòng thử lại.", "Lỗi", "", "", "OK", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            SettingStore.this.finish();
                        }
                    });
                } else {
                    setValuesListProduct(listItemProduct);
                }
                Common.FinishProgressDialog(progressDialog);
            }
        });
    }

    private void setValuesListProduct(ArrayList<ProductItem> listItemProduct) {
        adt_spinner_product = new ArrayAdapterListProductStore(this, this, listItemProduct);
        lv_product.setAdapter(adt_spinner_product);
    }

    private String getDateFormat(int day, int month, int year) {
        String date;
        date = String.format("%02d", day) + "/" + String.format("%02d", month + 1) + "/" + String.format("%04d", year);
        return date;
    }
}
