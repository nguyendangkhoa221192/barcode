package com.thienhaisoft.barcode.activity;

import android.app.ProgressDialog;
import android.content.pm.PackageInfo;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.AverageMonth;
import com.thienhaisoft.barcode.items.ResultStoreItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.HidenKeyboard;
import com.thienhaisoft.barcode.utils.MyListener;
import com.thienhaisoft.barcode.utils.StringUtil;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

public class ResultStore extends AppCompatActivity {

    private ProgressDialog dialog;
    private boolean isSuccess = false;
    private GlobalInfo info;
    private ResultStoreItem itemResult;
    private LinearLayout lnl_main;
    private ArrayList<AverageMonth> listAverage = new ArrayList<>();

    private TextView
            store_rs_user,
            store_rs_location,
            store_rs_date,
            store_rs_shop,
            store_rs_shift,
            store_rs_time,
            store_rs_YTV,
            store_rs_Kids,
            store_rs_kid,
            store_rs_justnes,
            store_rs_result,
            txt_header,
            txt_oats,
            txt_lockid,
            txt_promotion,
            txt_dailynest,
            txt_version,
            txt_try;
    private ImageButton btn_back;
    private WebView txt_web_text_title;
    private WebView txt_web_text_content;
    private boolean is_display_web = false;
    private StoreEntities store;
    private String text_html_content = "", text_html_title = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_store);
        HidenKeyboard.hideSoftKeyboard(this);
        init();
    }

    private void init() {
        dialog = new ProgressDialog(this);
        info = (GlobalInfo) getApplicationContext();
        store_rs_user = (TextView) findViewById(R.id.store_rs_user);
        store_rs_location = (TextView) findViewById(R.id.store_rs_location);
        store_rs_date = (TextView) findViewById(R.id.store_rs_date);
        store_rs_shop = (TextView) findViewById(R.id.store_rs_shop);
        store_rs_shift = (TextView) findViewById(R.id.store_rs_shift);
        store_rs_time = (TextView) findViewById(R.id.store_rs_time);
        store_rs_YTV = (TextView) findViewById(R.id.store_rs_YTV);
        store_rs_Kids = (TextView) findViewById(R.id.store_rs_Kids);
        store_rs_kid = (TextView) findViewById(R.id.store_rs_kid);
        store_rs_justnes = (TextView) findViewById(R.id.store_rs_justnes);
        store_rs_result = (TextView) findViewById(R.id.store_rs_result);
        txt_header = (TextView) findViewById(R.id.txt_title_header);
        txt_oats = (TextView) findViewById(R.id.store_rs_oats);
        txt_lockid = (TextView) findViewById(R.id.store_rs_lockid);
        txt_version = (TextView) findViewById(R.id.store_rs_version_app);
        txt_promotion = (TextView) findViewById(R.id.store_rs_product_promotion);
        txt_try = (TextView) findViewById(R.id.store_rs_product_try);
        txt_dailynest = (TextView) findViewById(R.id.store_rs_dailynest);
        txt_web_text_content = (WebView) findViewById(R.id.store_rs_html_txt_content);
        txt_web_text_title = (WebView) findViewById(R.id.store_rs_html_txt_title);
        lnl_main = (LinearLayout) findViewById(R.id.store_rs_lnl_main);
        txt_header.setText(R.string.title_result_store);
        btn_back = (ImageButton) findViewById(R.id.btn_back);
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResultStore.this.finish();
            }
        });
        store = info.getCurStore();
        String version_name = "";
        try {
            PackageInfo pInfo = ResultStore.this.getPackageManager().getPackageInfo(getPackageName(), 0);
            version_name = pInfo.versionName;
        } catch (Exception ex) {

        }
        txt_version.setText("Ver: " + version_name);
        handleAPIResult();
    }

    private void handleAPIResult() {
        HashMap<String, String> data = new HashMap<>();
//        String url = Constants.API_RESULT_STORE;
        String url = Constants.API_RESULT_ORDER_BY_SHIFT;

        HashMap<String, String> time_now = Common.getStartEndTimeInDate();
//        String start_time = time_now.get("start_time");
//        String end_time = time_now.get("end_time");

//        data.put("p1", start_time);
//        data.put("p2", end_time);
//        data.put("p3", String.valueOf(store.getStore_id()));
//        data.put("p4", String.valueOf(store.getCa()));
        data.put("p1", String.valueOf(info.getStaffId()));
        data.put("p2", store.getCreated_begin_by_int());
        data.put("p3", store.getCreated_begin_by_int());
        data.put("p4", String.valueOf(store.getId_store()));
        data.put("p5", store.getShift());
        data.put("p6", store.getCreated_begin_by_int());

        APIUtils.LoadJSON(this, data, url, new APICallBack() {
            @Override
            public void uiStart() {
                Common.showProgressDialog(dialog, "Đang tải dữ liệu...", "");
                isSuccess = false;
            }

            @Override
            public void success(String successString, int type) {
                try {
                    JSONObject result = new JSONObject(successString);
                    if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                        if (result.has("data0")) {
                            text_html_title = result.getString("data0");
                        }
                        if (result.has("data1")) {
                            text_html_content = result.getString("data1");
                        }
                    }
//                        is_display_web = false;
//                        if (result.has("hienthi") && result.getInt("hienthi") == 1) {
//                            is_display_web = true;
//                            if (result.has("datagrid"))
//                                text_html = result.getString("datagrid");
//                        } else {
//                            JSONArray data = result.getJSONArray(Constants.RESULTAPI_DATA);
//                            JSONObject item = data.getJSONObject(0);
//                            itemResult = new ResultStoreItem();
//                            itemResult.partJSON(item);
//                            is_display_web = false;
//                        }
//                        isSuccess = true;
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }

            @Override
            public void fail(String failString) {
                isSuccess = false;
            }

            @Override
            public void uiEnd() {
//                if (isSuccess) {
//                    handleAPIShift();
//                } else {
//                    Common.FinishProgressDialog(dialog);
//                    showDialogFail();
//                }
//                handleAPIShift();
                handleViewData();
                Common.FinishProgressDialog(dialog);
            }
        });
    }

    private void handleViewData() {
        lnl_main.setVisibility(View.GONE);
        txt_web_text_content.loadDataWithBaseURL(null, text_html_content, "text/html", "utf-8", null);
        txt_web_text_title.loadDataWithBaseURL(null, text_html_title, "text/html", "utf-8", null);
    }

    @Override
    protected void onDestroy() {
        info.setCurStore(null);
        super.onDestroy();
    }
}
