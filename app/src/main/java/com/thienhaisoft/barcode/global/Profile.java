/**
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.thienhaisoft.barcode.global;

import com.thienhaisoft.barcode.entities.StaffEntities;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.GlobalUtil;
import com.thienhaisoft.barcode.utils.StringUtil;
import java.io.Serializable;

/**
 *  Luu tru thong tin user dang nhap
 *  @author: TruongHN
 *  @version: 1.0
 *  @since: 1.0
 */
public class Profile implements Serializable {
	private static final long serialVersionUID = -5226771436632902463L;
	public static final String NONE = "NONE";
	public static final String UPDATEDB = "UPDATEDB";
	public static final String RESETDB = "RESETDB";
	private String actionLogin = NONE;

	public static final String AMSTHS_PROFILE = "amsthsProfile";
	private String softVersion = Constants.STR_BLANK;
	private String dbVersion = Constants.STR_BLANK;
	// revision cua thu muc cache hinh anh
	private String server_revision = "";
	// Luu chung thuc dang nhap
	private String authData = "";

    public void setStaff(StaffEntities staff) {
        this.staff = staff;
        save();
    }

    private StaffEntities staff;
    public StaffEntities getStaff() {
        return staff;
    }

    //is mode debug or not
    private boolean isDebugMode = true;
    public boolean isDebugMode() {
		return isDebugMode;
	}
	/**
	 * @param isDebugMode the isDebugMode to set
	 */
	public void setDebugMode(boolean isDebugMode) {
		this.isDebugMode = isDebugMode;
		save();
	}

	/**
	 * set chuoi chung thuc de dang nhap lan sau
	 *
	 * @author: BangHN
	 */
	public void setAuthData(String auth) {
		this.authData = auth;
		save();
	}

	/**
	 * Tra ve chuoi chung thuc dang nhap
	 *
	 * @author: BangHN
	 */
	public String getAuthData() {
		return authData;
	}

	/**
	 * set chuoi server revision
	 *
	 * @author: BangHN
	 */
	public void setServerRevision(String revision) {
		this.server_revision = revision;
		save();
	}



	/**
	 * @param versionName
	 *            the versionName to set
	 */
	public void setVersionApp(String versionName) {
		if (!StringUtil.isNullOrEmpty(versionName)) {
			int length = versionName.length();
			if (length == 3) {
				// 2.1 --> 2.10
				versionName += ".0";
			}
			// else if (length == 5){
			// // 2.1.1 --> 2.11
			// String lastChar = versionName.substring(length - 1, length);
			// versionName = versionName.substring(0,3) + lastChar;
			// }
		}
		this.softVersion = versionName;
		save();
	}


	/**
	 * set dbversion
	 * @author : BangHN
	 * since : 10:06:33 AM
	 */
	public void setVersionDB(String dbVersion){
		if(StringUtil.isNullOrEmpty(dbVersion)){
			dbVersion = "0.0.0";
		}
		this.dbVersion = dbVersion;
		save();
	}


	/**
	 *
	 * Luu thong in profile
	 * @author : DoanDM since : 9:15:31 AM
	 */
	public void save() {
		GlobalUtil.saveObject(this, AMSTHS_PROFILE);
	}


	/**
	 * Clear profile
	 * @author : BangHN
	 * since : 1.0
	 */
	public void clearProfile(){
        //set default tham so can thiet sau do save lai

		save();
	}

}
