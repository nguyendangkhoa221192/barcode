/**
 * Copyright 2012 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.thienhaisoft.barcode.global;

import java.util.ArrayList;
import java.util.List;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Handler;

import com.thienhaisoft.barcode.entities.StaffEntities;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.utils.Constants;
import com.thienhaisoft.barcode.utils.GlobalUtil;
import com.thienhaisoft.barcode.utils.GlobalUtil.IDataValidate;
import com.thienhaisoft.barcode.utils.LatLng;
import com.thienhaisoft.barcode.utils.StringUtil;


/**
 * Luu tru cac bien su dung chung trong chuong trinh
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class GlobalInfo extends Application {
	public static final String RADIUS_OF_POSITION = "RadiusOfPosition";
	private static boolean IS_VERSION_SEND_DATA = true;
	private static boolean IS_VERSION_FOR_EMULATOR = true;

	private static final String BLACK_LIST_APP_MOCK_LOCATION = "org.ajeje.fakelocation;"
			+ "com.fakegps.mock;"
			+ "com.incorporateapps.fakegps.fre;"
			+ "com.lexa.fakegps;"
			+ "com.blogspot.newapphorizons.fakegps;"
			+ "ait.com.locationfaker;";

	private static final String WHITE_LIST_APP_MOCK_LOCATION = "com.zing.zalo;com.zeroteam.zerolauncher;";
	private static final String DMS_LAST_LOGIN_ONLINE = "com.viettel.dms.lastLoginOnline";
	private static final String DMS_LAST_LOGIN_ONLINE_FROM_BOOT = "com.viettel.dms.lastLoginOnlineSinceBoot";
	private static final String DMS_LAST_RIGHT_TIME = "com.viettel.dms.lastRightTime";

	public static final String OAUTH_CLIENT_SECRET = "1192d23f3d8470e0deab0a51d5946f9c";
	private static final String VNM_FLAG_VALIDATE_LOCATION = "flagLocation";
	private static final String VNM_BLACKLIST_MOCK_LOCATION = "blackListMockLocation";
	private static final String VNM_WHITELIST_MOCK_LOCATION = "whitelistMockLocation";
	private static final String VNM_INTERVAL_FUSED_POSITION = "intervalFusedPosition";
	private static final String VNM_FAST_INTERVAL_FUSED_POSITION = "fastIntervalFusedPosition";
	private static final String VNM_RADIUS_FUSED_POSITION = "radiusFusedPosition";
	private static final String VNM_MAX_TIME_WRONG = "maxTimeWrong";

	public static final String WHITE_LIST_FILE_TYPE_UPLOAD = "3gp,mp4,mp3,ogg,wav,jpg,jpeg,gif,png,bmp,pdf,doc,docx,ppt,pptx,xls,xlsx,txt";
	private static final String REGISTER_APPROVED_KEYSHOP = "registerApprovedKeyshop";// duoc dang ky keyshop hay sau khi approved


	//tinh bang KB
	public static final int MAX_SIZE_UPLOAD_ATTACH = 5 * 1024;
	private static final String VNM_MAX_SIZE_UPLOAD_ATTACH = "maxSizeUploadAttach";
	private static final String VNM_WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH = "whiteListFileTypeUploadAttach";


	//loai keyboard 1 la cua may, 2 la custom
	private int flagKeyboard = 0;
	//loai ban do, 1 la Viettel map, 2 la Google map
	private int mapType = 1;

	// phan khong luu xuong file
	private static volatile GlobalInfo instance = null;
	private Context activityContext;// activity context
	public boolean isDebugMode = true;
	private boolean isAppActive;
	public final String VERSION = "2.4";
	public final String PLATFORM_SDK_STRING = android.os.Build.VERSION.SDK;
	public final String PHONE_MODEL = "Android_" + android.os.Build.MODEL;
	private String DEVICE_IMEI;
	// phan luu xuong file
	private Profile profile = new Profile();
	private Handler mHandler = new Handler();
	//gia tri phat sinh id o client
	// thoi gian hop le khi tao don hang trong ngay (gio:hour)
	private int timeTestOrder = 0;
	// gioi han ban kinh cho phep lay dinh vi, (do chinh xac):met
	private int radiusOfPosition = 0;
	// thoi gian triger luong dinh vi (phut)
	private int timeTrigPosition = 0;
	// thoi gian dong bo client len server
	private int timeSyncToServer = 0;
	// thoi gian bat dau trang thai idle, pause
	private long timeStartIdleStatus = 0;
	// thoi gian cho phep o trang thai IDLE
	private long timeAllowIdleStatus = 0;
	// thoi gian bat dau cho phep dong bo du lieu
	private int beginTimeAllowSynData = -1;
	// thoi gian ket thuc cho phep dong bo du lieu
	private int endTimeAllowSynData = -1;
	// bien dung de kiem tra co cho phep chinh sua khuyen mai tu dong hay khong
	private int allowEditPromotion = 0;
	// thoi gian luong dinh vi trong qua trinh cham cong
	private int timeTrigPositionAttendance = 120000;
	//khoang cach cho phep ket thuc ghe tham
	private int distanceAllowFinish = 300;
	//bien hien thi tinh nang kiem tra data chua duoc dong bo
	private int checkDataNotSyn = 1;
	//thoi gian sai lech thuc te
	private int visitTimeInterval = 5;
	//thoi gian ghe tham toi da cho phep
	private int visitTimeAllow = 20;
	// Thoi gian bat dau cham cong
	private String ccStartTime = "";
	// Thoi gian ket thuc cham cong
	private String ccEndTime = "";
	// khoang cach cham cong
	private int ccDistance = 0;
	//toa do khach hang dang ghe tham, muc dich de kiem tra dinh vi
	private LatLng positionCustomerVisiting;
	//1. Lam tron len: 2.345 = 2.35, 2.678 = 2.68
	//2. Lam tron xuong: 2.865 = 2.86, 2.841 = 2.84
	//3. Lam tron tu nhien: 2.355 = 2.36, 2.344 = 2.34, 2.567 = 2.57
	//Default khong co la lam tron tu nhien
	private int sysNumRounding = 0;
	private int sysNumRoundingPromotion = 0;
	//Don vi tien te ma he thong su dung
	//Mac dinh la VND
	private String sysCurrency = "";
	//So chu so phan thap phan cua tien te
	//mac dinh la 2 so
	private int sysDigitDecimal = -1;
	//Su dung dau phay (,) hay dau cham (.) phan cach thap phan? 1: dau phay; 2: dau cham
	//mac dinh la 2 dau cham
	private int sysDecimalPoint = 0;
	//Dinh dang ngay he thong su dung
	private String sysDateFormat = "";

	//So ngay toi da duoc duyet don hang
	private int sysMaxdayApprove = -1;
	//So ngay toi da duoc phep tra hang
	private int sysMaxdayReturn = -1;
	//Co hien thi gia tren don hang hay khong? 1 co, 0 khong
	private int sysShowPrice = -1;
	//Co cho phep edit gia tren don hang hay khong? 1 co, 0 khong
	private int sysModifyPrice = -1;
	//lay order_date hay approved_date : 1 order_date, 2 approved_date
	private int sysCalDate = 0;

	private ArrayList<String> listStackTag = new ArrayList<String>();

	private AlertDialog alertDialog;

	// bien dung de kiem tra ghi log - ko can luu khi bi reset
	private boolean isSendLogException = false;

	//locatin, count: su dung de kiem tra locating
	private Location location;

	// kiem tra ung dung da thoat hay chua
	private boolean isExitApp = false;
	// kiem tra login thanh con chua
	private int loginState = 0;

	//time send location khi connection changed
	private long timeSendLogPosition = 0;
	private long timeToastUnstrustPosition = 0;
	//time send location khi dinh vi
	private long timeSendLogLocating = 0;
	//link hinh anh server product
	private String serverImageProductVNM = "";
	// so lan toi da request ghi log cua 1 chuc nang
	private int allowRequestLogKPINumber = 10;
	// so ngay can ban SKU khi huan luyen cua gsnpp
	private int numDaySKUOfGSNPP = 0;
	// cho reset lai vi tri cua khach hang cua gsnpp
	private int allowResetLocation = 0;
	//1 Cau hinh tru vao sysdate/lockdate: ACCOUNT_DATE = SYSDATE.
	//2 Cau hinh tru vao order_date: ACCOUNT_DATE = ORDER_DATE cua don goc.
	//3 Cau hinh tru vao approved_date: ACCOUNT_DATE = APPROVED_DATE cua don goc.
	private int sysReturnDate = 0;

	// kiem tra dong bo dau ngay valsale
//	String lastDateFirstSynData;

	// cho gs chup hinh trung bay
	private int trainingStaffId = -1;
	private int trainngDetailId = -1;
	//kiem tra trang thai khong cho phep ket noi (1:online; -1 offline)
	private int stateConnectionMode = 0;

	private static volatile SharedPreferences sharedPreferencesPrivateDms = null;

	//kiem tra tranh truong hop dang cipher co phat sinh insert vao log-table (dinh vi...)
	private static boolean inProcessCipher = false;
	private String lastAppsList;

	private int unitAmountDynamicReport = 0;
	//muc xem cac man hinh cua role quan li
	private int managerViewLevel = 0;
	//Cau hinh tong hop cac bang 1 da duyet, 3 chua duyet hay 2 ca 2
	private int sysCalUnapproved = 0;
	// cau hinh gia tri don vi nho nhat mac dinh = 1
	private int sysCurrencyDivide = 0;
	// cau hinh tong hop theo tuyen hay nhan vien hoac shop
	private int sysSaleRoute = 0;
	//danh sach id km sai
	private List<Long> lstWrongProgrameId = new ArrayList<Long>();
	private int deviceId = 0;
	//validate vi tri (1 validate, 0 validate)
	private int flagValidateLocation = -1;
	private String blackListMockLocation = null;
	private String whiteListMockLocation = null;
	//chu ky request fused position
	private int intervalFusedPosition = 0;
	// chu ky ngan nhat request fused position
	private int fastIntervalFusedPosition = 0;
	// ban kinh cho phep cua vi tri Fused
	private int radiusFusedPosition = 0;
	// do chinh xac cua request GPS Fused (1 chinh xac cao, 2 can bang)
	private int fusedPositionPriority = 0;
	//thoi gian toi da sai lech
	private int maxTimeWrong = 0;

	private RightTimeInfo rightTimeInfo = null;
	// duoc dang ky keyshop hay ko sau khi da duyet, mac dinh la 0
	private int registerApprovedKeyshop = 0;
	// mang ds dc phep cai dat vao
	private ArrayList<String> whiteListAppInstall = new ArrayList<String>();
	// thoi gian dong bo client len server
	private long timeLockAppBlocked = 0;
	// co check ung dung dc phep cai dat hay ko
	private Integer checkAppInstall ; // 1: khong duoc cai dat, 2: duoc cai dat
	// mang ds cac dung chan ko cho du sung
	//private ArrayList<String> blackListAppBlocked = new ArrayList<String>();
	// bien kiem tra de chi send log cac package ung dung mot lan khi dang nhap
	private boolean isFirstTimeSendApp = false;
	// co check ung dung dc phep cai dat hay ko
	private int checkNetWork = 0; // 1: 3G, 2: WIFI, 3: check ca 2, 4 khong check gi, 5: check offline
	//thoi gian toi da sai lech
	private int registerCTHTTM = 1; //1: duoc dang ki keyshop cho role GS, QL
	private int allowDistanceOrder = 1; //1: cho GS, QL duoc cho phep dat hang tu xa hay ko
	private int dayOfCycle = 28; //1: So ngay cua 1 chu ky
	private static boolean isChangeInfoCurrentCustomer = false;

	private boolean vtMapSetServer = false;
	private String vtMapProtocol = "";
	private String vtMapIP = "";
	private int vtMapPort = 0;

	private static Context context;


	public GlobalInfo() {
		super();
	}

	public static Context getContext() {
		return context;
	}

	public static void setContext(Context context) {
		GlobalInfo.context = context;
	}

	private Location currentBestLocation;


	@Override
	public void onCreate() {
		super.onCreate();
//		SQLiteDatabase.loadLibs(this);
		instance = this;
//		if (Build.VERSION.SDK_INT > 6) {

//		} else {
//			TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
//			DEVICE_IMEI = telephonyManager.getDeviceId();
//		}
	}

	@Override
	protected void attachBaseContext(Context base) {
		super.attachBaseContext(base);
	}


	public static GlobalInfo getInstance() {
		return instance;
	}


	public Context getAppContext() {
		return instance;
	}

	public void setActivityContext(Context context) {
		if (this.activityContext != context) {
			this.activityContext = context;
			alertDialog = new AlertDialog.Builder(context).create();
		}
	}

	public Context getActivityContext() {
		return activityContext;
	}

	public Handler getAppHandler() {
		return mHandler;
	}

	public AlertDialog getAlertDialog() {
		return alertDialog;
	}

	public StaffEntities getStaff() {
		return staff;
	}

	public void setStaff(StaffEntities staff) {
		this.staff = staff;
	}

	public long getStaffId() {
		if (this.staff == null || this.staff.getRole() == null) {
			return 0;
		}
		return this.staff.getRole().getId();
	}

	public String getStaffName() {
		if (this.staff == null || this.staff.getRole() == null) {
			return "";
		}
		return this.staff.getRole().getUsername();
	}

	public String getStaffFullName() {
		if (this.staff == null || this.staff.getRole() == null) {
			return "";
		}
		return this.staff.getRole().getFull_name();
	}

	private StoreEntities curStore;

	public StoreEntities getCurStore() {
		return curStore;
	}

	public void setCurStore(StoreEntities curStore) {
		this.curStore = curStore;
	}

	public Location getCurrentBestLocation() {
		return currentBestLocation;
	}

	public void setCurrentBestLocation(Location currentBestLocation) {
		this.currentBestLocation = currentBestLocation;
	}


	public static class RightTimeInfo {
		private String lastRightTime = "";
		private long lastRightTimeSinceBoot = 0;
		private String lastTimeOnlineLogin = "";

		public RightTimeInfo() {}

		public RightTimeInfo(RightTimeInfo rightTimeInfo) {
			this.setLastRightTime(rightTimeInfo.getLastRightTime());
			this.setLastRightTimeSinceBoot(rightTimeInfo.getLastRightTimeSinceBoot());
			this.setLastTimeOnlineLogin(rightTimeInfo.getLastTimeOnlineLogin());
		}

		@Override
		public RightTimeInfo clone(){
			RightTimeInfo obClone = new RightTimeInfo(this);
			return obClone;
		}

		@Override
		public String toString() {
			return String.format("lastRightTime : %s, lastRightTimeSinceBoot: %s, lastTimeOnlineLogin: %s", getLastRightTime(), getLastRightTimeSinceBoot(), getLastTimeOnlineLogin());
		}

		public String getLastRightTime() {
			return lastRightTime;
		}

		public void setLastRightTime(String lastRightTime) {
			this.lastRightTime = lastRightTime;
		}

		public long getLastRightTimeSinceBoot() {
			return lastRightTimeSinceBoot;
		}

		public void setLastRightTimeSinceBoot(long lastRightTimeSinceBoot) {
			this.lastRightTimeSinceBoot = lastRightTimeSinceBoot;
		}

		public String getLastTimeOnlineLogin() {
			return lastTimeOnlineLogin;
		}

		public void setLastTimeOnlineLogin(String lastTimeOnlineLogin) {
			this.lastTimeOnlineLogin = lastTimeOnlineLogin;
		}
	}

	private IDataValidate<String> strNullConfigValidation = new IDataValidate<String>() {
		@Override
		public boolean valid(String ob) {
			boolean isValid = (ob != null);
			return isValid;
		}
	};

	private IDataValidate<String> strConfigValidation = new IDataValidate<String>() {
		@Override
		public boolean valid(String ob) {
			boolean isValid = !StringUtil.isNullOrEmpty(ob);
			return isValid;
		}
	};

	private IDataValidate<Integer> intConfigValidation = new IDataValidate<Integer>() {
		@Override
		public boolean valid(Integer ob) {
			boolean isValid = (ob != null && ob > 0);
			return isValid;
		}
	};

	IDataValidate<Integer> intNotNegativeConfigValidation = new IDataValidate<Integer>() {
		@Override
		public boolean valid(Integer ob) {
			boolean isValid = (ob != null && ob >= 0);
			return isValid;
		}
	};

	IDataValidate<Long> longConfigValidation = new IDataValidate<Long>() {
		@Override
		public boolean valid(Long ob) {
			boolean isValid = (ob != null && ob > 0);
			return isValid;
		}
	};

	/**
	 * ban kinh cho phep sai so dinh vi
	 * @author: banghn
	 * @return
	 */
	public long getRadiusOfPosition() {
		if (!intConfigValidation.valid(this.radiusOfPosition)) {
			//mac dinh la 300m
			this.radiusOfPosition = GlobalUtil.readObject(RADIUS_OF_POSITION, 300, intConfigValidation);
		}
		return this.radiusOfPosition;
	}

	/**
	 * Ban kinh cho phep sai so dinh vi
	 * @author: banghn
	 * @param radiusOfPosition
	 * @param status
	 */
	public void setRadiusOfPosition(int radiusOfPosition, int status) {
		this.radiusOfPosition = status != 0 ? radiusOfPosition : 300;
		GlobalUtil.saveObject(this.radiusOfPosition, RADIUS_OF_POSITION);
	}



	public long getTimeTrigPositionAttendance() {
		if(timeTrigPositionAttendance < 120000){
			timeTrigPositionAttendance = 120000;
		}
		return timeTrigPositionAttendance;
	}

	public void setTimeTrigPositionAttendance(int timeTrigPositionAttendance, int status) {
		this.timeTrigPositionAttendance = status != 0 ? timeTrigPositionAttendance : 120000;
	}

	public long getDistanceAllowFinish() {
		if(distanceAllowFinish < 300)
			distanceAllowFinish = 300;
		return distanceAllowFinish;
	}

	public void setDistanceAllowFinish(int distanceAllowFinish, int status) {
		this.distanceAllowFinish = status != 0 ? distanceAllowFinish : 300;
	}

	public long getCheckDataNotSyn() {
		return checkDataNotSyn;
	}

	public void setCheckDataNotSyn(int checkDataNotSyn, int status) {
		this.checkDataNotSyn = status != 0 ? checkDataNotSyn : 1;
	}

	private StaffEntities staff;

	public static String getDeviceName(Context context) {
		SharedPreferences prefs = context.getSharedPreferences(Constants.PREFERENCES_NAME, Context.MODE_PRIVATE);
		if (prefs != null) {
			String full_name = prefs.getString("device_name", "");
			return full_name;
		}
		return null;
	}



//
//	/**
//	 * set app co dang active hay ko
//	 * @author : DoanDM since : 10:12:47 AM
//	 */
//	public void setAppActive(boolean isActive) {
//
//		this.isAppActive = isActive;
//	}
//
//	/**
//	 * get thong tin activte cua app
//	 * @author : DoanDM since : 10:12:58 AM
//	 */
//	public boolean isAppActive() {
//
//		return isAppActive;
//	}
//
//	/**
//	*  Cap nhat current tag
//	*  @author: TruongHN
//	*  @param tag
//	*  @return: void
//	*  @throws:
//	 */
//	public void setCurrentTag(String tag) {
//		this.listStackTag.add(tag);
//		GlobalUtil.saveObject(this.listStackTag, VNM_CURRENT_TAG);
//	}
//
//	/**
//	*  Lay tag hien tai
//	*  @author: TruongHN
//	*  @return: String
//	*  @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public String getCurrentTag(){
//		String currentTag = "";
//		Object temp;
//		if (this.listStackTag == null ) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_CURRENT_TAG)) != null) {
//				this.listStackTag = (ArrayList<String>) temp;
//			}
//		}
//		if (this.listStackTag.size() > 0) {
//			currentTag = this.listStackTag.get(this.listStackTag.size() - 1);
//		}
//		return currentTag;
//	}
//
//	/**
//	 * Add vi tri vao danh sach luu tru
//	 * @author: TruongHN
//	 * @param tag
//	 * @return: void
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public void addPosition(StaffPositionLogDTO pos){
//		Object temp;
//		if (this.listPositionOffline == null || this.listPositionOffline.size() <= 0) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_LIST_POSITION)) != null) {
//				this.listPositionOffline = (ArrayList<StaffPositionLogDTO>) temp;
//			}
//		}
//		this.listPositionOffline.add(pos);
//		GlobalUtil.saveObject(this.listPositionOffline,VNM_LIST_POSITION);
//	}
//
//	/**
//	 * Lay ds vi tri luu tru offline
//	 * @author: TruongHN
//	 * @return: ArrayList<LatLng>
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<StaffPositionLogDTO> getListPositionOffline(){
//		ArrayList<StaffPositionLogDTO> listPos = new ArrayList<StaffPositionLogDTO>();
//		Object temp;
//		if (this.listPositionOffline == null || this.listPositionOffline.size() <= 0) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_LIST_POSITION)) != null) {
//				this.listPositionOffline = (ArrayList<StaffPositionLogDTO>) temp;
//			}
//		}
//		if (this.listPositionOffline.size() > 0){
//			listPos = (ArrayList<StaffPositionLogDTO>)this.listPositionOffline.clone();
//		}
//		return listPos;
//	}
//
//	/**
//	 * Reset lai list vi tri
//	 * @author: TruongHN
//	 * @return: void
//	 * @throws:
//	 */
//	public void clearListPosition(){
//		if (this.listPositionOffline != null){
//			this.listPositionOffline.clear();
//			GlobalUtil.saveObject(this.listPositionOffline,VNM_LIST_POSITION);
//		}
//	}
//
//
//	/**
//	 * Add thong tin vi tri nhan vien (luu su dung muc dich
//	 * @author: banghn
//	 * @param pos
//	 */
//	@SuppressWarnings("unchecked")
//	public void addStaffPosition(StaffPositionLogDTO pos){
//		Object temp;
//		if (this.staffPosition == null || this.staffPosition.size() <= 0) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_STAFF_POSITION)) != null) {
//				this.staffPosition = (ArrayList<StaffPositionLogDTO>) temp;
//			}
//		}
//		this.staffPosition.add(0, pos);
//		GlobalUtil.saveObject(this.staffPosition,VNM_STAFF_POSITION);
//	}
//
//	/**
//	 * Clear danh sach vi tri nhan vien
//	 * @author: banghn
//	 */
//	public void clearStaffPosition(){
//		if(this.staffPosition != null){
//			this.staffPosition.clear();
//			GlobalUtil.saveObject(this.staffPosition,VNM_STAFF_POSITION);
//		}
//	}
//
//	/**
//	 * Lay dach sach toa do cua nhan vien
//	 * @author: banghn
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<StaffPositionLogDTO> getStaffPosition(){
//		ArrayList<StaffPositionLogDTO> listPos = new ArrayList<StaffPositionLogDTO>();
//		Object temp;
//		if (this.staffPosition == null || this.staffPosition.size()<= 0) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_STAFF_POSITION)) != null) {
//				this.staffPosition = (ArrayList<StaffPositionLogDTO>) temp;
//			}
//		}
//		if (this.staffPosition.size() > 0){
//			listPos = (ArrayList<StaffPositionLogDTO>)this.staffPosition.clone();
//		}
//		return listPos;
//	}
//
//	/**
//	 * Lay dach sach toa do cua nhan vien tu thoi diem startTime
//	 * @author: banghn
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<StaffPositionLogDTO> getStaffPosition(String startTime){
//		ArrayList<StaffPositionLogDTO> listPos = new ArrayList<StaffPositionLogDTO>();
//		Object temp;
//		if (this.staffPosition == null || this.staffPosition.size()<= 0) {
//			if ((temp = GlobalUtil.readObject(
//					VNM_STAFF_POSITION)) != null) {
//				this.staffPosition = (ArrayList<StaffPositionLogDTO>) temp;
//			}
//		}
//		if (this.staffPosition.size() > 0){
//			for(StaffPositionLogDTO dto : staffPosition){
//				if(DateUtils.compareFullDate(dto.createDate, startTime) >= 0){
//					listPos.add(dto);
//				}else{
//					break;
//				}
//			}
//		}
//		return listPos;
//	}
//
//
//	/**
//	 * Lay trang thai login state
//	 * @author: banghn
//	 * @return
//	 */
//	public int getLoginState() {
//		Object temp;
//		try {
//			if (this.loginState <= 0) {
//				if ((temp = GlobalUtil.readObject(VNM_LOGIN_STATE)) != null) {
//					this.loginState = (Integer) temp;
//				}
//			}
//		} catch (Exception e) {
//		}
//		return this.loginState;
//	}
//
//	public void setLoginState(int loginState) {
//		this.loginState = loginState;
//		GlobalUtil.saveObject(this.loginState, VNM_LOGIN_STATE);
//	}
//
//
//	public void setPositionCustomerVisiting(LatLng position){
//		this.positionCustomerVisiting = position;
//	}
//
//	public LatLng getPositionCustomerVisiting(){
//		//co vi tri tri thi tra ve vi tri ko thi tra null
//		if (positionCustomerVisiting != null
//				&& positionCustomerVisiting.lat > 0
//				&& positionCustomerVisiting.lng > 0) {
//			return positionCustomerVisiting;
//		} else {
//			return null;
//		}
//	}
//
//
//	/**
//	 * Pop stack
//	 * @author: TruongHN
//	 * @return: void
//	 * @throws:
//	 */
//	public void popCurrentTag(){
//		if (this.listStackTag.size() > 0){
//			this.listStackTag.remove(this.listStackTag.size() -1);
//			GlobalUtil.saveObject(this.listStackTag,VNM_CURRENT_TAG);
//		}
//	}
//
//	/**
//	 * Pop all stack
//	 * @author: TruongHN
//	 * @return: void
//	 * @throws:
//	 */
//	public void popAllTag(){
//		this.listStackTag.clear();
//		GlobalUtil.saveObject(this.listStackTag,VNM_CURRENT_TAG);
//	}
//
//
	public Location getLoc() {
		return location;
	}

	public void setLoc(Location loc) {
		this.location = loc;
	}
//
//
//	public StatusNotificationHandler getStatusNotifier() {
//		if (statusNotifier == null) {
//			statusNotifier = new StatusNotificationHandler();
//		}
//		return statusNotifier;
//	}
//
//	public void setStatusNotification(StatusNotificationHandler vl) {
//		statusNotifier = vl;
//	}
//
//
//	/**
//	 * set thoi gian tao hash map KPI
//	 * @author: trungnt56
//	 * @param: @param timeCreateHashMapKPI
//	 * @return: void
//	 * @throws:
//	 */
//	public void setTimeCreateHashMapKPI(Calendar timeCreateHashMapKPI) {
//		GlobalUtil
//				.saveObject(timeCreateHashMapKPI, VNM_TIME_CREATE_HASHMAP_KPI);
//	}
//
//
//	/**
//	 * lay thoi gian tao hash map kpi lon nhat
//	 * @author: trungnt56
//	 * @param: @return
//	 * @return: Calendar
//	 * @throws:
//	 */
//	public Calendar getTimeCreateHashMapKPI() {
//		Object temp;
//		try {
//			if ((temp = GlobalUtil.readObject(VNM_TIME_CREATE_HASHMAP_KPI)) != null) {
//				return (Calendar) temp;
//			}
//		} catch (Exception e) {
//			 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//		return null;
//	}
//
//	/**
//	 * lay hashmap Kpi
//	 * @author: trungnt56
//	 * @param: @return
//	 * @return: SparseIntArray
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public HashMap<Integer, Integer> getHashMapKPI() {
//		Object temp;
//		if (this.hashMapKPI == null) {
//			try {
//				if ((temp = GlobalUtil.readObject(VNM_HASHMAP_KPI)) != null) {
//					this.hashMapKPI = (HashMap<Integer, Integer>) temp;
//				} else {
//					this.hashMapKPI = createHashMapKPI();
//				}
//			} catch (Exception e) {
//				 MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//			}
//		}
//		return this.hashMapKPI;
//	}
//
//
//	/**
//	 * cap nhat hash map kpi key
//	 * @author: trungnt56
//	 * @param: @param key
//	 * @param: @param value
//	 * @return: void
//	 * @throws:
//	 */
//	public void putHashMapKPI(int key, int value) {
//		if (this.getHashMapKPI() != null) {
//			this.getHashMapKPI().put(key, value);
//			GlobalUtil.saveObject(this.getHashMapKPI(), VNM_HASHMAP_KPI);
//		}
//	}
//
//	/**
//	 * reset hashmap kpi
//	 * @author: trungnt56
//	 * @param:
//	 * @return: void
//	 * @throws:
//	 */
//	public void resetHashMapKPI() {
//		try {
//			Calendar calendar = Calendar.getInstance();
//			calendar.set(Calendar.HOUR_OF_DAY, 0);
//			calendar.set(Calendar.MINUTE, 0);
//			calendar.set(Calendar.SECOND, 0);
//			calendar.set(Calendar.MILLISECOND, 0);
//			Calendar timeCreateHashMapKPI = getTimeCreateHashMapKPI();
//			HashMap<Integer, Integer> hashMapOld = getHashMapKPI();
//			if (hashMapOld == null
//					|| hashMapOld.size() != HashMapKPI.values().length
//					|| timeCreateHashMapKPI == null
//					|| timeCreateHashMapKPI.compareTo(calendar) < 0) {
//				setTimeCreateHashMapKPI(calendar);
//
//				// tao hashmap
//				this.setHashMapKPI(createHashMapKPI());
//				GlobalUtil.saveObject(this.getHashMapKPI(), VNM_HASHMAP_KPI);
//			}
//		} catch (Exception exception) {
//			MyLog.e("trungnt", exception.getMessage());
//		}
//	}
//
//	/**
//	 * tao hashmapKPI
//	 * @author: trungnt56
//	 * @param: @return
//	 * @return: HashMap<Integer,Integer>
//	 * @throws:
//	 */
//	@SuppressLint("UseSparseArrays")
//	private HashMap<Integer, Integer> createHashMapKPI() {
//		HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
//		for (HashMapKPI hashMapKPIEnum : HashMapKPI.values()) {
//			hashMap.put(hashMapKPIEnum.ordinal(), 0);
//		}
//		return hashMap;
//	}
//
//
//	/**
//	 * set so lan cho phep request ghi log KPI
//	 * @author: trungnt56
//	 * @param status
//	 * @param: @param allowRequestLogKPINumber
//	 * @return: void
//	 * @throws:
//	 */
//	public void setAllowRequestLogKPINumber(int allowRequestLogKPINumber, int status) {
//		this.allowRequestLogKPINumber = status != 0 ? allowRequestLogKPINumber : 10;
//	}
//
//	/**
//	 * lay so lan cho phep request ghi log KPI
//	 * @author: trungnt56
//	 * @param: @return
//	 * @return: int
//	 * @throws:
//	 */
//	public int getAllowRequestLogKPINumber() {
//		if (this.allowRequestLogKPINumber <= 0) {
//			return 10;
//		}
//		return this.allowRequestLogKPINumber;
//	}
//
//	/**
//	 * Add kpi log vao danh sach luu tru
//	 * @author: trungnt56
//	 * @param: @param kpiLog
//	 * @return: void
//	 * @throws:
//	 */
//	public void addKPILog(KPILogDTO kpiLog) {
//		this.listKPILogOffline.add(kpiLog);
//		GlobalUtil.saveObject(this.listKPILogOffline, VNM_LIST_KPI_LOG);
//	}
//
//	/**
//	 * lay danh sach luu tru log KPI offline
//	 * @author: trungnt56
//	 * @param: @return
//	 * @return: ArrayList<KPILogDTO>
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<KPILogDTO> getListKPILogOffline() {
//		ArrayList<KPILogDTO> listKPILog = new ArrayList<KPILogDTO>();
//		Object temp;
//		if (this.listKPILogOffline == null) {
//			if ((temp = GlobalUtil.readObject(VNM_LIST_KPI_LOG)) != null) {
//				this.listKPILogOffline = (ArrayList<KPILogDTO>) temp;
//			}
//		}
//		if (this.listKPILogOffline.size() > 0) {
//			listKPILog = (ArrayList<KPILogDTO>) this.listKPILogOffline.clone();
//		}
//		return listKPILog;
//	}
//
//	/**
//	 * reset lai kpi log list offline
//	 * @author: trungnt56
//	 * @param:
//	 * @return: void
//	 * @throws:
//	 */
//	public void clearLogKPI(){
//		if (this.listKPILogOffline != null){
//			this.listKPILogOffline.clear();
//			GlobalUtil.saveObject(this.listKPILogOffline,VNM_LIST_KPI_LOG);
//		}
//	}
//
//	/**
//	 * so ngay can ban SKU khi huan luyen
//	 * @author: banghn
//	 * @return
//	 */
//	public int getNumDaySKUOfGSNPP() {
//		Object temp;
//		try {
//			if (this.numDaySKUOfGSNPP <= 0) {
//				if ((temp = GlobalUtil.readObject(VNM_NUM_DAY_SKU)) != null) {
//					this.numDaySKUOfGSNPP = (Integer) temp;
//				}
//			}
//		} catch (Exception e) {
//		}
//		if(this.numDaySKUOfGSNPP <= 0){
//			this.numDaySKUOfGSNPP = 30;
//		}
//		return this.numDaySKUOfGSNPP;
//	}
//
//
//	public void setNumDaySKUOfGSNPP(int numDaySKUOfGSNPP, int status) {
//		this.numDaySKUOfGSNPP = status != 0 ? numDaySKUOfGSNPP : 30;
//		GlobalUtil.saveObject(this.numDaySKUOfGSNPP, VNM_NUM_DAY_SKU);
//	}
//
//	public boolean isAllowResetLocation() {
//		if (!intConfigValidation.valid(this.allowResetLocation)) {
//			//mac dinh la 2 khong cho reset
//			this.allowResetLocation = GlobalUtil.readObject(VNM_ALLOW_RESET_LOCATION, 2, intConfigValidation);
//		}
//		return (allowResetLocation == 1);
//	}
//
//	public void setAllowResetLocation(int allowResetLocation) {
//		this.allowResetLocation = allowResetLocation;
//		GlobalUtil.saveObject(this.allowResetLocation, VNM_ALLOW_RESET_LOCATION);
//	}
//
//	/**
//	 * Mo ta muc dich cua ham
//	 * @author: DungNX
//	 * @return
//	 * @return: StockLockDTO
//	 * @throws:
//	*/
//	public LockDateDTO getLockDateValsale() {
//		return lockDateValsale;
//	}
//
//	/**
//	 * Mo ta muc dich cua ham
//	 * @author: DungNX
//	 * @param lockDateValsale
//	 * @return: void
//	 * @throws:
//	*/
//	public void setLockDateValsale(LockDateDTO lockDateValsale) {
//		this.lockDateValsale = lockDateValsale;
//	}
//
//	/**
//	 * Mo ta muc dich cua ham
//	 * @author: DungNX
//	 * @return
//	 * @return: ShopLockDTO
//	 * @throws:
//	*/
//	public ShopLockDTO getShopLockDto() {
//		return shopLockDto;
//	}
//
//	/**
//	 * Mo ta muc dich cua ham
//	 * @author: DungNX
//	 * @param shopLockDto
//	 * @return: void
//	 * @throws:
//	*/
//	public void setShopLockDto(ShopLockDTO shopLockDto) {
//		this.shopLockDto = shopLockDto;
//	}
//	/**
//	 * getDmsPrivateSharePreference
//	 * @author: yennth16
//	 * @since: 11:09:46 04-09-2014
//	 * @return: SharedPreferences
//	 * @throws:
//	 * @return
//	 */
//	public SharedPreferences getDmsPrivateSharePreference(){
//		if (sharedPreferencesPrivateDms == null) {
//			sharedPreferencesPrivateDms = new SecurePreferences(LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
//		}
//		return sharedPreferencesPrivateDms;
//	}
//
//	/**
//	 * @return the priUtilsInstance
//	 */
//	public PriUtils getPriUtilsInstance() {
//		return priUtilsInstance;
//	}
//
//	/**
//	 * @param priUtilsInstance the priUtilsInstance to set
//	 */
//	public void setPriUtilsInstance(PriUtils priUtilsInstance) {
//		this.priUtilsInstance = priUtilsInstance;
//	}
//
//	/**
//	 * luu doi tuong PriUtils truoc khi bi huy
//	 * @author: DungNX
//	 * @return: void
//	 * @throws:
//	*/
//	public void savePriUtilsInstance(){
//		GlobalUtil.saveObject(this.priUtilsInstance, VNM_PRIUTILS_INSTANCE);
//	}
//
//	/**
//	 * lay lai doi tuong sau khi bi huy
//	 * @author: DungNX
//	 * @return
//	 * @return: boolean
//	 * @throws:
//	*/
//	public boolean restorePriUtilsInstance(){
//		Object temp;
//		boolean isRestoreSuccess = false;
//		try {
//			if ((temp = GlobalUtil.readObject(VNM_PRIUTILS_INSTANCE)) != null) {
//				this.priUtilsInstance = (PriUtils) temp;
//				isRestoreSuccess = true;
//			}
//		} catch (Exception e) {
//			isRestoreSuccess = false;
//		}
//		return isRestoreSuccess;
//	}
//
//	/**
//	 * get Flag Keyboard Custom
//	 * @author: duongdt3
//	 * @since: 10:32:48 12 Nov 2014
//	 * @return: long 1 keyboard he thong, 2 custom keyboard
//	 * @throws:
//	 * @return
//	 */
//	public long getFlagKeyboard() {
//		if (!intConfigValidation.valid(this.flagKeyboard)) {
//			//mac dinh la 2 keyboard custom
//			this.flagKeyboard = GlobalUtil.readObject(VNM_FLAG_KEYBOARD, 2, intConfigValidation);
//		}
//		return this.flagKeyboard;
//	}
//
//	/**
//	 * set Flag Keyboard Custom
//	 * @author: duongdt3
//	 * @since: 10:32:33 12 Nov 2014
//	 * @return: void
//	 * @throws:
//	 * @param flagKeyboard
//	 * @param status
//	 */
//	public void setFlagKeyboard(int flagKeyboard, int status) {
//		this.flagKeyboard = status != 0 ? flagKeyboard : 2;
//		GlobalUtil.saveObject(this.flagKeyboard, VNM_FLAG_KEYBOARD);
//	}
//
//	/**
//	 * Kiem tra su dung ban phim custom
//	 * @return
//	 */
//	public boolean isUsingCustomKeyboard() {
//		return (GlobalInfo.getInstance().getFlagKeyboard() == 2);
//	}
//
//	/**
//	 * Luu thong tin ds role
//	 *
//	 * @author: Tuanlt11
//	 * @param lstRole
//	 * @return: void
//	 * @throws:
//	 */
//	public void saveRoleManged(ArrayList<RoleUserDTO> lstRole) {
//		GlobalUtil.saveObject(lstRole, VNM_ROLE_MANAGED);
//	}
//
//	/**
//	 * Lay thong tin ds role
//	 *
//	 * @author: TruongHN
//	 * @return: ArrayList<LatLng>
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<RoleUserDTO> getRoleManaged() {
//		Object temp;
//		if ((temp = GlobalUtil.readObject(VNM_ROLE_MANAGED)) != null) {
//			return (ArrayList<RoleUserDTO>) temp;
//		}
//		return new ArrayList<RoleUserDTO>();
//	}
//
//	/**
//	 * Load ngon ngu hien tai
//	 *
//	 * @author: dungdq3
//	 * @since: 11:38:31 AM Jul 2, 2014
//	 * @return: void
//	 * @throws:
//	 */
//	public LanguagesDTO loadLanguages() {
//		// TODO Auto-generated method stub
//		LanguagesDTO dto = null;
//		LanguagesDTO dtoDefault = null;
//		String languageDefault = "vi";
//		//Context con = getApplicationContext();
//		SharedPreferences sharedPreferences = getSharedPreferences(LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
//		String nameLanguageLoad = sharedPreferences.getString(DMS_LANGUAGE, "");
//		if (StringUtil.isNullOrEmpty(nameLanguageLoad)) {
//			nameLanguageLoad = Locale.getDefault().getLanguage();
//		}
//
//		ArrayList<LanguagesDTO> listLanguage = getListLanguage();
//
//
//		for (LanguagesDTO languageDTO : listLanguage) {
//			if (languageDTO.getValue().equals(nameLanguageLoad)) {
//				dto = languageDTO;
//				break;
//			}
//
//			//lay dto en
//			if (languageDTO.getValue().equals(languageDefault)) {
//				dtoDefault = languageDTO;
//			}
//		}
//
//		//neu ko tim thay dto tuong ung, va co ngon ngu mac dinh thi su dung mac dinh
//		if (dto == null && dtoDefault != null) {
//			dto = dtoDefault;
//			//cap nhat lai ngon ngu mac dinh
//			//saveLanguage(dto.name);
//		}
//		return dto;
//	}
//
//	/**
//	 * Khoi tao danh sach ngon ngu
//	 *
//	 * @author: dungdq3
//	 * @since: 11:41:20 AM Jul 2, 2014
//	 * @return: List<LanguagesDTO>
//	 * @throws:
//	 * @return
//	 */
//	public ArrayList<LanguagesDTO> getListLanguage() {
//		// TODO Auto-generated method stub
//		ArrayList<LanguagesDTO> listLang = new ArrayList<LanguagesDTO>();
//		LanguagesDTO langVN = new LanguagesDTO(StringUtil.getString(R.string.TEXT_LANGUAGE_NAME_VI), "vi", R.drawable.icon_nation_vietnam);
//		LanguagesDTO langEN = new LanguagesDTO(StringUtil.getString(R.string.TEXT_LANGUAGE_NAME_EN), "en", R.drawable.icon_nation_united_kingdom);
//		listLang.add(langVN);
//		listLang.add(langEN);
//		return listLang;
//	}
//
//
//    /**
//     * Luu ngon ngu hien tai
//     * @author: duongdt3
//     * @since: 1.0
//     * @time: 12:58:27 19 Jun 2014
//     * @return: void
//     * @throws:
//     * @param lang
//     */
//    public void saveLanguages(String languageName){
//    	updateLanguageConfig(languageName);
//    	//cap nhat lai ngon ngu nay
//    	SharedPreferences sharedPreferences = getSharedPreferences(
//				LoginView.PREFS_PRIVATE, Context.MODE_PRIVATE);
//		Editor prefsPrivateEditor = sharedPreferences.edit();
//		prefsPrivateEditor.putString(DMS_LANGUAGE, languageName);
//		prefsPrivateEditor.commit();
//    }
//
//
//    /**
//     * Cap nhat config ngon ngu
//     * @author: duongdt3
//     * @since: 1.0
//     * @time: 15:12:53 30 Jun 2014
//     * @return: void
//     * @throws:
//     * @param languageName
//     */
//    private void updateLanguageConfig(String languageName){
//    	Locale loc = new Locale(languageName);
//    	//Locale.setDefault(loc);
//    	Configuration config = new Configuration();
//    	config.locale = loc;
//    	getApplicationContext().getResources().updateConfiguration(config, null);
//    	getBaseContext().getResources().updateConfiguration(config, null);
//    	if (getActivityContext() != null) {
//    		getActivityContext().getResources().updateConfiguration(config, null);
//    		((Activity)getActivityContext()).getBaseContext().getResources().updateConfiguration(config, null);
//		}
//    }
//
//	@Override
//	public void onConfigurationChanged(Configuration newConfig) {
//		//neu thay doi config co kem thay doi Locale, khong giong voi Locale hien tai, thi phai doi lai
//		//truong hop xoay man hinh...
////		if (newConfig.locale != null) {
////			LanguagesDTO dtoLang = loadLanguages();
////			if (dtoLang != null && !newConfig.locale.equals(dtoLang.getValue())) {
////				updateLanguageConfig(dtoLang.getValue());
////			}
////		}
//		super.onConfigurationChanged(newConfig);
//	}
//
//	 /**
//	 * set don vi tinh doanh so man hinh bao cao dong
//	 * @author: Tuanlt11
//	 * @param timeSyncToServer
//	 * @return: void
//	 * @throws:
//	*/
//	public void setUnitAmountDynamicReport(int unitAmountDynamicReport) {
//		this.unitAmountDynamicReport = unitAmountDynamicReport;
//		GlobalUtil.saveObject(this.unitAmountDynamicReport,VNM_UNIT_AMOUNT_DYNAMIC_REPORT);
//	}
//
//	 /**
//	 * get don vi tinh doanh so man hinh bao cao dong
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: long
//	 * @throws:
//	*/
//	public long getUnitAmountDynamicReport() {
//		if (!intConfigValidation.valid(this.unitAmountDynamicReport)) {
//			//mac dinh la 1000
//			this.unitAmountDynamicReport = GlobalUtil.readObject(VNM_UNIT_AMOUNT_DYNAMIC_REPORT, 1000, intConfigValidation);
//		}
//		return this.unitAmountDynamicReport;
//	}
//
//	/**set loai ban do se dung
//	 * @author cuonglt3
//	 * @param mapType
//	 * @param status
//	 */
//	public void setMapTypeIsUse(int mapType, int status) {
//		this.mapType = status != 0 ? mapType : 1;
//		GlobalUtil.saveObject(this.mapType, VNM_MAP_TYPE);
//	}
//
//	/**lay loai ban do se dung
//	 * @return
//	 */
//	public long getMapTypeIsUse(){
//		if (!intConfigValidation.valid(this.mapType)) {
//			//mac dinh la 1 Viettel Map
//			this.mapType = GlobalUtil.readObject(VNM_MAP_TYPE, 1, intConfigValidation);
//		}
//		return this.mapType;
//	}
//
//	 /**
//	 * set muc xem cac man hinh cua cap quan li
//	 * @author: Tuanlt11
//	 * @param managerViewLevel
//	 * @param status
//	 * @return: void
//	 * @throws:
//	*/
//	public void setManagerViewLevel(int managerViewLevel, int status) {
//		this.managerViewLevel = status != 0 ? managerViewLevel : 3;
//		GlobalUtil.saveObject(this.managerViewLevel, VNM_MANAGER_VIEW_LEVEL);
//	}
//
//	 /**
//	 * Lay muc xem cac man hinh cua cap quan li
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: int
//	 * @throws:
//	*/
//	public int getManagerViewLevel(){
//		if (!intConfigValidation.valid(this.managerViewLevel)) {
//			//mac dinh la 3 level
//			this.managerViewLevel = GlobalUtil.readObject(VNM_MANAGER_VIEW_LEVEL, 3, intConfigValidation);
//		}
//		return this.managerViewLevel;
//	}
//
//	/**
//	 *  Set Cau hinh tong hop cac bang da duyet, chua duyet hay ca 2
//	 * @author: hoanpd1
//	 * @param status
//	 * @since: 16:07:50 20-03-2015
//	 * @return: void
//	 * @throws:
//	 * @param valueOf
//	 */
//	public void setSysCalUnapproved(int sysCalUnapproved, int status) {
//		this.sysCalUnapproved = status != 0 ? sysCalUnapproved : ApParamDTO.SYS_CAL_UNAPPROVED;
//		GlobalUtil.saveObject(this.sysCalUnapproved, VNM_SYS_CAL_UNAPPROVED);
//	}
//
//	/**
//	 * lay Cau hinh tong hop cac bang da duyet, chua duyet hay ca 2
//	 * @author: hoanpd1
//	 * @since: 16:11:43 20-03-2015
//	 * @return: int
//	 * @throws:
//	 * @return
//	 */
//	public int getSysCalUnapproved(){
//		if (!intConfigValidation.valid(this.sysCalUnapproved)) {
//			//mac dinh la 2 ca 2
//			this.sysCalUnapproved = GlobalUtil.readObject(VNM_SYS_CAL_UNAPPROVED, ApParamDTO.SYS_CAL_UNAPPROVED, intConfigValidation);
//		}
//		return this.sysCalUnapproved;
//	}
//
//	/**
//	 * cau hinh don vi tinh
//	 * @author: hoanpd1
//	 * @since: 17:57:17 28-03-2015
//	 * @return: void
//	 * @throws:
//	 * @param sysCurrencyDivide
//	 * @param status
//	 */
//	public void setSysCurrencyDivide(int sysCurrencyDivide, int status) {
//		this.sysCurrencyDivide = status != 0 ? sysCurrencyDivide : 1;
//		// neu tren DB set = 0 thi set lai = 1 tranh phep chia bi loi
//		if(this.sysCurrencyDivide == 0 ){
//			this.sysCurrencyDivide = 1;
//		}
//		GlobalUtil.saveObject(this.sysCurrencyDivide, VNM_SYS_CURRENCY_DIVIDE);
//	}
//
//	/**
//	 * cau hinh don vi tinh
//	 * @author: hoanpd1
//	 * @since: 17:57:03 28-03-2015
//	 * @return: int
//	 * @throws:
//	 * @return
//	 */
//	public int getSysCurrencyDivide(){
//		if (!intConfigValidation.valid(this.sysCurrencyDivide)) {
//			//mac dinh la 1
//			this.sysCurrencyDivide = GlobalUtil.readObject(VNM_SYS_CURRENCY_DIVIDE, 1, intConfigValidation);
//		}
//		return this.sysCurrencyDivide;
//	}
//
//	/**
//	 * @return the sysNumRounding
//	 */
//	public int getSysNumRounding() {
//		if (!intConfigValidation.valid(this.sysNumRounding)) {
//			//mac dinh la 3 lam tron tu nhien
//			this.sysNumRounding = GlobalUtil.readObject(VNM_SYS_NUM_ROUNDING, 3, intConfigValidation);
//		}
//		return sysNumRounding;
//	}
//
//	/**
//	 * @param sysNumRounding the sysNumRounding to set
//	 */
//	public void setSysNumRounding(int sysNumRounding, int status) {
//		this.sysNumRounding = status != 0 ? sysNumRounding : 3;
//		GlobalUtil.saveObject(this.sysNumRounding, VNM_SYS_NUM_ROUNDING);
//	}
//
//	/**
//	 * @return the sysNumRoundingPromotion
//	 */
//	public int getSysNumRoundingPromotion() {
//		if (!intConfigValidation.valid(this.sysNumRoundingPromotion)) {
//			//mac dinh la 3 lam tron tu nhien
//			this.sysNumRoundingPromotion = GlobalUtil.readObject(VNM_SYS_NUM_ROUNDING_PROMOTION, 3, intConfigValidation);
//		}
//		return sysNumRoundingPromotion;
//	}
//
//	/**
//	 * @param sysNumRoundingPromotion the sysNumRoundingPromotion to set
//	 */
//	public void setSysNumRoundingPromotion(int sysNumRoundingPromotion, int status) {
//		this.sysNumRoundingPromotion = status != 0 ? sysNumRoundingPromotion : 3;
//		GlobalUtil.saveObject(this.sysNumRoundingPromotion, VNM_SYS_NUM_ROUNDING_PROMOTION);
//	}
//
//	/**
//	 * @return the sysCurrency
//	 */
//	public String getSysCurrency() {
//		if (!strConfigValidation.valid(this.sysCurrency)) {
//			//mac dinh la VND
//			this.sysCurrency = GlobalUtil.readObject(VNM_SYS_CURRENCY, "VND", strConfigValidation);
//		}
//		return sysCurrency;
//	}
//
//	/**
//	 * @param sysCurrency the sysCurrency to set
//	 * @param status
//	 */
//	public void setSysCurrency(String sysCurrency, int status) {
//		this.sysCurrency = status != 0 ? sysCurrency : "VND";
//		GlobalUtil.saveObject(this.sysCurrency, VNM_SYS_CURRENCY);
//	}
//
//	/**
//	 * @return the sysDigitDecimal
//	 */
//	public int getSysDigitDecimal() {
//		if (!intNotNegativeConfigValidation.valid(this.sysDigitDecimal)) {
//			//mac dinh la 2 so
//			this.sysDigitDecimal = GlobalUtil.readObject(VNM_SYS_DIGIT_DECIMAL, 2, intNotNegativeConfigValidation);
//		}
//		return sysDigitDecimal;
//	}
//
//	/**
//	 * @param sysDigitDecimal the sysDigitDecimal to set
//	 * @param status
//	 */
//	public void setSysDigitDecimal(int sysDigitDecimal, int status) {
//		this.sysDigitDecimal = status != 0 ? sysDigitDecimal : 2;
//		GlobalUtil.saveObject(this.sysDigitDecimal, VNM_SYS_DIGIT_DECIMAL);
//	}
//
//	/**
//	 * @return the sysDecimalPoint
//	 */
//	public int getSysDecimalPoint() {
//		if (!intConfigValidation.valid(this.sysDecimalPoint)) {
//			//mac dinh la 2 dau cham
//			this.sysDecimalPoint = GlobalUtil.readObject(VNM_SYS_DECIMAL_POINT, 2, intConfigValidation);
//		}
//		return sysDecimalPoint;
//	}
//
//	/**
//	 * @param sysDecimalPoint the sysDecimalPoint to set
//	 * @param status
//	 */
//	public void setSysDecimalPoint(int sysDecimalPoint, int status) {
//		this.sysDecimalPoint = status != 0 ? sysDecimalPoint : 2;
//		GlobalUtil.saveObject(this.sysDecimalPoint, VNM_SYS_DECIMAL_POINT);
//	}
//
//	/**
//	 * @return the sysDateFormat
//	 */
//	public String getSysDateFormat() {
//		if (!strConfigValidation.valid(this.sysDateFormat)) {
//			this.sysDateFormat = GlobalUtil.readObject(VNM_SYS_DATE_FORMAT,
//					DateUtils.DATE_FORMAT_DATE_VN, strConfigValidation);
//		}
//		return this.sysDateFormat;
//	}
//
//	/**
//	 * @param sysDateFormat the sysDateFormat to set
//	 * @param status
//	 */
//	public void setSysDateFormat(String sysDateFormat, int status) {
//		this.sysDateFormat = status != 0 ? sysDateFormat : DateUtils.DATE_FORMAT_DATE_VN;
//		GlobalUtil.saveObject(this.sysDateFormat, VNM_SYS_DATE_FORMAT);
//	}
//
//	/**
//	 * @return the sysMaxdayApprove
//	 */
//	public int getSysMaxdayApprove() {
//		if (!intNotNegativeConfigValidation.valid(this.sysMaxdayApprove)) {
//			//mac dinh la 3 ngay
//			this.sysMaxdayApprove = GlobalUtil.readObject(VNM_SYS_MAXDAY_APPROVE, 3, intNotNegativeConfigValidation);
//		}
//		return sysMaxdayApprove;
//	}
//
//	/**
//	 * @param sysMaxdayApprove the sysMaxdayApprove to set
//	 * @param status
//	 */
//	public void setSysMaxdayApprove(int sysMaxdayApprove, int status) {
//		this.sysMaxdayApprove = status != 0 ? sysMaxdayApprove : 3;
//		GlobalUtil.saveObject(this.sysMaxdayApprove, VNM_SYS_MAXDAY_APPROVE);
//	}
//
//	/**
//	 * @return the sysMaxdayReturn
//	 */
//	public int getSysMaxdayReturn() {
//		if (!intNotNegativeConfigValidation.valid(this.sysMaxdayReturn)) {
//			//mac dinh la 3 ngay
//			this.sysMaxdayReturn = GlobalUtil.readObject(VNM_SYS_MAXDAY_RETURN, 3, intNotNegativeConfigValidation);
//		}
//		return sysMaxdayReturn;
//	}
//
//	/**
//	 * @param sysMaxdayReturn the sysMaxdayReturn to set
//	 * @param status
//	 */
//	public void setSysMaxdayReturn(int sysMaxdayReturn, int status) {
//		this.sysMaxdayReturn = status != 0 ? sysMaxdayReturn : 3;
//		GlobalUtil.saveObject(this.sysMaxdayReturn, VNM_SYS_MAXDAY_RETURN);
//	}
//
//	/**
//	 * @return the sysShowPrice
//	 */
//	public boolean isSysShowPrice() {
//		if (!intNotNegativeConfigValidation.valid(this.sysShowPrice)) {
//			//mac dinh la 1 show gia
//			this.sysShowPrice = GlobalUtil.readObject(VNM_SYS_SHOW_PRICE, 1, intNotNegativeConfigValidation);
//		}
//		return (sysShowPrice == 1);
//	}
//
//	/**
//	 * @param sysShowPrice the sysShowPrice to set
//	 * @param status
//	 */
//	public void setSysShowPrice(int sysShowPrice, int status) {
//		this.sysShowPrice = status != 0 ? sysShowPrice : 1;
//		GlobalUtil.saveObject(this.sysShowPrice, VNM_SYS_SHOW_PRICE);
//	}
//
//	/**
//	 * @return the sysModifyPrice
//	 */
//	public boolean isSysModifyPrice() {
//		if (!intNotNegativeConfigValidation.valid(this.sysModifyPrice)) {
//			//mac dinh la 0 khong co edit
//			this.sysModifyPrice = GlobalUtil.readObject(VNM_SYS_MODIFY_PRICE, 0, intNotNegativeConfigValidation);
//		}
//		return (sysModifyPrice == 1);
//	}
//
//	/**
//	 * @param sysModifyPrice the sysModifyPrice to set
//	 * @param status
//	 */
//	public void setSysModifyPrice(int sysModifyPrice, int status) {
//		this.sysModifyPrice = status != 0 ? sysModifyPrice : 0;
//		GlobalUtil.saveObject(this.sysModifyPrice, VNM_SYS_MODIFY_PRICE);
//	}
//
//	public int getStateConnectionMode() {
//		if(stateConnectionMode == 0){
//			SharedPreferences sharedPreferences = getDmsPrivateSharePreference();
//			stateConnectionMode = sharedPreferences.getInt(
//					LoginView.VNM_ALLOW_OFFLINE_MODE,
//					Constants.CONNECTION_ONLINE);
//		}
//		return stateConnectionMode;
//	}
//
//	public void setStateConnectionMode(int stateConnectionMode) {
//		this.stateConnectionMode = stateConnectionMode;
//		// luu lai profile de auto login lan sau
//		SharedPreferences sharedPreferences = getDmsPrivateSharePreference();
//		Editor prefsPrivateEditor = sharedPreferences.edit();
//		prefsPrivateEditor.putInt(LoginView.VNM_ALLOW_OFFLINE_MODE, stateConnectionMode);
//		prefsPrivateEditor.commit();
//	}
//
//	/**
//	 * tong hop theo nhan vien hay tuyen
//	 * @author: hoanpd1
//	 * @since: 11:37:26 07-04-2015
//	 * @return: void
//	 * @throws:
//	 * @param sysSaleRoute
//	 * @param status
//	 */
//	public void setSysSaleRoute(int sysSaleRoute, int status) {
//		this.sysSaleRoute = status != 0 ? sysSaleRoute : ApParamDTO.SYS_SALE_STAFF;
//		GlobalUtil.saveObject(this.sysSaleRoute, VNM_SYS_SALE_ROUTE);
//	}
//
//	/**
//	 *  tong hop theo nhan vien hay tuyen
//	 * @author: hoanpd1
//	 * @since: 11:40:11 07-04-2015
//	 * @return: int
//	 * @throws:
//	 * @return
//	 */
//	public int getSysSaleRoute(){
//		if (!intConfigValidation.valid(this.sysSaleRoute)) {
//			this.sysSaleRoute = GlobalUtil.readObject(VNM_SYS_SALE_ROUTE, ApParamDTO.SYS_SALE_STAFF, intConfigValidation);
//			//mac dinh la 2 ( tong hop theo nv)
//		}
//		return this.sysSaleRoute;
//	}
//
//	/**
//	 * @return the sysMaxdayApprove
//	 */
//	public int getSysReturnDate() {
//		if (!intConfigValidation.valid(this.sysReturnDate)) {
//			//mac dinh la 1 SYSDATE
//			this.sysReturnDate = GlobalUtil.readObject(VNM_SYS_RETURN_DATE, 1, intConfigValidation);
//		}
//		return sysReturnDate;
//	}
//
//	/**
//	 * @param sysMaxdayApprove the sysMaxdayApprove to set
//	 * @param status
//	 */
//	public void setSysReturnDate(int sysReturnDate, int status) {
//		//mac dinh la 1 SYSDATE
//		this.sysReturnDate = status != 0 ? sysReturnDate : 1;
//		GlobalUtil.saveObject(this.sysReturnDate, VNM_SYS_RETURN_DATE);
//	}
//
//	/**
//	 * Cau hinh lay order_date hay approved_date
//	 * @author: hoanpd1
//	 * @since: 16:16:14 15-04-2015
//	 * @return: void
//	 * @throws:
//	 * @param sysCalDate
//	 * @param status
//	 */
//	public void setSysCalDate(int sysCalDate, int status) {
//		this.sysCalDate = status != 0 ? sysCalDate : ApParamDTO.SYS_CAL_ORDER_DATE;
//		GlobalUtil.saveObject(this.sysCalDate, VNM_SYS_CAL_DATE);
//	}
//
//	public int getSysCalDate() {
//		if (!intConfigValidation.valid(this.sysCalDate)) {
//			//mac dinh la 1 lam tron tu nhien
//			this.sysCalDate = GlobalUtil.readObject(VNM_SYS_CAL_DATE, ApParamDTO.SYS_CAL_ORDER_DATE, intConfigValidation);
//		}
//		return sysCalDate;
//	}
//
//	/**
//	 * add save map data
//	 * @author: duongdt3
//	 * @since: 12:37:50 14 May 2015
//	 * @return: void
//	 * @throws:
//	 * @param mapInfo
//	 */
//	public void addSaveMapData(MapDataInfo mapInfo) {
//		//add len tren cung
//		getListSaveMapData().add(0, mapInfo);
//		saveListSaveMapData();
//	}
//
//	/**
//	 * get list save map
//	 * @author: duongdt3
//	 * @since: 12:32:31 14 May 2015
//	 * @return: List<MapDataInfo>
//	 * @throws:
//	 * @return
//	 */
//	@SuppressWarnings("unchecked")
//	public List<MapDataInfo> getListSaveMapData() {
//		if (lstMapDataDownload == null || lstMapDataDownload.isEmpty()) {
//			try {
//				this.lstMapDataDownload = (List<MapDataInfo>) GlobalUtil.readObject(LIST_MAP_DATA_DOWNLOAD);
//			} catch (Exception e) {
//			}
//		}
//		if (this.lstMapDataDownload == null) {
//			this.lstMapDataDownload = new ArrayList<MapDataInfo>();
//		}
//		return this.lstMapDataDownload;
//	}
//
//	List<MapDataInfo> lstMapDataDownload = null;
//
//	private long distanceTimeDrawRouting = 0;
//	private int radiusDrawRouting = 0;
//	//max size (KB)
//	private int maxSizeUploadFile = 0;
//
//	private String whiteListUpload = null;
//	private String cskhInfo = null;
//	private int isCheckDisVisitFirstInRoute = -1;
//	private int isCheckDisVisitFirstOutRoute = -1;
//	private int isCheckDisOrderFirstInRoute = -1;
//	private int isCheckDisOrderFirstOutRoute = -1;
//	private int isCheckDisVisitSecondInRoute = -1;
//	private int isCheckDisVisitSecondOutRoute = -1;
//	private int isCheckDisOrderSecondInRoute = -1;
//	private int isCheckDisOrderSecondOutRoute = -1;
//	private int isNeedSaveRemainProduct = -1;
//	private int isNeedTakePhotoEquipment = -1;
//	private int isCheckCustomerLocationVisit = -1;
//	private int kpiRouteType = -1;
//	private int convertQuantityConfig = -1;
//	private int isValidateMultiClickOrder = -1;
//
//	public void saveListSaveMapData() {
//		GlobalUtil.saveObject(this.lstMapDataDownload, LIST_MAP_DATA_DOWNLOAD);
//	}
//
//	/**
//	 * Id cua thiet bi
//	 * @author: banghn
//	 * @return
//	 */
//	public int getDeviceId() {
//		if (!intConfigValidation.valid(this.deviceId)) {
//			//mac dinh 0
//			this.deviceId = GlobalUtil.readObject(VNM_DEVICE_ID, 0, intConfigValidation);
//		}
//		return this.deviceId;
//	}
//
//	public void setDeviceId(int deviceId) {
//		this.deviceId = deviceId;
//		GlobalUtil.saveObject(this.deviceId, VNM_DEVICE_ID);
//	}
//
//	/**
//	 * set validate mock location
//	 * @author: duongdt3
//	 * @since: 13:49:45 11 Feb 2015
//	 * @return: void
//	 * @throws:
//	 * @param flagValidateLocation 1 validate, 0 not validate is mock location
//	 */
//	public void setFlagValidateMockLocation(int flagValidateLocation, int status) {
//		//default 1 validate
//		this.flagValidateLocation = status != 0 ? flagValidateLocation : 1;
//		GlobalUtil.saveObject(this.flagValidateLocation, VNM_FLAG_VALIDATE_LOCATION);
//	}
//
//	/**
//	 * Vaildate Location Mocklocation
//	 * @author: duongdt3
//	 * @since: 08:56:46 11 Feb 2015
//	 * @return: boolean
//	 * @throws:
//	 * @return
//	 */
//	public boolean isVaildateMockLocation() {
//		if (!intNotNegativeConfigValidation.valid(this.flagValidateLocation)) {
//			//mac dinh la validate
//			this.flagValidateLocation = GlobalUtil.readObject(VNM_FLAG_VALIDATE_LOCATION, 1, intNotNegativeConfigValidation);
//		}
//
//		return (this.flagValidateLocation == 1);
//	}
//
//
//	/**
//	 * set App White List Mock Location
//	 * @author: duongdt3
//	 * @since: 08:51:36 11 Feb 2015
//	 * @return: void
//	 * @throws:
//	 * @param whiteListMockLocation
//	 */
//	public void setAppWhiteListMockLocation(String whiteListMockLocation) {
//		this.whiteListMockLocation = !StringUtil.isNullOrEmpty(whiteListMockLocation) ? whiteListMockLocation : WHITE_LIST_APP_MOCK_LOCATION;
//		GlobalUtil.saveObject(this.whiteListMockLocation, VNM_WHITELIST_MOCK_LOCATION);
//	}
//
//	/**
//	 * get App White List Mock Location
//	 * @author: duongdt3
//	 * @since: 08:50:38 11 Feb 2015
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public String getAppWhiteListMockLocation() {
//		if (!strConfigValidation.valid(this.whiteListMockLocation)) {
//			this.whiteListMockLocation = GlobalUtil.readObject(VNM_WHITELIST_MOCK_LOCATION,
//					WHITE_LIST_APP_MOCK_LOCATION, strConfigValidation);
//		}
//		return this.whiteListMockLocation;
//	}
//
//	/**
//	 * set App Black List Mock Location
//	 * @author: duongdt3
//	 * @since: 08:51:43 11 Feb 2015
//	 * @return: void
//	 * @throws:
//	 * @param blackListMockLocation
//	 */
//	public void setAppBlackListMockLocation(String blackListMockLocation) {
//		this.blackListMockLocation = !StringUtil.isNullOrEmpty(blackListMockLocation) ? blackListMockLocation : BLACK_LIST_APP_MOCK_LOCATION;
//		GlobalUtil.saveObject(this.blackListMockLocation, VNM_BLACKLIST_MOCK_LOCATION);
//	}
//
//	/**
//	 * get App Black List Mock Location
//	 * @author: duongdt3
//	 * @since: 08:51:08 11 Feb 2015
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public String getAppBlackListMockLocation() {
//		if (!strConfigValidation.valid(this.blackListMockLocation)) {
//			this.blackListMockLocation = GlobalUtil.readObject(VNM_BLACKLIST_MOCK_LOCATION,
//					BLACK_LIST_APP_MOCK_LOCATION, strConfigValidation);
//		}
//		return this.blackListMockLocation;
//	}
//
//
//	/**
//	 * get Interval Fused Position
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 11:01:44 18 Oct 2014
//	 * @return: int Cau hinh chu ky goi vi tri fused (phut)
//	 * @throws:
//	 * @return
//	 */
//	public int getIntervalFusedPosition() {
//		if (!intConfigValidation.valid(this.intervalFusedPosition)) {
//			//mac dinh 30s
//			this.intervalFusedPosition = GlobalUtil.readObject(VNM_INTERVAL_FUSED_POSITION, 30, intConfigValidation);
//		}
//		return this.intervalFusedPosition;
//	}
//
//	/**
//	 * set Interval Fused Position
//	 * @author: duongdt3
//	 * @since: 10:42:21 12 Nov 2014
//	 * @return: void
//	 * @throws:
//	 * @param intervalFusedPosition
//	 */
//	public void setIntervalFusedPosition(int intervalFusedPosition, int status) {
//		this.intervalFusedPosition = status != 0 ? intervalFusedPosition : 30;
//		GlobalUtil.saveObject(this.intervalFusedPosition, VNM_INTERVAL_FUSED_POSITION);
//	}
//
//	/**
//	 * get Fast Interval Fused Position
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 11:01:51 18 Oct 2014
//	 * @return: int Cau hinh chu ky ngan nhat goi vi tri fused (phut)
//	 * @throws:
//	 * @return
//	 */
//	public int getFastIntervalFusedPosition() {
//		if (!intConfigValidation.valid(this.fastIntervalFusedPosition)) {
//			//mac dinh 10s
//			this.fastIntervalFusedPosition = GlobalUtil.readObject(VNM_FAST_INTERVAL_FUSED_POSITION, 10, intConfigValidation);
//		}
//		return this.fastIntervalFusedPosition;
//	}
//
//	/**
//	 * set Fast Interval Fused Position
//	 * @author: duongdt3
//	 * @since: 10:34:51 12 Nov 2014
//	 * @return: void
//	 * @throws:
//	 * @param fastIntervalFusedPosition
//	 */
//	public void setFastIntervalFusedPosition(int fastIntervalFusedPosition, int status) {
//		this.fastIntervalFusedPosition = status != 0 ? fastIntervalFusedPosition : 10;
//		GlobalUtil.saveObject(this.fastIntervalFusedPosition, VNM_FAST_INTERVAL_FUSED_POSITION);
//	}
//
//	/**
//	 * get Radius Fused Position
//	 * @author: duongdt3
//	 * @since: 10:34:18 12 Nov 2014
//	 * @return: long ban kinh sai so toi da cho phep cua fused provider
//	 * @throws:
//	 * @return
//	 */
//	public int getRadiusFusedPosition() {
//		if (!intConfigValidation.valid(this.radiusFusedPosition)) {
//			//mac dinh 1000m
//			this.radiusFusedPosition = GlobalUtil.readObject(VNM_RADIUS_FUSED_POSITION, 1000, intConfigValidation);
//		}
//		return this.radiusFusedPosition;
//	}
//
//	/**
//	 * set Radius Fused Position
//	 * @author: duongdt3
//	 * @since: 10:34:05 12 Nov 2014
//	 * @return: void
//	 * @throws:
//	 * @param radiusFusedPosition
//	 */
//	public void setRadiusFusedPosition(int radiusFusedPosition, int status) {
//		this.radiusFusedPosition = status != 0 ? radiusFusedPosition : 1000;
//		GlobalUtil.saveObject(this.radiusFusedPosition, VNM_RADIUS_FUSED_POSITION);
//	}
//
//	/**
//	 * get fused Position Priority
//	 * @author: duongdt3
//	 * @since: 10:33:43 12 Nov 2014
//	 * @return: long 1 la do chinh xac cao, 2 la do chinh xac can bang voi pin
//	 * @throws:
//	 * @return
//	 */
//	public int getFusedPositionPriority() {
//		if (!intConfigValidation.valid(this.fusedPositionPriority)) {
//			//mac dinh la cx cao
//			this.fusedPositionPriority = GlobalUtil.readObject(VNM_FUSED_POSITION_PRIORITY, 1, intConfigValidation);
//		}
//		return this.fusedPositionPriority;
//	}
//
//	/**
//	 * set fused Position Priority
//	 * @author: duongdt3
//	 * @since: 10:33:27 12 Nov 2014
//	 * @return: void
//	 * @throws:
//	 * @param fusedPositionPriority
//	 */
//	public void setFusedPositionPriority(int fusedPositionPriority, int status) {
//		this.fusedPositionPriority = status != 0 ? fusedPositionPriority : 1;
//		GlobalUtil.saveObject(this.fusedPositionPriority, VNM_FUSED_POSITION_PRIORITY);
//	}
//
//	/**
//	 * set Max Time Wrong
//	 * @param maxTimeWrong
//	 * @param status
//	 */
//	public void setMaxTimeWrong(int maxTimeWrong, int status) {
//		this.maxTimeWrong = status != 0 ? maxTimeWrong : 50;
//		GlobalUtil.saveObject(this.maxTimeWrong, VNM_MAX_TIME_WRONG);
//	}
//
//	/**
//	 * get Max Time Wrong
//	 * @return
//	 */
//	public int getMaxTimeWrong() {
//		if (!intConfigValidation.valid(this.maxTimeWrong)) {
//			//mac dinh la 50p
//			this.maxTimeWrong = GlobalUtil.readObject(VNM_MAX_TIME_WRONG, 50, intConfigValidation);
//		}
//
//		//tra ve mili seconds
//		return this.maxTimeWrong * 60 * 1000;
//	}
//
//	/**
//	 * set Distance Time Draw Routing
//	 * @param distanceTimeDrawRouting
//	 * @param status
//	 */
//	public void setDistanceTimeDrawRouting(long distanceTimeDrawRouting, int status) {
//		this.distanceTimeDrawRouting = status != 0 ? distanceTimeDrawRouting : DateUtils.DISTANCE_TIME_ROUTING;
//		GlobalUtil.saveObject(this.distanceTimeDrawRouting, DISTANCE_TIME_DRAW_ROUTING);
//	}
//
//	/**
//	 * get Distance Time Draw Routing
//	 */
//	public long getDistanceTimeDrawRouting() {
//
//		if (!longConfigValidation.valid(this.distanceTimeDrawRouting )) {
//			//mac dinh la 50p
//			this.distanceTimeDrawRouting = GlobalUtil.readObject(DISTANCE_TIME_DRAW_ROUTING, DateUtils.DISTANCE_TIME_ROUTING, longConfigValidation);
//		}
//
//		//tra ve mili seconds
//		return this.distanceTimeDrawRouting * 60 * 1000;
//	}
//
//	/**
//	 * set radius Time Draw Routing
//	 * @param distanceTimeDrawRouting
//	 * @param status
//	 */
//	public void setRadiusDrawRouting(int radiusDrawRouting, int status) {
//		this.radiusDrawRouting = status != 0 ? radiusDrawRouting : 300;
//		GlobalUtil.saveObject(this.radiusDrawRouting, RADIUS_DRAW_ROUTING);
//	}
//
//	/**
//	 * get Radius Time Draw Routing
//	 */
//	public int getRadiusDrawRouting() {
//		if (!intConfigValidation.valid(this.radiusDrawRouting )) {
//			//mac dinh la 300m
//			this.radiusDrawRouting = GlobalUtil.readObject(RADIUS_DRAW_ROUTING, 300, intConfigValidation);
//		}
//
//		//tra ve mili seconds
//		return this.radiusDrawRouting;
//	}
//
//	/**
//	 * set White List Upload file type
//	 * @author: duongdt3
//	 * @since: 12:08:14 23 Aug 2015
//	 * @return: List<String>
//	 * @throws:
//	 * @return
//	 */
//	public void setWhiteListUploadAttach(String whiteListUpload, int status) {
//		this.whiteListUpload  = status != 0 ? whiteListUpload : WHITE_LIST_FILE_TYPE_UPLOAD;
//		GlobalUtil.saveObject(this.whiteListUpload, VNM_WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH);
//	}
//
//	/**
//	 * get White List Upload file type
//	 * @author: duongdt3
//	 * @since: 12:08:14 23 Aug 2015
//	 * @return: List<String>
//	 * @throws:
//	 * @return
//	 */
//	public String[] getWhiteListUploadAttach() {
//		if (!strConfigValidation.valid(this.whiteListUpload)) {
//			this.whiteListUpload = GlobalUtil.readObject(VNM_WHITE_LIST_FILE_TYPE_UPLOAD_ATTACH,
//				WHITE_LIST_FILE_TYPE_UPLOAD, strConfigValidation);
//		}
//		String[] arr = this.whiteListUpload.split(",");
//		return arr;
//	}
//
//	/**
//	 * set max size upload file
//	 * @author: duongdt3
//	 * @since: 11:56:10 24 Aug 2015
//	 * @return: void
//	 * @throws:
//	 * @param maxSizeUploadFile
//	 * @param status
//	 */
//	public void setMaxSizeUploadAttachFile(int maxSizeUploadFile, int status) {
//		this.maxSizeUploadFile  = status != 0 ? maxSizeUploadFile : MAX_SIZE_UPLOAD_ATTACH;
//		GlobalUtil.saveObject(this.maxSizeUploadFile, VNM_MAX_SIZE_UPLOAD_ATTACH);
//	}
//
//	/**
//	 * get max size upload file
//	 * @author: duongdt3
//	 * @since: 11:56:27 24 Aug 2015
//	 * @return: int
//	 * @throws:
//	 * @return
//	 */
//	public int getMaxSizeUploadFile() {
//		if (!intConfigValidation.valid(this.maxSizeUploadFile)) {
//			this.maxSizeUploadFile = GlobalUtil.readObject(VNM_MAX_SIZE_UPLOAD_ATTACH, MAX_SIZE_UPLOAD_ATTACH, intConfigValidation);
//		}
//
//		//tra ve mili seconds
//		return this.maxSizeUploadFile;
//	}
//
//	public void setRegisterApprovedKeyshop(int registerApprovedKeyshop, int status) {
//		this.registerApprovedKeyshop = status != 0 ? registerApprovedKeyshop : 0;
//		GlobalUtil.saveObject(this.registerApprovedKeyshop, REGISTER_APPROVED_KEYSHOP);
//	}
//
//	public int getRegisterApprovedKeyshop() {
//		if (!intConfigValidation.valid(this.registerApprovedKeyshop )) {
//			//mac dinh ko
//			this.registerApprovedKeyshop = GlobalUtil.readObject(REGISTER_APPROVED_KEYSHOP, 0, intConfigValidation);
//		}
//
//		return this.registerApprovedKeyshop;
//	}
//
//	/**
//	 * set mang ung dung dc cai dat
//	 * @author: Tuanlt11
//	 * @param whiteList
//	 * @return: void
//	 * @throws:
//	 */
//	public void setWhiteListApp(String whiteList) {
//		List<String> lst = Arrays.asList(whiteList.split(","));
//		ArrayList<String> arLst = new ArrayList<String>(lst);
//		this.whiteListAppInstall = !StringUtil.isNullOrEmpty(whiteList) ? arLst : new ArrayList<String>();
//		GlobalUtil.saveObject(this.whiteListAppInstall, VNM_WHITE_LIST);
//	}
//
//	/**
//	 * Lay ds cac ung dung dc cai dat vao
//	 *
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: ArrayList<String>
//	 * @throws:
//	 */
//	@SuppressWarnings("unchecked")
//	public ArrayList<String> getWhiteList() {
//		Object temp;
//		if (this.whiteListAppInstall == null || whiteListAppInstall.isEmpty()) {
//			if ((temp = GlobalUtil.readObject(VNM_WHITE_LIST)) != null) {
//				this.whiteListAppInstall = (ArrayList<String>) temp;
//			}
//
//			if (this.whiteListAppInstall == null || whiteListAppInstall.isEmpty()) {
//				whiteListAppInstall = new ArrayList<String>();
//				String[] arrayWhiteList = new String[] {"com.viettel.idp","com.ths.dms", "com.android.launcher", "com.teamviewer.quicksupport.market",
//						"com.google.android.gms", "com.android.systemui", "com.sec.android.app.camera",
//						"org.kman.WifiManager", "com.sec.android.app.popupcalculator", "com.android.email",
//						"com.google.android.gm", "com.asus.camera", "com.asus.email", "com.asus.calculator",
//						"com.android.packageinstaller", "com.asus.gallery", "com.sec.android.gallery3d", "com.google.android.apps.maps"};
//				for (String app : arrayWhiteList) {
//					whiteListAppInstall.add(app);
//				}
//			}
//		}
//		return this.whiteListAppInstall;
//	}
//
//	/**
//	 * set thoi gian lock lai ung dung
//	 * @author: Tuanlt11
//	 * @param timeLockAppPrevent
//	 * @return: void
//	 * @throws:
//	 */
//	public void setTimeLockAppBlocked(long timeLockAppPrevent, int status) {
//		this.timeLockAppBlocked = status != 0 ? timeLockAppPrevent : 5;
//		GlobalUtil.saveObject(this.timeLockAppBlocked,
//				VNM_TIME_LOCK_APP_BLOCKED);
//	}
//
//	/**
//	 * lay thoi gian lock ung dung
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: long
//	 * @throws:
//	 */
//	public long getTimeLockAppBlocked() {
//		Object temp;
//		try {
//			if (this.timeLockAppBlocked <= 0) {
//				if ((temp = GlobalUtil.readObject(VNM_TIME_LOCK_APP_BLOCKED)) != null) {
//					this.timeLockAppBlocked = (Long) temp;
//				}
//			}
//		} catch (Exception e) {}
//		if (this.timeLockAppBlocked <= 0) {
//			// mac dinh 5phut se khoa lai
//			this.timeLockAppBlocked = 5;
//		}
//		return this.timeLockAppBlocked * 60 * 1000;
//	}
//
//	/**
//	 * Co de kiem tra cac ung dung co duoc xoa hay ko khi cai dat
//	 * @author: Tuanlt11
//	 * @param whiteList
//	 * @return: void
//	 * @throws:
//	 */
//	public void setCheckAppInstall(int checkAppInstall, int status) {
//		this.checkAppInstall =  status != 0 ? checkAppInstall : 0;
//		GlobalUtil.saveObject(this.checkAppInstall, VNM_CHECK_APP_INSTALL);
//	}
//
//	/**
//	 * Co de kiem tra cac ung dung co duoc xoa hay ko khi cai dat
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: ArrayList<String>
//	 * @throws:
//	 */
//	public int getCheckAppInstall() {
//		Object temp;
//		if (this.checkAppInstall == null) {
//			if ((temp = GlobalUtil.readObject(VNM_CHECK_APP_INSTALL)) != null) {
//				this.checkAppInstall = (Integer) temp;
//			}
//			if (checkAppInstall == null) {
//				checkAppInstall = 0;// khong check
//			}
//		}
//		return this.checkAppInstall;
//	}
//
//	/**
//	 * Check network
//	 * @author: Tuanlt11
//	 * @param whiteList
//	 * @return: void
//	 * @throws:
//	 */
//	public void setCheckNetwork(int checkNetWork, int status) {
//		this.checkNetWork = status != 0 ? checkNetWork : UserDTO.CHECK_NETWORK_OFFLINE;
//		GlobalUtil.saveObject(this.checkNetWork, VNM_CHECK_NETWORK);
//	}
//
//	/**
//	 * Check network
//	 * @author: Tuanlt11
//	 * @return
//	 * @return: ArrayList<String>
//	 * @throws:
//	 */
//	public int getCheckNetwork() {
//		if (!intConfigValidation.valid(this.checkNetWork)) {
//			this.checkNetWork = GlobalUtil.readObject(VNM_CHECK_NETWORK, UserDTO.CHECK_NETWORK_OFFLINE, intConfigValidation);
//		}
//		return this.checkNetWork;
//	}
//
//	public void saveShopManaged(ShopDTO shopManaged) {
//		GlobalUtil.saveObject(shopManaged, VNM_SHOP_MANAGED);
//	}
//
//	public ShopDTO getShopManaged() {
//		Object temp;
//		if ((temp = GlobalUtil.readObject(VNM_SHOP_MANAGED)) != null) {
//			return (ShopDTO) temp;
//		}
//		return new ShopDTO();
//	}
//
//	public void setRegisterCTHTTM(int registerCTHTTM, int status) {
//		this.registerCTHTTM = status != 0 ? registerCTHTTM : Constants.TYPE_REGISTER_CTHTTM;
//		GlobalUtil.saveObject(this.registerCTHTTM, VNM_REGISTER_CTHTTM);
//	}
//
//	public int getRegisterCTHTTM() {
//		if (!intConfigValidation.valid(this.registerCTHTTM)) {
//			this.registerCTHTTM = GlobalUtil.readObject(VNM_REGISTER_CTHTTM, Constants.TYPE_REGISTER_CTHTTM, intConfigValidation);
//		}
//		return this.registerCTHTTM ;
//	}
//
//	public void setAllowDistanceOrder(int allowDistanceOrder, int status) {
//		this.allowDistanceOrder = status != 0 ? allowDistanceOrder : Constants.TYPE_ALLOW_DISTANCE_ORDER;
//		GlobalUtil.saveObject(this.allowDistanceOrder, VNM_ALLOW_DISTANCE_ORDER);
//	}
//
//	public int getAllowDistanceOrder() {
//		if (!intConfigValidation.valid(this.allowDistanceOrder)) {
//			this.allowDistanceOrder = GlobalUtil.readObject(VNM_ALLOW_DISTANCE_ORDER, Constants.TYPE_ALLOW_DISTANCE_ORDER, intConfigValidation);
//		}
//		return this.allowDistanceOrder ;
//	}
//
//	public void setDayOfCycle(int dayOfCycle, int status) {
//		this.dayOfCycle = status != 0 ? dayOfCycle : Constants.DAY_OF_CYCLE;
//		GlobalUtil.saveObject(this.dayOfCycle, VNM_DAY_OF_CYCLE);
//	}
//
//	public int getDayOfCycle() {
//		if (!intConfigValidation.valid(this.dayOfCycle)) {
//			this.dayOfCycle = GlobalUtil.readObject(VNM_DAY_OF_CYCLE, Constants.DAY_OF_CYCLE, intConfigValidation);
//		}
//		return this.dayOfCycle ;
//	}
//
//	public void setCskhInfo(String cskhInfo, int status) {
//		this.cskhInfo = status != 0 ? cskhInfo : CSKH_INFO_TITLE;
//		GlobalUtil.saveObject(this.cskhInfo, VNM_CSKH_INFO);
//	}
//
//	public String getCskhInfo() {
//		if (!strNullConfigValidation.valid(this.cskhInfo)) {
//			this.cskhInfo = GlobalUtil.readObject(VNM_CSKH_INFO, CSKH_INFO_TITLE, strNullConfigValidation);
//		}
//		return this.cskhInfo ;
//	}
//
//	public boolean isInSynTime(){
//		// h hien tai
//		int currentHour = DateUtils.getCurrentTimeByTimeType(Calendar.HOUR_OF_DAY);
//		// check thoi gian day vi tri trong thoi gian cho phep
//		boolean isInTime = (currentHour >= getBeginTimeAllowSynData()
//				&& currentHour < getEndTimeAllowSynData());
//		return isInTime;
//	}
//
//	public static class VistConfig{
//		private boolean isCheckDisVisitFirstInRoute;
//		private boolean isCheckDisVisitFirstOutRoute;
//		private boolean isCheckDisVisitSecondInRoute;
//		private boolean isCheckDisVisitSecondOutRoute;
//
//		private boolean isCheckDisOrderFirstInRoute;
//		private boolean isCheckDisOrderFirstOutRoute;
//		private boolean isCheckDisOrderSecondInRoute;
//		private boolean isCheckDisOrderSecondOutRoute;
//
//		public VistConfig() {
//			isCheckDisVisitFirstInRoute = GlobalInfo.getInstance().isCheckDisVisitFirstInRoute();
//			isCheckDisVisitFirstOutRoute = GlobalInfo.getInstance().isCheckDisVisitFirstOutRoute();
//			isCheckDisVisitSecondInRoute = GlobalInfo.getInstance().isCheckDisVisitSecondInRoute();
//			isCheckDisVisitSecondOutRoute = GlobalInfo.getInstance().isCheckDisVisitSecondOutRoute();
//
//			isCheckDisOrderFirstInRoute = GlobalInfo.getInstance().isCheckDisOrderFirstInRoute();
//			isCheckDisOrderFirstOutRoute = GlobalInfo.getInstance().isCheckDisOrderFirstOutRoute();
//			isCheckDisOrderSecondInRoute = GlobalInfo.getInstance().isCheckDisOrderSecondInRoute();
//			isCheckDisOrderSecondOutRoute = GlobalInfo.getInstance().isCheckDisOrderSecondOutRoute();
//		}
//
//		public boolean isAllowVisit(boolean isTooFar, int isOr, boolean isVisited, boolean isHaveCusLocation){
//			boolean isAllowVisit = false;
//			boolean isCheckLocationCustomer = GlobalInfo.getInstance().isCheckCustomerLocationVisit();
//			if (isCheckLocationCustomer && !isHaveCusLocation) {
//				isAllowVisit = false;
//			} else{
//				if (isTooFar) {
//					//TH 1: trong tuyen
//					if (isOr == 0) {
//						if (isVisited) {
//							isAllowVisit = (!this.isCheckDisVisitSecondInRoute);
//						} else{
//							isAllowVisit = (!this.isCheckDisVisitFirstInRoute);
//						}
//					} else {
//						//TH 2: ngoai tuyen
//						if (isVisited) {
//							isAllowVisit = (!this.isCheckDisVisitSecondOutRoute);
//						} else{
//							isAllowVisit = (!this.isCheckDisVisitFirstOutRoute);
//						}
//					}
//				} else{
//					isAllowVisit = true;
//				}
//			}
//			return isAllowVisit;
//		}
//
//		public boolean isAllowProcessAfterVisit(boolean isTooFar, int isOr, boolean isVisited){
//			boolean isAllowOrder = false;
//			if (isTooFar) {
//				//TH 1: trong tuyen
//				if (isOr == 0) {
//					if (isVisited) {
//						isAllowOrder = (!this.isCheckDisOrderSecondInRoute);
//					} else{
//						isAllowOrder = (!this.isCheckDisOrderFirstInRoute);
//					}
//				} else {
//					//TH 2: ngoai tuyen
//					if (isVisited) {
//						isAllowOrder = (!this.isCheckDisOrderSecondOutRoute);
//					} else{
//						isAllowOrder = (!this.isCheckDisOrderFirstOutRoute);
//					}
//				}
//			} else{
//				isAllowOrder = true;
//			}
//			return isAllowOrder;
//		}
//	}
//
//	public VistConfig getVisitConfig() {
//		return new VistConfig();
//	}
//
//	public void setCheckDisVisitFirstInRoute(int value, int status) {
//		this.isCheckDisVisitFirstInRoute = status != 0 ? value : 1;
//		GlobalUtil.saveObject(this.isCheckDisVisitFirstInRoute, VNM_CHECK_DIS_VISIT_FIRST_IN_ROUTE);
//	}
//
//	public boolean isCheckDisVisitFirstInRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisVisitFirstInRoute)) {
//			this.isCheckDisVisitFirstInRoute = GlobalUtil.readObject(VNM_CHECK_DIS_VISIT_FIRST_IN_ROUTE, 1, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisVisitFirstInRoute == 1);
//	}
//
//	public void setCheckDisVisitFirstOutRoute(int value, int status) {
//		this.isCheckDisVisitFirstOutRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisVisitFirstOutRoute, VNM_CHECK_DIS_VISIT_FIRST_OUT_ROUTE);
//	}
//
//	public boolean isCheckDisVisitFirstOutRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisVisitFirstOutRoute)) {
//			this.isCheckDisVisitFirstOutRoute = GlobalUtil.readObject(VNM_CHECK_DIS_VISIT_FIRST_OUT_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisVisitFirstOutRoute == 1);
//	}
//
//	public void setCheckDisOrderFirstInRoute(int value, int status) {
//		this.isCheckDisOrderFirstInRoute = status != 0 ? value : 1;
//		GlobalUtil.saveObject(this.isCheckDisOrderFirstInRoute, VNM_CHECK_DIS_ORDER_FIRST_IN_ROUTE);
//	}
//
//	public boolean isCheckDisOrderFirstInRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisOrderFirstInRoute)) {
//			this.isCheckDisOrderFirstInRoute = GlobalUtil.readObject(VNM_CHECK_DIS_ORDER_FIRST_IN_ROUTE, 1, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisOrderFirstInRoute == 1);
//	}
//
//	public void setCheckDisOrderFirstOutRoute(int value, int status) {
//		this.isCheckDisOrderFirstOutRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisOrderFirstOutRoute, VNM_CHECK_DIS_ORDER_FIRST_OUT_ROUTE);
//	}
//
//	public boolean isCheckDisOrderFirstOutRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisOrderFirstOutRoute)) {
//			this.isCheckDisOrderFirstOutRoute = GlobalUtil.readObject(VNM_CHECK_DIS_ORDER_FIRST_OUT_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisOrderFirstOutRoute == 1);
//	}
//
//	public void setCheckDisVisitSecondInRoute(int value, int status) {
//		this.isCheckDisVisitSecondInRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisVisitSecondInRoute, VNM_CHECK_DIS_VISIT_SECOND_IN_ROUTE);
//	}
//
//	public boolean isCheckDisVisitSecondInRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisVisitSecondInRoute)) {
//			this.isCheckDisVisitSecondInRoute = GlobalUtil.readObject(VNM_CHECK_DIS_VISIT_SECOND_IN_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisVisitSecondInRoute == 1);
//	}
//
//	public void setCheckDisVisitSecondOutRoute(int value, int status) {
//		this.isCheckDisVisitSecondOutRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisVisitSecondOutRoute, VNM_CHECK_DIS_ORDER_SECOND_OUT_ROUTE);
//	}
//
//	public boolean isCheckDisVisitSecondOutRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisVisitSecondOutRoute)) {
//			this.isCheckDisVisitSecondOutRoute = GlobalUtil.readObject(VNM_CHECK_DIS_ORDER_SECOND_OUT_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisVisitSecondOutRoute == 1);
//	}
//
//	public void setCheckDisOrderSecondInRoute(int value, int status) {
//		this.isCheckDisOrderSecondInRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisOrderSecondInRoute, VNM_CHECK_DIS_VISIT_SECOND_OUT_ROUTE);
//	}
//
//	public boolean isCheckDisOrderSecondInRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisOrderSecondInRoute)) {
//			this.isCheckDisOrderSecondInRoute = GlobalUtil.readObject(VNM_CHECK_DIS_VISIT_SECOND_OUT_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisOrderSecondInRoute == 1);
//	}
//
//	public void setCheckDisOrderSecondOutRoute(int value, int status) {
//		this.isCheckDisOrderSecondOutRoute = status != 0 ? value : 0;
//		GlobalUtil.saveObject(this.isCheckDisOrderSecondOutRoute, VNM_CHECK_DIS_ORDER_SECOND_IN_ROUTE);
//	}
//
//	public boolean isCheckDisOrderSecondOutRoute() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckDisOrderSecondOutRoute)) {
//			this.isCheckDisOrderSecondOutRoute = GlobalUtil.readObject(VNM_CHECK_DIS_ORDER_SECOND_IN_ROUTE, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckDisOrderSecondOutRoute == 1);
//	}
//
//	/**
//	 * @return the isNeedSaveRemainProduct
//	 */
//	public boolean isNeedSaveRemainProduct() {
//		if (!intNotNegativeConfigValidation.valid(this.isNeedSaveRemainProduct)) {
//			//mac dinh la 0, khong check
//			this.isNeedSaveRemainProduct = GlobalUtil.readObject(VNM_SYS_NEED_SAVE_REMAIN, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isNeedSaveRemainProduct == 1);
//	}
//
//	/**
//	 * @param isNeedSaveRemainProduct the isNeedSaveRemainProduct to set
//	 * @param status
//	 */
//	public void setSysNeedSaveRemain(int isNeedSaveRemainProduct, int status) {
//		//mac dinh la 0, khong check
//		this.isNeedSaveRemainProduct = status != 0 ? isNeedSaveRemainProduct : 0;
//		GlobalUtil.saveObject(this.isNeedSaveRemainProduct, VNM_SYS_NEED_SAVE_REMAIN);
//	}
//
//	/**
//	 * @return the isNeedSaveRemainProduct
//	 */
//	public boolean isNeedTakePhotoEquipment() {
//		if (!intNotNegativeConfigValidation.valid(this.isNeedTakePhotoEquipment)) {
//			//mac dinh la 0, khong check
//			this.isNeedTakePhotoEquipment = GlobalUtil.readObject(VNM_SYS_NEED_TAKE_PHOTO_EQUIPMENT, 0, intNotNegativeConfigValidation);
//		}
//		return (this.isNeedTakePhotoEquipment == 1);
//	}
//
//	/**
//	 * @param isNeedSaveRemainProduct the isNeedSaveRemainProduct to set
//	 * @param status
//	 */
//	public void setNeedTakePhotoEquipment(int isNeedTakePhotoEquipment, int status) {
//		//mac dinh la 0, khong check
//		this.isNeedTakePhotoEquipment = status != 0 ? isNeedTakePhotoEquipment : 0;
//		GlobalUtil.saveObject(this.isNeedTakePhotoEquipment, VNM_SYS_NEED_TAKE_PHOTO_EQUIPMENT);
//	}
//
//	public void setCheckCustomerLocationVisit(int value, int status) {
//		this.isCheckCustomerLocationVisit = status != 0 ? value : 1;
//		GlobalUtil.saveObject(this.isCheckCustomerLocationVisit, VNM_CHECK_CUSTOMER_LOCATION_VISIT);
//	}
//
//	public boolean isCheckCustomerLocationVisit() {
//		if (!intNotNegativeConfigValidation.valid(this.isCheckCustomerLocationVisit)) {
//			this.isCheckCustomerLocationVisit = GlobalUtil.readObject(VNM_CHECK_CUSTOMER_LOCATION_VISIT, 1, intNotNegativeConfigValidation);
//		}
//		return (this.isCheckCustomerLocationVisit == 1);
//	}
//
//	public void setKpiRouteType(int kpiRouteType, int status) {
//		this.kpiRouteType = status != 0 ? kpiRouteType : KPI_ROUTE_TYPE_IN_ROUTE;
//		GlobalUtil.saveObject(this.kpiRouteType, VNM_SYS_ROUTE_TYPE);
//	}
//
//	public int getKpiRouteType() {
//		if (!intConfigValidation.valid(this.kpiRouteType)) {
//			this.kpiRouteType = GlobalUtil.readObject(VNM_SYS_ROUTE_TYPE, KPI_ROUTE_TYPE_IN_ROUTE, intConfigValidation);
//		}
//		return this.kpiRouteType ;
//	}
//
//	/**
//	 * thiet lap cau hinh chuyen do so luong
//	 * @author: duongdt3
//	 * @time: 10:59:03 AM Nov 19, 2015
//	 * @return
//	*/
//	public void setConfigConvertQuantity(int value, int status) {
//		this.convertQuantityConfig  = status != 0 ? value : Constants.CHANGE_QUANTITY_2_RETAIL;
//		GlobalUtil.saveObject(this.convertQuantityConfig, VNM_CONFIG_CONVERT_QUANTITY);
//	}
//
//	/**
//	 * cau hinh chuyen do so luong
//	 * @author: duongdt3
//	 * @time: 10:59:03 AM Nov 19, 2015
//	 * @return
//	*/
//	public int getConfigConvertQuantity() {
//		if (!intNotNegativeConfigValidation.valid(this.convertQuantityConfig)) {
//			this.convertQuantityConfig = GlobalUtil.readObject(VNM_CONFIG_CONVERT_QUANTITY, Constants.CHANGE_QUANTITY_2_RETAIL, intNotNegativeConfigValidation);
//		}
//		return this.convertQuantityConfig;
//	}
//
//	/**
//	 * set cau hinh multi click order
//	 * @author: duongdt3
//	 * @time: 10:59:03 AM Dec 05, 2015
//	 * @param valueOf
//	 * @param status
//	*/
//	public void setValidateMultiClickOrder(Integer value, int status) {
//		this.isValidateMultiClickOrder = status != 0 ? value : Constants.NEED_VALIDATE_CLICK;
//		GlobalUtil.saveObject(this.isValidateMultiClickOrder, VNM_VALIDATE_MULTI_CLICK_ORDER);
//	}
//
//	/**
//	 * get cau hinh multi click order
//	 * @author: duongdt3
//	 * @time: 10:59:03 AM Dec 05, 2015
//	 * @return
//	*/
//	public boolean isValidateMultiClickOrder() {
//		if (!intNotNegativeConfigValidation.valid(this.isValidateMultiClickOrder)) {
//			this.isValidateMultiClickOrder = GlobalUtil.readObject(VNM_VALIDATE_MULTI_CLICK_ORDER, Constants.NEED_VALIDATE_CLICK, intNotNegativeConfigValidation);
//		}
//		return (this.isValidateMultiClickOrder == 1);
//	}
//
//	public boolean isVtMapSetServer() {
//		return vtMapSetServer;
//	}
//
//	public String getVtMapProtocol() {
//		return vtMapProtocol;
//	}
//
//	public String getVtMapIP() {
//		return vtMapIP;
//	}
//
//	public int getVtMapPort() {
//		return vtMapPort;
//	}
//
//	public void setVtMapPort(int vtMapPort) {
//		this.vtMapPort = vtMapPort;
//	}
//
//	public void setVtMapIP(String vtMapIP) {
//		this.vtMapIP = vtMapIP;
//	}
//
//	public void setVtMapProtocol(String vtMapProtocol) {
//		this.vtMapProtocol = vtMapProtocol;
//	}
//
//	public void setVtMapSetServer(boolean vtMapSetServer) {
//		this.vtMapSetServer = vtMapSetServer;
//	}
//
//	public static boolean isIS_VERSION_SEND_DATA() {
//		return IS_VERSION_SEND_DATA;
//	}
//
//	public static void setIS_VERSION_SEND_DATA(boolean iS_VERSION_SEND_DATA) {
//		IS_VERSION_SEND_DATA = iS_VERSION_SEND_DATA;
//	}
//
//	public static boolean isIS_VERSION_FOR_EMULATOR() {
//		return IS_VERSION_FOR_EMULATOR;
//	}
//
//	public static void setIS_VERSION_FOR_EMULATOR(boolean iS_VERSION_FOR_EMULATOR) {
//		IS_VERSION_FOR_EMULATOR = iS_VERSION_FOR_EMULATOR;
//	}
//
//	public static String getVIETTEL_MAP_KEY() {
//		return VIETTEL_MAP_KEY;
//	}
//
//	public static void setVIETTEL_MAP_KEY(String vIETTEL_MAP_KEY) {
//		VIETTEL_MAP_KEY = vIETTEL_MAP_KEY;
//	}
//
//	public boolean isSendLogException() {
//		return isSendLogException;
//	}
//
//	public void setSendLogException(boolean isSendLogException) {
//		this.isSendLogException = isSendLogException;
//	}
//
//	public NotifyInfoOrder getNotifyOrderReturnInfo() {
//		return notifyOrderReturnInfo;
//	}
//
//	public void setNotifyOrderReturnInfo(NotifyInfoOrder notifyOrderReturnInfo) {
//		this.notifyOrderReturnInfo = notifyOrderReturnInfo;
//	}
//
//	public GlobalBaseActivity getLastActivity() {
//		return lastActivity;
//	}
//
//	public void setLastActivity(GlobalBaseActivity lastActivity) {
//		this.lastActivity = lastActivity;
//	}
//
//	public boolean isExitApp() {
//		return isExitApp;
//	}
//
//	public void setExitApp(boolean isExitApp) {
//		this.isExitApp = isExitApp;
//	}
//
//	public long getTimeSendLogPosition() {
//		return timeSendLogPosition;
//	}
//
//	public void setTimeSendLogPosition(long timeSendLogPosition) {
//		this.timeSendLogPosition = timeSendLogPosition;
//	}
//
//	public long getTimeToastUnstrustPosition() {
//		return timeToastUnstrustPosition;
//	}
//
//	public void setTimeToastUnstrustPosition(long timeToastUnstrustPosition) {
//		this.timeToastUnstrustPosition = timeToastUnstrustPosition;
//	}
//
	public long getTimeSendLogLocating() {
		return timeSendLogLocating;
	}

	public void setTimeSendLogLocating(long timeSendLogLocating) {
		this.timeSendLogLocating = timeSendLogLocating;
	}
//
//	public int getStateSynData() {
//		return stateSynData;
//	}
//
//	public void setStateSynData(int stateSynData) {
//		this.stateSynData = stateSynData;
//	}
//
//	public void setHashMapKPI(HashMap<Integer, Integer> hashMapKPI) {
//		this.hashMapKPI = hashMapKPI;
//	}
//
//	public void setAllowRequestLogKPINumber(int allowRequestLogKPINumber) {
//		this.allowRequestLogKPINumber = allowRequestLogKPINumber;
//	}
//
//	public int getTrainingStaffId() {
//		return trainingStaffId;
//	}
//
//	public void setTrainingStaffId(int trainingStaffId) {
//		this.trainingStaffId = trainingStaffId;
//	}
//
//	public int getTrainngDetailId() {
//		return trainngDetailId;
//	}
//
//	public void setTrainngDetailId(int trainngDetailId) {
//		this.trainngDetailId = trainngDetailId;
//	}
//
//	public static boolean isInProcessCipher() {
//		return inProcessCipher;
//	}
//
//	public static void setInProcessCipher(boolean inProcessCipher) {
//		GlobalInfo.inProcessCipher = inProcessCipher;
//	}
//
//	public String getLastAppsList() {
//		return lastAppsList;
//	}
//
//	public void setLastAppsList(String lastAppsList) {
//		this.lastAppsList = lastAppsList;
//	}
//
//	public List<Long> getLstWrongProgrameId() {
//		return lstWrongProgrameId;
//	}
//
//	public void setLstWrongProgrameId(List<Long> lstWrongProgrameId) {
//		this.lstWrongProgrameId = lstWrongProgrameId;
//	}
//
//	public boolean isFirstTimeSendApp() {
//		return isFirstTimeSendApp;
//	}
//
//	public void setFirstTimeSendApp(boolean isFirstTimeSendApp) {
//		this.isFirstTimeSendApp = isFirstTimeSendApp;
//	}
//
//	public static boolean isChangeInfoCurrentCustomer() {
//		return isChangeInfoCurrentCustomer;
//	}
//
//	public static void setChangeInfoCurrentCustomer(
//			boolean isChangeInfoCurrentCustomer) {
//		GlobalInfo.isChangeInfoCurrentCustomer = isChangeInfoCurrentCustomer;
//	}
//
//	public static CustomerDTO getInfoCustomerChange() {
//		return infoCustomerChange;
//	}
//
//	public static void setInfoCustomerChange(CustomerDTO infoCustomerChange) {
//		GlobalInfo.infoCustomerChange = infoCustomerChange;
//	}

	public volatile static boolean isTablet;
}