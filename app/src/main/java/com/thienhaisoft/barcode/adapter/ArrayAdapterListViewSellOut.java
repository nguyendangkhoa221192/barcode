package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.OrderClass;

import java.util.ArrayList;

/**
 * Created by khoam on 25/05/2017.
 */

public class ArrayAdapterListViewSellOut extends BaseAdapter {

    private Context context = null;
    private ArrayList<OrderClass> orderClassArrayList = null;

    public ArrayAdapterListViewSellOut(Context context, ArrayList<OrderClass> orderClassArrayList) {
        this.context = context;
        this.orderClassArrayList = orderClassArrayList;
    }

    public void swapItem(ArrayList<OrderClass> listOrderClasses) {
        this.orderClassArrayList = listOrderClasses;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.orderClassArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            view = inflater.inflate(R.layout.earnpoint_layout_listview, null);
        }
        OrderClass orderItem = orderClassArrayList.get(position);
        TextView txt_date = (TextView) view.findViewById(R.id.earnpoit_itemlistview_date);
        TextView txt_product = (TextView) view.findViewById(R.id.earnpoit_itemlistview_product);
        txt_date.setText(orderItem.getOrder_date());
        txt_product.setText(orderItem.getProduct_name());

        return view;
    }
}
