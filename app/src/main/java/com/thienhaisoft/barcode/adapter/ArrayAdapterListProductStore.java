package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.InputFilterMinMax;
import com.thienhaisoft.barcode.utils.MyListener;

import java.util.ArrayList;

/**
 * Created by khoam on 26/05/2017.
 */

public class ArrayAdapterListProductStore extends BaseAdapter implements Filterable {

    private ArrayList<ProductItem> listProductItems = new ArrayList<>();
    private ArrayList<ProductItem> mOriginalValues = new ArrayList<>();
    private Context context = null;
    private Activity a = null;
    private boolean isFirst = false;

    static class ViewHolder {
        EditText caption;
    }

    public ArrayAdapterListProductStore(Activity a, Context context, ArrayList<ProductItem> productItems) {
        this.listProductItems = Common.sortProductItemStore(productItems);
        this.mOriginalValues = this.listProductItems;
        this.context = context;
        this.a = a;
    }

    public void swapItem(ArrayList<ProductItem> productItems) {
        this.listProductItems = Common.sortProductItemStore(productItems);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listProductItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder = new ViewHolder();
        // not recycling view
        LayoutInflater inflater = LayoutInflater.from(this.context);
        view = inflater.inflate(R.layout.layout_lv_item_store, null);
        holder.caption = (EditText) view.findViewById(R.id.store_listview_item_number);

        if (position % 2 != 0) {
            view.setBackgroundColor(parent.getResources().getColor(R.color.earnpoint_color_bg_item_lv));
        } else {
            view.setBackgroundColor(parent.getResources().getColor(R.color.WHITE));
        }
        final ProductItem productItem = listProductItems.get(position);

        TextView txt_name = (TextView) view.findViewById(R.id.store_listview_item_productname);
        TextView txt_exist = (TextView) view.findViewById(R.id.store_listview_item_exist);
        txt_name.setText(productItem.getCode() + "-" + productItem.getName());
        txt_exist.setText(productItem.getQuantity() + "");

        if (productItem.getNumber_choose() != 0) {
            holder.caption.setText(productItem.getNumber_choose() + "");
        }
        holder.caption.setId(position);


        holder.caption.addTextChangedListener(new MyTextWatcher(holder.caption));
        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && isFirst) {
                    swapItem(listProductItems);
                }
            }
        });
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        isFirst = false;
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText editText;

        private MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals("")) {
                s = "0";
            }
            if (s.toString().equals("-")) {
                s = "0";
            }
            int number_choose = 0;
            try {
                number_choose = Integer.parseInt(s.toString());
            } catch (Exception ex) {
                Toast.makeText(a, "Số lượng không hợp lệ.", Toast.LENGTH_SHORT).show();
                number_choose = 0;
                editText.setText("");
            }
            final int position = this.editText.getId();
            boolean check_error_number = false;
            int num_quanlity = listProductItems.get(position).getQuantity();
            if (num_quanlity <= 0 && number_choose < 0) {
                check_error_number = true;
            } else {
                if (number_choose < -num_quanlity) {
                    check_error_number = true;
                }
            }
            if (check_error_number) {
                Common.showAlertDialog(a, "Nhập sai số lượng!", "Cảnh báo", "", "", "OK", new MyListener() {
                    @Override
                    public void functionNegative() {

                    }

                    @Override
                    public void functionNeutral() {

                    }

                    @Override
                    public void functionPositive() {
                        listProductItems.get(position).setNumber_choose(0);
                        notifyDataSetChanged();
                    }
                });
            } else {
                listProductItems.get(position).setNumber_choose(number_choose);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFirst = true;
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<ProductItem> filterArray = new ArrayList<ProductItem>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(listProductItems);
                }

                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String data = Common.deAccent(mOriginalValues.get(i).getCode() + "-" + mOriginalValues.get(i).getName());
                        if (data.toLowerCase().contains(constraint.toString())) {
                            filterArray.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = filterArray.size();
                    results.values = filterArray;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listProductItems = (ArrayList<ProductItem>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
