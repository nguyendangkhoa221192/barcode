package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.NotifyEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;

import java.util.List;

/**
 * Created by user on 11/06/2017.
 */

public class ArrayNotifyAdapter extends BaseAdapter {
    private List<NotifyEntities> listNotify;
    private Context context;
    private int[] imageId;


    private GlobalInfo info;
    private String title = "";
    private String description = "";

    public ArrayNotifyAdapter(Context context, List<NotifyEntities> listNotify) {
        this.context = context;
        this.listNotify = listNotify;
        info = (GlobalInfo) context.getApplicationContext();
    }

    @Override
    public int getCount() {
        if (listNotify == null) {
            return 0;
        }
        return listNotify.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custome_listview_list_notifications, parent, false);

        LinearLayout layout_list_item = (LinearLayout) rowView.findViewById(R.id.layout_list_notify_item);
        TextView tvDescription = (TextView) rowView.findViewById(R.id.tvNotifyInfo);
        TextView tvTitle = (TextView) rowView.findViewById(R.id.tvNotifyTitle);
        ImageView imgAvatar = (ImageView) rowView.findViewById(R.id.imgNotify);


        if (listNotify != null && listNotify.get(position) != null) {
            title = listNotify.get(position).getTitle();
            description = listNotify.get(position).getDescription();
        }
        tvTitle.setText(Html.fromHtml(title).toString());
        tvDescription.setText(Html.fromHtml(description).toString());

        if (position % 2 != 0) {
            layout_list_item.setBackgroundResource(R.color.gray_line_list_item);
        }


//        imgAvatar.setImageResource(imageId[position]);


        return rowView;
    }
}
