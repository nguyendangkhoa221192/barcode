package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.ReportEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.utils.StringUtil;

import java.util.List;

/**
 * Created by user on 11/06/2017.
 */

public class ArrayReportAdapter extends BaseAdapter {
    private List<ReportEntities> listReport;
    private Context context;
    private int[] imageId;


    private GlobalInfo info;
    private String storeName = "";
    private String planNumber = "";
    private String realNumber = "";
    private String percent = "";

    public ArrayReportAdapter(Context context, List<ReportEntities> listReport) {
        this.context = context;
        this.listReport = listReport;
        info = (GlobalInfo) context.getApplicationContext();
    }

    @Override
    public int getCount() {
        if (listReport == null) {
            return 0;
        }
        return listReport.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custome_listview_list_report, parent, false);

        LinearLayout layout_list_item = (LinearLayout) rowView.findViewById(R.id.layout_list_report_item);
        TextView tvReportStore = (TextView) rowView.findViewById(R.id.tvReportStoreName);
        TextView tvReportPlanNumber = (TextView) rowView.findViewById(R.id.tvReportPlanNumber);
        TextView tvReportRealNumber = (TextView) rowView.findViewById(R.id.tvReportRealNumber);
        TextView tvReportPercent = (TextView) rowView.findViewById(R.id.tvReportPercent);


        if (listReport != null && listReport.get(position) != null) {
//            storeName = listReport.get(position).getStore_code() + " - " + listReport.get(position).getStore_name();
            storeName = listReport.get(position).getStore_name();
            planNumber = StringUtil.parseAmountMoney(listReport.get(position).getTarget_amount()) ;
            realNumber = StringUtil.parseAmountMoney(listReport.get(position).getAmount()) ;
            percent = StringUtil.parsePercentWith2Digit(listReport.get(position).getPercent());
        }

        tvReportStore.setText(storeName);
        tvReportPlanNumber.setText(planNumber);
        tvReportRealNumber.setText(realNumber);
        tvReportPercent.setText(percent + "%");

//        if (position % 2 != 0) {
//            layout_list_item.setBackgroundResource(R.color.gray_line_list_item);
//        }


//        imgAvatar.setImageResource(imageId[position]);


        return rowView;
    }

}
