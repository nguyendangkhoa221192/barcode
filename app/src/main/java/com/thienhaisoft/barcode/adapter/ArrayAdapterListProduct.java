package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.InputFilterMinMax;


import java.util.ArrayList;

/**
 * Created by khoam on 26/05/2017.
 */

public class ArrayAdapterListProduct extends BaseAdapter implements Filterable {

    private ArrayList<ProductItem> listProductItems = new ArrayList<>();
    private ArrayList<ProductItem> mOriginalValues = new ArrayList<>();
    private Double point_of_customer = null;
    private Activity a = null;
    private boolean isFirst = false;

    static class ViewHolder {
        EditText caption;
    }

    public ArrayAdapterListProduct(Activity a, ArrayList<ProductItem> productItems, Double point_of_customer) {
        this.listProductItems = Common.sortProductItem(productItems);
        this.mOriginalValues = this.listProductItems;
        this.a = a;
        this.point_of_customer = point_of_customer;
    }

    public void swapItem(ArrayList<ProductItem> productItems) {
        this.listProductItems = Common.sortProductItem(productItems);
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return listProductItems.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder = new ViewHolder();
        // not recycling view
        LayoutInflater inflater = LayoutInflater.from(this.a);
        view = inflater.inflate(R.layout.layout_lv_item, null);
        holder.caption = (EditText) view.findViewById(R.id.earnpoit_listview_item_number);

        if (position % 2 != 0) {
            view.setBackgroundColor(parent.getResources().getColor(R.color.earnpoint_color_bg_item_lv));
        } else {
            view.setBackgroundColor(parent.getResources().getColor(R.color.WHITE));
        }
        final ProductItem productItem = listProductItems.get(position);

        TextView txt_name = (TextView) view.findViewById(R.id.earnpoit_listview_item_productname);
        TextView txt_exist = (TextView) view.findViewById(R.id.earnpoit_listview_item_exist);
        txt_name.setText(productItem.getCode() + "-" + productItem.getName());
        txt_exist.setText(productItem.getQuantity() + "");

        if (productItem.getNumber_choose() != 0) {
            holder.caption.setText(productItem.getNumber_choose() + "");
        }
        holder.caption.setId(position);

        holder.caption.addTextChangedListener(new MyTextWatcher(holder.caption));
        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && isFirst) {
                    swapItem(listProductItems);
                }
            }
        });

        holder.caption.setFilters(new InputFilter[]{new InputFilterMinMax(0, productItem.getQuantity())});
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        isFirst = false;
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText editText;

        private MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals("")) {
                s = "0";
            }
            final int position = this.editText.getId();
//                final EditText Caption = this.editText;
            listProductItems.get(position).setNumber_choose(Integer.parseInt(s.toString()));
            TextView txt_point_to_product = (TextView) a.findViewById(R.id.sellout_txt_point_in_order);
            TextView txt_point_of_customer = (TextView) a.findViewById(R.id.sellout_txt_total_point);
            Double total_point = 0.0;
            for (int i = 0; i < listProductItems.size(); i++) {
                ProductItem productItem1 = listProductItems.get(i);
                if (productItem1.getNumber_choose() > 0) {
                    // tính thổng điểm khi chọn số lượng sản phẩm
                    total_point = total_point + productItem1.getNumber_choose() * productItem1.getPoint();
                }
            }
            txt_point_to_product.setText(total_point + "");
            txt_point_of_customer.setText((point_of_customer + total_point) + "");
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFirst = true;
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<ProductItem> filterArray = new ArrayList<ProductItem>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(listProductItems);
                }

                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        String data = Common.deAccent(mOriginalValues.get(i).getCode() + "-" + mOriginalValues.get(i).getName());
                        if (data.toLowerCase().contains(constraint.toString())) {
                            filterArray.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = filterArray.size();
                    results.values = filterArray;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                listProductItems = (ArrayList<ProductItem>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
