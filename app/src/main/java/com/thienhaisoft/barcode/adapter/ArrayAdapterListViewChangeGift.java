package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.ChangeGiftItem;
import com.thienhaisoft.barcode.items.OrderClass;

import java.util.ArrayList;

/**
 * Created by khoam on 25/05/2017.
 */

public class ArrayAdapterListViewChangeGift extends BaseAdapter {

    private Context context = null;
    private ArrayList<ChangeGiftItem> orderClassArrayList = null;

    public ArrayAdapterListViewChangeGift(Context context, ArrayList<ChangeGiftItem> orderClassArrayList) {
        this.context = context;
        this.orderClassArrayList = orderClassArrayList;
    }

    public void swapItem(ArrayList<ChangeGiftItem> listOrderClasses) {
        this.orderClassArrayList = listOrderClasses;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return this.orderClassArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            view = inflater.inflate(R.layout.changegift_layout_item_listview, null);
        }
        ChangeGiftItem orderItem = orderClassArrayList.get(position);
        TextView txt_date = (TextView) view.findViewById(R.id.listview_changegift_item_date);
        TextView txt_product = (TextView) view.findViewById(R.id.listview_changegift_item_giftname);
        TextView txt_point = (TextView) view.findViewById(R.id.listview_changegift_item_point);
        txt_date.setText(orderItem.getCreate_date());
        txt_product.setText(orderItem.getName());
        txt_point.setText(orderItem.getPoint());

        return view;
    }
}
