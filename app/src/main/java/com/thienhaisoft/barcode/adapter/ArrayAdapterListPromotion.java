package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.PromotionItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.InputFilterMinMax;

import java.util.ArrayList;
import java.util.TreeSet;

/**
 * Created by khoam on 29/05/2017.
 */

public class ArrayAdapterListPromotion extends BaseAdapter {

    private ArrayList<PromotionItem> promotionList = new ArrayList<>();
    private Activity activity = null;
    private View view;
    private boolean isFirst = false;
    private int total_number_buy = 0;


    public void addItem(final PromotionItem item) {
        promotionList.add(item);
        notifyDataSetChanged();
    }

    public ArrayAdapterListPromotion(Activity activity, ArrayList<PromotionItem> promotionItems, int total_number_buy) {
        this.promotionList = promotionItems;
        this.activity = activity;
        this.total_number_buy = total_number_buy;
    }

    public void swapItem(ArrayList<PromotionItem> productItems) {
        try {
            this.promotionList = productItems;
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return promotionList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.activity);
        view = inflater.inflate(R.layout.layout_changegift_lv_item, null);
        PromotionItem promotionItem = promotionList.get(position);

        TextView txt_gift_name = (TextView) view.findViewById(R.id.changegift_listview_item_giftname);
        TextView txt_point = (TextView) view.findViewById(R.id.changegift_listview_point);
        TextView txt_exist = (TextView) view.findViewById(R.id.changegift_listview_item_exist);

        if (position % 2 != 0) {
            view.setBackgroundColor(parent.getResources().getColor(R.color.earnpoint_color_bg_item_lv));
        } else {
            view.setBackgroundColor(parent.getResources().getColor(R.color.WHITE));
        }

        // Set values
        txt_gift_name.setText(promotionItem.getFree_product_name());
        txt_exist.setText(promotionItem.getSoluongtonkho() + "");
        promotionItem.hanldedPromotion(total_number_buy);
        txt_point.setText(promotionItem.getPromotion_canget() + "");


        final ArrayAdapterListProduct.ViewHolder holder = new ArrayAdapterListProduct.ViewHolder();
        holder.caption = (EditText) view.findViewById(R.id.changegift_listview_item_number);
        if (promotionItem.getNumber_choose() != 0) {
            holder.caption.setText(promotionItem.getNumber_choose() + "");
        }
        holder.caption.setId(position);

        holder.caption.addTextChangedListener(new MyTextWatcher(holder.caption));
        int temp_cout_product = 0;
        if (promotionItem.getSoluongtonkho() > promotionItem.getPromotion_canget()) {
            temp_cout_product = promotionItem.getPromotion_canget();
        } else {
            temp_cout_product = promotionItem.getSoluongtonkho();
        }

        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && isFirst) {
                    notifyDataSetChanged();
                }
            }
        });

        holder.caption.setFilters(new InputFilter[]{new InputFilterMinMax(0, temp_cout_product)});
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        isFirst = false;
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText editText;

        private MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals("")) {
                s = "0";
            }
            final int position = this.editText.getId();
//                final EditText  Caption = this.editText;
            int number_choose = Integer.parseInt(s.toString());
            int number_changed = number_choose*promotionList.get(position).getSale_qtty()/promotionList.get(position).getFree_qtty();
            total_number_buy = total_number_buy + promotionList.get(position).getNumber_product_changed() - number_changed;

            promotionList.get(position).setNumber_product_changed(number_changed);
            promotionList.get(position).setNumber_choose(number_choose);

            for (int i = 0; i < promotionList.size(); i++) {
                promotionList.get(i).hanldedPromotion(total_number_buy);
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFirst = true;
        }
    }
}
