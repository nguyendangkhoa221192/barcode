package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.text.Editable;
import android.text.InputFilter;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.GiftItem;
import com.thienhaisoft.barcode.items.SamplingItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.InputFilterMinMax;

import java.util.ArrayList;

/**
 * Created by khoam on 29/05/2017.
 */

public class ArrayAdapterListSamling extends BaseAdapter {

    private ArrayList<SamplingItem> samplingItemArrayList = new ArrayList<>();
    private Activity activity = null;
    private View view;
    private boolean isFirst = false;

    public ArrayAdapterListSamling(Activity activity, ArrayList<SamplingItem> samplingItemArrayList) {
        this.samplingItemArrayList = Common.sortSamlingItem(samplingItemArrayList);
        this.activity = activity;
    }

    public void swapItem(ArrayList<SamplingItem> productItems) {
        try {
            this.samplingItemArrayList = Common.sortSamlingItem(productItems);
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return samplingItemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.activity);
        view = inflater.inflate(R.layout.layout_samling_lv_item, null);
        SamplingItem item = samplingItemArrayList.get(position);

        TextView tv_productName = (TextView) view.findViewById(R.id.samling_lv_item_product_name);
        TextView tv_exist = (TextView) view.findViewById(R.id.samling_lv_item_exist);

        tv_productName.setText(item.getProductCodeName());
        tv_exist.setText(item.getQuantity());

        if (position % 2 != 0) {
            view.setBackgroundColor(parent.getResources().getColor(R.color.earnpoint_color_bg_item_lv));
        } else {
            view.setBackgroundColor(parent.getResources().getColor(R.color.WHITE));
        }


        final ArrayAdapterListProduct.ViewHolder holder = new ArrayAdapterListProduct.ViewHolder();
        holder.caption = (EditText) view.findViewById(R.id.samling_lv_item_number_choose);
        if (item.getNumber_choose() != 0) {
            holder.caption.setText(item.getNumber_choose() + "");
        }
        holder.caption.setId(position);
        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && isFirst) {
                    swapItem(samplingItemArrayList);
                }
            }
        });

        holder.caption.addTextChangedListener(new MyTextWatcher(holder.caption));
        int max_value = Integer.parseInt(item.getQuantity());
        holder.caption.setFilters(new InputFilter[]{new InputFilterMinMax(0, max_value)});
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        isFirst = false;
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText editText;

        private MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals("")) {
                s = "0";
            }
            final int position = this.editText.getId();
            samplingItemArrayList.get(position).setNumber_choose(Integer.parseInt(s.toString()));
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFirst = true;
        }
    }
}
