package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.ReportOrderItem;
import com.thienhaisoft.barcode.items.ReportStoreItem;

import java.util.ArrayList;

/**
 * Created by khoam on 10/20/2017.
 */

public class ArrayAdapterReportOrder extends BaseAdapter {

    private ArrayList<ReportOrderItem> list;
    private Context context;

    public ArrayAdapterReportOrder(Context context, ArrayList<ReportOrderItem> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            view = inflater.inflate(R.layout.lv_report_order_item, null);
        }

        ReportOrderItem item = list.get(i);

        TextView date = (TextView) view.findViewById(R.id.report_order_item_date);
        TextView code_order = (TextView) view.findViewById(R.id.report_order_item_code);
        TextView customer = (TextView) view.findViewById(R.id.report_order_item_customer);
        TextView product = (TextView) view.findViewById(R.id.report_order_item_product);
        TextView quanlity = (TextView) view.findViewById(R.id.report_order_item_number);

        String customer_data = item.getTen() + " - " + item.getDTDD();
        String product_data = item.getProduct_code() + " - " + item.getProduct_name();

        date.setText(item.getDateFromData());
        code_order.setText(item.getId_order());
        customer.setText(customer_data);
        product.setText(product_data);
        quanlity.setText(item.getQuantity());

        return view;
    }
}
