package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.entities.json.object.StoreGiftEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.utils.StringUtil;

import java.util.List;

/**
 * Created by user on 11/06/2017.
 */

public class ArrayStoreGiftAdapter extends BaseAdapter {
    private List<StoreGiftEntities> listStoreGift;
    private Context context;
    private int[] imageId;


    private GlobalInfo info;
    private String storeGiftName = "";
    private String quantity = "";
    private String avaiQuantity = "";
    private String percent = "";

    public ArrayStoreGiftAdapter(Context context, List<StoreGiftEntities> listReport) {
        this.context = context;
        this.listStoreGift = listReport;
        info = (GlobalInfo) context.getApplicationContext();
    }

    @Override
    public int getCount() {
        if (listStoreGift == null) {
            return 0;
        }
        return listStoreGift.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custome_listview_list_store_gift, parent, false);

        LinearLayout layout_list_item = (LinearLayout) rowView.findViewById(R.id.layout_list_report_item);
        TextView tvStoreGiftName = (TextView) rowView.findViewById(R.id.tvStoreGiftName);
        TextView tvStoreGiftQuantity = (TextView) rowView.findViewById(R.id.tvStoreGiftQuantity);
//        TextView tvStoreGiftAvaiQuantity = (TextView) rowView.findViewById(R.id.tvStoreGiftAvaiQuantity);


        if (listStoreGift != null && listStoreGift.get(position) != null) {
            storeGiftName = listStoreGift.get(position).getName() ;
            quantity = StringUtil.parseAmountMoney(listStoreGift.get(position).getQuantity()) ;
            avaiQuantity = StringUtil.parseAmountMoney(listStoreGift.get(position).getAvailable_quantity()) ;
        }

        tvStoreGiftName.setText(storeGiftName);
        tvStoreGiftQuantity.setText(quantity);
//        tvStoreGiftAvaiQuantity.setText(avaiQuantity);

//        if (position % 2 != 0) {
//            layout_list_item.setBackgroundResource(R.color.gray_line_list_item);
//        }


//        imgAvatar.setImageResource(imageId[position]);


        return rowView;
    }

}
