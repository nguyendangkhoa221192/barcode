package com.thienhaisoft.barcode.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.ReportStoreItem;
import com.thienhaisoft.barcode.items.StoreGiftItem;

import java.util.ArrayList;

/**
 * Created by khoam on 10/20/2017.
 */

public class ArrayAdapterReportStoreGift extends BaseAdapter {

    private ArrayList<StoreGiftItem> list;
    private Context context;

    public ArrayAdapterReportStoreGift(Context context, ArrayList<StoreGiftItem> list) {
        this.list = list;
        this.context = context;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            view = inflater.inflate(R.layout.lv_report_store_item, null);
        }

        StoreGiftItem item = list.get(i);

        TextView ten_sp = (TextView) view.findViewById(R.id.report_store_item_product_name);
        TextView ton_dau = (TextView) view.findViewById(R.id.report_store_item_ton_dau);
        TextView nhap = (TextView) view.findViewById(R.id.report_store_item_nhap);
        TextView dc_tang = (TextView) view.findViewById(R.id.report_store_item_dc_tang);
        TextView dc_giam = (TextView) view.findViewById(R.id.report_store_item_dc_giam);
        TextView xuat = (TextView) view.findViewById(R.id.report_store_item_xuat);
        TextView ton_cuoi = (TextView) view.findViewById(R.id.report_store_item_ton_cuoi);

        ten_sp.setText(item.getCode() + "-" + item.getName());
        ton_dau.setText(item.getTONDAU());
        nhap.setText(item.getNhapqua());
        dc_tang.setText(item.getDC_TANG());
        dc_giam.setText(item.getDC_GIAM());
        xuat.setText(item.getDoiqua());
        ton_cuoi.setText(item.getCur_quantity());

        return view;
    }
}
