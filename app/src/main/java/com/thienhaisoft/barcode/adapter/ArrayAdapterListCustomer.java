package com.thienhaisoft.barcode.adapter;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Telephony;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.InforCustomerByUser;
import com.thienhaisoft.barcode.items.InformationCustomer;
import com.thienhaisoft.barcode.utils.Common;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by khoam on 26/05/2017.
 */

public class ArrayAdapterListCustomer extends BaseAdapter implements Filterable {

    private ArrayList<InforCustomerByUser> informationCustomers = new ArrayList<>();
    private ArrayList<InforCustomerByUser> mOriginalValues = new ArrayList<>();
    private Context context = null;
    private Activity activity = null;
    private final int REQUEST_CODE_ASK_PERMISSION_CALL_PHONE = 1;

    public ArrayAdapterListCustomer(Activity activity, ArrayList<InforCustomerByUser> informationCustomers) {
        this.informationCustomers = informationCustomers;
        this.mOriginalValues = this.informationCustomers;
        this.context = activity;
        this.activity = activity;
    }

    public void swapItem(ArrayList<InforCustomerByUser> informationCustomers) {
        this.informationCustomers = informationCustomers;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return informationCustomers.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(this.context);
            view = inflater.inflate(R.layout.layout_lv_item_listcustomer, null);
        }
        final InforCustomerByUser user = informationCustomers.get(position);
        TextView txt_store = (TextView) view.findViewById(R.id.listcustomer_txt_store);
        TextView txt_customer = (TextView) view.findViewById(R.id.listcustomer_txt_customer);
        TextView txt_point = (TextView) view.findViewById(R.id.listcustomer_txt_point);
        ImageButton ibtn_sms = (ImageButton) view.findViewById(R.id.listcustomer_ibtn_sms);
        ImageButton ibtn_call = (ImageButton) view.findViewById(R.id.listcustomer_ibtn_call);

        ibtn_call.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAction(user.getDTDD());
            }
        });
        ibtn_sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendSMSAction(user.getDTDD());
            }
        });

        txt_store.setText(user.getStoreCode() + " - " + user.getStoreName());
        txt_customer.setText(user.getTen() + "\r\n" + user.getDTDD());
        txt_point.setText(user.getPointsproduct() + "");


        return view;
    }

//    @Override
//    public void onClick(View v) {
//        switch (v.getId()) {
//            case R.id.listcustomer_ibtn_sms:
//                sendSMSAction();
//                break;
//            case R.id.listcustomer_ibtn_call:
//                callAction();
//                break;
//            default:
//                break;
//        }
//    }

    private void callAction(String phone) {
        int checkPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);
        if (checkPermission == PackageManager.PERMISSION_GRANTED) {
            Intent call = new Intent(Intent.ACTION_CALL);
            call.setData(Uri.parse("tel:" + phone));
            context.startActivity(call);
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CALL_PHONE}, REQUEST_CODE_ASK_PERMISSION_CALL_PHONE);
        }
    }

    private void sendSMSAction(String phone) {
        int checkPermission = ContextCompat.checkSelfPermission(context, Manifest.permission.SEND_SMS);
        if (checkPermission == PackageManager.PERMISSION_GRANTED) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) // At least KitKat
            {
                String defaultSmsPackageName = Telephony.Sms.getDefaultSmsPackage(activity); // Need to change the build to API 19

                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("text/plain");
                sendIntent.putExtra("address", phone);

                if (defaultSmsPackageName != null)// Can be null in case that there is no default, then the user would be able to choose
                // any app that support this intent.
                {
                    sendIntent.setPackage(defaultSmsPackageName);
                }
                activity.startActivity(sendIntent);

            } else // For early versions, do what worked for you before.
            {
                Intent smsIntent = new Intent(android.content.Intent.ACTION_VIEW);
                smsIntent.setType("vnd.android-dir/mms-sms");
                smsIntent.putExtra("address", phone);
                smsIntent.putExtra("sms_body", "message");
                activity.startActivity(smsIntent);
            }
        } else {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.SEND_SMS}, REQUEST_CODE_ASK_PERMISSION_CALL_PHONE);
        }
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults results = new FilterResults();
                ArrayList<InforCustomerByUser> filterArray = new ArrayList<InforCustomerByUser>();

                if (mOriginalValues == null) {
                    mOriginalValues = new ArrayList<>(informationCustomers);
                }

                if (constraint == null || constraint.length() == 0) {
                    results.count = mOriginalValues.size();
                    results.values = mOriginalValues;
                } else {
                    constraint = constraint.toString().toLowerCase();
                    for (int i = 0; i < mOriginalValues.size(); i++) {
                        InforCustomerByUser userItem = mOriginalValues.get(i);
                        String data = Common.deAccent(userItem.getShopName() + userItem.getHoLot() + userItem.getTen() + userItem.getDTDD());
                        if (data.toLowerCase().contains(constraint.toString())) {
                            filterArray.add(mOriginalValues.get(i));
                        }
                    }
                    // set the Filtered result to return
                    results.count = filterArray.size();
                    results.values = filterArray;
                }
                return results;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                informationCustomers = (ArrayList<InforCustomerByUser>) results.values;
                notifyDataSetChanged();
            }
        };
        return filter;
    }
}
