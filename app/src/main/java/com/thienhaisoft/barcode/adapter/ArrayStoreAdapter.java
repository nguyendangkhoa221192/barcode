package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.activity.ResultStore;
import com.thienhaisoft.barcode.entities.json.object.StoreEntities;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.StringUtil;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 11/06/2017.
 */

public class ArrayStoreAdapter extends BaseAdapter {
    private List<StoreEntities> listStore;
    private Activity activity;
    private int[] imageId;


    private GlobalInfo info;
    private String storeName = "";
    private String storeAddress = "";
    private String time = "";
    private String phone = "";
    private String latlng = "";
    private String checkin = "";
    private String checkout = "";

    public ArrayStoreAdapter(Activity activity, List<StoreEntities> listStore) {
        this.activity = activity;
        this.listStore = listStore;
        info = (GlobalInfo) activity.getApplicationContext();
    }

    @Override
    public int getCount() {
        if (listStore == null) {
            return 0;
        }
        return listStore.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int position, View view, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(activity.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.custome_listview_list_store, parent, false);

        LinearLayout layout_list_item = (LinearLayout) rowView.findViewById(R.id.layout_list_item);
        TextView tvStoreName = (TextView) rowView.findViewById(R.id.tvStoreName);
        TextView tvStoreAddress = (TextView) rowView.findViewById(R.id.tvStoreAdress);
        TextView tvStoreTime = (TextView) rowView.findViewById(R.id.tvStoreTime);
        TextView tvCheckin = (TextView) rowView.findViewById(R.id.tvCheckin);
        TextView tvCheckout = (TextView) rowView.findViewById(R.id.tvCheckout);
        Button btnResult = (Button) rowView.findViewById(R.id.choosestore_btnResult);

        try {
            if (listStore != null && !listStore.isEmpty()) {
                StoreEntities store = listStore.get(position);
                time = "";
                checkin = "";
                checkout = "";
                storeName = "";
                storeAddress = "";
                if (store != null) {
                    storeName = store.getStore_name();
                    storeAddress = store.getAddress();
                    phone = store.getPhone();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
                    Date dBegin = sdf.parse(store.getCreated_begin());
                    if (dBegin != null) {

                        time += StringUtil.dateToString(dBegin, "dd/MM");
                    }
                    if (store.getCheckin() != null) {
                        checkin = store.getCheckin();
                    }
                    if (store.getCheckout() != null) {
                        checkout = store.getCheckout();
                    }
                    if (store.getCheckout() != null && store.getCheckin() != null) {
                        btnResult.setVisibility(View.VISIBLE);
                    }
                }
                if (store.getStatus() == 0) {
                    if (store.getLock_order() != 1 || !store.isActive()) {
                        layout_list_item.setBackgroundResource(R.color.WHITE);
                    }
                }
            }

            tvStoreName.setText(storeName);
            tvStoreAddress.setText(storeAddress);
            tvStoreTime.setText(time);
//            tvStorePhone.setText(latlng);
            if (!checkin.isEmpty()) {
                tvCheckin.setText("In: " + checkin + "H");
            } else {
                tvCheckin.setText("");
            }
            if (!checkout.isEmpty()) {
                tvCheckout.setText("Out: " + checkout + "H");
            } else {
                tvCheckout.setText("");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        btnResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(new Intent(activity, ResultStore.class));
                StoreEntities store = listStore.get(position);
                info.setCurStore(store);
                activity.startActivity(i);
            }
        });
        return rowView;
    }
}
