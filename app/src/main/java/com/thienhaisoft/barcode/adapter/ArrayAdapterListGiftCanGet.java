package com.thienhaisoft.barcode.adapter;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.TextView;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.items.GiftItem;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.utils.Common;
import com.thienhaisoft.barcode.utils.InputFilterMinMax;

import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * Created by khoam on 29/05/2017.
 */

public class ArrayAdapterListGiftCanGet extends BaseAdapter {

    private ArrayList<GiftItem> giftItemArrayList = new ArrayList<>();
    private Activity activity = null;
    private Double point_of_customer = 0.0;
    private View view;
    private boolean isFirst = false;

    public ArrayAdapterListGiftCanGet(Activity activity, ArrayList<GiftItem> giftItems, Double point_of_customer) {
        this.giftItemArrayList = Common.sortGiftItem(giftItems);
        this.activity = activity;
        this.point_of_customer = point_of_customer;
    }

    public void swapItem(ArrayList<GiftItem> productItems) {
        try {
            this.giftItemArrayList = Common.sortGiftItem(productItems);
            notifyDataSetChanged();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public int getCount() {
        return giftItemArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(this.activity);
        view = inflater.inflate(R.layout.layout_changegift_lv_item, null);
        GiftItem giftItem = giftItemArrayList.get(position);

        TextView txt_gift_name = (TextView) view.findViewById(R.id.changegift_listview_item_giftname);
        TextView txt_point = (TextView) view.findViewById(R.id.changegift_listview_point);
        TextView txt_exist = (TextView) view.findViewById(R.id.changegift_listview_item_exist);

        if (position % 2 != 0) {
            view.setBackgroundColor(parent.getResources().getColor(R.color.earnpoint_color_bg_item_lv));
        } else {
            view.setBackgroundColor(parent.getResources().getColor(R.color.WHITE));
        }

        // Set values
        txt_gift_name.setText(giftItem.getCode() + "-" +giftItem.getStr_gift_name());
        txt_exist.setText(giftItem.getI_quantity() + "");
        txt_point.setText(giftItem.getDou_point_to_product() + "");


        final ArrayAdapterListProduct.ViewHolder holder = new ArrayAdapterListProduct.ViewHolder();
        holder.caption = (EditText) view.findViewById(R.id.changegift_listview_item_number);
        if (giftItem.getI_number_choose() != 0) {
            holder.caption.setText(giftItem.getI_number_choose() + "");
        }
        holder.caption.setId(position);
        holder.caption.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b && isFirst) {
                    swapItem(giftItemArrayList);
                }
            }
        });

        holder.caption.addTextChangedListener(new MyTextWatcher(holder.caption));

        holder.caption.setFilters(new InputFilter[]{new InputFilterMinMax(0, giftItem.getI_quantity())});
        return view;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
        isFirst = false;
    }

    private class MyTextWatcher implements TextWatcher {

        private EditText editText;

        private MyTextWatcher(EditText editText) {
            this.editText = editText;
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if (s.toString().equals("")) {
                s = "0";
            }
            final int position = this.editText.getId();
//                final EditText  Caption = this.editText;
            giftItemArrayList.get(position).setI_number_choose(Integer.parseInt(s.toString()));
            TextView txt_point_to_product = (TextView) activity.findViewById(R.id.changegift_txt_point_in_order);
            TextView txt_total_point = (TextView) activity.findViewById(R.id.changegift_txt_point_accumulation);
            Double total_point = 0.0;
            for (int i = 0; i < giftItemArrayList.size(); i++) {
                GiftItem giftItem = giftItemArrayList.get(i);
                if (giftItem.getI_number_choose() > 0) {
                    // tính thổng điểm khi chọn số lượng sản phẩm
                    total_point = total_point + giftItem.getI_number_choose() * giftItem.getDou_point_to_product();
                }
            }
            txt_point_to_product.setText(total_point + "");
            txt_total_point.setText((point_of_customer - total_point) + "");
        }

        @Override
        public void afterTextChanged(Editable s) {
            isFirst = true;
//            swapItem(giftItemArrayList);
//            new Handler().postDelayed(new Runnable() {
//                @Override
//                public void run() {
//                    if (view != null) {
//                        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
//                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
//                    }
//                }
//            }, 100);  //scroll after 50ms
        }
    }
}
