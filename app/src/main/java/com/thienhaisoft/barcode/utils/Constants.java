package com.thienhaisoft.barcode.utils;

/**
 * Created by user on 21/05/2017.
 */

public class Constants {

    public static String ACTION_BROADCAST = "broadcast_dms_thienhaisoft";
    public static final String API_PARAM_COOKIE = "Cookie";
    public static final String PAR_SESSIONID = "sessionid";
    public static final String VERSION_APP = "2.3";
    public static final String FORMAT_DATE = "dd/MM/yyyy";

//    public static final String ROOT_URL = "http://demo.thienhaisoft.com/thsams/web/index.php";
    public static final String ROOT_URL = "http://14.176.231.222:8680/thsams/web/index.php";

    //Params result login for staff
    public static final String RESULT_LOGIN_ID = "id";
    public static final String RESULT_LOGIN_USERNAME = "username";
    public static final String RESULT_LOGIN_FULLNAME = "full_name";
    public static final String RESULT_LOGIN_AUTH_KEY = "auth_key";
    public static final String RESULT_LOGIN_PASSWORD_HASH = "password_hash";
    public static final String RESULT_LOGIN_PASSWORD_RESET_TOKEN = "password_reset_token";
    public static final String RESULT_LOGIN_EMAIL = "email";
    public static final String RESULT_LOGIN_ROLE = "role";
    public static final String RESULT_LOGIN_STATUS = "status";
    public static final String RESULT_LOGIN_CREATE_AT = "created_at";
    public static final String RESULT_LOGIN_UPDATE_AT = "updated_at";
    public static final String RESULT_LOGIN_IS_ONLINE = "is_online";
    public static final String RESULT_LOGIN_SHOP_ID = "shop_id";
    public static final String RESULT_LOGIN_DISTANCE = "distance";
    public static final String RESULT_LOGIN_LISTSHOP = "listshop";
    public static final String RESULT_LOGIN_LISTAREA = "listarea";



    //Params common
    public static final String RESULTAPI_STATUS = "status";
    public static final String RESULTAPI_DATA = "data";
    public static final int RESULTAPI_SUCCESS = 1;
    public static final int RESULTAPI_FAIL = 0;
    public static final String PAR_ID = "id";

    //Params API create customer
    public static final String API_CREATE_CUSTOMER = Constants.ROOT_URL + "?r=api%2Fcreatecustomer";
    public static final String PAR_CREATE_CUSTOMER_PHONE_NUMBER = "DTDD";
    public static final String PAR_CREATE_CUSTOMER_NAME = "full_name";
    public static final String PAR_CREATE_CUSTOMER_GENDER = "gender";
    public static final String PAR_CREATE_CUSTOMER_ADDRESS = "DiaChi";
    public static final String PAR_CREATE_CUSTOMER_EMAIL = "email";
    public static final String PAR_CREATE_CUSTOMER_CREATEBYUSER = "createbyuser";

    //Result API create customer
    public static final String RESULTAPI_CREATE_CUSTOMER_PID = "Pid";
    public static final String RESULTAPI_CREATE_CUSTOMER_FIRSTNAME = "HoLot";
    public static final String RESULTAPI_CREATE_CUSTOMER_LASTNAME = "Ten";
    public static final String RESULTAPI_CREATE_CUSTOMER_PHONE_NUMBER = "DTDD";
    public static final String RESULTAPI_CREATE_CUSTOMER_CREATED_DATE = "NgayTao";
    public static final String RESULTAPI_CREATE_CUSTOMER_B2BCODE = "B2BCode";
    public static final String RESULTAPI_CREATE_CUSTOMER_DATA1 = "data1";
    public static final String RESULTAPI_CREATE_CUSTOMER_CUSTID = "CustID";

    //Result API Information customer by user
    public static final String API_INFO_CUSTOMER_BY_USER = Constants.ROOT_URL + "?r=api%2Fviewcustomerbyuser&id=";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_MIEN_CODE = "mien_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_VUNG_CODE = "vung_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_SHOP_CODE = "shop_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_SHOP_NAME = "shop_name";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_TEAM_CODE = "team_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_TEAM_NAME = "team_name";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_USER_ID = "user_id";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_NA_CODE = "na_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_NA_NAME = "na_name";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_CHANNEL_CODE = "channel_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_PARENT_CHANNEL_CODE = "parent_channel_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_STORE_CODE = "store_code";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_STORE_NAME = "store_name";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_CUSTOMER_ID = "customer_id";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_HOLOT = "HoLot";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_TEN = "Ten";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_DTDD = "DTDD";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_DTBAN = "DTBan";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_DIACHI = "DiaChi";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_NGAYTAO = "NgayTao";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_POINTSPRODUCT = "pointsproduct";
    public static final String RESULTAPI_INFO_CUSTOMER_BY_USER_STORE_ID = "store_id";


    //Params API List Product
    public static final String API_LIST_PRODUCT = Constants.ROOT_URL;
    public static final String PAR_LIST_PRODUCT = "store_id";

    //Result API List Product
    public static final String API_LIST_GIFT_CAN_GET = Constants.ROOT_URL + "?r=api%2Fviewlistproductspromo";
    public static final String RESULTAPI_LIST_PRODUCT_ID = "id_product";
    public static final String RESULTAPI_LIST_PRODUCT_ID_CATEGORY = "id_category";
    public static final String RESULTAPI_LIST_PRODUCT_NAME = "name";
    public static final String RESULTAPI_LIST_PRODUCT_DESCRIPTION = "description";
    public static final String RESULTAPI_LIST_PRODUCT_ID_TAX = "id_tax";
    public static final String RESULTAPI_LIST_PRODUCT_BASE_PRICE = "base_price";
    public static final String RESULTAPI_LIST_PRODUCT_QUANTITY = "quantity";
    public static final String RESULTAPI_LIST_PRODUCT_STORE_ID = "store_id";
    public static final String RESULTAPI_LIST_PRODUCT_POINT = "point";
    public static final String RESULTAPI_LIST_PRODUCT_CODE = "code";

    public static final String STR_BLANK = "";

    //Gift for change
    public static final String RESULTAPI_LIST_GIFT_TOTALPOINT = "totalpoint";
    public static final String RESULTAPI_LIST_GIF_PID = "Pid";
    public static final String RESULTAPI_LIST_GIFT_CUSTOMER_NAME = "Ten";
    public static final String RESULTAPI_LIST_GIFT_QUANTITY = "quantity";
    public static final String RESULTAPI_LIST_GIFT_NAME = "name";
    public static final String RESULTAPI_LIST_GIFT_STORE_ID = "store_id";
    public static final String RESULTAPI_LIST_GIFT_PRODUCT_ID = "product_id";
    public static final String RESULTAPI_LIST_GIFT_POINT_TO_PRODUCT = "point_to_product";
    public static final String CUSTOMER_ID = "customer_id";

    public static final int LOCATION_REFRESH_TIME = 10000;  // milisecond
    public static final int LOCATION_REFRESH_DISTANCE = 100; // meter
    public static final int LOCATION_MULTI_DISTANCE = 10;
    public static final float LOCATION_DISTANCE_STORE = 100000000;

    //Search API
    public static final String API_SEARCH_CUSTOMER = Constants.ROOT_URL + "?r=api%2Fsearchcustomer";
    public static final String RESULT_INFO_CUSTOMER_PID = "Pid";
    public static final String RESULT_INFO_CUSTOMER_FIRSTNAME = "HoLot";
    public static final String RESULT_INFO_CUSTOMER_LASTNAME = "Ten";
    public static final String RESULT_INFO_CUSTOMER_PHONE = "DTDD";
    public static final String RESULT_INFO_CUSTOMER_HOME_PHONE = "DTBan";
    public static final String RESULT_INFO_CUSTOMER_ADDRESS = "DiaChi";
    public static final String RESULT_INFO_CUSTOMER_CREATE_DATE = "NgayTao";
    public static final String RESULT_INFO_CUSTOMER_B2B = "B2BCode";
    public static final String RESULT_INFO_CUSTOMER_POINTS_PRODUCT = "pointsproduct";
    public static final String RESULT_INFO_CUSTOMER_POINTS = "Points";
    public static final String RESULT_INFO_CUSTOMER_KHTT_CODE = "KHTTCode";
    public static final String RESULT_INFO_CUSTOMER_DELETE_FLG = "is_deleted";
    public static final String RESULT_INFO_CUSTOMER_CREATE_BY_USER = "createbyuser";
    public static final String RESULT_INFO_CUSTOMER_INFO = "createbyuser";

    public static final String LOGIN_SUCCESS = "LOGIN_SUCCESS";
    public static final String PREFERENCES_NAME = "AMSTHS_PRE";

    // Create order parameter for earn point
    public static final String API_CREATE_ORDER = ROOT_URL + "?r=api/createpesorder&data=";
    public static final String PAR_EARNPOINT_CREATEODER_ID_ORDER_STATUS = "id_order_status";
    public static final String PAR_EARNPOINT_CREATEODER_UPDATE_USER = "update_user";
    public static final String PAR_EARNPOINT_CREATEODER_STORE_ID = "store_id";
    public static final String PAR_EARNPOINT_CREATEODER_ID_STORE = "id_store";
    public static final String PAR_EARNPOINT_CREATEODER_ID_CUSTOMER = "id_customer";
    public static final String PAR_EARNPOINT_CREATEODER_ID_ORDER_TYPE = "id_order_type";
    public static final String PAR_EARNPOINT_CREATEODER_TOTAL_PRICE = "total_price";
    public static final String PAR_EARNPOINT_CREATEODER_TOTAL_PRICE_TAX = "total_price_tax";
    public static final String PAR_EARNPOINT_CREATEODER_GRAND_TOTAL = "grand_total";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST = "cart_list_";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID_PRODUCT = "id_product";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID_PRODUCT_FEATURE = "id_product_feature";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID = "id";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID_CUSTOMIZATION_GROUP = "id_customization_group";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID_CUSTOMIZATION = "id_customization";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_CUSTOMIZATION_NAME = "customization_name";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_NAME = "name";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_QUANTITY = "quantity";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_PRICE_TAX = "price_tax";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_PRICE = "price";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_TAX = "tax";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_ID_USER = "id_user";
    public static final String PAR_EARNPOINT_CREATEODER_CARTLIST_PRODUCT_NOTE = "product_note";

    // Change Gift
    public static final String API_CHANGE_GIFT = Constants.ROOT_URL + "?r=api%2Fchangegift";

    // Create order
    public static final String RESULT_CREATE_ORDER_ID_ORDER = "id_order";
    public static final String RESULT_CREATE_ORDER_LIST_PROMOTION = "dskm";

    // Promotion param
    public static final String RESULT_PROMOTION_NUMBER_BUY = "soluongdamua";
    public static final String RESULT_PROMOTION_NUMBER_PRODUCT_ID = "product_id";
    public static final String RESULT_PROMOTION_NUMBER_SALE_QTTY = "sale_qtty";
    public static final String RESULT_PROMOTION_NUMBER_FREE_QTTY = "free_qtty";
    public static final String RESULT_PROMOTION_NUMBER_FREE_PRODUCT_ID = "free_product_id";
    public static final String RESULT_PROMOTION_NUMBER_FREE_PRODUCT_NAME = "free_product_name";
    public static final String RESULT_PROMOTION_NUMBER_PRODUCT_NAME = "product_name";

    // Param list history order
    public static final String RESULT_HISTORY_ORDER_MIEN_CODE = "mien_code";
    public static final String RESULT_HISTORY_ORDER_PARENT_CHANNEL_CODE = "parent_channel_code";
    public static final String RESULT_HISTORY_ORDER_CHANNEL_CODE = "channel_code";
    public static final String RESULT_HISTORY_ORDER_TEAM_CODE = "team_code";
    public static final String RESULT_HISTORY_ORDER_TEAM_NAME = "team_name";
    public static final String RESULT_HISTORY_ORDER_NAME = "Ten";
    public static final String RESULT_HISTORY_ORDER_DATE = "order_date";
    public static final String RESULT_HISTORY_ORDER_PRODUCT_NAME = "product_name";
    public static final String RESULT_HISTORY_ORDER_QUANTITY = "quantity";
    public static final String RESULT_HISTORY_ORDER_TOTAL_PRICE = "total_price";

    //broadcast
    public static final String BROADCAST_CHOOSE_STORE  = "BROADCAST_CHOOSE_STORE";
    public static final String BROADCAST_LIST_FUNCTION = "BROADCAST_LIST_FUNCTION";
    public static final String BROADCAST_ALERT = "BROADCAST_ALERT";


    //data key broadcast
    public static final String KEY_FRAGMENT_VALUE  = "KEY_FRAGMENT_VALUE";
    public static final String KEY_ALERT_VALUE  = "KEY_ALERT_VALUE";


    //action
    public static final String ACTION_GO_HOME  = "ACTION_GO_HOME";
    public static final String ACTION_CHECKIN  = "ACTION_CHECKIN";
    public static final String ACTION_CREATE_SALEORDER  = "ACTION_CREATE_SALEORDER";
    public static final String ACTION_GET_GIFT  = "ACTION_GET_GIFT";
    public static final String ACTION_WAREHOUSE  = "ACTION_WAREHOUSE";
    public static final String ACTION_REPORT = "ACTION_REPORT";
    public static final String ACTION_CHECKOUT  = "ACTION_CHECKOUT";
    public static final String ACTION_NOTIFY  = "ACTION_NOTIFY";
    public static final String ACTION_CLOSE_STORE  = "ACTION_CLOSE_STORE";
    public static final String ACTION_CHECK_LOCATION  = "ACTION_CHECK_LOCATION";
    public static final String ACTION_LIST_CUSTOMER  = "ACTION_LIST_CUSTOMER";
    public static final String ACTION_REPORT_STORE  = "STORE_REPORT";
    public static final String ACTION_REPORT_ORDER  = "REPORT_ORDER";
    public static final String ACTION_STORE_GIFT  = "STORE_GIFT";


    //ACTION CAMERA
    public static final int ACTION_CAMENRA_DEFAULT = 0;
    public static final int ACTION_CAMENRA_CHECKIN = 1;
    public static final int ACTION_CAMENRA_CHECKOUT = 2;
    public static final int ACTION_CAMENRA_CLOSE_STORE = 3;

    //fragment
    public static final String TAG_HOME = "home";
    public static final String TAG_SALE_ORDER = "saleorder";
    public static final String TAG_CHOOSE_STORE = "chooseStore";
    public static final String TAG_NOTIFICATIONS = "notify";
    public static final String TAG_CHECK_LOCATION = "checklocation";
    public static final String TAG_REPORT = "report";
    public static final String TAG_STORE_GIFT = "storegift";
    public static final String TAG_LIST_CUSTOMER = "listcustomer";
    public static final String TAG_INSTRUCTION = "instrution";
    public static final String TAG_PROFILE = "profile";

    //params store
    public static final String KEY_LIST_STORE = "list_store_view";

    //take a photo
    public static final String JPG_EXTENSION = ".jpg";

    // API Update store
    public static final String API_UPDATE_STORE = Constants.ROOT_URL + "?r=api%2Fupdatestorestock";
    public static final String API_GET_LOCK_DATE = Constants.ROOT_URL + "?r=api%2Fgetlockdate&id=";

    //API Report store
    public static final String API_REPORT_STORE = Constants.ROOT_URL + "?r=api%2Fviewstorestockbystore";
    //API Report order
    public static final String API_REPORT_ORDER = Constants.ROOT_URL + "?r=api%2Freportsaledetail";
    public static final String API_REPORT_ORDER_FILTER = Constants.ROOT_URL + "?r=api%2Freportsaledetailfilter";
    //API Get KPI data
    public static final String API_KPI_DATA = Constants.ROOT_URL + "?r=api%2Freportkpibyuser";
    //API Report store gift more
    public static final String API_REPORT_STORE_GIFT = Constants.ROOT_URL + "?r=api%2Fviewstorestockbyusermore";
    //API Report store gift more
    public static final String API_REPORT_BUG = Constants.ROOT_URL + "?r=api%2Fdebug";
    public static final String API_CHANGE_INFO_PROFILE = Constants.ROOT_URL + "?r=api%2Fchangepassword";
    //API report
    public static final String API_CHANGE_REPORT_SELLOUT = Constants.ROOT_URL + "?r=api%2Freportsaleoutstorefilter";
    public static final String API_CHANGE_REPORT_SELLIN = Constants.ROOT_URL + "?r=api%2Freportsaleinstorefilter";
    public static final String API_CHANGE_REPORT_MHTT = Constants.ROOT_URL + "?r=api%2Freportmhttstorefilter";
    public static final String API_CHANGE_REPORT_KPI = Constants.ROOT_URL + "?r=api%2Freportkpibyuserfilter";

    //API version
    public static final String API_VIEW_VERSION = Constants.ROOT_URL + "?r=api%2Fviewversion";

    //API Control store
    public static final String API_LOCK_STOCK = Constants.ROOT_URL + "?r=api%2Fgetlockdateandstock";

    // API List Samling
    public static final String API_SAMLING = Constants.ROOT_URL + "?r=api%2Fviewlistproductsampling";

    // API Samling
    public static final String API_SAMLING_ACTION = Constants.ROOT_URL + "?r=api%2Fgiftsampling";

    // Result API
    public static final String API_RESULT_ORDER_BY_SHIFT = Constants.ROOT_URL + "?r=api%2Freportsaleoutbyshift";
}
