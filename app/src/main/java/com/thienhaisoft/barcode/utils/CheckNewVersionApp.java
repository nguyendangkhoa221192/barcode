package com.thienhaisoft.barcode.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import org.jsoup.Jsoup;

/**
 * Created by khoam on 10/18/2017.
 */

public class CheckNewVersionApp extends AsyncTask<String, String, String>{

    private Activity activity;
    private String currentVersion;
    private ProgressDialog progressDialog;
    private String newVersion = "";

    public CheckNewVersionApp(Activity activity, String currentVersion) {
        this.activity = activity;
        this.currentVersion = currentVersion;
        progressDialog = new ProgressDialog(activity);
    }

    @Override
    protected void onPreExecute() {
        Common.showProgressDialog(progressDialog, "Kiễm tra cập nhật.", "Vui lòng chờ...");
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... strings) {
        try {
            newVersion = Jsoup.connect("https://play.google.com/store/apps/details?id=com.thienhaisoft.ams&hl=en")
                    .timeout(20000)
                    .userAgent("Mozilla/5.0 (Windows; U; WindowsNT 5.1; en-US; rv1.8.1.6) Gecko/20070725 Firefox/2.0.0.6")
                    .referrer("http://www.google.com")
                    .get()
                    .select("div[itemprop=softwareVersion]")
                    .first()
                    .ownText();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return newVersion;
    }

    @Override
    protected void onPostExecute(String s) {
        try {
//            currentVersion = activity.getApplicationContext().getPackageManager().getPackageInfo("com.thienhaisoft.ams", 0).versionName;
//            Log.e("AA#DASDASDASDASD", currentVersion);
            if (!newVersion.equals(currentVersion)) {
                Common.showAlertDialog(activity, "Cập nhật phiên bản " + newVersion, "Update!", "", "", "OK", new MyListener() {
                    @Override
                    public void functionNegative() {

                    }

                    @Override
                    public void functionNeutral() {

                    }

                    @Override
                    public void functionPositive() {
                        Intent browser = new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.thienhaisoft.ams&hl=vi"));
                        activity.startActivity(browser);
                    }
                });
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Common.FinishProgressDialog(progressDialog);
        super.onPostExecute(s);
    }
}
