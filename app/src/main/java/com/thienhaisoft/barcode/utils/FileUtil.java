package com.thienhaisoft.barcode.utils;

import android.app.Activity;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.global.GlobalInfo;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;

/**
 * Created by Rantaro on 08/27/2017.
 */

public class FileUtil {

    public static Bitmap buildImageWithText(Activity act, Bitmap bitmap, double lat, double lng) {
        FileOutputStream out = null;
        Bitmap thumbnail = null;
        try {
            thumbnail = FileUtil.convertToMutable(bitmap);
            Canvas canvas = new Canvas(thumbnail);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setShadowLayer(10f, 10f, 10f, Color.BLACK);
            //paint.setTextSize(150);
            float scale2 = act.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.checker_image);

//            scale2 = (float) (scale2 * 2);
            paint.setTextSize(scale2);
            // Neu la galaxy s8 thi cho size nho lai
            if (GlobalInfo.getDeviceName(act).indexOf("GALAXY S8") >= 0)
                paint.setTextSize(80);
            // tunglh 17//11/2017
            if (GlobalInfo.getDeviceName(act).toUpperCase().indexOf("GALAXY J5") >= 0)
                paint.setTextSize(20);

            paint.setStrokeWidth(50);
            Rect bounds = new Rect();
            String locationStr = GlobalUtil.getLocationName(act.getApplicationContext(), lat, lng);

            Resources resources = act.getApplicationContext().getResources();
            float scale = resources.getDisplayMetrics().density;
            // tunglh lay dung ty le

            paint.getTextBounds(locationStr, 0, locationStr.length(), bounds);
            if (locationStr.split(";").length > 0) {
                paint.getTextBounds(locationStr.split(";")[0], 0, locationStr.split(";")[0].length(), bounds);
            } else {
                paint.getTextBounds(locationStr, 0, locationStr.length(), bounds);
            }
//            int x = (thumbnail.getWidth() - bounds.width())/12;
            int x = 20;
//                    int y = (thumbnail.getHeight() + bounds.height())/5;
            float y = scale2 + 20;

            for (String line: locationStr.split(";")) {
//                canvas.drawText(line, x * scale, y * scale, paint);
                canvas.drawText(line, x, y, paint);
                y += scale2 + 20;
            }

            Canvas canvasDeviceName = new Canvas(thumbnail);
            Rect boundsDeviceName = new Rect();
            String manufacturer = "";
            String deviceName = GlobalInfo.getDeviceName(act);
            //x = (thumbnail.getWidth() - bounds.width())/12;
            x = 100;
            y = thumbnail.getHeight() - 100;

            paint.getTextBounds(deviceName, 0, deviceName.length(), boundsDeviceName);
            canvasDeviceName.drawText(deviceName, x, y, paint);

//            out = new FileOutputStream(fileImage);
//            thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, out);
            // thumbnail = Bitmap.createScaledBitmap(thumbnail,  600 ,600, true);
//                    checkin_image.setImageBitmap(thumbnail);
//            out.flush();
//            out.close();
            return thumbnail;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return thumbnail;
    }

    public static Bitmap buildImageWithText(Activity act, Bitmap bitmap, String info, double lat, double lng) {
        FileOutputStream out = null;
        Bitmap thumbnail = null;
        try {
            thumbnail = FileUtil.convertToMutable(bitmap);
            Canvas canvas = new Canvas(thumbnail);
            Paint paint = new Paint();
            paint.setColor(Color.WHITE);
            paint.setShadowLayer(10f, 10f, 10f, Color.BLACK);
            //paint.setTextSize(150);
            float scale2 = act.getApplicationContext().getResources().getDimensionPixelSize(R.dimen.checker_image);

//            scale2 = (float) (scale2 * 2);
            paint.setTextSize(scale2);
            // Neu la galaxy s8 thi cho size nho lai
//            if (GlobalInfo.getDeviceName(act).indexOf("GALAXY S8") >= 0)
//                paint.setTextSize(80);
            // tunglh 17//11/2017
//            if (GlobalInfo.getDeviceName(act).toUpperCase().indexOf("GALAXY J5") >= 0)
//                paint.setTextSize(20);

            paint.setStrokeWidth(50);
            Rect bounds = new Rect();
            String address = GlobalUtil.getLocationName(act.getApplicationContext(), lat, lng);
            String locationStr = info;
            if (!address.isEmpty()) {
                locationStr = info + ";" + address;
            }

            Resources resources = act.getApplicationContext().getResources();
            float scale = resources.getDisplayMetrics().density;
            // tunglh lay dung ty le

            paint.getTextBounds(locationStr, 0, locationStr.length(), bounds);
            if (locationStr.split(";").length > 0) {
                paint.getTextBounds(locationStr.split(";")[0], 0, locationStr.split(";")[0].length(), bounds);
            } else {
                paint.getTextBounds(locationStr, 0, locationStr.length(), bounds);
            }
//            int x = (thumbnail.getWidth() - bounds.width())/12;
            int x = 20;
//                    int y = (thumbnail.getHeight() + bounds.height())/5;
            float y = scale2 + 20;

            for (String line: locationStr.split(";")) {
//                canvas.drawText(line, x * scale, y * scale, paint);
                canvas.drawText(line, x, y, paint);
                y += scale2 + 20;
            }

            Canvas canvasDeviceName = new Canvas(thumbnail);
            Rect boundsDeviceName = new Rect();
            String manufacturer = "";
            String deviceName = GlobalInfo.getDeviceName(act);
            //x = (thumbnail.getWidth() - bounds.width())/12;
            x = 100;
            y = thumbnail.getHeight() - 100;

            paint.getTextBounds(deviceName, 0, deviceName.length(), boundsDeviceName);
            canvasDeviceName.drawText(deviceName, x, y, paint);

//            out = new FileOutputStream(fileImage);
//            thumbnail.compress(Bitmap.CompressFormat.JPEG, 50, out);
            // thumbnail = Bitmap.createScaledBitmap(thumbnail,  600 ,600, true);
//                    checkin_image.setImageBitmap(thumbnail);
//            out.flush();
//            out.close();
            return thumbnail;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return thumbnail;
    }

    public static Bitmap convertToMutable(Bitmap imgIn) {
        try {
            //this is the file going to use temporally to save the bytes.
            // This file will not be a image, it will store the raw image data.
            File file = new File(Environment.getExternalStorageDirectory() + File.separator + "temp.tmp");

            //Open an RandomAccessFile
            //Make sure you have added uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE"
            //into AndroidManifest.xml file
            RandomAccessFile randomAccessFile = new RandomAccessFile(file, "rw");

            // get the width and height of the source bitmap.
            int width = imgIn.getWidth();
            int height = imgIn.getHeight();
            Bitmap.Config type = imgIn.getConfig();

            //Copy the byte to the file
            //Assume source bitmap loaded using options.inPreferredConfig = Config.ARGB_8888;
            FileChannel channel = randomAccessFile.getChannel();
            MappedByteBuffer map = channel.map(FileChannel.MapMode.READ_WRITE, 0, imgIn.getRowBytes()*height);
            imgIn.copyPixelsToBuffer(map);
            //recycle the source bitmap, this will be no longer used.
            imgIn.recycle();
            System.gc();// try to force the bytes from the imgIn to be released

            //Create a new bitmap to load the bitmap again. Probably the memory will be available.
            imgIn = Bitmap.createBitmap(width, height, type);
            map.position(0);
            //load it back from temporary
            imgIn.copyPixelsFromBuffer(map);
            //close the temporary file and channel , then delete that also
            channel.close();
            randomAccessFile.close();

            // delete the temp file
            file.delete();

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return imgIn;
    }

}
