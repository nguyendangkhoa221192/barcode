package com.thienhaisoft.barcode.utils;

import android.app.AlertDialog;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.location.Address;
import android.location.Geocoder;
import android.net.ConnectivityManager;
import android.telephony.TelephonyManager;

import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.global.ServerPath;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Chua cac ham util chung trong chuong trinh
 *
 * @author: TruongHN
 * @version: 1.0
 * @since: 1.0
 */
public class GlobalUtil extends Application{

	private static final String PERMISSION_ACCESS_MOCK_LOCATION = "android.permission.ACCESS_MOCK_LOCATION";
	static volatile GlobalUtil instance;
	private static String pathSynDataFileName = "";
	static boolean keyboardVisible = false;

	static volatile AlertDialog alertDialogCheckOnlineInDate = null;
	static volatile AlertDialog alertDialogCheckSettingTime = null;
	private static volatile AlertDialog alertDialogAutoTime = null;

	public GlobalUtil() {
	}

	public static boolean checkIsTablet(Context activityContext) {
        int screenLayout = activityContext.getResources().getConfiguration().screenLayout;
        screenLayout &= Configuration.SCREENLAYOUT_SIZE_MASK;
        if(screenLayout == Configuration.SCREENLAYOUT_SIZE_LARGE || screenLayout == Configuration.SCREENLAYOUT_SIZE_XLARGE){
            return true;
        }else{
            return false;
        }

	}

	public static interface IDataValidate<T>{
		boolean valid(T ob);
	}

	public static GlobalUtil getInstance() {
		if (instance == null) {
			instance = new GlobalUtil();
		}
		return instance;
	}

	public static int dip2Pixel(int dips) {
		int ret = (int) (GlobalInfo.getInstance().getAppContext()
				.getResources().getDisplayMetrics().density
				* dips + 0.5f);
		return ret;
	}

	/**
	 * Kiem tra dang ket noi mang theo gi (wifi, 2g, 3g..)
	 * @author: BANGHN
	 * @return
	 */
	public static int getTypeNetwork() {
		int type = 0;
		try {
			if (checkNetworkAccess()) {
				ConnectivityManager cm = (ConnectivityManager) GlobalInfo
						.getInstance().getAppContext()
						.getSystemService(Context.CONNECTIVITY_SERVICE);

				if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
						.isConnectedOrConnecting()) {
					type = 1;// wifi
				} else {
					TelephonyManager tm = (TelephonyManager) GlobalInfo
							.getInstance().getAppContext()
							.getSystemService(Context.TELEPHONY_SERVICE);

					if ((tm.getNetworkType() == TelephonyManager.NETWORK_TYPE_HSDPA)) {
						type = 2;// 3g
					} else if ((tm.getNetworkType() == TelephonyManager.NETWORK_TYPE_GPRS)) {
						type = 3;// GPRS"
					} else if ((tm.getNetworkType() == TelephonyManager.NETWORK_TYPE_EDGE)) {
						type = 4;// EDGE"
					}
				}
			}
		} catch (Exception ex) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
		}
		return type;
	}

	/**
	 * save object vao file
	 *
	 * @author: DoanDM
	 * @param object
	 * @param fileName
	 * @return: void
	 * @throws:
	 */
	public static void saveObject(Object object, String fileName) {
		try {
			FileOutputStream fos = GlobalInfo.getInstance().getAppContext()
					.openFileOutput(fileName, Context.MODE_PRIVATE);
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(object);
			oos.close();
			fos.close();
		} catch (Exception e) {
			//MyLog.logToFile("Profile", "Error save object: " + e.toString());
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
	}

	/**
	 * Server dang su dung
	 *
	 * @author banghn
	 * @return
	 */
	public static String getServerType() {
		String linkServer = ServerPath.SERVER_TYPE;

//		if (!GlobalInfo.isIS_VERSION_SEND_DATA()) {
//			linkServer += "_NOLOG";
//		}
//
//		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR()) {
//			linkServer += "_4EMU";
//		}
		return linkServer;
	}


	/**
	 * Delete nhung file anh chup truoc ngay hien tai
	 * @author: BANGHN
	 */
//	public static void deleteTempTakenPhoto(){
//		try{
//			SimpleDateFormat dateFormat = new SimpleDateFormat(DateUtils.DATE_FORMAT_DEFAULT);
//			Date d2 = dateFormat.parse(DateUtils
//					.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_DEFAULT));
//			File yourDir = new File(ExternalStorage.getTakenPhotoPath(
//					GlobalInfo.getInstance().getAppContext()).getAbsolutePath());
//			String name;
//			for (File f : yourDir.listFiles()) {
//				if (f.isFile() && f.getName().length() > 20 && f.getName().contains("_")) {
//					try {// co the co file ko phai dinh dang ngay thang
//						name = f.getName();
//						name = name.substring(11, 30);
//						Date d1 = dateFormat.parse(name);
//						int days = DateUtils.daysBetween(d1, d2);
//						if (days > 10) {
//							f.delete();
//						}
//					} catch (Exception e) {
//					}
//				}
//			}
//		} catch (Exception e) {
//
//		}
//	}


	@SuppressWarnings("unchecked")
	public static <T> T readObject(String fileName, T obDefault, IDataValidate<T> validation) {
		T result = null;
		Object ob = readObject(fileName);
		if (ob == null) {
			MyLog.d("readObject", fileName + " null set default " + obDefault);
		} else{
			try {
				result = (T) ob;
			} catch (ClassCastException ex) {
				MyLog.d("readObject", fileName + " cast fail " + ob);
			} catch (Exception ex) {
				MyLog.d("readObject", fileName + " fail " + ob);
			}
		}

		try {
			//valid result
			if (!validation.valid(result)) {
				result = obDefault;
			}
		} catch (ClassCastException ex) {
			result = obDefault;
			MyLog.d("readObject", "validation " + fileName + " cast fail " + ob);
		} catch (Exception ex) {
			result = obDefault;
			MyLog.d("readObject", "validation " + fileName + " fail " + ob);
		}

		return result;
	}
	/**
	 * Doc file
	 *
	 * @author: DoanDM
	 * @param: ct
	 * @param fileName
	 * @return: void
	 * @throws:
	 */
	public static Object readObject(String fileName) {
		Object object = null;
		FileInputStream fis = null;
		ObjectInputStream ois = null;
		try {
			if (isExistFile(fileName)) {
				fis = GlobalInfo.getInstance().getAppContext()
						.openFileInput(fileName);
				if (fis != null) {// ton tai file
					ois = new ObjectInputStream(fis);
					object = ois.readObject();

				}
			}
		} catch (Exception e) {
			object = null;
			//MyLog.logToFile("Profile", "Error read object: " + e.toString());
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		} finally {
			try {
				if (ois != null) {
					ois.close();
				}
			} catch (Exception e) {
			}
			try {
				if (fis != null) {
					fis.close();
				}
			} catch (Exception e) {
			}
		}
		return object;
	}

	/**
	 * Kiem tra file ton tai hay khong
	 *
	 * @author DoanDM
	 * @param fileName
	 * @return
	 */
	public static boolean isExistFile(String fileName) {
		try {
			if (!StringUtil.isNullOrEmpty(fileName)) {
				String[] s = GlobalInfo.getInstance().getAppContext()
						.fileList();
				for (int i = 0, size = s.length; i < size; i++) {
					if (fileName.equals(s[i].toString())) {
						return true;
					}
				}
			}
		} catch (Exception e) {
			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
		}
		return false;
	}

	public static String getLocationName(Context application, double lat, double lng) {
		String locationName = "";
		String addressLine = "";
		try {
			Geocoder geo = new Geocoder(application, Locale.getDefault());
			List<Address> addresses = geo.getFromLocation(lat, lng, 1);
			if (addresses.isEmpty()) {
				locationName = "";
			} else if (addresses.size() > 0) {
				locationName = addresses.get(0).getFeatureName() + " " + addresses.get(0).getSubThoroughfare() + ", " + addresses.get(0).getSubAdminArea() + ", " + addresses.get(0).getAdminArea();
				addressLine = addresses.get(0).getAddressLine(0);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		if (addressLine != null && !addressLine.isEmpty()) {
			if (addressLine.indexOf(", Việt Nam") != -1) {
				addressLine = addressLine.substring(0, addressLine.indexOf(", Việt Nam"));
			}

			boolean isBreak = false;
			int doc = 0;
			for (int i = 0; i < addressLine.length(); i++) {
				if(addressLine.charAt(i) == ',') {
					doc++;
				}
				if (doc == 2) {
					addressLine = addressLine.substring(0, i + 2) + ";" + addressLine.substring(i + 2, addressLine.length());
					break;
				}
			}

			return addressLine;
		}
		return locationName;
	}

	/*
	 * delete mot thu muc
	 */
//	public void deleteDir(File dir) {
//		if (dir.isDirectory()) {
//			String[] children = dir.list();
//			for (int i = 0; i < children.length; i++) {
//				new CacheCleanTask().execute(new File(dir, children[i]));
//				(new File(dir, children[i])).delete();
//			}
//		}
//	}
//
//	/**
//	 * Hien thi man hinh setting thoi gian
//	 *
//	 * @author: TruongHN
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogSettingTime() {
//		try {
//			if (alertDialogCheckSettingTime != null && alertDialogCheckSettingTime.isShowing()) {
//				try {
//					alertDialogCheckSettingTime.dismiss();
//				} catch (Exception e) {
//					MyLog.e("showDialogSettingTime", e);
//				}
//			}
//
//			alertDialogCheckSettingTime = new AlertDialog.Builder(GlobalInfo.getInstance().getActivityContext())
//					.create();
//			alertDialogCheckSettingTime.setCanceledOnTouchOutside(false);
//			alertDialogCheckSettingTime.setCancelable(false);
//			alertDialogCheckSettingTime.setTitle(StringUtil.getString(R.string.TEXT_WARNING));
//			alertDialogCheckSettingTime.setMessage(StringUtil.getString(R.string.ERROR_TIME_ONLINE_INVALID));
//			alertDialogCheckSettingTime.setButton(AlertDialog.BUTTON_POSITIVE,
//					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE), new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//							// hien thi man hinh setting
//							GlobalUtil.startActivityOtherApp(GlobalInfo.getInstance().getActivityContext(),
//									new Intent(Settings.ACTION_DATE_SETTINGS));
//							return;
//
//						}
//					});
//			alertDialogCheckSettingTime.show();
//		} catch (Exception ex) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//	}
//
//	/**
//	 * Hien thi man hinh setting thoi gian bat buoc chon auto setting time
//	 *
//	 * @author: BangHN
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogSettingTimeAutomatic() {
//		String language = Locale.getDefault().getDisplayLanguage();
//		SpannableObject strNotice = new SpannableObject();
//		if (language.equals("Tieng Viet")) {
//			strNotice.addSpan(
//					StringUtil.getString(R.string.TEXT_MSG_CHOOSE_OPTION),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//			strNotice.addSpan("[Thoi gian tu dong] ",
//					ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//			strNotice.addSpan(StringUtil.getString(R.string.TEXT_AND),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//			strNotice.addSpan("[Mui gio tu dong] (Mui gio +7)",
//					ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//			strNotice.addSpan(StringUtil.getString(R.string.TEXT_FOR_TABLET),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//		} else {
//			strNotice.addSpan(
//					StringUtil.getString(R.string.TEXT_MSG_CHOOSE_OPTION),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//			strNotice.addSpan("[Automatic date and time] ",
//					ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//			strNotice.addSpan(StringUtil.getString(R.string.TEXT_AND),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//			strNotice.addSpan("[Automatic time zone] (Time zone +7)",
//					ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//			strNotice.addSpan(StringUtil.getString(R.string.TEXT_FOR_TABLET),
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//		}
//
//		try {
//			if (getAlertDialogAutoTime() != null && getAlertDialogAutoTime().isShowing()) {
//				try {
//					getAlertDialogAutoTime().dismiss();
//				} catch (Exception e) {
//					MyLog.e("showDialogSettingTimeAutomatic", e);
//				}
//			}
//
//			setAlertDialogAutoTime(new AlertDialog.Builder(GlobalInfo.getInstance().getActivityContext()).create());
//			getAlertDialogAutoTime().setIcon(R.drawable.icon_warning);
//			getAlertDialogAutoTime().setCanceledOnTouchOutside(false);
//			getAlertDialogAutoTime().setCancelable(false);
//			getAlertDialogAutoTime().setTitle(StringUtil.getString(R.string.TEXT_WARNING));
//			getAlertDialogAutoTime().setMessage(strNotice.getSpan());
//			getAlertDialogAutoTime().setButton(AlertDialog.BUTTON_POSITIVE, StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//							// hien thi man hinh setting
//							GlobalUtil.startActivityOtherApp(GlobalInfo.getInstance().getActivityContext(),
//									new Intent(Settings.ACTION_DATE_SETTINGS));
//							return;
//
//						}
//					});
//			getAlertDialogAutoTime().show();
//		} catch (Exception ex) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//	}
//
//	/**
//	 * show dialog confirm setting gps
//	 *
//	 * @author banghn
//	 */
//	public static void showDialogSettingGPS() {
//		AlertDialog alertDialog = null;
//		try {
//			alertDialog = new AlertDialog.Builder(GlobalInfo.getInstance()
//					.getActivityContext()).create();
//			alertDialog.setMessage(StringUtil
//					.getString(R.string.NOTIFY_SETTING_GPS));
//			alertDialog.setButton(AlertDialog.BUTTON_POSITIVE,
//					StringUtil.getString(R.string.TEXT_BUTTON_CLOSE),
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//							GlobalUtil.startActivityOtherApp(GlobalInfo.getInstance().getActivityContext(),
//											new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//							return;
//
//						}
//					});
//			alertDialog.show();
//		} catch (Exception ex) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//	}
//
//	/**
//	 * show dialog confirm 2 button
//	 *
//	 * @author: HieuNH
//	 * @param view
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogConfirm(final GlobalBaseActivity view,
//			CharSequence notice, String ok, final int actionOk, String cancel,
//			final int actionCancel, final Object data) {
//		if (view != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(view).create();
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}

	/**
	 * Show dialog confrimed with action user define
	 *
	 * @param view
	 *            : activity
	 * @param title
	 *            : Title caption thong bao
	 * @param notice
	 *            : Noi dung thong bao
	 * @param ok
	 *            : Text dong y
	 * @param actionOk
	 *            : action dong y
	 * @param cancel
	 *            : Text cancel
	 * @param actionCancel
	 *            : Action cancel
	 * @param data
	 *            : user data define
	 */
//	public static void showDialogConfirm(final GlobalBaseActivity view,
//			String title, CharSequence notice, String ok, final int actionOk,
//			String cancel, final int actionCancel, final Object data) {
//		if (view != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(view).create();
//			alertDialog.setTitle(title);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	/**
//	 * Show dialog confrimed with action user define
//	 *
//	 * @param view
//	 *            : fragment dang dung
//	 * @param title
//	 *            : Title caption thong bao
//	 * @param notice
//	 *            : Noi dung thong bao
//	 * @param ok
//	 *            : Text dong y
//	 * @param actionOk
//	 *            : action dong y
//	 * @param cancel
//	 *            : Text cancel
//	 * @param actionCancel
//	 *            : Action cancel
//	 * @param data
//	 *            : user data define
//	 */
//	public static void showDialogConfirm(final BaseFragment view, String title,
//			CharSequence notice, String ok, final int actionOk, String cancel,
//			final int actionCancel, final Object data) {
//		if (view != null && view.getActivity() != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(
//					view.getActivity()).create();
//			alertDialog.setTitle(title);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	public static AlertDialog showDialogConfirmGetDialog(final BaseFragment view, String title,
//			CharSequence notice, String ok, final int actionOk, String cancel,
//			final int actionCancel, final Object data) {
//		AlertDialog alertDialog = null;
//		if (view != null && view.getActivity() != null) {
//			alertDialog = new AlertDialog.Builder(
//					view.getActivity()).create();
//			alertDialog.setTitle(title);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								view.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//
//		return alertDialog;
//	}
//
//	public static void showDialogConfirm(final BaseFragment fragment,
//			View view, String title, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel,
//			final Object data) {
//		if (view != null && fragment.getActivity() != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(
//					fragment.getActivity()).create();
//			alertDialog.setTitle(title);
//			alertDialog.setView(view);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	/**
//	 * Show dialog ho tro add view nguoi dung (checkbox, textbox,...)
//	 *
//	 * @author: BANGHN
//	 * @param fragment
//	 *            : fragment dang dung
//	 * @param view
//	 *            : view nguoi dung add them vao
//	 * @param title
//	 *            : tile dialog
//	 * @param notice
//	 *            : noi dung thong bao
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @param isCanBack
//	 * @param isTouchOutSide
//	 */
//	public static void showDialogConfirmCanBackAndTouchOutSide(
//			final BaseFragment fragment, View view, String title,
//			CharSequence notice, String ok, final int actionOk, String cancel,
//			final int actionCancel, final Object data, boolean isCanBack,
//			boolean isTouchOutSide) {
//		if (view != null && fragment.getActivity() != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(
//					fragment.getActivity()).create();
//			alertDialog.setTitle(title);
//			alertDialog.setView(view);
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setCanceledOnTouchOutside(isTouchOutSide);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	/**
//	 * Show dialog voi 1 action
//	 *
//	 * @author: TruongHN
//	 * @param view
//	 * @param message
//	 * @param ok
//	 * @param actionOk
//	 * @param data
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogConfirm(final BaseFragment fragment,
//			GlobalBaseActivity view, CharSequence message, String ok,
//			final int actionOk, final Object data, boolean isCanBack) {
//		if (view != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(view).create();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setMessage(message);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	/**
//	 * Show dialog voi 1 action cancel
//	 *
//	 * @author: Trungnt
//	 * @param view
//	 * @param message
//	 * @param ok
//	 * @param actionOk
//	 * @param data
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogConfirm(final GlobalBaseActivity view,
//			CharSequence message, String cancel,
//			final Object data, boolean isCanBack) {
//		if (view != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(view).create();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setMessage(message);
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								//view.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			alertDialog.show();
//		}
//	}
//
//	/**
//	 *
//	 * Show dialog cho phep back or touch outside
//	 *
//	 * @author:
//	 * @param fragment
//	 * @param view
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @return: void
//	 * @throws:
//	 */
//	public static AlertDialog showDialogConfirm(final BaseFragment fragment,
//			GlobalBaseActivity view, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel,
//			final Object data) {
//		return showDialogConfirmCanBack(fragment, view, notice, ok, actionOk, cancel,
//				actionCancel, data, true);
//	}
//
//	/**
//	 *
//	 * Show dialog confirm with allow can back or not
//	 *
//	 * @author: Nguyen Thanh Dung
//	 * @param fragment
//	 * @param view
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @param isCanBack
//	 * @return: void
//	 * @throws:
//	 */
//	public static AlertDialog showDialogConfirmCanBack(final BaseFragment fragment,
//			GlobalBaseActivity view, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel,
//			final Object data, boolean isCanBack) {
//		AlertDialog alertDialog = null;
//		if (view != null) {
//
//			alertDialog = GlobalInfo.getInstance().getAlertDialog();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//
//			if (!alertDialog.isShowing())
//				alertDialog.show();
//		}
//
//		return alertDialog;
//	}
//
//	public static AlertDialog showDialogConfirmWithBackButton(final BaseFragment fragment,
//			GlobalBaseActivity view, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel, String back, final int actionBack,
//			final Object data, boolean isCanBack) {
//		AlertDialog alertDialog = null;
//		if (view != null) {
//
//			alertDialog = GlobalInfo.getInstance().getAlertDialog();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setMessage(notice);
//
//
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, ok, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,
//							int which) {
//						fragment.onEvent(actionOk, null, data);
//						dialog.dismiss();
//					}
//				});
//			}
//
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, cancel, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int which) {
//						fragment.onEvent(actionCancel, null, data);
//						dialog.dismiss();
//					}
//				});
//			}
//
//			if(back != null && isCanBack) {
//				alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, back, new DialogInterface.OnClickListener() {
//					public void onClick(DialogInterface dialog,int which) {
//						fragment.onEvent(actionBack, null, data);
//						dialog.dismiss();
//					}
//				});
//			}
//
//			if (!alertDialog.isShowing())
//				alertDialog.show();
//		}
//
//		return alertDialog;
//	}
//
//	/**
//	 *
//	 * Show dialog confirm with allow can back or not
//	 *
//	 * @author: TamPQ
//	 * @param fragment
//	 * @param view
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @param isCanBack
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogConfirmCanBackAndTouchOutSide(
//			final BaseFragment fragment, GlobalBaseActivity view,
//			CharSequence notice, String ok, final int actionOk, String cancel,
//			final int actionCancel, final Object data, boolean isCanBack,
//			boolean isTouchOutSide) {
//		if (view != null) {
//			AlertDialog alertDialog = GlobalInfo.getInstance().getAlertDialog();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setCanceledOnTouchOutside(isTouchOutSide);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								fragment.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//
//			if (!alertDialog.isShowing())
//				alertDialog.show();
//		}
//	}
//
//	/**
//	 *
//	 * Show dialog confirm with allow can back or not
//	 *
//	 * @author: TamPQ
//	 * @param fragment
//	 * @param view
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @param isCanBack
//	 * @return: void
//	 * @throws:
//	 */
//	public static void showDialogConfirmCanBackAndTouchOutSide(
//			final GlobalBaseActivity activity, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel,
//			final Object data, boolean isCanBack, boolean isTouchOutSide) {
//		if (activity != null) {
//			AlertDialog alertDialog = GlobalInfo.getInstance().getAlertDialog();
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setCanceledOnTouchOutside(isTouchOutSide);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								activity.onEvent(actionOk, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								activity.onEvent(actionCancel, null, data);
//								dialog.dismiss();
//							}
//						});
//			}
//
//			if (!alertDialog.isShowing())
//				alertDialog.show();
//		}
//	}
//
//	/**
//	 * Dialog confirm co them icon title
//	 *
//	 * @param view
//	 * @param icon
//	 * @param title
//	 * @param notice
//	 * @param ok
//	 * @param actionOk
//	 * @param cancel
//	 * @param actionCancel
//	 * @param data
//	 * @param isCanBack
//	 * @param isTouchOutSide
//	 */
//	public static void showDialogConfirm(final GlobalBaseActivity view,
//			int icon, String title, CharSequence notice, String ok,
//			final int actionOk, String cancel, final int actionCancel,
//			final Object data, boolean isCanBack, boolean isTouchOutSide) {
//		if (view != null) {
//			AlertDialog alertDialog = new AlertDialog.Builder(view).create();
//			alertDialog.setIcon(icon);
//			alertDialog.setTitle(title);
//			alertDialog.setCancelable(isCanBack);
//			alertDialog.setCanceledOnTouchOutside(isTouchOutSide);
//			alertDialog.setMessage(notice);
//			if (ok != null && !Constants.STR_BLANK.equals(ok)) {
//				alertDialog.setButton(ok,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								try {
//									if (view != null && !view.isFinishing()) {
//										view.onEvent(actionOk, null, data);
//										dialog.dismiss();
//									}
//								} catch (IllegalArgumentException e) {
//									MyLog.e("showDialogConfirm", "cancel IllegalArgumentException", e);
//									ServerLogger.sendLog("showDialogConfirm cancel IllegalArgumentException" + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//								} catch (Exception e) {
//									MyLog.e("showDialogConfirm", "Exception", e);
//									ServerLogger.sendLog("showDialogConfirm cancel Exception" + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//								}
//							}
//						});
//			}
//			if (cancel != null && !Constants.STR_BLANK.equals(cancel)) {
//				alertDialog.setButton2(cancel,
//						new DialogInterface.OnClickListener() {
//							public void onClick(DialogInterface dialog,
//									int which) {
//								try {
//									if (view != null && !view.isFinishing()) {
//										view.onEvent(actionCancel, null, data);
//										dialog.dismiss();
//									}
//								} catch (IllegalArgumentException e) {
//									MyLog.e("showDialogConfirm", "cancel IllegalArgumentException", e);
//									ServerLogger.sendLog("showDialogConfirm cancel IllegalArgumentException" + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//								} catch (Exception e) {
//									MyLog.e("showDialogConfirm", "Exception", e);
//									ServerLogger.sendLog("showDialogConfirm cancel Exception" + MyLog.printStackTrace(e), TabletActionLogDTO.LOG_CLIENT);
//								}
//							}
//						});
//			}
//			view.showDialog(alertDialog);
//		}
//	}
//
//	/**
//	 * Removes the reference to the activity from every view in a view hierarchy
//	 * (listeners, images etc.). This method should be called in the onDestroy()
//	 * method of each activity
//	 *
//	 * @author: PhucNT
//	 * @param viewID
//	 *            normally the id of the root layout
//	 * @return
//	 * @throws:
//	 */
//
//	public static void nullViewDrawablesRecursive(View view) {
//		if (view != null) {
//			try {
//				ViewGroup viewGroup = (ViewGroup) view;
//
//				int childCount = viewGroup.getChildCount();
//				for (int index = 0; index < childCount; index++) {
//					View child = viewGroup.getChildAt(index);
//					nullViewDrawablesRecursive(child);
//				}
//			} catch (Exception e) {
//			}
//
//			unbindViewReferences(view);
//		}
//	}
//
//	/**
//	 * Remove view reference
//	 *
//	 * @author: TruongHN
//	 * @param view
//	 * @return: void
//	 * @throws:
//	 */
//	private static void unbindViewReferences(View view) {
//		// set all listeners to null (not every view and not every API level
//		// supports the methods)
//		try {
//			view.setOnClickListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//		try {
//			view.setOnCreateContextMenuListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//		try {
//			view.setOnFocusChangeListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//		try {
//			view.setOnKeyListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//		try {
//			view.setOnLongClickListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//		try {
//			view.setOnClickListener(null);
//		} catch (Exception mayHappen) {
//		}
//		;
//
//		// set background to null
//		Drawable d = view.getBackground();
//		if (d != null)
//			d.setCallback(null);
//		if (view instanceof ImageView) {
//			ImageView imageView = (ImageView) view;
//			d = imageView.getDrawable();
//			if (d != null)
//				d.setCallback(null);
//			imageView.setImageDrawable(null);
//			imageView.setBackgroundDrawable(null);
//			imageView = null;
//		}
//
//		// destroy webview
//		if (view instanceof WebView) {
//			((WebView) view).destroyDrawingCache();
//			((WebView) view).destroy();
//		}
//		view = null;
//	}
//
//	/**
//	 * Kiem tra request co can luu ko
//	 *
//	 * @author : DoanDM since : 11:10:57 AM
//	 */
//	public static boolean checkActionSave(int action) {
//		for (int i = 0, size = ActionEventConstant.getListActionNotSave().length; i < size; i++) {
//			if (action == ActionEventConstant.getListActionNotSave()[i]) {
//				return false;
//			}
//		}
//		return true;
//	}
//
//	/**
//	 * Tao log_id
//	 *
//	 * @author: TruongHN
//	 * @return: String
//	 * @throws:
//	 */
//	public static String generateLogId() {
//		StringBuffer logId = new StringBuffer();
//		String id = String.valueOf(System.currentTimeMillis());
//		logId.append(id);
//		if (id.length() < Constants.MAX_LENGTH_TRANSACTION_ID) {
//			logId.append("_");
//			logId.append(rand(0, 99));
//		}
//		logId.append("_");
//		logId.append(GlobalInfo.getInstance().getProfile().getUserData().getInheritId());
//		return logId.toString();
//	}
//
//	/**
//	 *
//	 * Generate id dua vao object id va timestamp
//	 *
//	 * @author: Nguyen Thanh Dung
//	 * @param tableName
//	 * @param objectId
//	 * @return
//	 * @return: String
//	 * @throws:
//	 */
//	public static String generateRandomId(String tableName, String objectId) {
//		StringBuffer logId = new StringBuffer();
//		String id = String.valueOf(System.currentTimeMillis());
//		if (LOG_TABLE.TABLE_NAME.equals(tableName)) {
//			logId.append(id);
//			if (id.length() < Constants.MAX_LENGTH_TRANSACTION_ID) {
//				logId.append("_");
//				logId.append(rand(0, 99));
//			}
//			logId.append("_");
//			logId.append(objectId);
//		} else if (DEBIT_TABLE.TABLE_NAME.equals(tableName)) {
//			int remainLength = Constants.MAX_LENGTH_RANDOM_ID
//					- objectId.length();
//			if (id.length() > remainLength) {
//				id = id.substring(id.length() - remainLength, id.length());
//			}
//			logId.append(objectId);
//			logId.append(id);
//		} else if (DEBIT_DETAIL_TABLE.TABLE_NAME.equals(tableName)) {
//			int remainLength = Constants.MAX_LENGTH_RANDOM_ID
//					- objectId.length();
//			if (id.length() > remainLength) {
//				id = id.substring(id.length() - remainLength, id.length());
//			}
//
//			logId.append(objectId);
//			logId.append(id);
//		}
//
//		return logId.toString();
//	}
//
//	/**
//	 * tao so ngau nhien trong pham vi
//	 *
//	 * @param lo
//	 *            : so ngau nhien thap nhat
//	 * @param hi
//	 *            : so ngau nhien cao nhat
//	 * @return
//	 */
//	public static int rand(int lo, int hi) {
//		SecureRandom  rand = new SecureRandom();
//
//		int n = hi - lo + 1;
//		int i = rand.nextInt() % n;
//		if (i < 0)
//			i = -i;
//		return lo + i;
//	}
//
//	/**
//	 * Tao json name, value
//	 *
//	 * @author: TruongHN
//	 * @param name
//	 * @param value
//	 * @return: JSONObject
//	 * @throws:
//	 */
//	public static JSONObject getJsonColumn(String name, Object value,
//			String type) {
//		JSONObject json = new JSONObject();
//		try {
//			json.put(IntentConstants.INTENT_COLUMN, name);
//			json.put(IntentConstants.INTENT_VALUE, value);
//			if (!StringUtil.isNullOrEmpty(type)) {
//				json.put(IntentConstants.INTENT_TYPE, type);
//			}
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//		return json;
//	}
//
//	/**
//	 * Tao json name, value, key
//	 *
//	 * @author: TruongHN
//	 * @param name
//	 * @param value
//	 * @return: JSONObject
//	 * @throws:
//	 */
//	public static JSONObject getJsonColumnWithKey(String name, Object value,
//			String type) {
//		JSONObject json = new JSONObject();
//		try {
//			json.put(IntentConstants.INTENT_KEY, "ISPK");
//			json.put(IntentConstants.INTENT_COLUMN, name);
//			json.put(IntentConstants.INTENT_VALUE, value);
//			if (!StringUtil.isNullOrEmpty(type)) {
//				json.put(IntentConstants.INTENT_TYPE, type);
//			}
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//		return json;
//	}
//
//	/**
//	 * Tao json cho menh de where
//	 *
//	 * @author: TruongHN
//	 * @param name
//	 * @param value
//	 * @param operator
//	 * @return
//	 * @return: JSONObject
//	 * @throws:
//	 */
//	public static JSONObject getJsonColumnWhere(String name, Object value,
//			String operator) {
//		JSONObject json = new JSONObject();
//		try {
//			json.put(IntentConstants.INTENT_COLUMN, name);
//			json.put(IntentConstants.INTENT_VALUE, value);
//			if (!StringUtil.isNullOrEmpty(operator)) {
//				json.put(IntentConstants.INTENT_OPERATOR, operator);
//			}
//		} catch (JSONException e) {
//			// TODO Auto-generated catch block
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//		return json;
//	}
//
//	/**
//	 * khoang cach giua 2 diem
//	 *
//	 * @author: BangHN
//	 * @param sourcePoint
//	 *            : dau
//	 * @param destinationPointLatLng
//	 *            : cuoi
//	 * @return khoang cach (m)
//	 * @return: double
//	 * @throws:
//	 */
//	public static double getDistanceBetween(LatLng sourcePoint,
//			LatLng destinationPoint) {
//		int EARTH_RADIUS = 6378137; // ban kinh trai dat theo don vi m
//		if (sourcePoint == null || destinationPoint == null) {
//			return 0;
//		}
//		double R = EARTH_RADIUS / 1000; // Radius of the earth in km
//		double dLat = Math.toRadians(destinationPoint.lat - sourcePoint.lat);
//		double dLon = Math.toRadians(destinationPoint.lng - sourcePoint.lng);
//
//		double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
//				+ Math.cos(Math.toRadians(sourcePoint.lat))
//				* Math.cos(Math.toRadians(destinationPoint.lat))
//				* Math.sin(dLon / 2) * Math.sin(dLon / 2);
//		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
//		double distance = (R * c) * 1000; // Distance in m
//		return Math.round(distance);
//	}
//
////	/**
////	 * Tinh goc giua 2 diem tren ban do tao nen
////	 *
////	 * @author banghn
////	 * @param sourcePoint
////	 *            : Diem dau
////	 * @param destinationPoint
////	 *            : Diem cuoi
////	 * @since 1.0
////	 * @return
////	 */
////	public static double getAngle(GEPoint2D sourcePoint,
////			GEPoint2D destinationPoint) {
////		double dx = destinationPoint.x - sourcePoint.x;
////		double dy = -(destinationPoint.y - sourcePoint.y);
////		double inRads = Math.atan2(dy, dx);
////		if (inRads < 0) {
////			inRads = Math.abs(inRads);
////		} else {
////			inRads = 2 * Math.PI - inRads;
////		}
////		return Math.toDegrees(inRads);
////	}
//
//	/**
//	 * Set chieu dai toi da cua edit otp bang voi chuoi otp
//	 *
//	 * @author: PhucNT
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setFilterInputConvfact(EditText edEditText, int length) {
//		// TODO Auto-generated method stub
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				// TODO Auto-generated method stub
//				String result = "";
//				for (int i = start; i < end; i++) {
//					char insertChar = source.charAt(i);
//					if (Character.isDigit(insertChar)) {
//						result += insertChar;
//					} else if(insertChar == '/'){
//						boolean isExists = false;
//						for (int j = 0, sizeDest = dest.length(); j < sizeDest; j++) {
//							char charDest = dest.charAt(j);
//							if (insertChar == charDest) {
//								isExists = true;
//								break;
//							}
//						}
//
//						//chi them khi chua ton tai trong chuoi
//						if (!isExists) {
//							result += insertChar;
//						}
//					}
//				}
//				return result;
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL
//					| InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
//					| InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
//			edEditText.setFilters(filterArray);
//		}
//
//	}
//
//	/**
//	 * filter price
//	 * @author: duongdt3
//	 * @since: 10:39:16 11 Apr 2015
//	 * @return: void
//	 * @throws:
//	 * @param edEditText
//	 * @param length
//	 */
//	public static void setFilterInputPrice(EditText edEditText, int length) {
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				String result = "";
//				for (int i = start; i < end; i++) {
//					char insertChar = source.charAt(i);
//					if (Character.isDigit(insertChar)) {
//						result += insertChar;
//					} else if(insertChar == '.' || insertChar == ','){
//						//go dau , .
//						if (source.length() == 1) {
//							boolean isExists = false;
//							for (int j = 0, sizeDest = dest.length(); j < sizeDest; j++) {
//								char charDest = dest.charAt(j);
//								if (insertChar == charDest) {
//									isExists = true;
//									break;
//								}
//							}
//
//							//chi them khi chua ton tai trong chuoi
//							if (!isExists) {
//								result += insertChar;
//							}
//						} else{
//							result += insertChar;
//						}
//					}
//				}
//				return result;
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//			edEditText.setFilters(filterArray);
//		}
//	}
//
//	/**
//	 * filter amount with ,
//	 * @author: duongdt3
//	 * @time: 10:15:28 AM Dec 12, 2015
//	 * @param edEditText
//	 * @param length
//	 */
//	public static void setFilterInputAmount(EditText edEditText, int length) {
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				String result = "";
//				for (int i = start; i < end; i++) {
//					char insertChar = source.charAt(i);
//					if (Character.isDigit(insertChar) || insertChar == ',') {
//						result += insertChar;
//					}
//				}
//				return result;
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//			edEditText.setFilters(filterArray);
//		}
//	}
//
//	/**
//	 *
//	*  thiet lap validate du lieu nhap vao cho textbox dang nhap tien
//	*  @author: HaiTC3
//	*  @param edEditText
//	*  @param length
//	*  @return: void
//	*  @throws:
//	*  @since: May 9, 2013
//	 */
//	public static void setFilterInputMoney(EditText edEditText, int length) {
//		// TODO Auto-generated method stub
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				// TODO Auto-generated method stub
//				String result = "";
//				for (int i = start; i < end; i++) {
//					if (Character.isDigit(source.charAt(i))
//							|| source.charAt(i) == ',') {
//						result = result + source.charAt(i);
//					}
//				}
//				return result;
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//			edEditText.setFilters(filterArray);
//		}
//
//	}
//
//	/**
//	 *
//	 * Filter de remove cac ky tu dac biet cua sql
//	 *
//	 * @author: Nguyen Thanh Dung
//	 * @param edEditText
//	 * @param length
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setFilterInputForSearchLike(EditText edEditText,
//			int length) {
//		// TODO Auto-generated method stub
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				// TODO Auto-generated method stub
//				StringBuilder text = new StringBuilder();
//				String OpPat = ",;&"; // search operator pattern
//				String SpPat = "<>./!@#$%^*'\"-_():|[]~+{}?\\\n"; // special
//																	// char
//				boolean preCheck = true;
//				for (int i = start; i < end; i++) {
//					boolean canAdd = true;
//
//					if (SpPat.indexOf(source.charAt(i)) >= 0) {
//						canAdd = false;
//					} else if (OpPat.indexOf(source.charAt(i)) >= 0) {
//						if (preCheck) {
//							canAdd = false;
//						}
//						preCheck = true;
//					} else
//						preCheck = false;
//
//					if (canAdd) {
//						text.append(source.charAt(i));
//					}
//				}
//				return text.toString();
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			// edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//			edEditText.setFilters(filterArray);
//		}
//
//	}
//
//
//	public static class QuantityInfo implements Serializable{
//		private static final long serialVersionUID = 1L;
//		public long quantitySingle;
//		public long quantityPackage;
//		public long quantityTotal;
//	}
//
//
//	public static String getTextQuantity(String value) {
//		String str = value;
//		if (!StringUtil.isNullOrEmpty(str)) {
//			if (isValidQuantity(str)) {
//				String[] arr = str.split("/");
//				//theo cau hinh
//				int config = GlobalInfo.getInstance().getConfigConvertQuantity();
//				if (arr.length == 0) {
//					str = "0/0";
//				} else if (arr.length == 1) {
//					if (str.startsWith("/")) {
//						str = "0" + str;
//					} else if (str.endsWith("/")) {
//						str += "0";
//					} else {
//						//theo cau hinh dang nhap 9 hoặc /9 hoặc 9/
//						switch (config) {
//							case Constants.CHANGE_QUANTITY_2_RETAIL:{
//								// vd 9 -> 0/9
//								str = "0/" + str;
//								break;
//							}
//							case Constants.CHANGE_QUANTITY_2_PACKAGE:{
//								// vd 9 -> 9/0
//								str += "/0";
//								break;
//							}
//							case Constants.CHANGE_QUANTITY_NO_CHANGE:{
//								str = value;
//								break;
//							}
//							default:{
//								// vd 9 -> 0/9
//								str = "0/" + str;
//								break;
//							}
//						}
//					}
//				} else if (arr.length == 2) {
//					if (str.startsWith("/")) {
//						str = "0" + str;
//					} else if (str.endsWith("/")) {
//						str += "0";
//					}
//				}
//			}
//		}
//		return str;
//	}
//	/**
//	 * get quantity info from string order
//	 * @author: duongdt3
//	 * @since: 14:31:58 9 Apr 2015
//	 * @return: QuantityInfo
//	 * @throws:
//	 * @param value
//	 * @param convfact
//	 * @return
//	 */
//	public static QuantityInfo calQuantityFromOrderStr(String value, int convfact) {
//		QuantityInfo info = new QuantityInfo();
//		String numValue = value.replaceAll(",", "");
//		if (isValidQuantity(numValue)) {
//			// chi duoc phep 1 ky tu '/'
//			String[] arr = numValue.split("/");
//			if (arr.length <= 2 && arr.length > 0) {
//				if (arr.length == 1) {
//					if (numValue.contains("/")) {
//						// thung: vd 9/
//						info.quantityPackage = Long.valueOf(arr[0]);
//						info.quantitySingle = 0;
//					} else {
//						info.quantitySingle = Long.valueOf(arr[0]);
//						info.quantityPackage = 0;
//					}
//
//				} else {
//					if (!StringUtil.isNullOrEmpty(arr[0])) {
//						info.quantityPackage = Long.valueOf(arr[0]);
//					}
//					if (!StringUtil.isNullOrEmpty(arr[1])) {
//						info.quantitySingle = Long.valueOf(arr[1]);
//					}
//				}
//			}
//		}
//		info.quantityTotal = (info.quantityPackage * convfact) + info.quantitySingle;
//		return info;
//	}
//
//	/**
//	 * Tinh gia tri thuc dat de luu DB
//	 *
//	 * @author: TruongHN
//	 * @param numValue
//	 * @param convfact
//	 * @return: int
//	 * @throws:
//	 */
//	public static int calRealOrder(String value, int convfact) {
//		String numValue = value.replaceAll(",", "");
//		int realOrder = 0;
//		if (isValidQuantity(numValue)) {
//			// chi duoc phep 1 ky tu '/'
//			String[] arr = numValue.split("/");
//			if (arr.length <= 2 && arr.length > 0) {
//				if (arr.length == 1) {
//					if (numValue.contains("/")) {
//						// thung: vd 9/
//						realOrder = Integer.valueOf(arr[0]) * convfact;
//					} else {
//						// hop
//						realOrder = Integer.valueOf(arr[0]);
//					}
//
//				} else {
//					int n1 = 0;
//					int n2 = 0;
//					if (!StringUtil.isNullOrEmpty(arr[0])) {
//						n1 = Integer.valueOf(arr[0]);
//					}
//					if (!StringUtil.isNullOrEmpty(arr[1])) {
//						n2 = Integer.valueOf(arr[1]);
//					}
//					realOrder = n1 * convfact + n2;
//				}
//			}
//		}
//		return realOrder;
//	}
//
//	/**
//	 *
//	 * dinh dang so luong san pham theo dang thung / hop
//	 *
//	 * @author: HaiTC3
//	 * @param numberProduct
//	 * @param convfact
//	 * @return
//	 * @return: String
//	 * @throws:
//	 * @since: May 6, 2013
//	 */
//	public static String formatNumberProductFlowConvfact(long numberProduct,
//			int convfact) {
//		StringBuilder availableProductFormat = new StringBuilder();
//		availableProductFormat.append("0/0");
//
//		if (numberProduct != 0 && convfact != 0) {
//			availableProductFormat.setLength(0);
//			if (numberProduct < 0) {
//				availableProductFormat.append("-");
//			}
//			availableProductFormat
//					.append(StringUtil.parseAmountMoney(Math.abs(numberProduct
//							/ convfact)))
//					.append("/")
//					.append(StringUtil.parseAmountMoney(Math.abs(numberProduct
//							% convfact)));
//			//		} else {
////			availableProductFormat = "0/0";
//		}
//		return availableProductFormat .toString();
//	}
//
//	/**
//	 * Kiem tra so luong nhap co hop le hay khong
//	 *
//	 * @author: TruongHN
//	 * @param name
//	 * @return: boolean
//	 * @throws:
//	 */
//	public static boolean isValidQuantity(String name) {
//		// Thuc dat chi chua cac 0-9 va ky tu /
//		Boolean isValid = false;
//		int length = name.length();
//		for (int i = 0; i < length; i++) {
//			char ch = name.charAt(i);
//			if (((ch >= '0' && ch <= '9') || (ch == '/'))) {
//				isValid = true;
//			} else {
//				isValid = false;
//				break;
//			}
//		}
//		return isValid;
//	}
//
//	/**
//	 * set enable button send in ugc
//	 *
//	 * @author: BangHN
//	 * @param enable
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setEnableButton(Button button, boolean enable) {
//		if (button == null) {
//			return;
//		}
//		if (enable) {
//			button.setEnabled(true);
//			button.setBackgroundResource(R.drawable.vnm_button_selector);
//		} else {
//			button.setEnabled(false);
//			button.setBackgroundResource(R.color.COLOR_BG_DISABLE);
//		}
//
//	}
//
//	/**
//	 * set enable button send in ugc
//	 *
//	 * @author: BangHN
//	 * @param enable
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setEnableEditText(EditText ed,
//			boolean enable) {
//		if (ed == null) {
//			return;
//		}
//		if (enable) {
//			ed.setEnabled(true);
//			ed.setBackgroundResource(R.drawable.bg_white_rounded);
//		} else {
//			ed.setEnabled(false);
//			ed.setBackgroundResource(R.drawable.bg_white_rounded_disable);
//			ed.setPadding(GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5), GlobalUtil.dip2Pixel(5),
//					GlobalUtil.dip2Pixel(5));
//		}
//
//	}
//
//	/**
//	 * set enable button send in ugc
//	 *
//	 * @author: BangHN
//	 * @param enable
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setEnableEditTextClear(VNMEditTextClearable button,
//			boolean enable) {
//		if (button == null) {
//			return;
//		}
//		if (enable) {
//			button.setEnabled(true);
//			button.setBackgroundResource(R.drawable.bg_white_rounded);
//		} else {
//			button.setEnabled(false);
//			button.setBackgroundResource(R.drawable.bg_white_rounded_disable);
//		}
//
//	}
//
//	/**
//	 * set max lenght cho edit text
//	 *
//	 * @author: AnhND
//	 * @param edText
//	 * @param maxLength
//	 * @return: void
//	 * @throws:
//	 */
//	public static void setEditTextMaxLength(EditText edText, int maxLength) {
//		InputFilter[] inputFilters = new InputFilter[1];
//		inputFilters[0] = new InputFilter.LengthFilter(maxLength);
//		edText.setFilters(inputFilters);
//	}
//
	/**
	 * Kiem tra mang
	 *
	 * @author: TruongHN
	 * @return: boolean
	 * @throws:
	 */
	public static boolean checkNetworkAccess() {
		boolean res = false;
//		if (GlobalInfo.isIS_VERSION_FOR_EMULATOR()) {
//			res = true;
//		} else{
			ConnectivityManager cm = (ConnectivityManager) GlobalInfo.getInstance()
					.getAppContext().getSystemService(Context.CONNECTIVITY_SERVICE);

			if (cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
					.isConnectedOrConnecting()
					|| cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
					.isConnectedOrConnecting()) {
				MyLog.i("Test Network Access", "Network available!");
				res = true;
			} else {
				res = false;
				MyLog.i("Test Network Access", "No network available!");
			}
//		}
		return res;
	}
//
//	/**
//	 * Set mobile data enable || disable
//	 *
//	 * @author banghn
//	 * @param context
//	 * @param enabled
//	 */
//	@SuppressWarnings({ "rawtypes", "unchecked" })
//	public static void setMobileDataEnabled(Context context, boolean enabled) {
//		try {
//			final ConnectivityManager conman = (ConnectivityManager) context
//					.getSystemService(Context.CONNECTIVITY_SERVICE);
//			final Class conmanClass = Class
//					.forName(conman.getClass().getName());
//			final Field iConnectivityManagerField = conmanClass
//					.getDeclaredField("mService");
//			iConnectivityManagerField.setAccessible(true);
//			final Object iConnectivityManager = iConnectivityManagerField
//					.get(conman);
//			final Class iConnectivityManagerClass = Class
//					.forName(iConnectivityManager.getClass().getName());
//			final Method setMobileDataEnabledMethod = iConnectivityManagerClass
//					.getDeclaredMethod("setMobileDataEnabled", Boolean.TYPE);
//			setMobileDataEnabledMethod.setAccessible(true);
//
//			setMobileDataEnabledMethod.invoke(iConnectivityManager, enabled);
//		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//	}
//
//	/**
//	 * Back lai man hinh truoc khi khong nhan phim back mac dinh
//	 *
//	 * @author: TruongHN
//	 * @param activity
//	 * @return: void
//	 * @throws:
//	 */
//	public static void popBackStack(Activity activity) {
//		if (activity != null) {
//			try {
//				activity.getFragmentManager().popBackStack();
//				GlobalInfo.getInstance().popCurrentTag();
//			} catch (Exception e) {
//				MyLog.e("popBackStack", "fail", e);
//			}
//		}
//	}
//
//	/**
//	 * FOrce hide Keyboard
//	 *
//	 * @author: VietHQ
//	 * @param activity
//	 * @return: void
//	 * @throws:
//	 */
//	public static void forceHideKeyboard(Activity activity) {
//		forceHideKeyboard(activity, true);
//	}
//
//	public static void forceHideKeyboard(Activity activity, boolean isHideCustomKeyboard) {
//		if (activity != null && activity.getCurrentFocus() != null) {
//			InputMethodManager inputManager = (InputMethodManager) activity
//					.getSystemService(Context.INPUT_METHOD_SERVICE);
//			inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
//					.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//		}
//
//		if (isHideCustomKeyboard && activity != null && activity instanceof GlobalBaseActivity) {
//			GlobalBaseActivity ac = (GlobalBaseActivity) activity;
//			ac.hideKeyboardCustom();
//		}
//	}
//
//	/**
//	 * Hide keyboard with input, use in Popup
//	 * * @author: DuongDT
//	 * @param parent
//	 * @param input
//	 */
//	public static void forceHideKeyboardInput(Context context, View input) {
//			InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
//			imm.hideSoftInputFromWindow(input.getWindowToken(), 0);
//
//			if (context != null && context instanceof GlobalBaseActivity) {
//				GlobalBaseActivity ac = (GlobalBaseActivity) context;
//				ac.hideKeyboardCustom();
//			}
//	}
//
//	/**
//	 *
//	 * hide keyboard when before user show keyboard use Toggle.
//	 *
//	 * @param activity
//	 * @return: void
//	 * @throws:
//	 * @author: HaiTC3
//	 * @date: Dec 25, 2012
//	 */
//	public void forceHideKeyboardUseToggle(Activity activity) {
//		if (activity != null && activity.getCurrentFocus() != null) {
//			if (keyboardVisible) {
//				InputMethodManager imm = (InputMethodManager) activity
//						.getSystemService(Context.INPUT_METHOD_SERVICE);
//				if (imm != null) {
//					imm.toggleSoftInput(0, 0);
//				}
//				keyboardVisible = false;
//			} else {
//				InputMethodManager inputManager = (InputMethodManager) activity
//						.getSystemService(Context.INPUT_METHOD_SERVICE);
//				inputManager.hideSoftInputFromWindow(activity.getCurrentFocus()
//						.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
//			}
//		}
//
//		if (activity != null && activity instanceof GlobalBaseActivity) {
//			GlobalBaseActivity ac = (GlobalBaseActivity) activity;
//			ac.hideKeyboardCustom();
//		}
//	}
//
//	/**
//	 *
//	 * show keyboard use ToogleSoftInput
//	 *
//	 * @param activity
//	 * @return: void
//	 * @throws:
//	 * @author: HaiTC3
//	 * @date: Dec 25, 2012
//	 */
//	public void showKeyboardUseToggle(Activity activity) {
//		if (activity != null && activity.getCurrentFocus() != null) {
//			InputMethodManager imm = (InputMethodManager) activity
//					.getSystemService(Context.INPUT_METHOD_SERVICE);
//			imm.toggleSoftInput(InputMethodManager.SHOW_FORCED,
//					InputMethodManager.HIDE_IMPLICIT_ONLY);
//			keyboardVisible = true;
//		}
//	}
//
//	/**
//	 * Lay method hien tai dang thuc thi
//	 *
//	 * @author: TruongHN
//	 * @return: String
//	 * @throws:
//	 */
//	public static String getCurrentMethod() {
//		String method = "";
//		try {
//			StackTraceElement[] stacktrace = Thread.currentThread()
//					.getStackTrace();
//			if (stacktrace.length > 1) {
//				StackTraceElement e = stacktrace[1];
//				method = e.getMethodName();
//			}
//
//		} catch (SecurityException e) {
//			// TODO: handle exception
//		}
//		return method;
//	}
//
//	/**
//	 * chup hinh
//	 *
//	 * @author: AnhND
//	 * @param parent
//	 * @param requestCode
//	 * @return
//	 * @return: File
//	 * @throws:
//	 */
//	public static File takePhoto(GlobalBaseActivity sender, int requestCode) {
//		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//
//		// String fileName = "img_" +
//		// Long.valueOf(Calendar.getInstance().getTimeInMillis()) + ".tmp";
//		String fileName = Constants.TEMP_IMG + "_"
//				+ DateUtils.getCurrentDateTimeWithFormat(null) + ".jpg";
//		File retFile = new File(ExternalStorage.getTakenPhotoPath(sender),
//				fileName);
//		if (!retFile.exists())
//			try {
//				retFile.createNewFile();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//			}
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(retFile));
//		sender.startActivityForResult(intent, requestCode, StringUtil.getString(R.string.TEXT_TAKEN_PHOTO));
//		return retFile;
//	}
//
//	public static File takePhoto(BaseFragment sender, int requestCode) {
//			String fileName = Constants.TEMP_IMG + "_"
//					+ DateUtils.getCurrentDateTimeWithFormat(null) + ".jpg";
//			return takePhoto(sender, requestCode, fileName);
//	}
//
//	public static File takePhotoAttach(BaseFragment sender, int requestCode) {
//		String fileName = Constants.TEMP_IMG_ATTACH + "_"
//				+ DateUtils.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_FILE_EXPORT) + ".jpg";
//		return takePhoto(sender, requestCode, fileName);
//	}
//
//	public static File takePhoto(BaseFragment sender, int requestCode, String fileName) {
//		System.gc();
//		final Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
//		File retFile = new File(ExternalStorage.getTakenPhotoPath(sender
//				.getActivity()), fileName);
//		if (!retFile.exists())
//			try {
//				retFile.createNewFile();
//			} catch (IOException e) {
//				// TODO Auto-generated catch block
//				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//			}
//		intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(retFile));
//		sender.startActivityForResult(intent, requestCode, StringUtil.getString(R.string.TEXT_TAKEN_PHOTO));
//		return retFile;
//	}
//
//	/**
//	 * Chuyen doi so luong sang quy cach
//	 *
//	 * @author: TruongHN
//	 * @return: String
//	 * @throws:
//	 */
//	public static String convertQuantityToRealOrderOnView(long quantity,
//			int convfact) {
//		String res = "";
//		if (quantity > 0 && convfact > 0) {
//			res = (quantity / convfact) + "/" + (quantity % convfact);
//			if (res.length() > Constants.MAX_LENGHT_QUANTITY) {
//				int index = res.indexOf("/");
//				String hop = res.substring(index + 1);
//				if(Integer.parseInt(hop) == 0) {
//					res = res.substring(0, index + 1);
//				} else {
//					res = String.valueOf(quantity);
//				}
//			}
//		}
//		return res;
//	}
//
//	/**
//	 * Doc file sang mang byte
//	 *
//	 * @author TamPQ
//	 * @param filePath
//	 * @return
//	 */
//	public static byte[] readFileInByteArray(String filePath) {
//		// create file object
//		File file = new File(filePath);
//		byte fileContent[] = null;
//		FileInputStream fin = null;
//		try {
//			// create FileInputStream object
//			fin = new FileInputStream(file);
//
//			/*
//			 * Create byte array large enough to hold the content of the file.
//			 * Use File.length to determine size of the file in bytes.
//			 */
//
//			fileContent = new byte[(int) file.length()];
//
//			/*
//			 * To read content of the file in byte array, use int read(byte[]
//			 * byteArray) method of java FileInputStream class.
//			 */
//			fin.read(fileContent);
//
//			// create string from byte array
//			String strFileContent = new String(fileContent);
//
//			MyLog.w("",	 "File content : ");
//			MyLog.w("",	 strFileContent);
//		} catch (FileNotFoundException e) {
//			MyLog.w("",	 "File not found " + e.toString());
//		} catch (IOException ioe) {
//			MyLog.w("",	 "Exception while reading the file " + ioe.toString());
//		} finally {
//			if (fin != null) {
//				try {
//					fin.close();
//				} catch (IOException e) {
//					MyLog.e("readFileInByteArray", e);
//				}
//			}
//		}
//		return fileContent;
//	}
//
//	/**
//	 * clear cache (xoa) hinh
//	 *
//	 * @author phucnt
//	 */
//	public class CacheCleanTask extends AsyncTask<File, Void, Void> {
//		@Override
//		protected Void doInBackground(File... files) {
//			try {
//				walkDir(files[0]);
//			} catch (Exception t) {
//				MyLog.e("PhucNT4", "Exception cleaning cache", t);
//			}
//			return null;
//		}
//
//		void walkDir(File dir) {
//			if (dir.isDirectory()) {
//				String[] children = dir.list();
//
//				for (int i = 0; i < children.length; i++) {
//					walkDir(new File(dir, children[i]));
//				}
//			} else {
//				dir.delete();
//			}
//		}
//	}
//
//	/**
//	 * Clear data khi thoat chuong trinh
//	 *
//	 * @author: TruongHN
//	 * @return: void
//	 * @throws:
//	 */
//	public void clearAllData() {
//		// cancel position
//		PositionManager.getInstance().stop();
//		// cancel thong bao ngoai chuong trinh
//		GlobalInfo.getInstance().getStatusNotifier().cancelNotifications();
//		GlobalInfo.getInstance().setTimeSyncToServer(0, 1);
//		GlobalInfo.getInstance().setAllowEditPromotion(-1, 1);
//		// cancel luong dong bo
//		TransactionProcessManager.getInstance().cancelTimer();
//		// xoa thong tin ca nhan
//		GlobalInfo.getInstance().getProfile().clearProfile();
//		GlobalInfo.getInstance().setNotifyOrderReturnInfo(new NotifyInfoOrder());
//		// cap nhat thoat ung dung
//		GlobalInfo.getInstance().setExitApp(true);
//		// kiem tra dong database
//		if (!SQLUtils.getInstance().isProcessingTrans) {
//			// cancel sqlLite
//			SQLUtils.getInstance().closeDB();
//		}
//	}
//
//	/**
//	 *
//	 * Noi dung can mo ta : kiem tra thiet bi co cho phep xu dung vi tri ao
//	 * khong (root location)
//	 *
//	 * @author: DucHHA
//	 * @return: boolean: true neu root, nguoc lai false
//	 * @throws:
//	 */
//	public static boolean isMockLocation() {
//		if (Settings.Secure.getString(
//				GlobalInfo.getInstance().getAppContext()
//						.getContentResolver(),
//				Settings.Secure.ALLOW_MOCK_LOCATION).equals("0")) {
//			return false;
//		} else {
//			return true;
//
//		}
//	}
//
//	/**
//	 *
//	 * Noi dung can mo ta: Kiem tra va xu ly thiet bi xem gia lap vi tri ao
//	 * (root location) hay khong ,neu co hien thi thong bao cho nguoi dung biet
//	 * de tat tool cho phep su dung vi tri ao hoac thoat khoi chuong trinh neu
//	 * khong thi thoat khoi chuong trinh. nguoc lai chuong trinh chay binh
//	 * thuong
//	 *
//	 * @author: DucHHA
//	 * @param: language: ngon ngu hien thi hien tai cua thiet bi
//	 * @return: void
//	 * @throws:
//	 */
//	public void checkAllowMockLocation() {
//		String language = Locale.getDefault().getDisplayLanguage();
//		// code here
//		if (language.equals("Tieng Viet")) {
//			GlobalUtil.showDialogConfirm((GlobalBaseActivity) GlobalInfo
//					.getInstance().getActivityContext(),
//					R.drawable.icon_warning, StringUtil
//							.getString(R.string.TEXT_WARNING),
//					"Vui long bo tuy chon [Cho Phep Vi Tri Gia] ", StringUtil
//							.getString(R.string.TEXT_BUTTON_AGREE),
//					GlobalBaseActivity.ACTION_ALLOW_MOCK_LOCATION_OK,
//					StringUtil.getString(R.string.TEXT_BUTTON_DENY),
//					GlobalBaseActivity.ACTION_ALLOW_MOCK_LOCATION_CANCEL, null,
//					false, false);
//		} else {
//			GlobalUtil.showDialogConfirm((GlobalBaseActivity) GlobalInfo
//					.getInstance().getActivityContext(),
//					R.drawable.icon_warning, StringUtil
//							.getString(R.string.TEXT_WARNING),
//					"Vui long bo tuy chon [ALLOW MOCK LOCATION] ", StringUtil
//							.getString(R.string.TEXT_BUTTON_AGREE),
//					GlobalBaseActivity.ACTION_ALLOW_MOCK_LOCATION_OK,
//					StringUtil.getString(R.string.TEXT_BUTTON_DENY),
//					GlobalBaseActivity.ACTION_ALLOW_MOCK_LOCATION_CANCEL, null,
//					false, false);
//		}
//	}
//
//	/**
//	 * Get mang byte de tao chuoi MD5 checksum
//	 *
//	 * @author banghn
//	 * @param filename
//	 * @return
//	 * @throws
//	 */
//	public static byte[] createChecksum(String filename) {
//		InputStream fis = null;
//		byte[] result = null;
//		try {
//			fis = new FileInputStream(filename);
//			byte[] buffer = new byte[1024];
//			MessageDigest complete = MessageDigest.getInstance("SHA-256");
//			int numRead;
//			do {
//				numRead = fis.read(buffer);
//				if (numRead > 0) {
//					complete.update(buffer, 0, numRead);
//				}
//			} while (numRead != -1);
//			result = complete.digest();
//		} catch (Exception ex) {
//			MyLog.e("createChecksum", "fail", ex);
//		} finally {
//			if (fis != null) {
//				try {
//					fis.close();
//				} catch (Exception e) {
//					MyLog.e("createChecksum", "fail", e);
//				}
//			}
//		}
//		return result;
//	}
//
//	/**
//	 * Tao chuoi check sum MD5
//	 *
//	 * @author banghn
//	 * @param filename
//	 * @return
//	 * @throws Exception
//	 */
//	public static String getMD5Checksum(String filename) {
//		byte[] b = createChecksum(filename);
//		String result = "";
//		if (b != null) {
//			try {
//				result = StringUtil.getHexStringFaster(b);
//			} catch (UnsupportedEncodingException u) {
//				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(u));
//			} catch (Exception e) {
//				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//			}
//		}
//		return result;
//	}
//
//	/**
//	 * Tao chuoi check sum MD5 cho file DB VINMAILK
//	 *
//	 * @author banghn
//	 * @param filename
//	 * @return
//	 * @throws Exception
//	 */
//	public static String getMD5DBChecksum() {
//		String md5Checksum = "";
//		try {
//			String dbFilePath = ExternalStorage.getFileDBPath(
//					GlobalInfo.getInstance().getAppContext()).getAbsolutePath()
//					+ "/VinamilkDatabase";
//			md5Checksum = getMD5Checksum(dbFilePath);
//		} catch (Exception ex) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//		MyLog.i("MD5Checksum", ".......chuoi MD5 checksum: " + md5Checksum);
//		return md5Checksum;
//	}
//
//	/**
//	 * Checksum md5 file database truoc khi thoat app
//	 *
//	 * @author: BangHN
//	 * @return: void
//	 * @throws:
//	 */
//	public static void checksumMD5Database() {
//		try {
//			String md5Checksum = GlobalUtil.getMD5DBChecksum();
////			SharedPreferences sharedPreferences = GlobalInfo
////					.getInstance()
////					.getActivityContext()
////					.getSharedPreferences(
////							GlobalInfo.getInstance().getActivityContext()
////									.getPackageName(), Context.MODE_PRIVATE);
//			SharedPreferences sharedPreferences = GlobalInfo.getInstance().getDmsPrivateSharePreference();
//			Editor prefsPrivateEditor = sharedPreferences.edit();
//			prefsPrivateEditor.putString(LoginView.VNM_MD5_CHECKSUM_DB,
//					md5Checksum);
//			prefsPrivateEditor.commit();
//		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//	}
//
//	/**
//	 * Kiem tra co dang bat co don't keep activities hay khong
//	 *
//	 * @author banghn
//	 * @return
//	 */
//	public void checkStatusAlwaysFinishActivities() {
//		synchronized (instance) {
//			if (Settings.System.getInt(GlobalInfo.getInstance()
//					.getActivityContext().getContentResolver(),
//					Settings.System.ALWAYS_FINISH_ACTIVITIES, 0) > 0) {
//				// Settings.System.putInt(GlobalInfo.getInstance().getActivityContext().getContentResolver(),
//				// Settings.System.ALWAYS_FINISH_ACTIVITIES, 0);
//				writeFinishOptions();
//				// int pid = android.os.Process.myPid();
//				// android.os.Process.killProcess(pid);
//			}
//		}
//	}
//
//	/**
//	 * update setting: setAlwaysFinish
//	 *
//	 * @author banghn
//	 */
//	private static void writeFinishOptions() {
//		try {
//			// Due to restrictions related to hidden APIs, need to emulate the
//			// line below
//			// using reflection:
//			// ActivityManagerNative.getDefault().setAlwaysFinish(mAlwaysFinish);
//			final Class<?> classActivityManagerNative = Class
//					.forName("android.app.ActivityManagerNative");
//			final Method methodGetDefault = classActivityManagerNative
//					.getMethod("getDefault");
//			final Method methodSetAlwaysFinish = classActivityManagerNative
//					.getMethod("setAlwaysFinish", new Class[] { boolean.class });
//			final Object objectInstance = methodGetDefault.invoke(null);
//			boolean mAlwaysFinish = false;
//			methodSetAlwaysFinish.invoke(objectInstance,
//					new Object[] { mAlwaysFinish });
//		} catch (Exception ex) {
//			MyLog.i("GlobalUtil", ex.toString());
//		}
//	}
//
//	/**
//	 * Kiem tra co setting thoi gian theo che do auto hay khong
//	 *
//	 * @author banghn
//	 * @return
//	 */
//	public static boolean isSettingAutoTimeUpdate() {
//		boolean isAutoTimeUpdate = true;
//		try{
//			if (Settings.System.getInt(GlobalInfo.getInstance()
//					.getAppContext().getContentResolver(),
//					Settings.System.AUTO_TIME, 0) <= 0
//					|| Settings.System.getInt(GlobalInfo.getInstance()
//							.getAppContext().getContentResolver(),
//							Settings.System.AUTO_TIME_ZONE, 0) <= 0) {
//				isAutoTimeUpdate = false;
//			}
//		}catch(Exception e){
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		}
//		return isAutoTimeUpdate;
//	}
//
//	/**
//	 * Check TimeZone
//	 * @author: duongdt3
//	 * @since: 10:49:34 7 Mar 2014
//	 * @return: boolean
//	 * @throws:
//	 * @return
//	 */
//	public static boolean isSettingRightTimeZone() {
//		return DateUtils.isGMT7TimeZone();
//	}
//
//	/**
//	 * Kiem tra auto time voi timezone phai la +7
//	 * @author: banghn
//	 * @return
//	 */
//	public static boolean isRightSettingTime() {
//		boolean isRightSettingTime = true;
//		if (!isSettingAutoTimeUpdate() || !isSettingRightTimeZone()) {
//			isRightSettingTime = false;
//		}
//		return isRightSettingTime;
//	}
//
//	/**
//	 * Kiem tra file ton tai trong thu muc hay khong
//	 * @author: BANGHN
//	 * @param dir : Thu muc
//	 * @param fileName : ten file
//	 * @return true: co - false:khong
//	 */
//	public static boolean isFileExistsInDirectory(File dir, String fileName){
//		boolean isExists = false;
//		File myFile = new File(dir.getAbsolutePath() + "/" +  fileName);
//		if(myFile.exists()){
//			isExists = true;
//		}
//		return isExists;
//	}
//
//	/**
//	 * Kiem tra mot toa do co nam gan toa do NPP
//	 * @author: BANGHN
//	 * @param location
//	 * @return
//	 */
//	public static boolean isNearNPPPosition(Location location){
//		boolean isNear = false;
//		double shopLat = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopLat();
//		double shopLng = GlobalInfo.getInstance().getProfile().getUserData().getInheritShopLng();
//		if(location != null && shopLat > 0 && shopLng > 0){
//			if (getDistanceBetween(new LatLng(location.getLatitude(), location.getLongitude()),
//					new LatLng(shopLat, shopLng)) <= GlobalInfo.getInstance().getCcDistance()){
//				isNear = true;
//			}
//		}
//		return isNear;
//	}
//
//
//	/**
//	 * Kiem tra mot toa do co nam gan toa do khach hang dang ghe tham
//	 * @author: BANGHN
//	 * @param location
//	 * @return
//	 */
//	public static boolean isNearCustomerVisitingPosition(Location location){
//		boolean isNear = false;
//		if(GlobalInfo.getInstance().getPositionCustomerVisiting() != null && location != null){
//			if (getDistanceBetween(
//					new LatLng(location.getLatitude(), location.getLongitude()),
//					GlobalInfo.getInstance().getPositionCustomerVisiting())
//					<= GlobalInfo.getInstance().getDistanceAllowFinish()) {
//				isNear = true;
//			}
//		}
//		return isNear;
//	}
//
//	/**
//	 * Tong so dong trong file
//	 * @author: banghn
//	 * @param file
//	 * @return
//	 */
//	public static long countLineOfFile(File file) {
//		int lines = 0;
//		BufferedReader reader = null;
//		try {
//			reader = new BufferedReader(new FileReader(file));
//			String line = null;
//			while ((line = reader.readLine()) != null){
//				lines++;
//			}
//		} catch (Exception e) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(e));
//		} finally {
//			if (reader != null) {
//				try {
//					reader.close();
//				} catch (IOException e) {
//				}
//			}
//		}
//		return lines;
//	}
//
//
//	/**
//	 * Hien thi dialog khong cho phep login offline vi sai ngay
//	 * @author: duongdt
//	 * @modifier: banghn
//	 */
//	public static void showDialogNotAllowOfflineLogin() {
//		try {
//			SpannableObject strNotice = new SpannableObject();
//			String dateOfWeekRight = "";
//			String dateOfWeekNow = DateUtils.getDayOfWeek(new Date());
//
//			String additionString = "\n";
//			strNotice.addSpan(StringUtil.getString(R.string.ERROR_NOT_ALLOW_OFFLINE), ImageUtil.getColor(R.color.WHITE),
//					Typeface.NORMAL);
//			try {
//				String lastTimeOnline = GlobalInfo.getInstance().getRightTimeInfo().getLastTimeOnlineLogin();
//
//				if (!StringUtil.isNullOrEmpty(lastTimeOnline) && !"null".equals(lastTimeOnline)) {
//					Date dFromLastLogIn = DateUtils.parseDateFromString(lastTimeOnline, DateUtils.DATE_FORMAT_NOW);
//					Date rightTime = new Date(dFromLastLogIn.getTime());
//					dateOfWeekRight = DateUtils.getDayOfWeek(rightTime);
//	    			String dateRight = dateOfWeekRight + ", "
//	    					+ DateUtils.formatDate(rightTime, DateUtils.DATE_TIME_FORMAT_VN);
//
//					strNotice.addSpan(additionString + StringUtil.getString(R.string.LAST_TIME_ONLINE) + ": ",
//							ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//					strNotice.addSpan(dateRight, ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//				}
//			} catch (Exception e) {}
//
//			strNotice.addSpan(additionString + StringUtil.getString(R.string.CURRENT_TIME) + ": ",
//					ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
//			String dateNow = dateOfWeekNow + ", " + DateUtils.nowVN();
//			strNotice.addSpan(dateNow, ImageUtil.getColor(R.color.COLOR_LINK), Typeface.NORMAL);
//
//			AlertDialog alertDialogNotAllowOfflineLogin = null;
//			alertDialogNotAllowOfflineLogin = new AlertDialog.Builder(GlobalInfo.getInstance().getActivityContext()).create();
//			alertDialogNotAllowOfflineLogin.setCanceledOnTouchOutside(false);
//			alertDialogNotAllowOfflineLogin.setCancelable(false);
//			alertDialogNotAllowOfflineLogin.setTitle(StringUtil.getString(R.string.TEXT_WARNING));
//			alertDialogNotAllowOfflineLogin.setMessage(strNotice.getSpan());
//			alertDialogNotAllowOfflineLogin.setButton(
//					StringUtil.getString(R.string.TEXT_BUTTON_RELOGIN),
//					new DialogInterface.OnClickListener() {
//						public void onClick(DialogInterface dialog, int which) {
//							dialog.dismiss();
//							return;
//						}
//					});
//			alertDialogNotAllowOfflineLogin.show();
//		} catch (Exception ex) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//		}
//	}
//
//	/**
//	 * Show dialog kiem tra ngay gio bi chinh sua khong co ket noi mang de check tu dong
//	 * @author: duongdt
//	 * @modifer: banghn
//	 * @param wrongTimeType
//	 */
//	public static void showDialogCheckWrongTime(int wrongTimeType) {
////		if ((alertDialogCheckSettingTime == null || !alertDialogCheckSettingTime
////				.isShowing()) && (getAlertDialogAutoTime() == null || !getAlertDialogAutoTime().isShowing())) {
////			try {
////				// neu ton tai 1 dialog dang show, an no di, hien cai moi len
////				if (alertDialogCheckOnlineInDate != null
////						&& alertDialogCheckOnlineInDate.isShowing()) {
////					try {
////						alertDialogCheckOnlineInDate.dismiss();
////					} catch (Exception e) {
////						MyLog.e("showDialogCheckWrongTime", e);
////					}
////				}
////
////				String lastRightTime = GlobalInfo.getInstance().getRightTimeInfo().getLastRightTime();
////				SpannableObject strNotice = new SpannableObject();
////
////				strNotice.addSpan(StringUtil
////						.getString(R.string.ERROR_TIME_TABLET_INVALID),
////						ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
////				// neu co thoi gian cuoi online, thi hien them vao thong bao
////				if (!StringUtil.isNullOrEmpty(lastRightTime)
////						&& !"null".equals(lastRightTime)) {
////					long lastTimeOnlineFromBoot = GlobalInfo.getInstance().getRightTimeInfo().getLastRightTimeSinceBoot();
////					Date dLastRightTime = DateUtils.parseDateFromString(
////							lastRightTime, DateUtils.DATE_FORMAT_NOW);
////					long timeSinceBootNow = SystemClock.elapsedRealtime();
////					long dis = (timeSinceBootNow - lastTimeOnlineFromBoot);
////					if (dis < 0) {
////						dis = 0;
////					}
////					String dateOfWeekRight = "";
////					String dateOfWeekNow = DateUtils.getDayOfWeek(new Date());
////					Date rightTime = new Date(dLastRightTime.getTime() + dis);
////					dateOfWeekRight = DateUtils.getDayOfWeek(rightTime);
////					String dateRight = dateOfWeekRight + ", "
////							+ DateUtils.formatDate(rightTime, DateUtils.DATE_TIME_FORMAT_VN);
////					String dateNow = dateOfWeekNow + ", " + DateUtils.nowVN();
////
////					String additionString = "\n";
////					strNotice.addSpan(
////							additionString
////									+ StringUtil.getString(R.string.RIGHT_TIME) + ": ",
////							ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
////					strNotice.addSpan(dateRight,
////							ImageUtil.getColor(R.color.COLOR_LINK),
////							Typeface.NORMAL);
////					strNotice.addSpan(
////							additionString
////									+ StringUtil.getString(R.string.CURRENT_TIME)
////									+ ": ", ImageUtil.getColor(R.color.WHITE), Typeface.NORMAL);
////					strNotice.addSpan(dateNow,
////							ImageUtil.getColor(R.color.COLOR_LINK),
////							Typeface.NORMAL);
////				}
////				alertDialogCheckOnlineInDate = new AlertDialog.Builder(
////						GlobalInfo.getInstance().getActivityContext()).create();
////				alertDialogCheckOnlineInDate.setCanceledOnTouchOutside(false);
////				alertDialogCheckOnlineInDate.setCancelable(false);
////				alertDialogCheckOnlineInDate.setTitle(StringUtil
////						.getString(R.string.TEXT_WARNING));
////				alertDialogCheckOnlineInDate
////						.setButton(AlertDialog.BUTTON_POSITIVE, StringUtil
////								.getString(R.string.TEXT_BUTTON_TIME_SETTING),
////								new DialogInterface.OnClickListener() {
////
////									@Override
////									public void onClick(DialogInterface dialog,
////											int which) {
////										dialog.dismiss();
////										GlobalUtil.startActivityOtherApp(GlobalInfo.getInstance().getActivityContext(),
////												new Intent(android.provider.Settings.ACTION_DATE_SETTINGS));
////										return;
////									}
////								});
////				alertDialogCheckOnlineInDate.setButton(AlertDialog.BUTTON_NEGATIVE,
////						StringUtil.getString(R.string.TEXT_BUTTON_RELOGIN),
////						new DialogInterface.OnClickListener() {
////
////							@Override
////							public void onClick(DialogInterface dialog,
////									int which) {
////								dialog.dismiss();
////								((GlobalBaseActivity) GlobalInfo.getInstance()
////										.getActivityContext())
////										.sendBroadcast(ActionEventConstant.ACTION_FINISH_AND_SHOW_LOGIN, new Bundle());
////								return;
////							}
////						});
////
////				alertDialogCheckOnlineInDate.setMessage(strNotice.getSpan());
////				alertDialogCheckOnlineInDate.show();
////
////			} catch (Exception ex) {
////				MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
////			}
////		}
//	}
//
//	/**
//	 * Tao Header
//	 *
//	 * @param width
//	 * @param title
//	 * @param textColor
//	 * @param bgColor
//	 * @param context
//	 * @return
//	 */
//	public static TextView createColumnHeader(int width,String title, int textColor, int bgColor, Context context){
//		TextView myTextView = new TextView(context);
//		//myTextView.setTextAppearance(getContext(), R.style.HeaderVinamilkTable);
//		TableRow.LayoutParams param=new TableRow.LayoutParams();
//		param.width=GlobalUtil.dip2Pixel(width);
//		param.height=LayoutParams.MATCH_PARENT;
////		param.bottomMargin=1;
////		param.topMargin=1;
//		param.leftMargin=GlobalUtil.dip2Pixel(2);
////		param.rightMargin=1;
//		myTextView.setLayoutParams(param);
//		myTextView.setMinHeight(GlobalUtil.dip2Pixel(35));
//		myTextView.setText(title);
//		myTextView.setPadding(0, GlobalUtil.dip2Pixel(5), 0, GlobalUtil.dip2Pixel(5));
//		myTextView.setTextColor(textColor);
//		myTextView.setBackgroundColor(bgColor);
//		myTextView.setTypeface(null, Typeface.BOLD);
//		myTextView.setGravity(Gravity.CENTER);
//
//		return myTextView;
//	}
//
//	/**
//	 * backup file database voi role va shop
//	 * @author: Tuanlt11
//	 * @return: void
//	 * @throws:
//	*/
//	public static void backupDatabaseToZIPWithRoleShop(String roleId, String shopId) {
//		try {
//			// truong hop ko co roleId va shopId thi bo quan luong nay
//			if (!StringUtil.isNullOrEmpty(roleId)
//					&& !StringUtil.isNullOrEmpty(shopId)) {
//				File zipDir = ExternalStorage.getFileDBPath(GlobalInfo
//						.getInstance().getAppContext());
//				// File path to store .zip file before unzipping
//				String nameZip = "/"
//						+ DateUtils
//								.getCurrentDateTimeWithFormat(DateUtils.DATE_FORMAT_FILE_DEFAULT)
//						+ ".zip";
//				File zipFile = new File(zipDir.getPath() + nameZip);
//				String dbFilePath = ExternalStorage.getFileDBPath(
//						GlobalInfo.getInstance().getAppContext())
//						.getAbsolutePath()
//						+ "/"
//						+ roleId
//						+ "_"
//						+ shopId
//						+ "_"
//						+ Constants.DATABASE_NAME;
//
//				File f = new File(dbFilePath);
//				if (f.exists()) {
//					Compress compress = new Compress(new String[] { dbFilePath },
//							zipFile.getPath());
//					compress.zip();
//				}
//			}
//		} catch (Exception e) {
//			MyLog.e("backupDatabaseToZIPWithRoleShop", "fail", e);
//		}
//	}
//
//	 /**
//	 * Lay dpi man hinh
//	 * @author: Tuanlt11
//	 * @return: void
//	 * @throws:
//	*/
//	public int getDPIScreen() {
//		DisplayMetrics dm = GlobalInfo.getInstance().getAppContext()
//				.getResources().getDisplayMetrics();
//		int width = dm.widthPixels;
//		int height = dm.heightPixels;
//
//		int deviceWith = Math.max(width, height);
//		//margin left + right in FragmentView
//		int totalMarginTable = dip2Pixel(Constants.LEFT_MARGIN_TABLE_DIP) + dip2Pixel(Constants.RIGHT_MARGIN_TABLE_DIP);
//		//padding left + right in DMSTableView
//		int totalPaddingTable = dip2Pixel(1) + dip2Pixel(1);
//
//		// tru margin left right 10dp
//		int result = (int) (deviceWith) - totalMarginTable - totalPaddingTable;
//		//Log.e("getDPIScreen DMSTableView", String.format("deviceWith: %d, result: %d", deviceWith, result));
//		return result;
//	}
//
//	/**
//	 * get tag of class
//	 * @author: duongdt3
//	 * @since: 09:39:59 11 Feb 2015
//	 * @return: String
//	 * @throws:
//	 * @param clz
//	 * @return
//	 */
//	public static String getTag(Class<?> clz){
//		String tag = "";
//		if (clz != null) {
//			tag = clz.getName();
//		}
//		return tag;
//	}
//
//	/**
//	 * set textview color for multi textview
//	 * @author: duongdt3
//	 * @since: 09:52:37 13 Apr 2015
//	 * @return: void
//	 * @throws:
//	 * @param color
//	 * @param views
//	 */
//	public static void setTextColor(int color, TextView ... views){
//		for (int i = 0; i < views.length; i++) {
//			TextView v = views[i];
//			if (v != null) {
//				v.setTextColor(color);
//			}
//		}
//	}
//
//	 /**
//	 * set type face
//	 * @author: Tuanlt11
//	 * @param type
//	 * @param views
//	 * @return: void
//	 * @throws:
//	*/
//	public static void setTypeFace(int type, TextView... views) {
//		for (int i = 0; i < views.length; i++) {
//			TextView v = views[i];
//			if (v != null) {
//				v.setTypeface(null, type);
//			}
//		}
//	}
//
//	/**
//	 * set background view
//	 * @author: duongdt3
//	 * @since: 14:04:55 17 Apr 2015
//	 * @return: void
//	 * @throws:
//	 * @param color
//	 * @param views
//	 */
//	public static void setBackgroundColor(int color, View ... views){
//		for (int i = 0; i < views.length; i++) {
//			View v = views[i];
//			if (v != null) {
//				setBackgroundByColor(v, color);
//			}
//		}
//	}
//
//	/**
//	 * Set background for View, by drawable selector
//	 *
//	 * @param v
//	 * @param idResourceBackground
//	 */
//	public static void setBackgroundByDrawable(View v, int idResourceBackground) {
//		setBackgroundView(v, idResourceBackground, TYPE_BACKGROUND_DRAWABLE);
//	}
//
//	/**
//	 * Set background for View, color get by ImageUtil.getColor(idResourceColor)
//	 *
//	 * @param v
//	 * @param color
//	 */
//	public static void setBackgroundByColor(View v, int color) {
//		setBackgroundView(v, color, TYPE_BACKGROUND_COLOR);
//	}
//
//	/**
//	 * Set background for View, id resource color from color.xml
//	 *
//	 * @param v
//	 * @param color
//	 */
//	public static void setBackgroundColorResource(View v, int idResourceColor) {
//		 int color =  ImageUtil.getColor(idResourceColor);
//		setBackgroundView(v, color, TYPE_BACKGROUND_COLOR);
//	}
//
//	static final int TYPE_BACKGROUND_DRAWABLE = 1;
//	static final int TYPE_BACKGROUND_COLOR = 2;
//
//	static void setBackgroundView(View v, int resource, int typeBackground) {
//		if (v != null) {
//			// store layout param
//			int paddingLeft = v.getPaddingLeft();
//			int paddingRight = v.getPaddingRight();
//			int paddingTop = v.getPaddingTop();
//			int paddingBottom = v.getPaddingBottom();
//			ViewGroup.LayoutParams viewParam = v.getLayoutParams();
//
//			switch (typeBackground) {
//			case TYPE_BACKGROUND_DRAWABLE:
//				// set background
//				v.setBackgroundResource(resource);
//				break;
//			case TYPE_BACKGROUND_COLOR:
//				// set background
//				v.setBackgroundColor(resource);
//				break;
//
//			default:
//				break;
//			}
//
//			// re layout
//			if (viewParam != null) {
//				v.setLayoutParams(viewParam);
//			}
//
//			v.setPadding(paddingLeft, paddingTop, paddingRight, paddingBottom);
//		}
//	}
//
//	/**
//	 * reset layout params
//	 * @author: duongdt3
//	 * @since: 17:08:54 24 Apr 2015
//	 * @return: ViewGroup.LayoutParams
//	 * @throws:
//	 * @param v
//	 * @return
//	 */
//	public static ViewGroup.LayoutParams resetLayoutParams(View v) {
//		ViewGroup.LayoutParams params = v != null ? v.getLayoutParams() : null;
//		if (params != null) {
//			if (params instanceof LayoutParams) {
//				LayoutParams paramsEdit = (LayoutParams) params;
//				paramsEdit.leftMargin = 0;
//				paramsEdit.rightMargin = 0;
//				paramsEdit.topMargin = 0;
//				paramsEdit.bottomMargin = 0;
//			} else if (params instanceof RelativeLayout.LayoutParams) {
//				RelativeLayout.LayoutParams paramsEdit = (RelativeLayout.LayoutParams) params;
//				paramsEdit.leftMargin = 0;
//				paramsEdit.rightMargin = 0;
//				paramsEdit.topMargin = 0;
//				paramsEdit.bottomMargin = 0;
//			} else if (params instanceof TableRow.LayoutParams) {
//				TableRow.LayoutParams paramsEdit = (TableRow.LayoutParams) params;
//				paramsEdit.leftMargin = 0;
//				paramsEdit.rightMargin = 0;
//				paramsEdit.topMargin = 0;
//				paramsEdit.bottomMargin = 0;
//			}
//		}
//		return params;
//	}
//
//	public static String logLayoutView(View v){
//		String log = "";
//		if (v != null) {
//			ViewGroup.LayoutParams params = v != null ? v.getLayoutParams() : null;
//			if (params != null) {
//				int leftMargin = 0;
//				int rightMargin = 0;
//				int topMargin = 0;
//				int bottomMargin = 0;
//				if (params instanceof LayoutParams) {
//					LayoutParams paramsEdit = (LayoutParams) params;
//					leftMargin = paramsEdit.leftMargin;
//					rightMargin = paramsEdit.rightMargin;
//					topMargin = paramsEdit.topMargin;
//					bottomMargin =paramsEdit.bottomMargin;
//				} else if (params instanceof RelativeLayout.LayoutParams) {
//					RelativeLayout.LayoutParams paramsEdit = (RelativeLayout.LayoutParams) params;
//					leftMargin = paramsEdit.leftMargin;
//					rightMargin = paramsEdit.rightMargin;
//					topMargin = paramsEdit.topMargin;
//					bottomMargin =paramsEdit.bottomMargin;
//				} else if (params instanceof TableRow.LayoutParams) {
//					TableRow.LayoutParams paramsEdit = (TableRow.LayoutParams) params;
//					leftMargin = paramsEdit.leftMargin;
//					rightMargin = paramsEdit.rightMargin;
//					topMargin = paramsEdit.topMargin;
//					bottomMargin =paramsEdit.bottomMargin;
//				}
//
//				log = String.format("width: %d height: %d, pL: %d, pR: %d, pT: %d, pB: %d, mL: %d, mR: %d, mT: %d, mB: %d",
//						v.getWidth(), v.getHeight(),
//						v.getPaddingLeft(), v.getPaddingRight(), v.getPaddingTop(), v.getPaddingBottom(),
//						leftMargin, rightMargin, topMargin, bottomMargin);
//			}
//		}
//		return log;
//	}
//
//	/**
//	 * Lay khoang cach giua 2 diem lat, lng bat ki
//	 *
//	 * @author: Tuanlt11
//	 * @param cusLatLng
//	 * @param myLatLng
//	 * @return
//	 * @return: double
//	 * @throws:
//	 */
//	public static double getDistance(LatLng cusLatLng, LatLng myLatLng) {
//		double distance = 0;
//		if (myLatLng.lat > 0 && myLatLng.lng > 0 && cusLatLng.lat > 0
//				&& cusLatLng.lng > 0) {
//			distance = GlobalUtil.getDistanceBetween(myLatLng, cusLatLng);
//		}
//		return distance;
//	}
//
//	public static class AppInfo{
//		public String appName;
//		public String packageName;
//
//		public AppInfo(String appName, String packageName) {
//			this.appName = appName;
//			this.packageName = packageName;
//		}
//	}
//	/**
//	 * @author: duongdt3
//	 * @since: 08:09:34 27 Jan 2015
//	 * @return: void
//	 * @throws:
//	 */
//	public static List<AppInfo> getListMockApps(Context ac, boolean isOnlyGetFirstApp) {
//		List<AppInfo> lstAppMock = new ArrayList<AppInfo>();
//		long time = SystemClock.elapsedRealtime();
//		MyLog.d("getListMockApps", "start "  + time);
//		PackageManager pm = ac.getPackageManager();
//		List<ApplicationInfo> packages = pm.getInstalledApplications(PackageManager.GET_META_DATA);
//		if (packages != null) {
//			for (ApplicationInfo applicationInfo : packages) {
//				try {
//					//khong phai ud he thong thi lay quyen de check
//					if (applicationInfo != null && applicationInfo.packageName != null) {
//						MyLog.d("getListMockApps", "Package: " + applicationInfo.packageName);
//						boolean isFakeGPSApp = GlobalUtil.isAppFakeLocation(pm, applicationInfo.packageName);
//
//						//them vao danh sach
//						if (isFakeGPSApp) {
//							CharSequence appName = pm.getApplicationLabel(applicationInfo);
//							if (appName == null) {
//								appName = "";
//							}
//							AppInfo app = new AppInfo(appName.toString(), applicationInfo.packageName);
//							lstAppMock.add(app);
//							MyLog.d("getListMockApps", "Package: " + applicationInfo.packageName + " " + "mock enable");
//
//							//neu chi can lay 1 ung dung dau tien thi break;
//							if (isOnlyGetFirstApp) {
//								break;
//							}
//						}
//					}
//				} catch (Exception e) {
//					MyLog.d("getListMockApps", "error "  + MyLog.printStackTrace(e));
//				}
//			}
//		}
//		long timeEnd = SystemClock.elapsedRealtime();
//		MyLog.d("getListMockApps", "end "  + timeEnd);
//		MyLog.d("getListMockApps", "total "  + (timeEnd - time) );
//
//		return lstAppMock;
//	}
//
//	/**
//	 * check app have permission
//	 * @author: duongdt3
//	 * @since: 12:40:33 27 Jan 2015
//	 * @return: boolean
//	 * @throws:
//	 * @return
//	 * @throws Exception
//	 */
//	public static boolean isAppHavePermission(PackageManager pm, String packageName,
//			String permission) throws Exception {
//		PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
//		boolean isHavePermission = isAppHavePermission(packageInfo, permission);
//		return isHavePermission;
//	}
//
//	/**
//	 * check app have permission
//	 * @author: duongdt3
//	 * @since: 11:58:08 30 Jan 2015
//	 * @return: boolean
//	 * @throws:
//	 * @param packageInfo
//	 * @param permission
//	 * @return
//	 * @throws Exception
//	 */
//	public static boolean isAppHavePermission(PackageInfo packageInfo, String permission)
//			throws Exception {
//		boolean isHavePermission = false;
//		if (packageInfo != null && !StringUtil.isNullOrEmpty(permission)) {
//			// Get Permissions
//			String[] requestedPermissions = packageInfo.requestedPermissions;
//
//			if (requestedPermissions != null) {
//				for (int i = 0; i < requestedPermissions.length; i++) {
//					if (permission.equals(requestedPermissions[i])) {
//						isHavePermission = true;
//						break;
//					}
//				}
//			}
//		}
//		return isHavePermission;
//	}
//
//	/**
//	 * call uninstall activity
//	 * @author: duongdt3
//	 * @since: 16:00:20 28 Jan 2015
//	 * @return: void
//	 * @throws:
//	 * @param Activity
//	 * @param packageName
//	 */
//	public static void uninstallApp(Activity ac, String packageName){
//        Uri packageUri = Uri.parse("package:" + packageName);
//        Intent uninstallIntent =
//          new Intent(Intent.ACTION_DELETE, packageUri);
//        try {
//        	GlobalUtil.startActivityOtherApp(ac, uninstallIntent);
//		} catch (Exception e) {
//		}
//	}
//
//	/**
//	 * check app in black list mock location
//	 * @author: duongdt3
//	 * @since: 11:24:48 30 Jan 2015
//	 * @return: boolean
//	 * @throws:
//	 * @param packageName
//	 * @return
//	 */
//	public static boolean isAppInBlackListMockLocation(String packageName) {
//		boolean isInBlacklist = false;
//		String blackList = GlobalInfo.getInstance().getAppBlackListMockLocation();
//		if (!StringUtil.isNullOrEmpty(packageName) && !StringUtil.isNullOrEmpty(blackList)) {
//			String [] arrBlackList = blackList.split(";");
//			if (arrBlackList != null) {
//				for (int i = 0; i < arrBlackList.length; i++) {
//					if(!StringUtil.isNullOrEmpty(arrBlackList[i])
//							&& arrBlackList[i].equals(packageName)){
//						isInBlacklist = true;
//						break;
//					}
//				}
//			}
//		}
//		return isInBlacklist;
//	}
//
//	/**
//	 * check app in white list mock location
//	 * @author: duongdt3
//	 * @since: 11:24:48 30 Jan 2015
//	 * @return: boolean
//	 * @throws:
//	 * @param packageName
//	 * @return
//	 */
//	public static boolean isAppInWhiteListMockLocation(String packageName) {
//		boolean isInWhitelist = false;
//		String whiteList = GlobalInfo.getInstance().getAppWhiteListMockLocation();
//		if (!StringUtil.isNullOrEmpty(packageName) && !StringUtil.isNullOrEmpty(whiteList)) {
//			String [] arrWhiteList = whiteList.split(";");
//			if (arrWhiteList != null) {
//				for (int i = 0; i < arrWhiteList.length; i++) {
//					if(!StringUtil.isNullOrEmpty(arrWhiteList[i])
//							&& arrWhiteList[i].equals(packageName)){
//						isInWhitelist = true;
//						break;
//					}
//				}
//			}
//		}
//		return isInWhitelist;
//	}
//
//	/**
//	 * check app is fake location app
//	 * @author: duongdt3
//	 * @since: 11:54:38 30 Jan 2015
//	 * @return: boolean
//	 * @throws:
//	 * @param pm
//	 * @param packageName
//	 * @return
//	 * @throws Exception
//	 */
//	public static boolean isAppFakeLocation(PackageManager pm, String packageName) throws Exception{
//		boolean isFakeGPSApp = false;
//		if (!StringUtil.isNullOrEmpty(packageName) && pm != null) {
//			// check in black list, this is fake app
//			if (GlobalUtil.isAppInBlackListMockLocation(packageName)) {
//				isFakeGPSApp = true;
//			} else {
//				try {
//					//check permission
//					PackageInfo packageInfo = pm.getPackageInfo(packageName, PackageManager.GET_PERMISSIONS);
//					if (packageInfo != null) {
//						// check not in white list, check system app + check permission
//						if ((packageInfo.applicationInfo != null
//								&& (packageInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) == 0)
//								&& !GlobalUtil.isAppInWhiteListMockLocation(packageName)) {
//							isFakeGPSApp = isAppHavePermission(packageInfo, PERMISSION_ACCESS_MOCK_LOCATION);
//						}
//					}
//				} catch (NameNotFoundException e) {
//					//mot so ung dung he thong se ko tim thay
//					isFakeGPSApp = false;
//				} catch (Exception e) {
//					throw e;
//				}
//			}
//		}
//		return isFakeGPSApp;
//	}
//
//	public static boolean isInTimeSyn(){
//		boolean isInTimeSyn = false;
//		// h hien tai
//		int currentHour = DateUtils.getCurrentTimeByTimeType(Calendar.HOUR_OF_DAY);
//		// check thoi gian day vi tri trong thoi gian cho phep
//		if (currentHour >= GlobalInfo.getInstance().getBeginTimeAllowSynData()
//				&& currentHour < GlobalInfo.getInstance().getEndTimeAllowSynData()) {
//			isInTimeSyn = true;
//		}
//		return isInTimeSyn;
//	}
//
//	/**
//	 * Request KPI
//	 * @author: banghn
//	 * @param kpi (chuc nang kpi)
//	 * @param startTimeKPI: Thoi gian bat dau
//	 * @param endTimeKPI : Thoi gian ket thuc
//	 * @param note :ghi chu trang thai
//	 */
//	public static void requestInsertLogKPI(HashMapKPI kpi,
//			Calendar startTimeKPI, Calendar endTimeKPI, String note) {
//		try {
//			int valueKey = GlobalInfo.getInstance().getHashMapKPI().get(kpi.ordinal());
//			KPILogDTO kpiLogDTO = new KPILogDTO();
//			kpiLogDTO.setClientLat(GlobalInfo.getInstance().getProfile()
//					.getMyGPSInfo().getLatitude());
//			kpiLogDTO.setClientLng(GlobalInfo.getInstance().getProfile()
//					.getMyGPSInfo().getLongtitude());
//			kpiLogDTO.setCreateDate(DateUtils.now());
//			kpiLogDTO.setMobileData(GlobalUtil.getTypeNetwork());
//			kpiLogDTO.setServiceBeginTime(DateUtils.convertDateTimeWithFormat(
//					startTimeKPI.getTime(), DateUtils.DATE_FORMAT_NOW));
//			kpiLogDTO.setServiceEndTime(DateUtils.convertDateTimeWithFormat(
//					endTimeKPI.getTime(), DateUtils.DATE_FORMAT_NOW));
//			kpiLogDTO.setServiceCode(kpi.getNote());
//			kpiLogDTO.setShopId(Long.valueOf(GlobalInfo.getInstance()
//					.getProfile().getUserData().getInheritShopId()));
//			kpiLogDTO.setStaffId(Long.valueOf(GlobalInfo.getInstance()
//					.getProfile().getUserData().getInheritId()));
//			long totalExecutationTime = endTimeKPI.getTimeInMillis()
//					- startTimeKPI.getTimeInMillis();
//			kpiLogDTO.setTotalExecutationTime(totalExecutationTime);
//
//			//add note
//			note += " " + getKPINote(kpi);
//
//			kpiLogDTO.setNote(note);
//			GlobalInfo.getInstance().addKPILog(kpiLogDTO);
//			MyLog.d("VTLog", "insert KPI log in '+" + kpi.getNote() + "': measure : "
//					+ totalExecutationTime);
//			GlobalInfo.getInstance().putHashMapKPI(kpi.ordinal(), valueKey + 1);
//		} catch (Exception ex) {
//			MyLog.w("", VNMTraceUnexceptionLog.getReportFromThrowable(ex));
//			ServerLogger.sendLog("requestInsertLogKPI", VNMTraceUnexceptionLog.getReportFromThrowable(ex), false,
//					TabletActionLogDTO.LOG_CLIENT);
//		}
//	}
//
//	/**
//	 * get KPI note
//	 * @param kpi
//	 * @return
//	 */
//	private static String getKPINote(HashMapKPI kpi) {
//		StringBuffer buffer = new StringBuffer();
//		//xem phan tram pin
//		buffer.append(" PIN: " + getBatteryPercent() + "%");
//
//		//xem luong RAM con lai
//		buffer.append(" RAM: " + getMemoryAv() + "MB");
//
//		//task info
//		buffer.append(" Task: " + AsyncTaskUtil.getThreadInfo());
//
//		//dung luong trong cua o dia
//		buffer.append(" " + getInternalDiskSpace() + " " + getExtranalDiskSpace());
//
//		//thong tin GPS
//		buffer.append(" GPS: " + getGpsProviderListStr());
//
//		//xem so luong ung dung dang chay hien tai, toi da 20 ung dung
//		List<RunningTaskInfo> lTask = getListTaskRunning(20);
//		if (lTask != null && !lTask.isEmpty()) {
//			String apps = " APP RUNNING: ";
//			for (RunningTaskInfo runningTaskInfo : lTask) {
//				if (runningTaskInfo != null
//						&& runningTaskInfo.topActivity != null) {
//					String appName = runningTaskInfo.topActivity.getPackageName();
//					apps += appName + ";";
//				}
//			}
//
//			// kiem tra danh sach ung dung hien tai co thay doi hay khong.
//			if (!apps.equals(GlobalInfo.getInstance().getLastAppsList())) {
//				buffer.append(apps);
//				// set lai danh sach ung dung moi
//				GlobalInfo.getInstance().setLastAppsList(apps);
//			}
//		}
//
//		//them nhung thuoc tinh khac
//		// neu la dang nhap thi se co them IMEI + SERIAL
//		if (kpi.ordinal() == HashMapKPI.GLOBAL_LOGIN.ordinal()) {
//			buffer.append(String.format(" AppVersion: %s IMEI: %s Serial: %s SysInfo: %s Apps Install: %s",
//					GlobalUtil.getAppVersion(), GlobalInfo.getInstance().getDeviceIMEI(),
//					StringUtil.getSimSerialNumber(), getSystemInfoStr(), getListAppInstallStr()));
//		}
//		return buffer.toString();
//	}
//
//	/**
//	 * get system info
//	 * @author: duongdt3
//	 * @since: 17:40:33 26 Nov 2014
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public static String getSystemInfoStr(){
//		String result = "Device: " + Build.MANUFACTURER + ", "
//				+ Build.MODEL + ", "
//				+ Build.VERSION.SDK_INT + ", " + getAppVersion()
//				+ ", " + Build.CPU_ABI + ", " + Build.CPU_ABI2;
//		return result;
//	}
//
//	/**
//	 * get soft app version
//	 * @author: duongdt3
//	 * @since: 18:08:02 26 Nov 2014
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public static String getAppVersion(){
//		String result = "";
//		try {
//			result = GlobalInfo.getInstance().getPackageManager()
//					.getPackageInfo(GlobalInfo.getInstance().getPackageName(), 0).versionName;
//		} catch (Exception e) {
//		}
//		return result;
//	}
//
//	/**
//	 * get list gps provider
//	 * @author: duongdt3
//	 * @since: 17:54:58 26 Nov 2014
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public static String getGpsProviderListStr(){
//		String result = "GPS list: ";
//		try {
//			LocationManager locManager = (LocationManager) GlobalInfo
//					.getInstance().getAppContext()
//					.getSystemService(Context.LOCATION_SERVICE);
//
//			List<String> listProvider = locManager.getAllProviders();
//			for (String provider : listProvider) {
//				result += provider + ": " + (locManager.isProviderEnabled(provider) ? "on" : "off") + " ";
//			}
//		} catch (Exception e) {
//		}
//		return result;
//	}
//
//	/**
//	 * Get % pin
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 09:59:37 26 Jun 2014
//	 * @return: float
//	 * @throws:
//	 * @return
//	 */
//	public static int getBatteryPercent() {
//	    Intent batteryIntent = GlobalInfo.getInstance().getAppContext().registerReceiver(null, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));
//	    float level = (float)batteryIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
//	    float scale = (float)batteryIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
//
//	    // Error checking that probably isn't needed but I added just in case.
//	    if(level <0 || scale < 0) {
//	        return 0;
//	    }
//	    return (int)((level / scale) * 100.0d);
//	}
//
//	/**
//	 * Lay luong RAM con lai
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 11:23:05 26 Jun 2014
//	 * @return: long
//	 * @throws:
//	 * @return
//	 */
//	public static long getMemoryAv() {
//		MemoryInfo mi = new MemoryInfo();
//		ActivityManager activityManager = (ActivityManager)  GlobalInfo.getInstance().getAppContext().getSystemService(Context.ACTIVITY_SERVICE);
//		activityManager.getMemoryInfo(mi);
//		long availableMegs = mi.availMem / 1048576L;
//		return availableMegs;
//	}
//
//	/**
//	 * get KPI AsyncTask status note
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 17:24:04 6 Aug 2014
//	 * @return: String
//	 * @throws:
//	 * @return
//	 */
//	public static String getKPIAsyncTaskNote() {
//		return "A: " + AsyncTaskUtil.getNumAsyncTaskActive() + " W: " + AsyncTaskUtil.getNumAsyncTaskWait();
//	}
//
//	public static String getInternalDiskSpace(){
//		String path = Environment.getDataDirectory().getAbsolutePath();
//		return getDiskSpace(path, "DISK");
//	}
//
//	public static String getExtranalDiskSpace(){
//		String path = Environment.getExternalStorageDirectory().getAbsolutePath();
//		return getDiskSpace(path, "SD_CARD");
//	}
//
//	public static String getDiskSpace(String pathDisk, String name){
//		String result = "";
//		if (!StringUtil.isNullOrEmpty(pathDisk)) {
//			//Environment.getRootDirectory().getAbsolutePath()
//			//File path = Environment.getDataDirectory();
//			StatFs stat = new StatFs(pathDisk);
//			long blockSize = stat.getBlockSize();
//			long availableBlocks = stat.getAvailableBlocks();
//			result = name + ":" + Formatter.formatFileSize(GlobalInfo.getInstance(), availableBlocks * blockSize);
//		}
//		return result;
//	}
//
//	public static void showToast(String message) {
//		SuperToast.makeText(GlobalInfo.getInstance(), message, Toast.LENGTH_SHORT).show();
//	}
//
//	public static void showToastLong(String message) {
//		SuperToast.makeText(GlobalInfo.getInstance(), message, Toast.LENGTH_LONG).show();
//	}
//
//	public static String getRealPathFromURI(Context context, Uri contentUri) {
//		String path = "";
//		Cursor cursor = null;
//		try {
//			String[] proj = { MediaStore.Images.Media.DATA };
//			cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
//			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
//			if (cursor.moveToFirst()) {
//				path = cursor.getString(column_index);
//			}
//		} catch (Exception e){
//			MyLog.e("getRealPathFromURI", contentUri.getPath(), e);
//		} finally {
//			if (cursor != null) {
//				cursor.close();
//			}
//		}
//		return path;
//	}
//
//	/**
//	 * show activity pick image video
//	 * @author: duongdt3
//	 * @since: 15:42:11 24 Aug 2015
//	 * @return: void
//	 * @throws:
//	 * @param frag
//	 * @param action
//	 */
//	public static void pickMedia(BaseFragment frag, int action) {
//		// Create the Intent for Image Gallery.
//        Intent i = new Intent(Intent.ACTION_GET_CONTENT);
//        i.setType("image/* video/*");
//        // Start new activity with the LOAD_IMAGE_RESULTS to handle back the results
//        //when image is picked from the Image Gallery.
//        frag.startActivityForResult(i, action, StringUtil.getString(R.string.TEXT_TAKEN_PHOTO));
//	}
//
//	/**
//	 * get app name from package name
//	 * @author: duongdt3
//	 * @since: 09:36:07 12 Feb 2015
//	 * @return: String
//	 * @throws:
//	 * @param pm
//	 * @param packageName
//	 * @return
//	 */
//	public static String getAppName(PackageManager pm, String packageName){
//		String result = "";
//		try {
//			PackageInfo packageInfo = pm.getPackageInfo(packageName, 0);
//			CharSequence appName = pm.getApplicationLabel(packageInfo.applicationInfo);
//			result = appName.toString();
//		} catch (Exception e) {
//		}
//		return result;
//	}
//
//	/**
//	 * Danh sach app dang chay
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 13:51:36 26 Jun 2014
//	 * @return: List<RunningAppProcessInfo>
//	 * @throws:
//	 * @return
//	 */
//	public static List<RunningAppProcessInfo> getListAppRunning() {
//		ActivityManager am = (ActivityManager) GlobalInfo
//				.getInstance().getAppContext()
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningAppProcessInfo> procInfos = am.getRunningAppProcesses();
//		return procInfos;
//	}
//
//	public static String getListAppInstallStr() {
//		StringBuffer buffer = new StringBuffer();
//		buffer.append("APP_INSTALLED: ");
//		PackageManager pm = GlobalInfo.getInstance().getPackageManager();
//		List<ApplicationInfo> listApp = pm.getInstalledApplications(PackageManager.GET_META_DATA);
//		if (listApp != null) {
//			for(ApplicationInfo item : listApp){
//				boolean isSystemApp = ((item.flags & ApplicationInfo.FLAG_SYSTEM) != 0);
//				//khong lay app he thong
//				if (!isSystemApp) {
//					buffer.append(item.packageName + ";");
//				}
//			}
//		}
//		return buffer.toString();
//	}
//
//	/**
//	 * Danh sach ung dung dang chay foreground
//	 * @author: duongdt3
//	 * @since: 1.0
//	 * @time: 13:51:50 26 Jun 2014
//	 * @return: List<RunningTaskInfo>
//	 * @throws:
//	 * @param maxList
//	 * @return
//	 */
//	public static List<RunningTaskInfo> getListTaskRunning(int maxList) {
//		ActivityManager am = (ActivityManager) GlobalInfo
//				.getInstance().getAppContext()
//				.getSystemService(Context.ACTIVITY_SERVICE);
//		List<RunningTaskInfo> procInfos = am.getRunningTasks(maxList);
//		return procInfos;
//	}
//
//
//	 /**
//	 * set image resource cho imageview
//	 * @author: Tuanlt11
//	 * @param resId
//	 * @param views
//	 * @return: void
//	 * @throws:
//	*/
//	public static void setImageResource(int resId, ImageView ... views){
//		for (int i = 0; i < views.length; i++) {
//			ImageView v = views[i];
//			if (v != null) {
//				v.setImageResource(resId);
//			}
//		}
//	}
//
//	public static void startActivityOtherApp(Context con, Intent intent){
//		con.startActivity(intent);
//		//khong cam setting neu bi chan
//		AccessInternetService.unlockAppPrevent(true);
//	}
//
//	public static void startActivityOtherAppFromFragment(Fragment fragment, Intent intent){
//		fragment.startActivity(intent);
//		//khong cam setting neu bi chan
//		AccessInternetService.unlockAppPrevent(true);
//	}
//
//	public static void startActivityForResultFromFragment(Fragment fragment, Intent intent, int requestCode){
//		fragment.startActivityForResult(intent, requestCode);
//		//khong cam setting neu bi chan
//		AccessInternetService.unlockAppPrevent(true);
//	}
//
//	public static void startActivityForResultFromActivity(Activity activity, Intent intent, int requestCode){
//		activity.startActivityForResult(intent, requestCode);
//		//khong cam setting neu bi chan
//		AccessInternetService.unlockAppPrevent(true);
//	}
//
//	public static void setFilterInputNumber(EditText edEditText, int length) {
//		// TODO Auto-generated method stub
//		InputFilter[] filterArray = new InputFilter[2];
//		filterArray[0] = new InputFilter.LengthFilter(length) {
//			@Override
//			public CharSequence filter(CharSequence source, int start, int end,
//					Spanned dest, int dstart, int dend) {
//				// TODO Auto-generated method stub
//				String result = "";
//				for (int i = start; i < end; i++) {
//					if (Character.isDigit(source.charAt(i))) {
//						result = result + source.charAt(i);
//					}
//				}
//				return result;
//			}
//		};
//
//		filterArray[1] = new InputFilter.LengthFilter(length);
//		if (edEditText != null) {
//			edEditText.setInputType(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//			edEditText.setFilters(filterArray);
//		}
//
//	}
//
//	public static AlertDialog getAlertDialogAutoTime() {
//		return alertDialogAutoTime;
//	}
//
//	public static void setAlertDialogAutoTime(AlertDialog alertDialogAutoTime) {
//		GlobalUtil.alertDialogAutoTime = alertDialogAutoTime;
//	}
//
//	public static String getPathSynDataFileName() {
//		return pathSynDataFileName;
//	}
//
//	public static void setPathSynDataFileName(String pathSynDataFileName) {
//		GlobalUtil.pathSynDataFileName = pathSynDataFileName;
//	}
}
