package com.thienhaisoft.barcode.utils;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;

/**
 * Created by user on 23/05/2017.
 */

public class JsonUtils {

    private static final ObjectMapper mapper = new ObjectMapper();

    public static String toString(Object obj) {
        if (obj == null) {
            return "";
        }

        try {
            return mapper.writeValueAsString(obj);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     * example
     * String json = "{\"staffId\": 123456,\"staffCode\": \"tamvnm\",\"staffName\": \"minh tam\"}";
     * StaffEntities staff = (StaffEntities)JsonUtils.toObject(json, StaffEntities.class);
     *
     * @param json
     * @param clazz
     * @return
     */
    public static Object toObject(String json, Class clazz) {
        if (json == null || json.isEmpty()) {
            return null;
        }
        json = json.replace("\n", "");
        try {
            mapper.configure(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            return mapper.readValue(json, clazz);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

}
