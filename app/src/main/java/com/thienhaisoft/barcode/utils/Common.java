package com.thienhaisoft.barcode.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Build;
import android.os.Handler;
import android.widget.DatePicker;
import android.widget.TextView;

import com.google.gson.Gson;
import com.thienhaisoft.barcode.global.GlobalInfo;
import com.thienhaisoft.barcode.httpUtils.APICallBack;
import com.thienhaisoft.barcode.httpUtils.APIUtils;
import com.thienhaisoft.barcode.items.GiftItem;
import com.thienhaisoft.barcode.items.ProductItem;
import com.thienhaisoft.barcode.items.SamplingItem;

import org.json.JSONObject;

import java.text.Normalizer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

/**
 * Created by khoam on 24/05/2017.
 */

public class Common {
    public static void showProgressDialog(final ProgressDialog progressDialog, String message, String title) {
        String messageDialog = "Đang tải dữ liệu...";
        if (message != null && !message.equals("")) {
            messageDialog = message;
        }
        progressDialog.setMessage(messageDialog);
        if (title != null && !title.equals("")) {
            progressDialog.setTitle(title);
        }
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();
        // Set timeout for progress dialog
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        }, 30000);
    }

    public static void FinishProgressDialog(ProgressDialog progressDialog) {
        progressDialog.dismiss();
    }

    public static void showAlertDialog(
            Activity activity,
            String message,
            String title,
            String messageNegative,
            String messageNeutral,
            String messagePositive,
            final MyListener myListener
    ) {
        AlertDialog.Builder alerBuilder = new AlertDialog.Builder(activity);
        alerBuilder.setMessage(message);
        alerBuilder.setTitle(title);
        alerBuilder.setCancelable(false);
        if (messageNegative != null && !messageNegative.equals("")) {
            alerBuilder.setNegativeButton(messageNegative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    myListener.functionNegative();
                }
            });
        }
        if (messageNeutral != null && !messageNeutral.equals("")) {
            alerBuilder.setNeutralButton(messageNeutral, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    myListener.functionNeutral();
                }
            });
        }
        if (messagePositive != null && !messagePositive.equals("")) {
            alerBuilder.setPositiveButton(messagePositive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    myListener.functionPositive();
                }
            });
        }
        alerBuilder.show();
        alerBuilder.create();
    }

    public static void showAlertDialog(
            Activity activity,
            String title,
            String message,
            String messageNegative,
            final SingleListener myListener
    ) {
        AlertDialog.Builder alerBuilder = new AlertDialog.Builder(activity);
        alerBuilder.setTitle(title);
        alerBuilder.setMessage(message);
        alerBuilder.setCancelable(false);
        if (messageNegative != null && !messageNegative.equals("")) {
            alerBuilder.setNegativeButton(messageNegative, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    myListener.myFunction();
                }
            });
        }
        alerBuilder.show();
        alerBuilder.create();
    }

    public static ArrayList<ProductItem> sortProductItem(ArrayList<ProductItem> list) {
        try {
            Collections.sort(list, new Comparator<ProductItem>() {
                @Override
                public int compare(ProductItem lhs, ProductItem rhs) {
                    if (lhs.getNumber_choose() < rhs.getNumber_choose()) {
                        return 1;
                    } else {
                        if (lhs.getNumber_choose() > rhs.getNumber_choose()) {
                            return -1;
                        } else {
                            if (lhs.getQuantity() > rhs.getQuantity()) {
                                return -1;
                            } else {
                                if (lhs.getQuantity() < rhs.getQuantity()) {
                                    return 1;
                                } else {
                                    int result = (lhs.getCode() + "-" + lhs.getName()).compareToIgnoreCase(rhs.getCode() + "-" + rhs.getName());
                                    if (result > 0) {
                                        return 1;
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            });
            return list;
        } catch (Exception ex) {
            return list;
        }
    }

    public static ArrayList<ProductItem> sortProductItemStore(ArrayList<ProductItem> list) {
        try {
            Collections.sort(list, new Comparator<ProductItem>() {
                @Override
                public int compare(ProductItem lhs, ProductItem rhs) {
                    if (lhs.getNumber_choose() != 0 && rhs.getNumber_choose() != 0) {
                        if (lhs.getNumber_choose() < rhs.getNumber_choose()) {
                            return 1;
                        } else {
                            if (lhs.getNumber_choose() > rhs.getNumber_choose()) {
                                return -1;
                            } else {
                                int result = lhs.getName().compareToIgnoreCase(rhs.getName());
                                if (result > 0) {
                                    return 1;
                                } else {
                                    return 0;
                                }
                            }
                        }
                    } else {
                        if (lhs.getNumber_choose() != 0 && rhs.getNumber_choose() == 0) {
                            return -1;
                        } else {
                            if (lhs.getNumber_choose() == 0 && rhs.getNumber_choose() != 0) {
                                return 1;
                            } else {
                                int result = lhs.getName().compareToIgnoreCase(rhs.getName());
                                if (result > 0) {
                                    return 1;
                                } else {
                                    return 0;
                                }
                            }
                        }
                    }
                }
            });
            return list;
        } catch (Exception ex) {
            return list;
        }
    }

    public static ArrayList<GiftItem> sortGiftItem(ArrayList<GiftItem> list) {
        try {
            Collections.sort(list, new Comparator<GiftItem>() {
                @Override
                public int compare(GiftItem lhs, GiftItem rhs) {
                    if (lhs.getI_number_choose() < rhs.getI_number_choose()) {
                        return 1;
                    } else {
                        if (lhs.getI_number_choose() > rhs.getI_number_choose()) {
                            return -1;
                        } else {
                            if (lhs.getI_quantity() > rhs.getI_quantity()) {
                                return -1;
                            } else {
                                if (lhs.getI_quantity() < rhs.getI_quantity()) {
                                    return 1;
                                } else {
                                    int result = lhs.getStr_gift_name().compareToIgnoreCase(rhs.getStr_gift_name());
                                    if (result > 0) {
                                        return 1;
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            });
            return list;
        } catch (Exception ex) {
            return list;
        }
    }

    public static ArrayList<SamplingItem> sortSamlingItem(ArrayList<SamplingItem> list) {
        try {
            Collections.sort(list, new Comparator<SamplingItem>() {
                @Override
                public int compare(SamplingItem lhs, SamplingItem rhs) {
                    if (lhs.getNumber_choose() < rhs.getNumber_choose()) {
                        return 1;
                    } else {
                        if (lhs.getNumber_choose() > rhs.getNumber_choose()) {
                            return -1;
                        } else {
                            int lhs_quantity = Integer.parseInt(lhs.getQuantity());
                            int rhs_quantity = Integer.parseInt(rhs.getQuantity());
                            if (lhs_quantity > rhs_quantity) {
                                return -1;
                            } else {
                                if (lhs_quantity < rhs_quantity) {
                                    return 1;
                                } else {
                                    int result = lhs.getProductCodeName().compareToIgnoreCase(rhs.getProductName());
                                    if (result > 0) {
                                        return 1;
                                    } else {
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                }
            });
            return list;
        } catch (Exception ex) {
            return list;
        }
    }

    public static String deAccent(String str) {
        String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(nfdNormalizedString).replaceAll("");
    }

    public static String getModelDevice() {
        return Build.MANUFACTURER
                + " " + Build.MODEL + " " + Build.VERSION.RELEASE
                + " " + Build.VERSION_CODES.class.getFields()[android.os.Build.VERSION.SDK_INT].getName();
    }

    public static boolean sendDebugMessage(final String messErr, final Activity activity) {
        String model_device = Common.getModelDevice();

        HashMap<String, String> data = new HashMap<>();
        String url = Constants.API_REPORT_BUG;
        boolean success = true;

        GlobalInfo info = (GlobalInfo) activity.getApplicationContext();
        if (info.getStaffId() != 0) {
            String user_id = StringUtil.toString(info.getStaffId());
            data.put("userid", user_id);
            if (info.getCurStore() != null && info.getCurStore().getStore_id() != 0) {
                String store_id = StringUtil.toString(info.getCurStore().getStore_id());
                data.put("storeid", store_id);
            }
            data.put("model_app", model_device);
            data.put("log_type", "debug");
            data.put("note", messErr);

            APIUtils.LoadJSON(activity, data, url, new APICallBack() {
                @Override
                public void uiStart() {
                }

                @Override
                public void success(String successString, int type) {
                    try {
                        JSONObject result = new JSONObject(successString);
                        if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {

                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

                @Override
                public void fail(String failString) {
                    Common.showAlertDialog(activity, "Gữi thông báo lỗi không thành công", "Lỗi", "", "", "Ok", new MyListener() {
                        @Override
                        public void functionNegative() {

                        }

                        @Override
                        public void functionNeutral() {

                        }

                        @Override
                        public void functionPositive() {
                            activity.finish();
                        }
                    });
                }

                @Override
                public void uiEnd() {
                }
            });

        } else {
            success = false;
        }

        return success;
    }

    public static long getTimeFromTextview(TextView edt, String FORMAT_DATE) {
        String text = edt.getText().toString();

        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
        Date date = Calendar.getInstance().getTime();
        try {
            date = sdf.parse(text);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return date.getTime();
    }

    public static String getTextFromTime(long time, String FORMAT_DATE) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(time);

        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
        Date date = cal.getTime();

        return sdf.format(date).toString();
    }

    public static String getTextFromTime(Date date, String FORMAT_DATE) {
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
        return sdf.format(date).toString();
    }

    public static long getTimeDivision1000(long time) {
        return time/1000;
    }


    public static String getTextFromTime(Calendar cal, String FORMAT_DATE) {
        Date date = cal.getTime();
        SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
        return sdf.format(date).toString();
    }

    public static void showDatePickerDialog(Activity activity, final TextView txt, final String FORMAT_DATE) {
        DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar myCalendar = Calendar.getInstance();
                myCalendar.set(Calendar.YEAR, i);
                myCalendar.set(Calendar.MONTH, i1);
                myCalendar.set(Calendar.DAY_OF_MONTH, i2);
                SimpleDateFormat sdf = new SimpleDateFormat(FORMAT_DATE);
                String textTime = sdf.format(myCalendar.getTime());
                txt.setText(textTime);
            }
        };

        Calendar currentCalendar = Calendar.getInstance();
        DatePickerDialog datePickerDialog = new DatePickerDialog(activity, date, currentCalendar.get(Calendar.YEAR), currentCalendar.get(Calendar.MONTH), currentCalendar.get(Calendar.DAY_OF_MONTH));

        datePickerDialog.show();
    }

    public static Calendar setTimeToZero(Calendar cal) {
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        return cal;
    }

    public static boolean isSuccessAPI(JSONObject result) {
        try {
            if (result.has(Constants.RESULTAPI_STATUS) && result.getInt(Constants.RESULTAPI_STATUS) == Constants.RESULTAPI_SUCCESS) {
                if (result.has(Constants.RESULTAPI_DATA)) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            return false;
        }
    }

    public static String returnStringInstealNull(String str) {
        if (str == null || str == "null" || str.equals("") || str.equals("null") || str.isEmpty()) {
            return "0";
        } else {
            return str;
        }
    }

    public static String returnStringIfNull(String str) {
        if (str == null || str == "null" || str.equals("null") || str.isEmpty()) {
            return "";
        } else {
            return str;
        }
    }

    public static Gson getGson() {
        return new Gson();
    }

    public static HashMap<String, String> getStartEndTimeInDate() {
        HashMap<String, String> result = new HashMap<>();
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        String start_time = String.valueOf(cal.getTimeInMillis() / 1000);
        cal.set(Calendar.HOUR, 23);
        cal.set(Calendar.MINUTE, 59);
        cal.set(Calendar.SECOND, 59);
        String end_time = String.valueOf(cal.getTimeInMillis() / 1000);
        result.put("start_time", start_time);
        result.put("end_time", start_time);
        return result;
    }
}
