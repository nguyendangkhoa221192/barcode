/**
 * Copyright 2011 Viettel Telecom. All rights reserved.
 * VIETTEL PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.thienhaisoft.barcode.utils;

import android.util.Log;

import com.thienhaisoft.barcode.global.GlobalInfo;
import com.google.code.microlog4android.LoggerFactory;

/**
 *  Show log trace(inteface)
 *  @author: TamPQ
 *  @version: 1.1
 *  @since: 1.0
 */
public class MyLog{

	public static final com.google.code.microlog4android.Logger logger = LoggerFactory.getLogger();
	
	public static void e(String tag, Throwable throwable){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.e(tag, Log.getStackTraceString(throwable));
		}
	}
	
	public static void d(String tag, String msg){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.d(tag, msg);
		}
	}
	
	public static void d(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.d(tag, msg, tr);
		}
	}
	
	public static void e(String tag, String msg){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.e(tag, msg);
		}
	}
	
	public static void e(String tag, String msg, Throwable tr) {
		if(GlobalInfo.getInstance().isDebugMode){
			Log.e(tag, msg, tr);
		}
	}
	
	public static void i(String tag, String msg){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.i(tag, msg);
		}
	}
	
	public static void i(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.i(tag, msg, tr);
		}
	}	

	public static synchronized void logToFile(String title, String content) {
		if(GlobalInfo.getInstance().isDebugMode){
			logger.debug(title + " : " + content+"\r\n");
			logger.debug("-------------------------------------");
		}
		
	}
	public static synchronized void logToFileInReleaseMode(String title, String content) {
		//if(GlobalInfo.getInstance().isDebugMode){
			logger.debug(title + " : " + content+"\r\n");
			logger.debug("-------------------------------------");
		//}
	}
	
	public static void v(String tag, String msg){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.v(tag, msg);
		}
	}
	
	public static void v(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.v(tag, msg, tr);
		}
	}
	
	public static void w(String tag, String msg){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.w(tag, msg);
		}
	}
	
	public static void w(String tag, String msg, Throwable tr){
		if(GlobalInfo.getInstance().isDebugMode){
			Log.w(tag, msg, tr);
		}
	}

	public static String printStackTrace(Throwable e) {
		String report = "";
		if (e != null) {
			report = e.toString() + "\n\n";
			StackTraceElement[] arr = e.getStackTrace();
			
			report += "--------- Stack trace ---------\n\n";
			for (int i = 0; i < arr.length; i++) {
				report += "    " + arr[i].toString() + "\n";
			}
			report += "-------------------------------\n\n";
			
			// If the exception was thrown in a background thread inside
			// AsyncTask, then the actual exception can be found with getCause
			report += "--------- Cause ---------\n\n";
			Throwable cause = e.getCause();
			if (cause != null) {
				report += cause.toString() + "\n\n";
				arr = cause.getStackTrace();
				for (int i = 0; i < arr.length; i++) {
					report += "    " + arr[i].toString() + "\n";
				}
			}
			report += "-------------------------------\n\n";
		}
		return report;
	}
}
