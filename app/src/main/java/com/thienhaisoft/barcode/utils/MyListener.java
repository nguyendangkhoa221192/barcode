package com.thienhaisoft.barcode.utils;

/**
 * Created by khoam on 24/05/2017.
 */

public interface MyListener {
    /**
     * Function for button Negative
     * @author Khoand
     */
    public void functionNegative();

    /**
     * Function for button Neutral
     * @author Khoand
     */
    public void functionNeutral();

    /**
     * Function for button Positive
     * @author Khoand
     */
    public void functionPositive();
}
