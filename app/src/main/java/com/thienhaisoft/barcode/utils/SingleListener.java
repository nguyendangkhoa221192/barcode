package com.thienhaisoft.barcode.utils;

/**
 * Created by khoam on 3/15/2018.
 */

public interface SingleListener {
    /**
     * My function
     * @author khoam
     */
    public void myFunction();
}
