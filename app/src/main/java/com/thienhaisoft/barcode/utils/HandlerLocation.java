package com.thienhaisoft.barcode.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.widget.Toast;

import com.thienhaisoft.barcode.R;
import com.thienhaisoft.barcode.httpUtils.APICallBack;

/**
 * Created by user on 25/05/2017.
 */

public class HandlerLocation implements APICallBack {

//    private Context context;
    private View view;

    private ProgressDialog progressDialog;

    boolean isSuccess = false;

    public HandlerLocation(View view, ProgressDialog progressDialog) {
        this.progressDialog = progressDialog;
        this.view = view;
    }

    @Override
    public void uiStart() {
        Common.showProgressDialog(progressDialog, "Đang gửi thông tin checkin, vui lòng chờ...", "Thông báo");
    }

    @Override
    public void success(String successString, int type) {
        Snackbar.make(view, R.string.UPDATED_LOCATION, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    @Override
    public void fail(String failString) {
        Snackbar.make(view, R.string.UPDATE_LOCATION_FAIL, Snackbar.LENGTH_SHORT)
                .setAction("Action", null).show();
    }

    @Override
    public void uiEnd() {
        Common.FinishProgressDialog(progressDialog);
    }
}
