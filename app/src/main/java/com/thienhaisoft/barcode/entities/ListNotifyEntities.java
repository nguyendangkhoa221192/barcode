package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.NotifyEntities;

import java.util.List;

/**
 * Created by user on 26/06/2017.
 */

public class ListNotifyEntities {
    private int status;
    private List<NotifyEntities> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<NotifyEntities> getData() {
        return data;
    }

    public void setData(List<NotifyEntities> data) {
        this.data = data;
    }
}
