package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.StoreGiftEntities;

import java.util.List;

/**
 * Created by tamvnm on 17/09/2017.
 */

public class ListStoreGiftEntities {
    private int status;
    private List<StoreGiftEntities> data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<StoreGiftEntities> getData() {
        return data;
    }

    public void setData(List<StoreGiftEntities> data) {
        this.data = data;
    }
}
