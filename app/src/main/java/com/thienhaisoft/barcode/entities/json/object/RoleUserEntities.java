package com.thienhaisoft.barcode.entities.json.object;

/**
 * Created by user on 04/06/2017.
 */

public class RoleUserEntities {


    private long id;
    private String username;
    private String full_name;
    private String auth_key;
    private String password_hash;
    private String password_reset_token;
    private String email;
    private String role;
    private int status;
    private long created_at;
    private long updated_at;
    private short is_online;
    private long shop_id;
    private int distance;
    private String listshop;
    private String listarea;

    public String getListarea() {
        return listarea;
    }

    public void setListarea(String listarea) {
        this.listarea = listarea;
    }

    public int getDistance() {
        return distance;
    }

    public void setDistance(int distance) {
        this.distance = distance;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getPassword_hash() {
        return password_hash;
    }

    public void setPassword_hash(String password_hash) {
        this.password_hash = password_hash;
    }

    public String getPassword_reset_token() {
        return password_reset_token;
    }

    public void setPassword_reset_token(String password_reset_token) {
        this.password_reset_token = password_reset_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getCreated_at() {
        return created_at;
    }

    public void setCreated_at(long created_at) {
        this.created_at = created_at;
    }

    public long getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(long updated_at) {
        this.updated_at = updated_at;
    }

    public short getIs_online() {
        return is_online;
    }

    public void setIs_online(short is_online) {
        this.is_online = is_online;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }


    public String getListshop() {
        return listshop;
    }

    public void setListshop(String listshop) {
        this.listshop = listshop;
    }
}


