package com.thienhaisoft.barcode.entities.json.object;

/**
 * Created by fbiminhtam on 09/11/2017.
 */

public class VersionEntities {

    private String id;
    private String change_note;
    private String current_version;
    private String force_download;
    private String major;
    private String minor;
    private String create_date;
    private String role_type;
    private String url_guide;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getChange_note() {
        return change_note;
    }

    public void setChange_note(String change_note) {
        this.change_note = change_note;
    }

    public String getCurrent_version() {
        return current_version;
    }

    public void setCurrent_version(String current_version) {
        this.current_version = current_version;
    }

    public String getForce_download() {
        return force_download;
    }

    public void setForce_download(String force_download) {
        this.force_download = force_download;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getMinor() {
        return minor;
    }

    public void setMinor(String minor) {
        this.minor = minor;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public String getRole_type() {
        return role_type;
    }

    public void setRole_type(String role_type) {
        this.role_type = role_type;
    }

    public String getUrl_guide() {
        return url_guide;
    }

    public void setUrl_guide(String url_guide) {
        this.url_guide = url_guide;
    }
}
