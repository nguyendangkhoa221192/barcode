package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.StoreEntities;

import java.io.Serializable;
import java.util.List;

/**
 * Created by user on 11/06/2017.
 */

public class ListStoreEntities implements Serializable{
    private int status;
    private List<StoreEntities> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }


    public List<StoreEntities> getData() {
        return data;
    }

    public void setData(List<StoreEntities> data) {
        this.data = data;
    }
}
