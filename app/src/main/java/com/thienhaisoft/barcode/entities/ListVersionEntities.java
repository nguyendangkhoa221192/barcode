package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.VersionEntities;

import java.util.List;

/**
 * Created by fbiminhtam on 09/11/2017.
 */

public class ListVersionEntities {
    private int status;
    private List<VersionEntities> data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<VersionEntities> getData() {
        return data;
    }

    public void setData(List<VersionEntities> data) {
        this.data = data;
    }
}
