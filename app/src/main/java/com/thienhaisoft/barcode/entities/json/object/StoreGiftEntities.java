package com.thienhaisoft.barcode.entities.json.object;

/**
 * Created by tamvnm on 17/09/2017.
 */

public class StoreGiftEntities {
    private String name;
    private long quantity;
    private long available_quantity;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public long getAvailable_quantity() {
        return available_quantity;
    }

    public void setAvailable_quantity(long available_quantity) {
        this.available_quantity = available_quantity;
    }
}
