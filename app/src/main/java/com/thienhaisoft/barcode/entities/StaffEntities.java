package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.RoleUserEntities;

/**
 * Created by user on 23/05/2017.
 */

public class StaffEntities {

    private int status;
    private RoleUserEntities role;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public RoleUserEntities getRole() {
        return role;
    }

    public void setRole(RoleUserEntities role) {
        this.role = role;
    }
}
