package com.thienhaisoft.barcode.entities.json.object;

/**
 * Created by user on 26/06/2017.
 */

public class NotifyEntities {

    private String title;
    private String description;
    private String start_date;
    private String end_date;
    private String shop_code;
    private String shop_name;
    private long shop_type_id;
    private String mien_code;
    private String vung_code;
    private String team_code;
    private String team_name;
    private long user_id;
    private String na_code;
    private String na_name;
    private long id_store;
    private String store_code;
    private String store_name;
    private double lat;
    private double lng;
    private String address;
    private String phone;
    private long shop_id;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getShop_code() {
        return shop_code;
    }

    public void setShop_code(String shop_code) {
        this.shop_code = shop_code;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public long getShop_type_id() {
        return shop_type_id;
    }

    public void setShop_type_id(long shop_type_id) {
        this.shop_type_id = shop_type_id;
    }

    public String getMien_code() {
        return mien_code;
    }

    public void setMien_code(String mien_code) {
        this.mien_code = mien_code;
    }

    public String getVung_code() {
        return vung_code;
    }

    public void setVung_code(String vung_code) {
        this.vung_code = vung_code;
    }

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getNa_code() {
        return na_code;
    }

    public void setNa_code(String na_code) {
        this.na_code = na_code;
    }

    public String getNa_name() {
        return na_name;
    }

    public void setNa_name(String na_name) {
        this.na_name = na_name;
    }

    public long getId_store() {
        return id_store;
    }

    public void setId_store(long id_store) {
        this.id_store = id_store;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public long getShop_id() {
        return shop_id;
    }

    public void setShop_id(long shop_id) {
        this.shop_id = shop_id;
    }
}
