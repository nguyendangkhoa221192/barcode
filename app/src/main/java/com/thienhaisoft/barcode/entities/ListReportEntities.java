package com.thienhaisoft.barcode.entities;

import com.thienhaisoft.barcode.entities.json.object.ReportEntities;

import java.util.List;

/**
 * Created by fbiminhtam on 09/07/2017.
 */

public class ListReportEntities {
    private int status;
    private List<ReportEntities> data;


    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<ReportEntities> getData() {
        return data;
    }

    public void setData(List<ReportEntities> data) {
        this.data = data;
    }
}
