package com.thienhaisoft.barcode.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by khoam on 3/15/2018.
 */

public class SamplingItem {
    @SerializedName("userid")
    @Expose
    private String userid;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("tondauthang")
    @Expose
    private String tondauthang;
    @SerializedName("product_code_name")
    @Expose
    private String productCodeName;
    @SerializedName("quantity")
    @Expose
    private String quantity;

    private int number_choose = 0;

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getTondauthang() {
        return tondauthang;
    }

    public void setTondauthang(String tondauthang) {
        this.tondauthang = tondauthang;
    }

    public String getProductCodeName() {
        return productCodeName;
    }

    public void setProductCodeName(String productCodeName) {
        this.productCodeName = productCodeName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public int getNumber_choose() {
        return number_choose;
    }

    public void setNumber_choose(int number_choose) {
        this.number_choose = number_choose;
    }
}
