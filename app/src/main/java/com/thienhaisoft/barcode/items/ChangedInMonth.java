package com.thienhaisoft.barcode.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ChangedInMonth {
    @SerializedName("ytv")
    @Expose
    private String ytv;
    @SerializedName("dailynest")
    @Expose
    private String dailynest;
    @SerializedName("knp")
    @Expose
    private String knp;
    @SerializedName("kns")
    @Expose
    private String kns;
    @SerializedName("honhop")
    @Expose
    private String honhop;
    @SerializedName("yento")
    @Expose
    private String yento;
    @SerializedName("justnest")
    @Expose
    private String justnest;
    @SerializedName("vitalgin")
    @Expose
    private String vitalgin;
    @SerializedName("quatang")
    @Expose
    private String quatang;
    @SerializedName("gift")
    @Expose
    private String gift;
    @SerializedName("ytv_lotang")
    @Expose
    private String ytvLotang;
    @SerializedName("dln_lotang")
    @Expose
    private String dlnLotang;
    @SerializedName("knp_lotang")
    @Expose
    private String knpLotang;
    @SerializedName("jn_lotang")
    @Expose
    private String jnLotang;
    @SerializedName("kns_lotang")
    @Expose
    private String knsLotang;

    public String getYtv() {
        return ytv;
    }

    public void setYtv(String ytv) {
        this.ytv = ytv;
    }

    public String getDailynest() {
        return dailynest;
    }

    public void setDailynest(String dailynest) {
        this.dailynest = dailynest;
    }

    public String getKnp() {
        return knp;
    }

    public void setKnp(String knp) {
        this.knp = knp;
    }

    public String getKns() {
        return kns;
    }

    public void setKns(String kns) {
        this.kns = kns;
    }

    public String getHonhop() {
        return honhop;
    }

    public void setHonhop(String honhop) {
        this.honhop = honhop;
    }

    public String getYento() {
        return yento;
    }

    public void setYento(String yento) {
        this.yento = yento;
    }

    public String getJustnest() {
        return justnest;
    }

    public void setJustnest(String justnest) {
        this.justnest = justnest;
    }

    public String getVitalgin() {
        return vitalgin;
    }

    public void setVitalgin(String vitalgin) {
        this.vitalgin = vitalgin;
    }

    public String getQuatang() {
        return quatang;
    }

    public void setQuatang(String quatang) {
        this.quatang = quatang;
    }

    public String getGift() {
        return gift;
    }

    public void setGift(String gift) {
        this.gift = gift;
    }

    public String getYtvLotang() {
        return ytvLotang;
    }

    public void setYtvLotang(String ytvLotang) {
        this.ytvLotang = ytvLotang;
    }

    public String getDlnLotang() {
        return dlnLotang;
    }

    public void setDlnLotang(String dlnLotang) {
        this.dlnLotang = dlnLotang;
    }

    public String getKnpLotang() {
        return knpLotang;
    }

    public void setKnpLotang(String knpLotang) {
        this.knpLotang = knpLotang;
    }

    public String getJnLotang() {
        return jnLotang;
    }

    public void setJnLotang(String jnLotang) {
        this.jnLotang = jnLotang;
    }

    public String getKnsLotang() {
        return knsLotang;
    }

    public void setKnsLotang(String knsLotang) {
        this.knsLotang = knsLotang;
    }
}
