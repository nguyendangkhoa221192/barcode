package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by khoam on 03/07/2017.
 */

public class InforCustomerByUser {


    @SerializedName("mien_id")
    @Expose
    private String mienId;
    @SerializedName("mien_code")
    @Expose
    private String mienCode;
    @SerializedName("vung_id")
    @Expose
    private String vungId;
    @SerializedName("vung_code")
    @Expose
    private String vungCode;
    @SerializedName("shop_id")
    @Expose
    private String shopId;
    @SerializedName("shop_code")
    @Expose
    private String shopCode;
    @SerializedName("shop_name")
    @Expose
    private String shopName;
    @SerializedName("team_code")
    @Expose
    private String teamCode;
    @SerializedName("team_name")
    @Expose
    private String teamName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("na_code")
    @Expose
    private String naCode;
    @SerializedName("na_name")
    @Expose
    private String naName;
    @SerializedName("channel_code")
    @Expose
    private String channelCode;
    @SerializedName("parent_channel_code")
    @Expose
    private String parentChannelCode;
    @SerializedName("store_code")
    @Expose
    private String storeCode;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("customer_id")
    @Expose
    private String customerId;
    @SerializedName("HoLot")
    @Expose
    private String hoLot;
    @SerializedName("Ten")
    @Expose
    private String ten;
    @SerializedName("DTDD")
    @Expose
    private String dTDD;
    @SerializedName("DTBan")
    @Expose
    private Object dTBan;
    @SerializedName("DiaChi")
    @Expose
    private String diaChi;
    @SerializedName("NgayTao")
    @Expose
    private String ngayTao;
    @SerializedName("pointsproduct")
    @Expose
    private String pointsproduct;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("Gender")
    @Expose
    private String gender;
    @SerializedName("Email")
    @Expose
    private Object email;
    @SerializedName("sn")
    @Expose
    private String sn;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("isactive")
    @Expose
    private String isactive;

    public String getMienId() {
        return mienId;
    }

    public void setMienId(String mienId) {
        this.mienId = mienId;
    }

    public String getMienCode() {
        return mienCode;
    }

    public void setMienCode(String mienCode) {
        this.mienCode = mienCode;
    }

    public String getVungId() {
        return vungId;
    }

    public void setVungId(String vungId) {
        this.vungId = vungId;
    }

    public String getVungCode() {
        return vungCode;
    }

    public void setVungCode(String vungCode) {
        this.vungCode = vungCode;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopCode() {
        return shopCode;
    }

    public void setShopCode(String shopCode) {
        this.shopCode = shopCode;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getTeamCode() {
        return teamCode;
    }

    public void setTeamCode(String teamCode) {
        this.teamCode = teamCode;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getNaCode() {
        return naCode;
    }

    public void setNaCode(String naCode) {
        this.naCode = naCode;
    }

    public String getNaName() {
        return naName;
    }

    public void setNaName(String naName) {
        this.naName = naName;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public String getParentChannelCode() {
        return parentChannelCode;
    }

    public void setParentChannelCode(String parentChannelCode) {
        this.parentChannelCode = parentChannelCode;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getHoLot() {
        return hoLot;
    }

    public void setHoLot(String hoLot) {
        this.hoLot = hoLot;
    }

    public String getTen() {
        return ten;
    }

    public void setTen(String ten) {
        this.ten = ten;
    }

    public String getDTDD() {
        return dTDD;
    }

    public void setDTDD(String dTDD) {
        this.dTDD = dTDD;
    }

    public Object getDTBan() {
        return dTBan;
    }

    public void setDTBan(Object dTBan) {
        this.dTBan = dTBan;
    }

    public String getDiaChi() {
        return diaChi;
    }

    public void setDiaChi(String diaChi) {
        this.diaChi = diaChi;
    }

    public String getNgayTao() {
        return ngayTao;
    }

    public void setNgayTao(String ngayTao) {
        this.ngayTao = ngayTao;
    }

    public String getPointsproduct() {
        return pointsproduct;
    }

    public void setPointsproduct(String pointsproduct) {
        this.pointsproduct = pointsproduct;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public Object getEmail() {
        return email;
    }

    public void setEmail(Object email) {
        this.email = email;
    }

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIsactive() {
        return isactive;
    }

    public void setIsactive(String isactive) {
        this.isactive = isactive;
    }
}
