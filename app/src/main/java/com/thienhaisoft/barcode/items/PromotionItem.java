package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by khoam on 8/16/2017.
 */

public class PromotionItem implements Serializable{
    private String id_order = "";
    private int soluongdamua = 0;
    private String promotion_program_name = "";
    private String from_date = "";
    private String to_date = "";
    private String product_id = "";
    private String product_name = "";
    private int sale_qtty = 0;
    private int free_qtty = 0;
    private String free_product_id = "";
    private String free_product_name = "";
    private int number_product_changed = 0;
    private int soluongtonkho = 0;
    private int promotion_canget = 0;
    private int number_choose = 0;

    public int getNumber_product_changed() {
        return number_product_changed;
    }

    public void setNumber_product_changed(int number_produst_changed) {
        this.number_product_changed = number_produst_changed;
    }

    public int getNumber_choose() {
        return number_choose;
    }

    public void setNumber_choose(int number_choose) {
        this.number_choose = number_choose;
    }

    public int getSoluongdamua() {
        return soluongdamua;
    }

    public int getSale_qtty() {
        return sale_qtty;
    }

    public int getFree_qtty() {
        return free_qtty;
    }

    public void setSoluongdamua(int soluongdamua) {
        this.soluongdamua = soluongdamua;
    }

    public void setSale_qtty(int sale_qtty) {
        this.sale_qtty = sale_qtty;
    }

    public void setFree_qtty(int free_qtty) {
        this.free_qtty = free_qtty;
    }

    public int getPromotion_canget() {
        return promotion_canget;
    }

    public void setPromotion_canget(int promotion_canget) {
        this.promotion_canget = promotion_canget;
    }

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getPromotion_program_name() {
        return promotion_program_name;
    }

    public void setPromotion_program_name(String promotion_program_name) {
        this.promotion_program_name = promotion_program_name;
    }

    public String getFrom_date() {
        return from_date;
    }

    public void setFrom_date(String from_date) {
        this.from_date = from_date;
    }

    public String getTo_date() {
        return to_date;
    }

    public void setTo_date(String to_date) {
        this.to_date = to_date;
    }

    public String getProduct_id() {
        return product_id;
    }

    public void setProduct_id(String product_id) {
        this.product_id = product_id;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getFree_product_id() {
        return free_product_id;
    }

    public void setFree_product_id(String free_product_id) {
        this.free_product_id = free_product_id;
    }

    public String getFree_product_name() {
        return free_product_name;
    }

    public void setFree_product_name(String free_product_name) {
        this.free_product_name = free_product_name;
    }

    public int getSoluongtonkho() {
        return soluongtonkho;
    }

    public void setSoluongtonkho(int soluongtonkho) {
        this.soluongtonkho = soluongtonkho;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has("id_order")) {
                setId_order(jsonObject.getString("id_order"));
            }
            if (jsonObject.has("soluongdamua")) {
                setSoluongdamua(jsonObject.getInt("soluongdamua"));
            }
            if (jsonObject.has("promotion_program_name")) {
                setPromotion_program_name(jsonObject.getString("promotion_program_name"));
            }
            if (jsonObject.has("from_date")) {
                setFrom_date(jsonObject.getString("from_date"));
            }
            if (jsonObject.has("to_date")) {
                setTo_date(jsonObject.getString("to_date"));
            }
            if (jsonObject.has("product_id")) {
                setProduct_id(jsonObject.getString("product_id"));
            }
            if (jsonObject.has("product_name")) {
                setProduct_name(jsonObject.getString("product_name"));
            }
            if (jsonObject.has("sale_qtty")) {
                setSale_qtty(jsonObject.getInt("sale_qtty"));
            }
            if (jsonObject.has("free_qtty")) {
                setFree_qtty(jsonObject.getInt("free_qtty"));
            }
            if (jsonObject.has("free_product_id")) {
                setFree_product_id(jsonObject.getString("free_product_id"));
            }
            if (jsonObject.has("free_product_name")) {
                setFree_product_name(jsonObject.getString("free_product_name"));
            }
            if (jsonObject.has("soluongtonkho")) {
                setSoluongtonkho(jsonObject.getInt("soluongtonkho"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void hanldedPromotion(int total_can_get) {
        int number = (total_can_get * getFree_qtty()) / getSale_qtty();
        setPromotion_canget(number);
    }
}
