package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by khoam on 5/28/2017.
 */

public class GiftItem implements Serializable{
    private int i_Pid = 0;
    private String code = "";
    private String str_customer_name = "";
    private double dou_totalpoint = 0.0;
    private int i_quantity = 0;
    private String str_gift_name = "";
    private int i_store_id = 0;
    private int i_product_id = 0;
    private double dou_point_to_product = 0.0;
    private int i_number_choose = 0;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getI_number_choose() {
        return i_number_choose;
    }

    public void setI_number_choose(int i_number_choose) {
        this.i_number_choose = i_number_choose;
    }

    public int getI_Pid() {
        return i_Pid;
    }

    public void setI_Pid(int i_Pid) {
        this.i_Pid = i_Pid;
    }

    public String getStr_customer_name() {
        return str_customer_name;
    }

    public void setStr_customer_name(String str_customer_name) {
        this.str_customer_name = str_customer_name;
    }

    public double getDou_totalpoint() {
        return dou_totalpoint;
    }

    public void setDou_totalpoint(double dou_totalpoint) {
        this.dou_totalpoint = dou_totalpoint;
    }

    public int getI_quantity() {
        return i_quantity;
    }

    public void setI_quantity(int i_quantity) {
        this.i_quantity = i_quantity;
    }

    public String getStr_gift_name() {
        return str_gift_name;
    }

    public void setStr_gift_name(String str_gift_name) {
        this.str_gift_name = str_gift_name;
    }

    public int getI_store_id() {
        return i_store_id;
    }

    public void setI_store_id(int i_store_id) {
        this.i_store_id = i_store_id;
    }

    public int getI_product_id() {
        return i_product_id;
    }

    public void setI_product_id(int i_product_id) {
        this.i_product_id = i_product_id;
    }

    public double getDou_point_to_product() {
        return dou_point_to_product;
    }

    public void setDou_point_to_product(double dou_point_to_product) {
        this.dou_point_to_product = dou_point_to_product;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIF_PID)) {
                setI_Pid(jsonObject.getInt(Constants.RESULTAPI_LIST_GIF_PID));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_CUSTOMER_NAME)) {
                setStr_customer_name(jsonObject.getString(Constants.RESULTAPI_LIST_GIFT_CUSTOMER_NAME));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_TOTALPOINT)) {
                setDou_totalpoint(jsonObject.getDouble(Constants.RESULTAPI_LIST_GIFT_TOTALPOINT));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_QUANTITY)) {
                setI_quantity(jsonObject.getInt(Constants.RESULTAPI_LIST_GIFT_QUANTITY));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_NAME)) {
                setStr_gift_name(jsonObject.getString(Constants.RESULTAPI_LIST_GIFT_NAME));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_STORE_ID)) {
                setI_store_id(jsonObject.getInt(Constants.RESULTAPI_LIST_GIFT_STORE_ID));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_PRODUCT_ID)) {
                setI_product_id(jsonObject.getInt(Constants.RESULTAPI_LIST_GIFT_PRODUCT_ID));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_GIFT_POINT_TO_PRODUCT)) {
                setDou_point_to_product(jsonObject.getDouble(Constants.RESULTAPI_LIST_GIFT_POINT_TO_PRODUCT));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_CODE)) {
                this.setCode(jsonObject.getString(Constants.RESULTAPI_LIST_PRODUCT_CODE));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
