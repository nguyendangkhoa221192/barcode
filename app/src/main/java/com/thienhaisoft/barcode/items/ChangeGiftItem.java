package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;

/**
 * Created by khoam on 19/05/2017.
 */

public class ChangeGiftItem {
    private String mien_code = "";
    private String parent_channel_code = "";
    private String channel_code = "";
    private String team_code = "";
    private String team_name = "";
    private String Ten = "";
    private String name = "";
    private String point = "";
    private String quantity = "";
    private String create_date = "";

    public String getMien_code() {
        return mien_code;
    }

    public void setMien_code(String mien_code) {
        this.mien_code = mien_code;
    }

    public String getParent_channel_code() {
        return parent_channel_code;
    }

    public void setParent_channel_code(String parent_channel_code) {
        this.parent_channel_code = parent_channel_code;
    }

    public String getChannel_code() {
        return channel_code;
    }

    public void setChannel_code(String channel_code) {
        this.channel_code = channel_code;
    }

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreate_date() {
        return create_date;
    }

    public void setCreate_date(String create_date) {
        this.create_date = create_date;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_MIEN_CODE)) {
                setMien_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_MIEN_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_PARENT_CHANNEL_CODE)) {
                setParent_channel_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_PARENT_CHANNEL_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_CHANNEL_CODE)) {
                setChannel_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_CHANNEL_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_TEAM_CODE)) {
                setTeam_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_TEAM_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_TEAM_NAME)) {
                setTeam_name(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_TEAM_NAME));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_NAME)) {
                setTen(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_NAME));
            }
            if (jsonObject.has("point")) {
                setPoint(jsonObject.getString("point"));
            }
            if (jsonObject.has("create_date")) {
                setCreate_date(jsonObject.getString("create_date"));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_QUANTITY)) {
                setQuantity(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_QUANTITY));
            }
            if (jsonObject.has("name")) {
                setName(jsonObject.getString("name"));
            }
        } catch (Exception ex) {

        }
    }
}
