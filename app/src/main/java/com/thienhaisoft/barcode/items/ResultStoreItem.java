package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

/**
 * Created by khoam on 4/28/2018.
 */

public class ResultStoreItem {
    private int dailynest = 0;
    private int lodungthu = 0;
    private int YenTo = 0;
    private int lokid = 0;
    private int lojustnest = 0;
    private int ytv = 0;
    private int kidnest = 0;
    private int JustNest = 0;
    private int id_store = 0;
    private int tonglo = 0;
    private int lotangytv = 0;
    private int lockid = 0;
    private int lodailynest = 0;

    public int getDailynest() {
        return dailynest;
    }

    public void setDailynest(int dailynest) {
        this.dailynest = dailynest;
    }

    public int getLodungthu() {
        return lodungthu;
    }

    public void setLodungthu(int lodungthu) {
        this.lodungthu = lodungthu;
    }

    public int getYenTo() {
        return YenTo;
    }

    public void setYenTo(int yenTo) {
        YenTo = yenTo;
    }

    public int getLokid() {
        return lokid;
    }

    public void setLokid(int lokid) {
        this.lokid = lokid;
    }

    public int getLojustnest() {
        return lojustnest;
    }

    public void setLojustnest(int lojustnest) {
        this.lojustnest = lojustnest;
    }

    public int getYtv() {
        return ytv;
    }

    public void setYtv(int ytv) {
        this.ytv = ytv;
    }

    public int getKidnest() {
        return kidnest;
    }

    public void setKidnest(int kidnest) {
        this.kidnest = kidnest;
    }

    public int getJustNest() {
        return JustNest;
    }

    public void setJustNest(int justNest) {
        JustNest = justNest;
    }

    public int getId_store() {
        return id_store;
    }

    public void setId_store(int id_store) {
        this.id_store = id_store;
    }

    public int getTonglo() {
        return tonglo;
    }

    public void setTonglo(int tonglo) {
        this.tonglo = tonglo;
    }

    public int getLotangytv() {
        return lotangytv;
    }

    public void setLotangytv(int lotangytv) {
        this.lotangytv = lotangytv;
    }

    public int getLockid() {
        return lockid;
    }

    public void setLockid(int lockid) {
        this.lockid = lockid;
    }

    public int getLodailynest() {
        return lodailynest;
    }

    public void setLodailynest(int lodailynest) {
        this.lodailynest = lodailynest;
    }

    public void partJSON(JSONObject object) {
        try {
            if (object.has("dailynest")) {
                setDailynest(object.getInt("dailynest"));
            }
            if (object.has("lodungthu")) {
                setLodungthu(object.getInt("lodungthu"));
            }
            if (object.has("YenTo")) {
                setYenTo(object.getInt("YenTo"));
            }
            if (object.has("lokid")) {
                setLokid(object.getInt("lokid"));
            }
            if (object.has("lojustnest")) {
                setLojustnest(object.getInt("lojustnest"));
            }
            if (object.has("ytv")) {
                setYtv(object.getInt("ytv"));
            }
            if (object.has("kidnest")) {
                setKidnest(object.getInt("kidnest"));
            }
            if (object.has("JustNest")) {
                setJustNest(object.getInt("JustNest"));
            }
            if (object.has("id_store")) {
                setId_store(object.getInt("id_store"));
            }
            if (object.has("tonglo")) {
                setTonglo(object.getInt("tonglo"));
            }
            if (object.has("lotangytv")) {
                setLotangytv(object.getInt("lotangytv"));
            }
            if (object.has("lockid")) {
                setLockid(object.getInt("lockid"));
            }
            if (object.has("lodailynest")) {
                setLodailynest(object.getInt("lodailynest"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
