package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

/**
 * Created by khoam on 10/31/2017.
 */

public class StoreGiftItem {
    private String user_store = "";
    private String code = "";
    private String name = "";
    private String TONDAU = "";
    private String doiqua = "";
    private String nhapqua = "";
    private String DC_TANG = "";
    private String DC_GIAM = "";
    private String cur_quantity = "";
    private String diff_value = "";

    public String getUser_store() {
        return user_store;
    }

    public void setUser_store(String user_store) {
        this.user_store = user_store;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTONDAU() {
        return TONDAU;
    }

    public void setTONDAU(String TONDAU) {
        this.TONDAU = TONDAU;
    }

    public String getDoiqua() {
        return doiqua;
    }

    public void setDoiqua(String doiqua) {
        this.doiqua = doiqua;
    }

    public String getNhapqua() {
        return nhapqua;
    }

    public void setNhapqua(String nhapqua) {
        this.nhapqua = nhapqua;
    }

    public String getDC_TANG() {
        return DC_TANG;
    }

    public void setDC_TANG(String DC_TANG) {
        this.DC_TANG = DC_TANG;
    }

    public String getDC_GIAM() {
        return DC_GIAM;
    }

    public void setDC_GIAM(String DC_GIAM) {
        this.DC_GIAM = DC_GIAM;
    }

    public String getCur_quantity() {
        return cur_quantity;
    }

    public void setCur_quantity(String cur_quantity) {
        this.cur_quantity = cur_quantity;
    }

    public String getDiff_value() {
        return diff_value;
    }

    public void setDiff_value(String diff_value) {
        this.diff_value = diff_value;
    }

    public void partJSON(JSONObject object) {
        try {
            if (object.has("user_store")) {
                setUser_store(object.getString("user_store"));
            }
            if (object.has("code")) {
                setCode(object.getString("code"));
            }
            if (object.has("name")) {
                setName(object.getString("name"));
            }
            if (object.has("TONDAU")) {
                setTONDAU(object.getString("TONDAU"));
            }
            if (object.has("doiqua")) {
                setDoiqua(object.getString("doiqua"));
            }
            if (object.has("nhapqua")) {
                setNhapqua(object.getString("nhapqua"));
            }
            if (object.has("DC_TANG")) {
                setDC_TANG(object.getString("DC_TANG"));
            }
            if (object.has("DC_GIAM")) {
                setDC_GIAM(object.getString("DC_GIAM"));
            }
            if (object.has("diff_value")) {
                setDiff_value(object.getString("diff_value"));
            }
            if (object.has("cur_quantity")) {
                setCur_quantity(object.getString("cur_quantity"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
