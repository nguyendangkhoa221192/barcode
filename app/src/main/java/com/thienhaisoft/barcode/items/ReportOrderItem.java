package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

/**
 * Created by khoam on 10/23/2017.
 */

public class ReportOrderItem {
    private String id_order = "";
    private String mien_code = "";
    private String vung_code = "";
    private String shop_code = "";
    private String shop_name = "";
    private String store_code = "";
    private String store_name = "";
    private String Ten = "";
    private String DTDD = "";
    private String product_code = "";
    private String product_name = "";
    private String quantity = "";
    private String amount = "";
    private String point = "";
    private String order_date = "";
    private String product_type = "";

    public String getId_order() {
        return id_order;
    }

    public void setId_order(String id_order) {
        this.id_order = id_order;
    }

    public String getMien_code() {
        return mien_code;
    }

    public void setMien_code(String mien_code) {
        this.mien_code = mien_code;
    }

    public String getVung_code() {
        return vung_code;
    }

    public void setVung_code(String vung_code) {
        this.vung_code = vung_code;
    }

    public String getShop_code() {
        return shop_code;
    }

    public void setShop_code(String shop_code) {
        this.shop_code = shop_code;
    }

    public String getShop_name() {
        return shop_name;
    }

    public void setShop_name(String shop_name) {
        this.shop_name = shop_name;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public String getDTDD() {
        return DTDD;
    }

    public void setDTDD(String DTDD) {
        this.DTDD = DTDD;
    }

    public String getProduct_code() {
        return product_code;
    }

    public void setProduct_code(String product_code) {
        this.product_code = product_code;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getPoint() {
        return point;
    }

    public void setPoint(String point) {
        this.point = point;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getProduct_type() {
        return product_type;
    }

    public void setProduct_type(String product_type) {
        this.product_type = product_type;
    }

    public String getDateFromData() {
        String data = getOrder_date();
        String date = data.split(" ")[0];
        return date;
    }

    public void partJSON(JSONObject object) {
        try {
            if (object.has("id_order")) {
                setId_order(object.getString("id_order"));
            }
            if (object.has("mien_code")) {
                setMien_code(object.getString("mien_code"));
            }
            if (object.has("shop_code")) {
                setShop_code(object.getString("shop_code"));
            }
            if (object.has("shop_name")) {
                setShop_name(object.getString("shop_name"));
            }
            if (object.has("store_code")) {
                setStore_code(object.getString("store_code"));
            }
            if (object.has("store_name")) {
                setStore_name(object.getString("store_name"));
            }
            if (object.has("Ten")) {
                setTen(object.getString("Ten"));
            }
            if (object.has("DTDD")) {
                setDTDD(object.getString("DTDD"));
            }
            if (object.has("product_code")) {
                setProduct_code(object.getString("product_code"));
            }
            if (object.has("product_name")) {
                setProduct_name(object.getString("product_name"));
            }
            if (object.has("quantity")) {
                setQuantity(object.getString("quantity"));
            }
            if (object.has("amount")) {
                setAmount(object.getString("amount"));
            }
            if (object.has("point")) {
                setPoint(object.getString("point"));
            }
            if (object.has("order_date")) {
                setOrder_date(object.getString("order_date"));
            }
            if (object.has("product_type")) {
                setProduct_type(object.getString("product_type"));
            }
        } catch (Exception $ex) {
            $ex.printStackTrace();
        }
    }
}
