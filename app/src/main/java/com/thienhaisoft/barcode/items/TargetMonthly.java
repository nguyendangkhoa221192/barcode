package com.thienhaisoft.barcode.items;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TargetMonthly {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("store_id")
    @Expose
    private String storeId;
    @SerializedName("store_code")
    @Expose
    private String storeCode;
    @SerializedName("store_name")
    @Expose
    private String storeName;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_code")
    @Expose
    private String userCode;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("loai_user")
    @Expose
    private String loaiUser;
    @SerializedName("shift")
    @Expose
    private String shift;
    @SerializedName("product_id")
    @Expose
    private String productId;
    @SerializedName("product_code")
    @Expose
    private String productCode;
    @SerializedName("product_name")
    @Expose
    private String productName;
    @SerializedName("quantity")
    @Expose
    private String quantity;
    @SerializedName("create_begin")
    @Expose
    private String createBegin;
    @SerializedName("create_end")
    @Expose
    private String createEnd;
    @SerializedName("create_date")
    @Expose
    private String createDate;
    @SerializedName("update_date")
    @Expose
    private String updateDate;
    @SerializedName("create_user")
    @Expose
    private String createUser;
    @SerializedName("update_user")
    @Expose
    private String updateUser;
    @SerializedName("mien_id")
    @Expose
    private String mienId;
    @SerializedName("mien_code")
    @Expose
    private String mienCode;
    @SerializedName("mien_name")
    @Expose
    private String mienName;
    @SerializedName("vung_id")
    @Expose
    private String vungId;
    @SerializedName("vung_code")
    @Expose
    private String vungCode;
    @SerializedName("vung_name")
    @Expose
    private String vungName;
    @SerializedName("npp_id")
    @Expose
    private String nppId;
    @SerializedName("npp_code")
    @Expose
    private String nppCode;
    @SerializedName("npp_name")
    @Expose
    private String nppName;
    @SerializedName("category_id")
    @Expose
    private String categoryId;
    @SerializedName("category_code")
    @Expose
    private String categoryCode;
    @SerializedName("category_name")
    @Expose
    private String categoryName;
    @SerializedName("ytv")
    @Expose
    private String ytv;
    @SerializedName("knp")
    @Expose
    private String knp;
    @SerializedName("kns")
    @Expose
    private String kns;
    @SerializedName("dln")
    @Expose
    private String dln;
    @SerializedName("jn")
    @Expose
    private String jn;
    @SerializedName("month")
    @Expose
    private String month;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStoreId() {
        return storeId;
    }

    public void setStoreId(String storeId) {
        this.storeId = storeId;
    }

    public String getStoreCode() {
        return storeCode;
    }

    public void setStoreCode(String storeCode) {
        this.storeCode = storeCode;
    }

    public String getStoreName() {
        return storeName;
    }

    public void setStoreName(String storeName) {
        this.storeName = storeName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserCode() {
        return userCode;
    }

    public void setUserCode(String userCode) {
        this.userCode = userCode;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getLoaiUser() {
        return loaiUser;
    }

    public void setLoaiUser(String loaiUser) {
        this.loaiUser = loaiUser;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getCreateBegin() {
        return createBegin;
    }

    public void setCreateBegin(String createBegin) {
        this.createBegin = createBegin;
    }

    public String getCreateEnd() {
        return createEnd;
    }

    public void setCreateEnd(String createEnd) {
        this.createEnd = createEnd;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate;
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }

    public String getMienId() {
        return mienId;
    }

    public void setMienId(String mienId) {
        this.mienId = mienId;
    }

    public String getMienCode() {
        return mienCode;
    }

    public void setMienCode(String mienCode) {
        this.mienCode = mienCode;
    }

    public String getMienName() {
        return mienName;
    }

    public void setMienName(String mienName) {
        this.mienName = mienName;
    }

    public String getVungId() {
        return vungId;
    }

    public void setVungId(String vungId) {
        this.vungId = vungId;
    }

    public String getVungCode() {
        return vungCode;
    }

    public void setVungCode(String vungCode) {
        this.vungCode = vungCode;
    }

    public String getVungName() {
        return vungName;
    }

    public void setVungName(String vungName) {
        this.vungName = vungName;
    }

    public String getNppId() {
        return nppId;
    }

    public void setNppId(String nppId) {
        this.nppId = nppId;
    }

    public String getNppCode() {
        return nppCode;
    }

    public void setNppCode(String nppCode) {
        this.nppCode = nppCode;
    }

    public String getNppName() {
        return nppName;
    }

    public void setNppName(String nppName) {
        this.nppName = nppName;
    }

    public String getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(String categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryCode() {
        return categoryCode;
    }

    public void setCategoryCode(String categoryCode) {
        this.categoryCode = categoryCode;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getYtv() {
        return ytv;
    }

    public void setYtv(String ytv) {
        this.ytv = ytv;
    }

    public String getKnp() {
        return knp;
    }

    public void setKnp(String knp) {
        this.knp = knp;
    }

    public String getKns() {
        return kns;
    }

    public void setKns(String kns) {
        this.kns = kns;
    }

    public String getDln() {
        return dln;
    }

    public void setDln(String dln) {
        this.dln = dln;
    }

    public String getJn() {
        return jn;
    }

    public void setJn(String jn) {
        this.jn = jn;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }
}
