package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;

/**
 * Created by khoam on 19/05/2017.
 */

public class OrderClass {
    private String mien_code = "";
    private String parent_channel_code = "";
    private String channel_code = "";
    private String team_code = "";
    private String team_name = "";
    private String Ten = "";
    private String order_date = "";
    private String product_name = "";
    private String quantity = "";
    private String total_price = "";

    public String getMien_code() {
        return mien_code;
    }

    public void setMien_code(String mien_code) {
        this.mien_code = mien_code;
    }

    public String getParent_channel_code() {
        return parent_channel_code;
    }

    public void setParent_channel_code(String parent_channel_code) {
        this.parent_channel_code = parent_channel_code;
    }

    public String getChannel_code() {
        return channel_code;
    }

    public void setChannel_code(String channel_code) {
        this.channel_code = channel_code;
    }

    public String getTeam_code() {
        return team_code;
    }

    public void setTeam_code(String team_code) {
        this.team_code = team_code;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getTen() {
        return Ten;
    }

    public void setTen(String ten) {
        Ten = ten;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getTotal_price() {
        return total_price;
    }

    public void setTotal_price(String total_price) {
        this.total_price = total_price;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_MIEN_CODE)) {
                setMien_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_MIEN_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_PARENT_CHANNEL_CODE)) {
                setParent_channel_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_PARENT_CHANNEL_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_CHANNEL_CODE)) {
                setChannel_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_CHANNEL_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_TEAM_CODE)) {
                setTeam_code(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_TEAM_CODE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_TEAM_NAME)) {
                setTeam_name(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_TEAM_NAME));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_NAME)) {
                setTen(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_NAME));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_DATE)) {
                setOrder_date(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_DATE));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_PRODUCT_NAME)) {
                setProduct_name(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_PRODUCT_NAME));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_QUANTITY)) {
                setQuantity(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_QUANTITY));
            }
            if (jsonObject.has(Constants.RESULT_HISTORY_ORDER_TOTAL_PRICE)) {
                setTotal_price(jsonObject.getString(Constants.RESULT_HISTORY_ORDER_TOTAL_PRICE));
            }
        } catch (Exception ex) {

        }
    }
}
