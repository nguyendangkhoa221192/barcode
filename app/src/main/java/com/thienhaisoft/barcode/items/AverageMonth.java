package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

/**
 * Created by khoam on 1/2/2018.
 */

public class AverageMonth {
    private String store_id = "";
    private String store_code = "";
    private String store_name = "";
    private String target_amount = "";
    private String now_3 = "";
    private String now_2 = "";
    private String now_1 = "";
    private String now = "";
    private String soluong = "";
    private String average_month = "";
    private String percent = "";
    private String soxuat = "";
    private String percent_soluong = "";
    private String chitieu = "";

    public String getStore_id() {
        return store_id;
    }

    public void setStore_id(String store_id) {
        this.store_id = store_id;
    }

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getTarget_amount() {
        return target_amount;
    }

    public void setTarget_amount(String target_amount) {
        this.target_amount = target_amount;
    }

    public String getNow_2() {
        return now_2;
    }

    public void setNow_2(String now_2) {
        this.now_2 = now_2;
    }

    public String getNow_1() {
        return now_1;
    }

    public void setNow_1(String now_1) {
        this.now_1 = now_1;
    }

    public String getNow() {
        return now;
    }

    public void setNow(String now) {
        this.now = now;
    }

    public String getSoluong() {
        return soluong;
    }

    public void setSoluong(String soluong) {
        this.soluong = soluong;
    }

    public String getAverage_month() {
        return average_month;
    }

    public void setAverage_month(String average_month) {
        this.average_month = average_month;
    }

    public String getPercent() {
        return percent;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public String getSoxuat() {
        return soxuat;
    }

    public void setSoxuat(String soxuat) {
        this.soxuat = soxuat;
    }

    public String getPercent_soluong() {
        return percent_soluong;
    }

    public void setPercent_soluong(String percent_soluong) {
        this.percent_soluong = percent_soluong;
    }

    public String getNow_3() {
        return now_3;
    }

    public void setNow_3(String now_3) {
        this.now_3 = now_3;
    }

    public String getChitieu() {
        return chitieu;
    }

    public void setChitieu(String chitieu) {
        this.chitieu = chitieu;
    }

    public void partJSON(JSONObject object) {
        try {
            if (object.has("store_id")) {
                setStore_id(object.getString("store_id"));
            }
            if (object.has("store_code")) {
                setStore_code(object.getString("store_code"));
            }
            if (object.has("store_name")) {
                setStore_name(object.getString("store_name"));
            }
            if (object.has("target_amount")) {
                setTarget_amount(object.getString("target_amount"));
            }
            if (object.has("now_3")) {
                setNow_3(object.getString("now_3"));
            }
            if (object.has("now_2")) {
                setNow_2(object.getString("now_2"));
            }
            if (object.has("now_1")) {
                setNow_1(object.getString("now_1"));
            }
            if (object.has("now")) {
                setNow(object.getString("now"));
            }
            if (object.has("soluong")) {
                setSoluong(object.getString("soluong"));
            }
            if (object.has("average_month")) {
                setAverage_month(object.getString("average_month"));
            }
            if (object.has("percent")) {
                setPercent(object.getString("percent"));
            }
            if (object.has("soxuat")) {
                setSoxuat(object.getString("soxuat"));
            }
            if (object.has("percent_soluong")) {
                setPercent_soluong(object.getString("percent_soluong"));
            }
            if (object.has("chitieu")) {
                setChitieu(object.getString("chitieu"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
