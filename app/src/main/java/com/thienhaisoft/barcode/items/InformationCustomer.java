package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;

import java.io.Serializable;

/**
 * Created by khoam on 24/05/2017.
 */

public class InformationCustomer implements Serializable{
    private int i_Pid = 0;
    private String str_HoLot = "";
    private String str_Ten = "";
    private String str_DTDD = "";
    private String str_DTBan = "";
    private String str_DiaChi = "";
    private String str_NgayTao = "";
    private String str_B2BCode = "";
    private double dou_pointsproduct = 0.0;
    private double dou_Points = 0.0;
    private String str_KHTTCode = "";
    private int i_is_deleted = 0;
    private int i_createbyuser = 0;

    public int getI_Pid() {
        return i_Pid;
    }

    public void setI_Pid(int i_Pid) {
        this.i_Pid = i_Pid;
    }

    public String getStr_HoLot() {
        return str_HoLot;
    }

    public void setStr_HoLot(String str_HoLot) {
        this.str_HoLot = str_HoLot;
    }

    public String getStr_Ten() {
        return str_Ten;
    }

    public void setStr_Ten(String str_Ten) {
        this.str_Ten = str_Ten;
    }

    public String getStr_DTDD() {
        return str_DTDD;
    }

    public void setStr_DTDD(String str_DTDD) {
        this.str_DTDD = str_DTDD;
    }

    public String getStr_DTBan() {
        return str_DTBan;
    }

    public void setStr_DTBan(String str_DTBan) {
        this.str_DTBan = str_DTBan;
    }

    public String getStr_DiaChi() {
        return str_DiaChi;
    }

    public void setStr_DiaChi(String str_DiaChi) {
        this.str_DiaChi = str_DiaChi;
    }

    public String getStr_NgayTao() {
        return str_NgayTao;
    }

    public void setStr_NgayTao(String str_NgayTao) {
        this.str_NgayTao = str_NgayTao;
    }

    public String getStr_B2BCode() {
        return str_B2BCode;
    }

    public void setStr_B2BCode(String str_B2BCode) {
        this.str_B2BCode = str_B2BCode;
    }

    public double getDou_pointsproduct() {
        return dou_pointsproduct;
    }

    public void setDou_pointsproduct(double dou_pointsproduct) {
        this.dou_pointsproduct = dou_pointsproduct;
    }

    public double getDou_Points() {
        return dou_Points;
    }

    public void setDou_Points(double dou_Points) {
        this.dou_Points = dou_Points;
    }

    public String getStr_KHTTCode() {
        return str_KHTTCode;
    }

    public void setStr_KHTTCode(String str_KHTTCode) {
        this.str_KHTTCode = str_KHTTCode;
    }

    public int getI_is_deleted() {
        return i_is_deleted;
    }

    public void setI_is_deleted(int i_is_deleted) {
        this.i_is_deleted = i_is_deleted;
    }

    public int getI_createbyuser() {
        return i_createbyuser;
    }

    public void setI_createbyuser(int i_createbyuser) {
        this.i_createbyuser = i_createbyuser;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_PID)) {
                setI_Pid(jsonObject.getInt(Constants.RESULT_INFO_CUSTOMER_PID));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_FIRSTNAME)) {
                setStr_HoLot(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_FIRSTNAME));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_LASTNAME)) {
                setStr_Ten(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_LASTNAME));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_PHONE)) {
                setStr_DTDD(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_PHONE));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_HOME_PHONE)) {
                setStr_DTBan(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_HOME_PHONE));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_ADDRESS)) {
                setStr_DiaChi(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_ADDRESS));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_CREATE_DATE)) {
                setStr_NgayTao(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_CREATE_DATE));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_B2B)) {
                setStr_B2BCode(jsonObject.getString(Constants.RESULT_INFO_CUSTOMER_B2B));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_POINTS_PRODUCT)) {
                setDou_pointsproduct(jsonObject.getDouble(Constants.RESULT_INFO_CUSTOMER_POINTS_PRODUCT));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_POINTS)) {
                setDou_Points(jsonObject.getDouble(Constants.RESULT_INFO_CUSTOMER_POINTS));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_DELETE_FLG)) {
                setI_is_deleted(jsonObject.getInt(Constants.RESULT_INFO_CUSTOMER_DELETE_FLG));
            }
            if (jsonObject.has(Constants.RESULT_INFO_CUSTOMER_CREATE_BY_USER)) {
                setI_createbyuser(jsonObject.getInt(Constants.RESULT_INFO_CUSTOMER_CREATE_BY_USER));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
