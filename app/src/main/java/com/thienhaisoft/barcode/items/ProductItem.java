package com.thienhaisoft.barcode.items;

import com.thienhaisoft.barcode.utils.Constants;

import org.json.JSONObject;

/**
 * Created by khoam on 26/05/2017.
 */

public class ProductItem {
    private int id_product = 0;
    private String code = "";
    private int id_category = 0;
    private String name = "";
    private String description = "";
    private int id_tax = 0;
    private double base_price = 0.0;
    private int quantity = 0;
    private int store_id = 0;
    private int number_choose = 0;
    private Double point = 0.0;
    private boolean isDisplay = true;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isDisplay() {
        return isDisplay;
    }

    public void setDisplay(boolean display) {
        isDisplay = display;
    }

    public Double getPoint() {
        return point;
    }

    public void setPoint(Double point) {
        this.point = point;
    }

    public int getNumber_choose() {
        return number_choose;
    }

    public void setNumber_choose(int number_choose) {
        this.number_choose = number_choose;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getStore_id() {
        return store_id;
    }

    public void setStore_id(int store_id) {
        this.store_id = store_id;
    }

    public int getId_product() {
        return id_product;
    }

    public void setId_product(int id_product) {
        this.id_product = id_product;
    }

    public int getId_category() {
        return id_category;
    }

    public void setId_category(int id_category) {
        this.id_category = id_category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_tax() {
        return id_tax;
    }

    public void setId_tax(int id_tax) {
        this.id_tax = id_tax;
    }

    public double getBase_price() {
        return base_price;
    }

    public void setBase_price(double base_price) {
        this.base_price = base_price;
    }

    public void partJSON(JSONObject jsonObject) {
        try {
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_ID)) {
                this.setId_product(jsonObject.getInt(Constants.RESULTAPI_LIST_PRODUCT_ID));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_ID_CATEGORY)) {
                this.setId_category(jsonObject.getInt(Constants.RESULTAPI_LIST_PRODUCT_ID_CATEGORY));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_NAME)) {
                this.setName(jsonObject.getString(Constants.RESULTAPI_LIST_PRODUCT_NAME));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_DESCRIPTION)) {
                this.setDescription(jsonObject.getString(Constants.RESULTAPI_LIST_PRODUCT_DESCRIPTION));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_ID_TAX)) {
                this.setId_tax(jsonObject.getInt(Constants.RESULTAPI_LIST_PRODUCT_ID_TAX));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_BASE_PRICE)) {
                this.setBase_price(jsonObject.getDouble(Constants.RESULTAPI_LIST_PRODUCT_BASE_PRICE));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_QUANTITY)) {
                this.setQuantity(jsonObject.getInt(Constants.RESULTAPI_LIST_PRODUCT_QUANTITY));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_STORE_ID)) {
                this.setStore_id(jsonObject.getInt(Constants.RESULTAPI_LIST_PRODUCT_STORE_ID));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_POINT)) {
                this.setPoint(jsonObject.getDouble(Constants.RESULTAPI_LIST_PRODUCT_POINT));
            }
            if (jsonObject.has(Constants.RESULTAPI_LIST_PRODUCT_CODE)) {
                this.setCode(jsonObject.getString(Constants.RESULTAPI_LIST_PRODUCT_CODE));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}
