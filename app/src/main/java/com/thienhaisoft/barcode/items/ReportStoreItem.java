package com.thienhaisoft.barcode.items;

import org.json.JSONObject;

/**
 * Created by khoam on 10/20/2017.
 */

public class ReportStoreItem {
    private String store_code = "";
    private String store_name = "";
    private String product_name = "";
    private String ton_dau = "";
    private String sale_out = "";
    private String hang_tra = "";
    private String nhapkho = "";
    private String DC_TANG = "";
    private String DC_GIAM = "";
    private String cur_quantity = "";
    private String diff_value = "";

    public String getStore_code() {
        return store_code;
    }

    public void setStore_code(String store_code) {
        this.store_code = store_code;
    }

    public String getStore_name() {
        return store_name;
    }

    public void setStore_name(String store_name) {
        this.store_name = store_name;
    }

    public String getProduct_name() {
        return product_name;
    }

    public void setProduct_name(String product_name) {
        this.product_name = product_name;
    }

    public String getTon_dau() {
        return ton_dau;
    }

    public void setTon_dau(String ton_dau) {
        this.ton_dau = ton_dau;
    }

    public String getSale_out() {
        return sale_out;
    }

    public void setSale_out(String sale_out) {
        this.sale_out = sale_out;
    }

    public String getHang_tra() {
        return hang_tra;
    }

    public void setHang_tra(String hang_tra) {
        this.hang_tra = hang_tra;
    }

    public String getNhapkho() {
        return nhapkho;
    }

    public void setNhapkho(String nhapkho) {
        this.nhapkho = nhapkho;
    }

    public String getDC_TANG() {
        return DC_TANG;
    }

    public void setDC_TANG(String DC_TANG) {
        this.DC_TANG = DC_TANG;
    }

    public String getDC_GIAM() {
        return DC_GIAM;
    }

    public void setDC_GIAM(String DC_GIAM) {
        this.DC_GIAM = DC_GIAM;
    }

    public String getCur_quantity() {
        return cur_quantity;
    }

    public void setCur_quantity(String cur_quantity) {
        this.cur_quantity = cur_quantity;
    }

    public String getDiff_value() {
        return diff_value;
    }

    public void setDiff_value(String diff_value) {
        this.diff_value = diff_value;
    }

    public void partJSON(JSONObject object) {
        try {
            if (object.has("store_code")) {
                setStore_code(object.getString("store_code"));
            }
            if (object.has("store_name")) {
                setStore_name(object.getString("store_name"));
            }
            if (object.has("product_name")) {
                setProduct_name(object.getString("product_name"));
            }
            if (object.has("ton_dau")) {
                setTon_dau(object.getString("ton_dau"));
            }
            if (object.has("sale_out")) {
                setSale_out(object.getString("sale_out"));
            }
            if (object.has("hang_tra")) {
                setHang_tra(object.getString("hang_tra"));
            }
            if (object.has("nhapkho")) {
                setNhapkho(object.getString("nhapkho"));
            }
            if (object.has("DC_TANG")) {
                setDC_TANG(object.getString("DC_TANG"));
            }
            if (object.has("DC_GIAM")) {
                setDC_GIAM(object.getString("DC_GIAM"));
            }
            if (object.has("cur_quantity")) {
                setCur_quantity(object.getString("cur_quantity"));
            }
            if (object.has("diff_value")) {
                setDiff_value(object.getString("diff_value"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
